psql -h localhost -p 5432 -d pr_management -U admin -c "\COPY (
    SELECT m.\"Name\", m.\"Name\", s.\"Name\", ss.\"Name\", ms.\"TypeOfMedia\", 
    p.\"PlatFormType\", p.\"Link\", p.\"Potential\", p.\"TierPotential\"
    FROM public.\"Platforms\" p
    INNER JOIN public.\"MediaSections\" ms ON ms.\"Id\" = p.\"MediaSectionId\"
    INNER JOIN public.\"Medias\" m ON m.\"Id\" = ms.\"MediaId\"
    INNER JOIN public.\"Sections\" s ON s.\"Id\" = ms.\"SectionId\"
    INNER JOIN public.\"SubSections\" ss ON ss.\"Id\" = ms.\"SubSectionId\"
) TO '/var/lib/postgresql/data/platform_export.csv' CSV HEADER"

docker cp <container_id>:/var/lib/postgresql/data/platform_export.csv .