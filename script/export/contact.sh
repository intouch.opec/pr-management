psql -h localhost -p 5432 -d pr_management -U admin -c "\COPY (
    SELECT m.\"Name\", s.\"Name\", ss.\"Name\", ms.\"TypeOfMedia\" , 
    c.\"FirstName\",c.\"LastName\",c.\"NickName\",c.\"Position\",c.\"LineId\", 
    a.\"PhoneNumbers\", a.\"Email\", a.\"Text\"
    FROM public.\"ContactMediaSections\" cms
    INNER JOIN public.\"MediaSections\" ms ON ms.\"Id\" = cms.\"MediaSectionId\"
    INNER JOIN public.\"Medias\" m ON m.\"Id\" = ms.\"MediaId\"
    INNER JOIN public.\"Sections\" s ON s.\"Id\" = ms.\"SectionId\"
    INNER JOIN public.\"SubSections\" ss ON ss.\"Id\" = ms.\"SubSectionId\"
    INNER JOIN public.\"Contacts\" c ON c.\"Id\" = cms.\"ContactId\"
    LEFT JOIN public.\"Address\" a ON c.\"AddressId\" = a.\"Id\"
) TO '/var/lib/postgresql/data/contact_export.csv' CSV HEADER"

docker cp <container_id>:/var/lib/postgresql/data/contact_export.csv .
