psql -h localhost -p 5432 -d pr_management -U admin -c "\COPY (
    SELECT m.\"Name\", a.\"PhoneNumbers\", a.\"Email\", a.\"Text\"
    FROM public.\"Medias\" m
    LEFT JOIN public.\"Address\" a ON m.\"AddressId\" = a.\"Id\"
) TO '/var/lib/postgresql/data/medias_export.csv' CSV HEADER"

docker cp <container_id>:/var/lib/postgresql/data/medias_export.csv .