﻿using System.Reflection;
using System.Runtime.CompilerServices;
using AutoMapper;
using NUnit.Framework;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.Common.Models;
using PRManagement.Application.Contacts.Commands.CreateContact;
using PRManagement.Application.Contacts.Commands.UpdateContact;
using PRManagement.Application.Contacts.Queries.GetContactById;
using PRManagement.Application.Contacts.Queries.GetContactWithPagination;
using PRManagement.Application.Districts.Queries.GetDistrictOptions;
using PRManagement.Application.Medias.Commands.CreateMedia;
using PRManagement.Application.Medias.Commands.UpdateMedia;
using PRManagement.Application.Medias.Queries.GetMediaById;
using PRManagement.Application.Medias.Queries.GetMediaWithPagination;
using PRManagement.Application.MediaSections.Commands.CreateMediaSection;
using PRManagement.Application.MediaSections.Commands.UpdateMediaSection;
using PRManagement.Application.MediaSections.Queries.GetMediaSectionWithPagination;
using PRManagement.Application.Platforms.Commands.CreatePlatform;
using PRManagement.Application.Platforms.Commands.UpdatePlatform;
using PRManagement.Application.Platforms.Queries.GetPlatformById;
using PRManagement.Application.Platforms.Queries.GetPlatformWithPagination;
using PRManagement.Application.Provinces.Queries.GetProvinceOptions;
using PRManagement.Application.Sections.Queries.GetSectionOptions;
using PRManagement.Application.SubDistricts.Queries.GetSubDistrictOptions;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.UnitTests.Common.Mappings;
public class MappingTests
{
  private readonly IConfigurationProvider _configuration;
  private readonly IMapper _mapper;

  public MappingTests()
  {
    _configuration = new MapperConfiguration(config => 
      config.AddMaps(Assembly.GetAssembly(typeof(IApplicationDbContext))));

    _mapper = _configuration.CreateMapper();
  }

  [Test]
  public void ShouldHaveValidConfiguration()
  {
      _configuration.AssertConfigurationIsValid();
  }

  [Test]
  // [TestCase(typeof(CreateContactCommand), typeof(Contact))]
  // [TestCase(typeof(CreateMediaCommand), typeof(Media))]
  // [TestCase(typeof(UpdateMediaCommand), typeof(Address))]
  // [TestCase(typeof(UpdateMediaCommand), typeof(Media))]
  // [TestCase(typeof(CreateMediaCommand), typeof(Address))]
  // [TestCase(typeof(CreateMediaCommand), typeof(MediaSection))]
  // [TestCase(typeof(UpdateMediaCommand), typeof(Media))]
  [TestCase(typeof(Contact), typeof(CreateContactResponse))]
  [TestCase(typeof(UpdateContactCommand), typeof(Contact))]
  [TestCase(typeof(Contact), typeof(UpdateContactResponse))]
  [TestCase(typeof(Contact), typeof(GetContactByIdResponse))]
  [TestCase(typeof(Contact), typeof(GetContactWithPaginationResponse))]
  [TestCase(typeof(District), typeof(GetDistrictOptionsResponse))]
  [TestCase(typeof(Media), typeof(CreateMediaResponse))]
  [TestCase(typeof(Platform), typeof(LookupDto))]
  [TestCase(typeof(Section), typeof(LookupDto))]
  [TestCase(typeof(PlatformDto), typeof(Platform))]
  [TestCase(typeof(Media), typeof(UpdateMediaResponse))]
  [TestCase(typeof(Media), typeof(GetMediaByIdResponse))]
  [TestCase(typeof(Media), typeof(GetMediaWithPaginationResponse))]
  [TestCase(typeof(Platform), typeof(PlatformItem))]
  [TestCase(typeof(MediaSection), typeof(Medias.Queries.GetMediaWithPagination.MediaSectionItem))]
  [TestCase(typeof(MediaSection), typeof(CreateMediaSectionResponse))]
  [TestCase(typeof(CreateMediaSectionCommand), typeof(MediaSection))]
  [TestCase(typeof(UpdateMediaSectionCommand), typeof(MediaSection))]
  [TestCase(typeof(MediaSection), typeof(UpdateMediaSectionResponse))]
  [TestCase(typeof(MediaSection), typeof(GetMediaSectionItem))]
  [TestCase(typeof(Platform), typeof(CreatePlatformResponse))]
  [TestCase(typeof(CreatePlatformCommand), typeof(Platform))]
  [TestCase(typeof(Platform), typeof(UpdatePlatformResponse))]
  [TestCase(typeof(UpdatePlatformCommand), typeof(Platform))]
  [TestCase(typeof(Platform), typeof(GetPlatformByIdResponse))]
  [TestCase(typeof(Platform), typeof(GetPlatformItem))]
  [TestCase(typeof(Province), typeof(GetProvinceOptionsResponse))]
  [TestCase(typeof(Section), typeof(GetSectionOptionsResponse))]
  [TestCase(typeof(SubDistrict), typeof(GetSubDistrictOptionsResponse))]
  public void ShouldSupportMappingFromSourceToDestination(Type source, Type destination)
  {
    var instance = GetInstanceOf(source);

    _mapper.Map(instance, source, destination);
  }

  private object GetInstanceOf(Type type)
  {
    if (type.GetConstructor(Type.EmptyTypes) != null)
        return Activator.CreateInstance(type)!;

    // Type without parameterless constructor
    return RuntimeHelpers.GetUninitializedObject(type);
  }

}
