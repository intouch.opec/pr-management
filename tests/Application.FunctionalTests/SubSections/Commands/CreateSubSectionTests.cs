using PRManagement.Application.Common.Extensions;
using PRManagement.Application.SubSections.Commands.CreateSubSection;
using PRManagement.Domain.Entities;
using ValidationException = PRManagement.Application.Common.Exceptions.ValidationException;

namespace PRManagement.Application.FunctionalTests.SubSections.Commands;
using static Testing;

public class CreateSectionTests : BaseTestFixture
{
    [Test]
    public async Task ShouldRequireMinimumFields()
    {
        var command = new CreateSubSectionCommand();

        await FluentActions.Invoking(() =>
            SendAsync(command))
                .Should()
                .ThrowAsync<ValidationException>();
    }

    [Test]
    public async Task ShouldCreateSubSection()
    {
        var command = new CreateSubSectionCommand
        {
            Name = "New Sub Section",
            SectionId = "56d22a41-6b16-4335-9ba2-dfeb63c5a830",
        };

        var section = await SendAsync(command);

        var item = await FindAsync<SubSection>(section.Id);
        item.Should().NotBeNull();
        item!.Id.Should().Be(section.Id);
        item!.Name.Should().Be(command.Name);
        item!.SectionId.Should().Be(command.SectionId.ToGuidOnly());

    }

    [Test]
    public async Task ShouldHaveErrorWhenSectionIdIsInvalidGuid()
    {
        var command = new CreateSubSectionCommand
        {
            Name = "Valid Name",
            SectionId = "invalid-guid"
        };

        await FluentActions.Invoking(() =>
            SendAsync(command))
                .Should()
                .ThrowAsync<ValidationException>();
    }

    [Test]
    public async Task ShouldHaveErrorWhenNameIsDuplicate()
    {
        var command = new CreateSubSectionCommand
        {
            Name = "Duplicate Name",
            SectionId = "56d22a41-6b16-4335-9ba2-dfeb63c5a830"
        };

        await SendAsync(command);
        await FluentActions.Invoking(() =>
            SendAsync(new CreateSubSectionCommand
            {
                Name = "Duplicate Name",
                SectionId = "56d22a41-6b16-4335-9ba2-dfeb63c5a830"
            }))
            .Should()
            .ThrowAsync<ValidationException>();
    }

}
