using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Sections.Commands.UpdateSection;
using PRManagement.Application.SubSections.Commands.CreateSubSection;
using PRManagement.Application.SubSections.Commands.UpdateSubSection;
using PRManagement.Domain.Entities;
using ValidationException = PRManagement.Application.Common.Exceptions.ValidationException;

namespace PRManagement.Application.FunctionalTests.SubSections.Commands;
using static Testing;

public class UpdateSectionTests : BaseTestFixture
{
    [Test]
    public async Task ShouldRequireMinimumFields()
    {
        var command = new UpdateSubSectionCommand();

        await FluentActions.Invoking(() =>
            SendAsync(command))
                .Should()
                .ThrowAsync<ValidationException>();
    }

    [Test]
    public async Task ShouldUpdateSubSection()
    {
        var command = new CreateSubSectionCommand
        {
            Name = "New Update Section",
            SectionId = "56d22a41-6b16-4335-9ba2-dfeb63c5a830",
        };

        var section = await SendAsync(command);

        var updateSectionCommand = new UpdateSubSectionCommand
        {
            Id = section.Id.ToString(),
            Name = "Section Updated",
            SectionId = "26c42685-c10b-4d85-9e7f-95f9087ff33f"
        };

        var result = await SendAsync(updateSectionCommand);

        var item = await FindAsync<SubSection>(result.Id);
        item.Should().NotBeNull();
        item!.Id.Should().Be(updateSectionCommand.Id);
        item!.Name.Should().Be(updateSectionCommand.Name);
        item!.SectionId.Should().Be(updateSectionCommand.SectionId.ToGuidOnly());

    }
}
