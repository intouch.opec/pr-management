using PRManagement.Application.Common.Models;
using PRManagement.Application.Medias.Commands.CreateMedia;
using PRManagement.Application.Medias.Commands.DeleteMedia;
using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.FunctionalTests.Medias.Commands;
using static Testing;

public class DeleteMediaTests : BaseTestFixture
{

  [Test]
  public async Task ShouldRequireValidMedia()
  {
    var command = new DeleteMediaCommand()
    {
      Id = Guid.NewGuid()
    };

    await FluentActions.Invoking(() =>
        SendAsync(command)).Should().ThrowAsync<NotFoundException>();
  }

  [Test]
  public async Task ShouldDeletedMedia()
  {
    var mockUp = MockUpMediaCommand();
    var media = await SendAsync(mockUp);

    var command = new DeleteMediaCommand()
    {
      Id = media.Id
    };
    var result = await SendAsync(command);

    var item = await FindAsync<Media>(media.Id);
    item.Should().BeNull();
    result.Id.Should().Be(media.Id);
  }

  [Test]
  public async Task ShouldDeletedMediaAndAddRemark()
  {
    var mockUp = MockUpMediaDeleteCommand();
    var media = await SendAsync(mockUp);

    var command = new DeleteMediaCommand()
    {
      Id = media.Id,
      Remark = "Remark"
    };
    var result = await SendAsync(command);

    var item = await FindAsync<Media>(media.Id);
    item.Should().BeNull();
    result.Id.Should().Be(media.Id);
  }

  internal CreateMediaCommand MockUpMediaCommand()
  {
    var platforms = new List<PlatformDto>()
    {
      new PlatformDto()
      {
        Name = "PR Company Platform 2",
        Link = "https://www.pr-platform.com",
        PlatformType = PlatformType.NEWSPAPER.ToString(),
      }
    };

    return new CreateMediaCommand
    {
      Name = "PR Company2",
      Address = "Address",
      Email = new string[] { "email6@email.com", "email5@email.com" },
      PhoneNumbers = new string[] { "0651244569" },
      ProvinceId = "1",
      DistrictId = "1001",
      SubDistrictId = "100101",
      PostCode = "10200",
      TypeOfMedia = TypeOfMedia.MAINSTREAM.ToString(),
      Platforms = platforms,
      SectionId = "56d22a41-6b16-4335-9ba2-dfeb63c5a830",
      SubSectionId = "90a6e746-a379-4603-b39c-c10409314445"
    };
  }

  internal CreateMediaCommand MockUpMediaDeleteCommand()
  {
    var platforms = new List<PlatformDto>()
    {
      new PlatformDto()
      {
        Name = "888 Company 8888",
        Link = "https://www.pr-platform.com",
        PlatformType = PlatformType.NEWSPAPER.ToString(),
      }
    };

    return new CreateMediaCommand
    {
      Name = "PR 888",
      Address = "Address",
      Email = new string[] { "email6@email.com", "email5@email.com" },
      PhoneNumbers = new string[] { "0651244569" },
      ProvinceId = "1",
      DistrictId = "1001",
      SubDistrictId = "100101",
      PostCode = "10200",
      TypeOfMedia = TypeOfMedia.MAINSTREAM.ToString(),
      Platforms = platforms,
      SectionId = "56d22a41-6b16-4335-9ba2-dfeb63c5a830",
      SubSectionId = "90a6e746-a379-4603-b39c-c10409314445"
    };
  }

}