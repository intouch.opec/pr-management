using ValidationException = PRManagement.Application.Common.Exceptions.ValidationException;
using PRManagement.Application.Common.Models;
using PRManagement.Application.Medias.Commands.CreateMedia;
using PRManagement.Application.Medias.Commands.UpdateMedia;
using PRManagement.Domain.Enums;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.FunctionalTests.Medias.Commands;
using static Testing;

public class UpdateMediaTests : BaseTestFixture
{
  [Test]
  public async Task ShouldRequireMinimumFields()
  {
    var command = new UpdateMediaCommand();
    await FluentActions.Invoking(() =>
        SendAsync(command)).Should().ThrowAsync<ValidationException>();
  }

  [Test]
  public async Task ShouldUpdateMedia()
  {
    var createMediaCommand = MockUpCreateMediaCommand();
    var media = await SendAsync(createMediaCommand);
    var updateMediaCommand = MockUpUpdateMediaCommand(media.Id);

    var result = await SendAsync(updateMediaCommand);
    var item = await FindAsync<Media>(media.Id);
    item.Should().NotBeNull();
    item!.PercentageOfFullField.Should().Be(100);
    var address = await FindAsync<Address>(item!.AddressId!);
    result.Should().NotBeNull();
    item.Name.Should().NotBe(createMediaCommand.Name);
    address.Should().NotBeNull();
    address!.DistrictId.Should().NotBe(createMediaCommand.DistrictId);
    address.SubDistrictId.Should().NotBe(createMediaCommand.SubDistrictId);
    address.PostCode.Should().NotBe(createMediaCommand.PostCode);
  }

  [Test]
  public async Task ShouldUpdateMediaNonAddressCreateAddress()
  {
    var createMediaCommand = MockUpCreateMediaNonAddressCommand();
    var media = await SendAsync(createMediaCommand);
    var updateMediaCommand = MockUpUpdateMediaCreateAddressCommand(media.Id);

    await SendAsync(updateMediaCommand);
    var result = await FindAsync<Media>(media.Id);
    var resultAddress = await FindAsync<Address>(result!.AddressId!);
    result.Should().NotBeNull();
    result.Name.Should().NotBe(createMediaCommand.Name);
    resultAddress.Should().NotBeNull();
    resultAddress!.DistrictId.Should().NotBe(createMediaCommand.DistrictId);
    resultAddress.SubDistrictId.Should().NotBe(createMediaCommand.SubDistrictId);
    resultAddress.PostCode.Should().NotBe(createMediaCommand.PostCode);
  }

  internal CreateMediaCommand MockUpCreateMediaCommand()
  {
    var platforms = new List<PlatformDto>()
    {
      new PlatformDto()
      {
        Name = "Update PR Company Platform",
        Link = "https://www.pr-platform.com",
        PlatformType = PlatformType.X.ToString(),
        Potential = 1000
      }
    };

    return new CreateMediaCommand
    {
      Name = "UpdateMediaAfterUpdate",
      Address = "Address",
      Email = new string[] { "Media@email.com", "Media2@email.com" },
      PhoneNumbers = new string[] { "0650634567", "0823234567" },
      ProvinceId = "1",
      DistrictId = "1001",
      SubDistrictId = "100101",
      PostCode = "10200",
      TypeOfMedia = TypeOfMedia.DIGITAL.ToString(),
      Platforms = platforms,
      SectionId = "56d22a41-6b16-4335-9ba2-dfeb63c5a830",
      SubSectionId = "90a6e746-a379-4603-b39c-c10409314445"
    };
  }

  internal UpdateMediaCommand MockUpUpdateMediaCommand(Guid id)
  {
    return new UpdateMediaCommand()
    {
      Id = id,
      Name = "Updated",
      Address = "test",
      Email = ["test_create_5@gmail.com"],
      PhoneNumbers = ["0659356784"],
      ProvinceId = "1",
      DistrictId = "1003",
      SubDistrictId = "100301",
      PostCode = "10530",
    };
  }

  internal CreateMediaCommand MockUpCreateMediaNonAddressCommand()
  {
    return new CreateMediaCommand
    {
      Name = "9999 mumu",
      TypeOfMedia = TypeOfMedia.DIGITAL.ToString(),
      SectionId = "56d22a41-6b16-4335-9ba2-dfeb63c5a830",
      SubSectionId = "90a6e746-a379-4603-b39c-c10409314445"
    };
  }
  internal UpdateMediaCommand MockUpUpdateMediaCreateAddressCommand(Guid id)
  {
    return new UpdateMediaCommand()
    {
      Id = id,
      Name = "mUMU 8888",
      Address = "test",
      Email = ["test_crea1235@gmail.com"],
      PhoneNumbers = ["0159356784"],
      ProvinceId = "1",
      DistrictId = "1003",
      SubDistrictId = "100301",
      PostCode = "10530",
    };
  }
}