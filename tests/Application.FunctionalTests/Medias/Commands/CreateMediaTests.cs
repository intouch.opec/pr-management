using PRManagement.Application.Common.Models;
using PRManagement.Application.Medias.Commands.CreateMedia;
using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;
using ValidationException = PRManagement.Application.Common.Exceptions.ValidationException;

namespace PRManagement.Application.FunctionalTests.Medias.Commands;
using static Testing;

public class CreateMediaTests : BaseTestFixture
{
    [Test]
    public async Task ShouldRequireMinimumFields()
    {
        var command = new CreateMediaCommand();

        await FluentActions.Invoking(() =>
            SendAsync(command)).Should().ThrowAsync<ValidationException>();
    }

    [Test]
    public async Task ShouldCreateMedia()
    {
        var command = MockUpCreateMediaCommand();
        var media = await SendAsync(command);
        var item = await FindAsync<Media>(media.Id);
        item.Should().NotBeNull();
        item!.Id.Should().Be(media!.Id);
        item.Name.Should().Be(command.Name);
        item.PercentageOfFullField.Should().Be(100);
        var address = await FindAsync<Address>(item!.AddressId!);
        address.Should().NotBeNull();
        address!.Text.Should().Be(command.Address);
        address!.DistrictId.Should().Be(command.DistrictId);
        address!.SubDistrictId.Should().Be(command.SubDistrictId);
        address!.ProvinceId.Should().Be(command.ProvinceId);
        address!.PostCode.Should().Be(command.PostCode);
        foreach (var phoneNumber in address!.PhoneNumbers)
        {
            var commandPhoneNumber = command.PhoneNumbers.First(p => p.Equals(phoneNumber));
            phoneNumber.Should().Be(commandPhoneNumber);
        }

        foreach (var email in address!.Email)
        {
            var commandEmail = command.Email.First(p => p.Equals(email));
            email.Should().Be(commandEmail);
        }

        var mediaSection = await FindAsync<MediaSection>(media.MediaSectionId);
        var mediaSectionAddress = await FindAsync<Address>(mediaSection!.AddressId!);
        
        foreach (var phoneNumber in mediaSectionAddress!.PhoneNumbers)
        {
            var commandPhoneNumber = command.MediaSectionPhoneNumbers.First(p => p.Equals(phoneNumber));
            phoneNumber.Should().Be(commandPhoneNumber);
        }

        foreach (var email in mediaSectionAddress!.Email)
        {
            var commandEmail = command.Email.First(p => p.Equals(email));
            email.Should().Be(commandEmail);
        }
    }

    [Test]
    public async Task ShouldBeThrowValidationExceptionInValidPlatformTypeDigitalType()
    {
        var command = MockUpCreateMediaCommandInValidPlatformTypeDigitalType();
        try
        {
            await SendAsync(command);
        }
        catch (ValidationException result)
        {
            var error = result.Errors.First();
            var key = $"{nameof(CreateMediaCommand.Platforms)}[0].{nameof(PlatformDto.PlatformType)}";
            error.Key.Should().Be(key);
            error.Value[0].Should().Be($"Invalid Platform Type. Allowed values are: " + string.Join(", ", DigitalPlatformType.PlatformTypeStrings));
        }
    }

    [Test]
    public async Task ShouldCreateMediaNonAddress()
    {
        var command = MockUpCreateMediaNonAddressCommand();
        var media = await SendAsync(command);
        var item = await FindAsync<Media>(media.Id);
        item.Should().NotBeNull();
        item!.Id.Should().Be(media!.Id);
        item.Name.Should().Be(command.Name);
        item.AddressId.Should().BeNull();
    }

    [Test]
    public async Task ShouldCreateMediaAndMediaSectionHasAddress()
    {
        var command = MockUpCreateMediaHasMediaSectionAddressCommand();

        var result = await SendAsync(command);

        var item = await FindAsync<Media>(result.Id);
        var mediaSection = await FindAsync<MediaSection>(result.MediaSectionId);
        var address = await FindAsync<Address>(mediaSection!.AddressId!);

        item.Should().NotBeNull();
        item!.Id.Should().Be(result!.Id);
        item.Name.Should().Be(command.Name);
        mediaSection!.AddressId.Should().NotBeNull();
        address.Should().NotBeNull();
        var email = address!.Email.First();
        email.Should().Be(command.MediaSectionEmail.First());
        var phoneNumber = address!.PhoneNumbers.First();
        phoneNumber.Should().Be(command.MediaSectionPhoneNumbers.First());
    }

    internal CreateMediaCommand MockUpCreateMediaCommand()
    {
        var platforms = new List<PlatformDto>()
    {
      new PlatformDto()
      {
        Name = "Update PR Company Platform",
        Link = "https://www.pr-platform.com",
        PlatformType = PlatformType.NEWSPAPER.ToString(),
        Potential = 1000
      }
    };

        return new CreateMediaCommand
        {
            Name = "Create PR Company",
            Address = "Address",
            Email = new string[] { "email@email.com", "email2@email.com" },
            PhoneNumbers = new string[] { "0651934567" },
            ProvinceId = "1",
            DistrictId = "1001",
            SubDistrictId = "100101",
            PostCode = "10200",
            TypeOfMedia = TypeOfMedia.MAINSTREAM.ToString(),
            Platforms = platforms,
            SectionId = "56d22a41-6b16-4335-9ba2-dfeb63c5a830",
            SubSectionId = "90a6e746-a379-4603-b39c-c10409314445"
        };
    }

    internal CreateMediaCommand MockUpCreateMediaCommandInValidPlatformTypeDigitalType()
    {
        var platforms = new List<PlatformDto>()
    {
      new PlatformDto()
      {
        Name = "InValidPlatformType",
        Link = "https://www.pr-platform.com",
        PlatformType = PlatformType.NEWSPAPER.ToString(),
      }
    };

        return new CreateMediaCommand
        {
            Name = "InValidPlatformTypeDigitalType",
            Address = "Address",
            Email = new string[] { "InValidPlatformTypeDigitalType@email.com" },
            PhoneNumbers = new string[] { "0651394567" },
            ProvinceId = "1",
            DistrictId = "1001",
            SubDistrictId = "100101",
            PostCode = "10200",
            TypeOfMedia = TypeOfMedia.DIGITAL.ToString(),
            Platforms = platforms,
            SectionId = "56d22a41-6b16-4335-9ba2-dfeb63c5a830",
            SubSectionId = "90a6e746-a379-4603-b39c-c10409314445"
        };
    }

    internal CreateMediaCommand MockUpCreateMediaNonAddressCommand()
    {
        return new CreateMediaCommand
        {
            Name = "PR Company Non Address",
            SectionId = "56d22a41-6b16-4335-9ba2-dfeb63c5a830",
            SubSectionId = "90a6e746-a379-4603-b39c-c10409314445",
            TypeOfMedia = TypeOfMedia.DIGITAL.ToString(),
        };
    }

    internal CreateMediaCommand MockUpCreateMediaHasMediaSectionAddressCommand()
    {
        var platforms = new List<PlatformDto>()
    {
      new PlatformDto()
      {
        Name = "HasMediaSectionPlatform",
        Link = "https://www.pr-platform.com",
        PlatformType = PlatformType.NEWSPAPER.ToString(),
        Potential = 1000
      }
    };

        return new CreateMediaCommand
        {
            Name = "CreateHasMediaSectionPlatform",
            Address = "Address",
            Email = new string[] { "emailHas@email.com" },
            PhoneNumbers = new string[] { "0654914267" },
            ProvinceId = "1",
            DistrictId = "1001",
            SubDistrictId = "100101",
            PostCode = "10200",
            TypeOfMedia = TypeOfMedia.MAINSTREAM.ToString(),
            Platforms = platforms,
            SectionId = "56d22a41-6b16-4335-9ba2-dfeb63c5a830",
            SubSectionId = "90a6e746-a379-4603-b39c-c10409314445",
            MediaSectionEmail = new string[] { "emHas@email.com" },
            MediaSectionPhoneNumbers = new string[] { "0994914268" },
        };
    }

}
