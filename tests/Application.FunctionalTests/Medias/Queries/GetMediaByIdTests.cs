using PRManagement.Application.Common.Models;
using PRManagement.Application.Medias.Commands.CreateMedia;
using PRManagement.Application.Medias.Queries.GetMediaById;
using PRManagement.Domain.Enums;
using PRManagement.Domain.Exceptions;

namespace PRManagement.Application.FunctionalTests.Medias.Queries;

using static Testing;
public class GetMediaByIdTests : BaseTestFixture
{
    [Test]
    public async Task ShouldBeNotFound()
    {
        var query = new GetMediaByIdQuery(Guid.NewGuid());
        var action = () => SendAsync(query);
        await action.Should().ThrowAsync<NotFoundException>();
    }

    [Test]
    public async Task ShouldReturnMedia()
    {
        var commandMedia = MockUpCreateMediaCommand();
        var media = await SendAsync(commandMedia);
        var query = new GetMediaByIdQuery(media.Id);
        var result = await SendAsync(query);
        result.Name.Should().Be(commandMedia.Name);
        result.Address.Should().Be(commandMedia.Address);
        result.Email.Count().Should().Be(commandMedia.Email.Count());
        result.PhoneNumbers.Count().Should().Be(commandMedia.PhoneNumbers.Count());
        result.MediaSections.Count().Should().Be(1);
        foreach (var mediaSectionItem in result.MediaSections)
        {
            mediaSectionItem.SectionId.Should().Be(commandMedia.SectionId);
            mediaSectionItem.SubSectionId.Should().Be(commandMedia.SubSectionId);
            mediaSectionItem.TypeOfMedia.Should().Be(commandMedia.TypeOfMedia.ToEnum<TypeOfMedia>());
            foreach (var email in commandMedia.MediaSectionEmail)
            {
                var findEmail = mediaSectionItem.Email.FirstOrDefault(e => e.Equals(email));
                findEmail.Should().NotBeNull();
            }
            foreach (var phoneNumber in commandMedia.MediaSectionPhoneNumbers)
            {
                var findPhoneNumber = mediaSectionItem.PhoneNumbers.FirstOrDefault(e => e.Equals(phoneNumber));
                findPhoneNumber.Should().NotBeNull();
            }
        }

    }

    internal CreateMediaCommand MockUpCreateMediaCommand()
    {
        var platforms = new List<PlatformDto>()
    {
      new PlatformDto()
      {
        Name = "GetMediaById_Platform",
        Link = "https://www.pr-platform.com",
        PlatformType = PlatformType.NEWSPAPER.ToString(),
      }
    };

        return new CreateMediaCommand
        {
            Name = "GetMediaByIdQuery",
            Address = "Address",
            Email = new string[] { "email@email.com" },
            PhoneNumbers = new string[] { "0991934567" },
            ProvinceId = "1",
            DistrictId = "1001",
            SubDistrictId = "100101",
            PostCode = "10200",
            TypeOfMedia = TypeOfMedia.MAINSTREAM.ToString(),
            Platforms = platforms,
            SectionId = "56d22a41-6b16-4335-9ba2-dfeb63c5a830",
            SubSectionId = "90a6e746-a379-4603-b39c-c10409314445",
            MediaSectionEmail = new[] { "aa_aa@gmail.com" },
            MediaSectionPhoneNumbers = new[] { "09919324367" }
        };
    }
}