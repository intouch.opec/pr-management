using PRManagement.Application.Medias.Queries.GetMediaOptions;

namespace PRManagement.Application.FunctionalTests.Medias.Queries;

using static Testing;
public class GetMediaOptionTests : BaseTestFixture
{

  [Test]
  public async Task ShouldReturnMedias()
  {
    var query = new GetMediaOptionsQuery();
    var options = await SendAsync(query);

    options.Should().NotBeNullOrEmpty();
    foreach (var item in options)
    {
      item.Id.Should().NotBeNullOrEmpty();
      item.Name.Should().NotBeNullOrEmpty();
    }
  }
}