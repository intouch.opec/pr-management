using PRManagement.Application.RegisterForms.Commands.CreateRegisterForm;
using PRManagement.Domain.Entities;
using PRManagement.Application.Common.Exceptions;
using ValidationException = PRManagement.Application.Common.Exceptions.ValidationException;
using PRManagement.Domain.Exceptions;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.FunctionalTests.RegisterForms.Commands;
using static Testing;

public class CreateRegisterFormTests : BaseTestFixture
{
    [Test]
    public async Task ShouldRequireMinimumFields()
    {
        var command = new CreateRegisterFormCommand();

        await FluentActions.Invoking(() =>
            SendAsync(command))
            .Should()
            .ThrowAsync<ValidationException>();
    }

    [Test]
    public async Task ShouldCreateRegisterForm()
    {
        var command = new CreateRegisterFormCommand
        {
            Name = "Unique Form",
            Detail = "Detail",
            RegisterFields = new List<RegisterFieldItem>
                {
                    new RegisterFieldItem { InputType = InputInformation.CompanyName.GetEnumMemberValue(), IsRequired = true },
                    new RegisterFieldItem { InputType = InputInformation.PhoneNumber.GetEnumMemberValue(), IsRequired = true }
                }
        };

        var registerForm = await SendAsync(command);

        var item = await FindAsync<RegisterForm>(registerForm.Id);
        item.Should().NotBeNull();
        item!.Id.Should().Be(registerForm.Id);
        item!.Name.Should().Be(command.Name);
        item!.Detail.Should().Be(command.Detail);

        var fields = registerForm.RegisterFields.ToList();

        foreach (var field in command.RegisterFields)
        {
            var found = fields.Find(e => e.InputType == field.InputType.ToEnum<InputInformation>());
            found.Should().NotBeNull();
            found!.InputType.Should().Be(field.InputType.ToEnum<InputInformation>());
        }
    }

    [Test]
    public async Task ShouldHaveErrorWhenNameIsDuplicate()
    {
        var name = "DuplicateForm";
        var command = new CreateRegisterFormCommand
        {
            Name = name,
            RegisterFields = new List<RegisterFieldItem>
                {
                    new RegisterFieldItem { InputType = InputInformation.CompanyName.GetEnumMemberValue(), IsRequired = true  },
                    new RegisterFieldItem { InputType = InputInformation.ShirtSize.GetEnumMemberValue(), IsRequired = true  }
                }
        };

        await SendAsync(command);

        await FluentActions.Invoking(() =>
            SendAsync(new CreateRegisterFormCommand
            {
                Name = name,
                RegisterFields = new List<RegisterFieldItem>()
            }))
            .Should()
            .ThrowAsync<ValidationException>();
    }


    [Test]
    public async Task ShouldHaveErrorWhenRegisterFieldItemIsDuplicate()
    {
        var name = "RegisterFieldItem DuplicateForm";
        var command = new CreateRegisterFormCommand
        {
            Name = name,
            RegisterFields = new List<RegisterFieldItem>
                {
                    new RegisterFieldItem { InputType = InputInformation.CompanyName.GetEnumMemberValue(), IsRequired = true  },
                    new RegisterFieldItem { InputType = InputInformation.CompanyName.GetEnumMemberValue(), IsRequired = true  }
                }
        };

        await FluentActions.Invoking(() =>
                SendAsync(command))
            .Should()
            .ThrowAsync<ValidationException>();
    }
}
