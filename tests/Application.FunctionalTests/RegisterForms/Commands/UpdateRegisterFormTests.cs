using PRManagement.Application.RegisterForms.Commands.CreateRegisterForm;
using PRManagement.Domain.Entities;
using PRManagement.Application.Common.Exceptions;
using PRManagement.Application.RegisterForms.Commands.UpdateRegisterForm;
using PRManagement.Domain.Exceptions;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.FunctionalTests.RegisterForms.Commands;
using static Testing;

public class UpdateRegisterFormTests : BaseTestFixture
{
    [Test]
    public async Task ShouldRequireMinimumFields()
    {
        var command = new UpdateRegisterFormCommand();

        await FluentActions.Invoking(() =>
            SendAsync(command))
            .Should()
            .ThrowAsync<ValidationException>();
    }

    [Test]
    public async Task ShouldUpdateRegisterForm()
    {
        var createCommand = new CreateRegisterFormCommand
        {
            Name = "Original Form",
            RegisterFields = new List<RegisterFieldItem>
                {
                    new RegisterFieldItem { InputType = InputInformation.CompanyName.GetEnumMemberValue(),  IsRequired = false },
                    new RegisterFieldItem { InputType = InputInformation.Email.GetEnumMemberValue(),  IsRequired = false }
                }
        };

        var registerForm = await SendAsync(createCommand);

        var updateCommand = new UpdateRegisterFormCommand
        {
            Id = registerForm.Id.ToString(),
            Name = "Updated Form",
            RegisterFields = new List<RegisterFieldItem>
                {
                    new RegisterFieldItem { InputType = InputInformation.CompanyName.GetEnumMemberValue(), IsRequired = false },
                    new RegisterFieldItem { InputType = InputInformation.LineID.GetEnumMemberValue(), IsRequired = false },
                    new RegisterFieldItem { InputType = InputInformation.Email.GetEnumMemberValue(), IsRequired = false }
                }
        };

        var updatedRegisterForm = await SendAsync(updateCommand);

        var item = await FindAsync<RegisterForm>(updatedRegisterForm.Id);
        item.Should().NotBeNull();
        item!.Id.Should().Be(registerForm.Id);
        item!.Name.Should().Be(updateCommand.Name);

        var fields = item.RegisterFields.ToList();

        foreach (var field in updateCommand.RegisterFields)
        {
            var found = fields.Find(e => e.InputType == field.InputType.ToEnum<InputInformation>());
            found.Should().NotBeNull();
            found!.InputType.Should().Be(field.InputType.ToEnum<InputInformation>());
        }
    }

    [Test]
    public async Task ShouldHaveErrorWhenNameIsDuplicate()
    {
        var name = "DuplicateForm RegisterForm";
        var createCommand = new CreateRegisterFormCommand
        {
            Name = name,
            RegisterFields = new List<RegisterFieldItem>
                {
                    new RegisterFieldItem { InputType = InputInformation.CompanyName.GetEnumMemberValue(),  IsRequired = false },
                    new RegisterFieldItem { InputType = InputInformation.Drinks.GetEnumMemberValue(),  IsRequired = false }
                }
        };

        await SendAsync(createCommand);

        var secondCreateCommand = new CreateRegisterFormCommand
        {
            Name = "AnotherForm",
            RegisterFields = new List<RegisterFieldItem>()
        };

        var secondRegisterForm = await SendAsync(secondCreateCommand);

        var updateCommand = new UpdateRegisterFormCommand
        {
            Id = secondRegisterForm.Id.ToString(),
            Name = name,
            RegisterFields = new List<RegisterFieldItem>()
        };

        await FluentActions.Invoking(() =>
            SendAsync(updateCommand))
            .Should()
            .ThrowAsync<ValidationException>();

    }
}
