using PRManagement.Application.BookmarkGroups.Commands.CreateBookmarkGroup;
using PRManagement.Application.Bookmarks.Commands.UpdateBookmarkActiveEmail;
using PRManagement.Application.Bookmarks.Queries.GetBookmarkWithPagination;
using PRManagement.Application.Medias.Commands.CreateMedia;
using PRManagement.Application.MediaSections.Commands.UpdateMediaSection;
using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.FunctionalTests.Bookmarks.Commands;
using static Testing;

public class UpdateBookmarkActiveEmailCommandTests : BaseTestFixture
{
    [Test]
    public async Task ShouldUpdateBookmarkActiveEmail()
    {
        var mediaCommand = MockUpCreateMediaHasAddressMediaSectionCommand();
        var media = await SendAsync(mediaCommand);

        var commandBookmarkGroup = new CreateBookmarkGroupCommand()
        {
            Name = "IT Group",
            MediaSectionIds = new[] { media.MediaSectionId.ToString() },
            Tags = new List<Tag>() {
                new Tag() {
                    Name = "IT NEW",
                    Color = "#7FFFD4"
                }
            }
        };

        var bookmarkGroup = await SendAsync(commandBookmarkGroup);
        var bookmarkWithPaginationQuery = new GetBookmarkWithPaginationQuery()
        {
            BookMarkGroupId = bookmarkGroup.Id.ToString()
        };
        var bookmarkWithPagination = await SendAsync(bookmarkWithPaginationQuery);
        var bookmark = bookmarkWithPagination.Items.First();
        var updateMediaSectionCommand = new UpdateMediaSectionCommand(media.MediaSectionId)
        {
            MediaId = media.Id.ToString(),
            TypeOfMedia = TypeOfMedia.DIGITAL.ToString(),
            SectionId = "56d22a41-6b16-4335-9ba2-dfeb63c5a830",
            SubSectionId = "90a6e746-a379-4603-b39c-c10409314445",
            Email = new[] { "new@gmail.com", "new2@gmail.com" },
            PhoneNumbers = new[] { "0912316843", "0812124123" },
        };

        await SendAsync(updateMediaSectionCommand);

        var updateBookmarkActiveEmailCommand = new UpdateBookmarkActiveEmailCommand()
        {
            ActiveEmail = new List<int> { 1 },
            Id = bookmark!.Id.ToString()
        };

        var book = await SendAsync(updateBookmarkActiveEmailCommand);
        book.ActiveEmail.Should().Equal(updateBookmarkActiveEmailCommand.ActiveEmail);

        var bookmarkFirst = await FindAsync<BookMark>(bookmark.Id);
        bookmarkFirst!.ActiveEmail.Should().Equal(updateBookmarkActiveEmailCommand.ActiveEmail);
    }

    internal CreateMediaCommand MockUpCreateMediaHasAddressMediaSectionCommand()
    {
        return new CreateMediaCommand
        {
            Name = "Jim And Muuu",
            Address = "Address",
            Email = new string[] { "Muuu@email.com" },
            PhoneNumbers = new string[] { "0631194567" },
            ProvinceId = "1",
            DistrictId = "1001",
            SubDistrictId = "100101",
            PostCode = "10200",
            TypeOfMedia = TypeOfMedia.DIGITAL.ToString(),
            SectionId = "56d22a41-6b16-4335-9ba2-dfeb63c5a830",
            SubSectionId = "90a6e746-a379-4603-b39c-c10409314445",
            MediaSectionEmail = new string[] { "jinny@email.com" },
            MediaSectionPhoneNumbers = new string[] { "0911115567" },
        };
    }

}
