namespace PRManagement.Application.FunctionalTests.Bookmarks.Commands;

using PRManagement.Application.BookmarkGroups.Commands.CreateBookmarkGroup;
using PRManagement.Application.Bookmarks.Commands.RemoveBookmark;
using PRManagement.Application.Bookmarks.Queries.GetBookmarkWithPagination;
using PRManagement.Application.Contacts.Commands.CreateContact;
using PRManagement.Application.Medias.Commands.CreateMedia;
using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;
using static Testing;

public class RemoveBookmarkTests : BaseTestFixture
{
    [Test]
    public async Task ShouldRemoveBookmark()
    {
        var mediaCommand = MockUpCreateMediaCommand();
        var media = await SendAsync(mediaCommand);
        var contactCommand = MockUpCreateContactCommand(media.MediaSectionId);
        var contact = await SendAsync(contactCommand);
        var bookmarkGroupCommand = new CreateBookmarkGroupCommand()
        {
            Name = "Bookmark T2",
            ContactIds = new[] { contact.Id.ToString() },
            MediaIds = new[] { media.Id.ToString() }
        };

        var bookmarkGroup = await SendAsync(bookmarkGroupCommand);
        var bookmarks = await SendAsync(new GetBookmarkWithPaginationQuery()
        {
            BookMarkGroupId = bookmarkGroup.Id.ToString()
        });
        var bookmarkId = bookmarks.Items.First().Id;

        await SendAsync(new RemoveBookmarkCommand()
        {
            Id = bookmarkId,
            Remark = "Remark"

        });

        var bookmark = await FindAsync<BookMark>(bookmarkId);
        bookmark.Should().BeNull();
    }


    internal CreateMediaCommand MockUpCreateMediaCommand()
    {
        return new CreateMediaCommand
        {
            Name = "max media",
            Address = "Address",
            Email = new string[] { "max@email.com" },
            PhoneNumbers = new string[] { "0621394567" },
            ProvinceId = "1",
            DistrictId = "1001",
            SubDistrictId = "100101",
            PostCode = "10200",
            TypeOfMedia = TypeOfMedia.DIGITAL.ToString(),
            SectionId = "56d22a41-6b16-4335-9ba2-dfeb63c5a830",
            SubSectionId = "90a6e746-a379-4603-b39c-c10409314445"
        };
    }

    internal CreateContactCommand MockUpCreateContactCommand(Guid mediaSectionId)
    {
        return new CreateContactCommand
        {
            FirstName = "nuudee_",
            LastName = "mahamuu_",
            Address = "Address",
            Position = "Position",
            LineId = "LineId",
            Remark = "Remark",
            AddressType = "Home",
            Email = new string[] { "nuudee@email.com" },
            PhoneNumbers = new string[] { "0612134567" },
            ProvinceId = "1",
            DistrictId = "1001",
            SubDistrictId = "100101",
            PostCode = "10200",
            MediaSectionId = mediaSectionId.ToString()
        };
    }

}