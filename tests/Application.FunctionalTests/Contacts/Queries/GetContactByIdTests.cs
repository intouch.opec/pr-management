using PRManagement.Application.Contacts.Queries.GetContactById;
using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.FunctionalTests.Contacts.Queries;
using static Testing;
public class GetContactByIdTests : BaseTestFixture
{
    [Test]
    public async Task ShouldBeNotFound()
    {
        var query = new GetContactByIdQuery(new Guid("b28d188f-9bbf-48f1-b3b2-c515c03202af"));
        var action = () => SendAsync(query);
        await action.Should().ThrowAsync<NotFoundException>();
    }

    [Test]
    public async Task ShouldReturnContact()
    {
        var address = new Address
        {
            Id = new Guid("80c49a82-40a2-4d72-8e1f-e3ba5ecccebc"),
            Text = "Address",
            Email = new string[] { "emailemail@email.com" },
            PhoneNumbers = new string[] { "0651221567" },
            ProvinceId = "1",
            DistrictId = "1001",
            SubDistrictId = "100101",
            PostCode = "10200"
        };

        var contact = new Contact
        {
            AddressId = address.Id,
            Id = new Guid("0395f862-d0b1-4769-b5dc-898f2a3c0088"),
            FirstName = "FirstName",
            LastName = "LastName",
            Position = "Position",
            LineId = "LineId",
            Remark = "Remark",
            AddressType = AddressType.Home,
        };
        await AddAsync(address);
        await AddAsync(contact);

        var query = new GetContactByIdQuery(new Guid("0395f862-d0b1-4769-b5dc-898f2a3c0088"));
        var result = await SendAsync(query);

        result.Should().NotBeNull();
        result.FirstName.Should().Be(contact.FirstName);
        result.LastName.Should().Be(contact.LastName);
        result.Position.Should().Be(contact.Position);
        result.LineId.Should().Be(contact.LineId);
        result.SubDistrictId.Should().Be(address.SubDistrictId);
        result.Address.Should().Be(address.Text);
        result.DistrictId.Should().Be(address.DistrictId);
        result.ProvinceId.Should().Be(address.ProvinceId);
        result.PostCode.Should().Be(address.PostCode);
        result.AddressType.Should().Be(contact.AddressType);
        var email = result.Email.First(e => e.Equals(address.Email[0]));
        email.Should().NotBeNull();
    }

}
