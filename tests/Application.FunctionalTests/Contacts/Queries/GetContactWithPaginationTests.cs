namespace PRManagement.Application.FunctionalTests.Contacts.Queries;

using PRManagement.Application.Contacts.Queries.GetContactWithPagination;
using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;
using static Testing;
public class GetContactWithPaginationTests : BaseTestFixture
{
    [Test]
    public async Task ShouldReturnContactWithPagination()
    {
        var address = new Address
        {
            Id = new Guid("9cb12212-e6d6-4533-bc0e-02579c26df94"),
            Text = "Address",
            Email = new string[] { "emailemailPage@email.com" },
            PhoneNumbers = new string[] { "0651221567" },
            ProvinceId = "1",
            DistrictId = "1001",
            SubDistrictId = "100101",
            PostCode = "10200"
        };

        var address2 = new Address
        {
            Id = new Guid("70a83949-ac6e-4bd5-895e-6014fa8fc7e9"),
            Text = "Address",
            Email = new string[] { "address2@email.com" },
            PhoneNumbers = new string[] { "0651222357" },
            ProvinceId = "1",
            DistrictId = "1001",
            SubDistrictId = "100101",
            PostCode = "10200"
        };

        var contact = new Contact
        {
            Address = address,
            Id = new Guid("e696a21c-622f-4dd3-8b6c-106231ad8a8c"),
            FirstName = "FirstNamePage",
            LastName = "LastNamePage",
            Position = "Position",
            LineId = "LineId",
            Remark = "Remark",
            AddressType = AddressType.Home,
            NickName = "NickName"
        };

        var contact2 = new Contact
        {
            Address = address2,
            Id = new Guid("daddc1a7-7fdc-426b-8682-653b23923947"),
            FirstName = "FirstNamePage2",
            LastName = "LastNamePage2",
            Position = "Position2",
            LineId = "LineId2",
            Remark = "Remark2",
            AddressType = AddressType.Home,
            NickName = "NickName2"
        };
        await AddAsync(contact);
        await AddAsync(contact2);

        var query = new GetContactWithPaginationQuery();
        var result = await SendAsync(query);

        var count = await CountAsync<Contact>();

        result.Should().NotBeNull();
        result.TotalCount.Should().Be(count);
        result.Items.Should().NotBeEmpty();

        var firstRecord = result.Items.FirstOrDefault(e => e.Id.Equals(contact.Id));
        firstRecord.Should().NotBeNull();
        firstRecord!.FirstName.Should().Be(contact.FirstName);
        firstRecord!.LastName.Should().Be(contact.LastName);
        firstRecord!.LineId.Should().Be(contact.LineId);
        firstRecord.Position.Should().Be(contact.Position);
        firstRecord.NickName.Should().Be(contact.NickName);
        firstRecord.PhoneNumbers.Count().Should().Be(address.PhoneNumbers.Count());
        var phone = firstRecord.PhoneNumbers.First(phoneNumber => phoneNumber.Equals(address.PhoneNumbers[0]));
        phone.Should().NotBeNull();
    }
}