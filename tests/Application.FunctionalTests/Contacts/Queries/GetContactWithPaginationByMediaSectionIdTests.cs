
namespace PRManagement.Application.FunctionalTests.Contacts.Queries;

using PRManagement.Application.Common.Models;
using PRManagement.Application.Contacts.Queries.GetContactWithPaginationByMediaSectionId;
using PRManagement.Application.Medias.Commands.CreateMedia;
using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;
using static Testing;
public class GetContactWithPaginationByMediaSectionIdTests : BaseTestFixture
{
    [Test]
    public async Task ShouldReturnContactWithPaginationByMediaSectionId()
    {
        var contact = new Contact
        {
            Id = new Guid("31f0e2b0-8b80-4d98-b79d-80e49cdeda60"),
            FirstName = "jezz",
            LastName = "papiyoong",
            Position = "jokker",
            LineId = "jezz001",
            Remark = "Remark",
            AddressType = AddressType.Home,
            NickName = "jezzy"
        };

        var contact2 = new Contact
        {
            Id = new Guid("84372246-b3f8-41a8-86d5-68bf3a48a840"),
            FirstName = "robert",
            LastName = "sai-kwan",
            Position = "walker boy",
            LineId = "robertrzzz",
            Remark = "Remark2",
            AddressType = AddressType.Home,
            NickName = "rob"
        };

        var commandMedia = MockUpCreateMediaCommand();
        var media = await SendAsync(commandMedia);
        var contactMediaSection = new ContactMediaSection()
        {
            ContactId = contact.Id,
            MediaSectionId = media.MediaSectionId,
            ContactStatus = ContactStatus.FULL_TIME,
            Remark = "xxxx"
        };
        var contactMediaSection2 = new ContactMediaSection()
        {
            ContactId = contact2.Id,
            MediaSectionId = media.MediaSectionId,
            ContactStatus = ContactStatus.FULL_TIME,
            Remark = "xxxx"
        };
        await AddAsync(contact);
        await AddAsync(contact2);
        await AddAsync(contactMediaSection);
        await AddAsync(contactMediaSection2);
        var query = new GetContactWithPaginationByMediaSectionIdQuery()
        {
            MediaSectionId = media.MediaSectionId,
        };

        var result = await SendAsync(query);

        result.Should().NotBeNull();
        result.TotalCount.Should().Be(2);
        result.Items.Should().NotBeEmpty();

        var firstRecord = result.Items.FirstOrDefault(e => e.Id.Equals(contact.Id));
        firstRecord.Should().NotBeNull();
        firstRecord!.FirstName.Should().Be(contact.FirstName);
        firstRecord!.LastName.Should().Be(contact.LastName);
        firstRecord!.LineId.Should().Be(contact.LineId);
        firstRecord.Position.Should().Be(contact.Position);
        firstRecord.NickName.Should().Be(contact.NickName);
    }

    internal CreateMediaCommand MockUpCreateMediaCommand()
    {
        var platforms = new List<PlatformDto>()
    {
      new PlatformDto()
      {
        Name = "Update PR Company Platform",
        Link = "https://www.pr-platform.com",
        PlatformType = PlatformType.NEWSPAPER.ToString(),
      }
    };

        return new CreateMediaCommand
        {
            Name = "page contact",
            Address = "Address",
            Email = new string[] { "contactpage@email.com" },
            PhoneNumbers = new string[] { "0999234567" },
            ProvinceId = "1",
            DistrictId = "1001",
            SubDistrictId = "100101",
            PostCode = "10200",
            TypeOfMedia = TypeOfMedia.MAINSTREAM.ToString(),
            Platforms = platforms,
            SectionId = "56d22a41-6b16-4335-9ba2-dfeb63c5a830",
            SubSectionId = "90a6e746-a379-4603-b39c-c10409314445"
        };
    }
}
