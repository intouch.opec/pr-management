using PRManagement.Application.Contacts.Queries;
using PRManagement.Application.Contacts.Queries.GetContactAndMediaWithPagination;
namespace PRManagement.Application.FunctionalTests.Contacts.Queries;
using static Testing;
public class GetContactAndMediaWithPaginationTests : BaseTestFixture
{
    [Test]
    public async Task ShouldGetContactAndMediaWithPagination()
    {
        var query = new GetContactAndMediaWithPaginationQuery();

        var result = await SendAsync(query);

        result.Should().NotBeNull();
    }
}