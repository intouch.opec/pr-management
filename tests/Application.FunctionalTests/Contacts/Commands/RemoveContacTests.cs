namespace PRManagement.Application.FunctionalTests.Contacts.Commands;

using PRManagement.Application.Contacts.Commands.CreateContact;
using PRManagement.Application.Contacts.Commands.DeleteContact;
using PRManagement.Application.Medias.Commands.CreateMedia;
using PRManagement.Domain.Enums;
using static Testing;
public class RemoveContactTests : BaseTestFixture
{
  
  [Test]
  public async Task ShouldBeNotFoundContact()
  {
    var command = new DeleteContactCommand()
    {
      Id = Guid.NewGuid(),
      Remark = "Remaek"
    };

    await FluentActions.Invoking(() =>
      SendAsync(command)).Should().ThrowAsync<NotFoundException>();

  }

  [Test]
  public async Task ShouldDeletedContact()
  {
    var mediaCommand = MockUpMediaCommand();
    var media = await SendAsync(mediaCommand);
    var contactCommand = MockUpCreateContactCommand(media.MediaSectionId);
    var contact = await SendAsync(contactCommand);
    var command = new DeleteContactCommand()
    {
      Id = contact.Id,
      Remark = "Remark"
    };

    var result = await SendAsync(command);
    result.Should().NotBeNull();
  }

  internal CreateMediaCommand MockUpMediaCommand()
  {
    return new CreateMediaCommand
    {
      Name = "Delete Contact",
      Address = "Address",
      Email = new string[] { "email236@email.com", "email_test_99@email.com" },
      PhoneNumbers = new string[] { "0651144569" },
      ProvinceId = "1",
      DistrictId = "1001",
      SubDistrictId = "100101",
      PostCode = "10200",
      TypeOfMedia = TypeOfMedia.MAINSTREAM.ToString(),
      SectionId = "56d22a41-6b16-4335-9ba2-dfeb63c5a830",
      SubSectionId = "90a6e746-a379-4603-b39c-c10409314445"
    };
  }

  internal CreateContactCommand MockUpCreateContactCommand(Guid mediaSectionId)
  {
    return new CreateContactCommand
    {
      FirstName = "num ",
      LastName = "tongdee",
      Address = "Address",
      Position = "Position",
      LineId = "LineId",
      Remark = "Remark",
      AddressType = "Home",
      Email = new string[] { "email_remove@email.com" },
      PhoneNumbers = new string[] { "0655534997" },
      ProvinceId = "1",
      DistrictId = "1001",
      SubDistrictId = "100101",
      PostCode = "10200",
      MediaSectionId = mediaSectionId.ToString()
    };
  }
}