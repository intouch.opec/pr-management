using PRManagement.Application.Contacts.Commands.UpdateContact;
using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;
using ValidationException = PRManagement.Application.Common.Exceptions.ValidationException;

namespace PRManagement.Application.FunctionalTests.Contacts.Commands;
using static Testing;
public class UpdateContactTests : BaseTestFixture
{
  [Test]
  public async Task ShouldRequireMinimumFields()
  {
    var command = new UpdateContactCommand();

    await FluentActions.Invoking(() =>
        SendAsync(command)).Should().ThrowAsync<ValidationException>();
  }

  [Test]
  public async Task ShouldUpdatedContact()
  {
    var address = new Address
    {
      Id = new Guid("61661d83-856b-4738-9102-e852877f5743"),
      Text = "AddressUpdated",
      Email = new string[] { "emailUpdate@email.com" },
      PhoneNumbers = new string[] { "0851221567" },
      ProvinceId = "1",
      DistrictId = "1001",
      SubDistrictId = "100101",
      PostCode = "10200"
    };

    var contact = new Contact
    {
      AddressId = address.Id,
      Address = address,
      Id = new Guid("59b8e8b0-05a8-4623-a7d0-7926b5c6a34b"),
      FirstName = "FirstNameUpdated",
      LastName = "LastNameUpdated",
      Position = "PositionUpdate",
      LineId = "LineIdUpdated",
      Remark = "RemarkUpdated",
      AddressType = AddressType.Home,
      NickName = "NickNameUpdated"
    };
    await AddAsync(contact);
    var command = new UpdateContactCommand()
    {
      Id = new Guid("59b8e8b0-05a8-4623-a7d0-7926b5c6a34b"),
      FirstName = "FirstNameUpdated2",
      LastName = "LastNameUpdated2",
      Position = "PositionUpdate2",
      LineId = "LineIdUpdated2",
      Remark = "RemarkUpdated2",
      NickName = "NickNameUpdated2",
      AddressType = AddressType.Work,
      Address = "text",
      ProvinceId = "1",
      DistrictId = "1002",
      SubDistrictId = "100201",
      PostCode = "10300",
      PhoneNumbers = new string[] { "0851001567" },
      Email = new string[] { "afterUpdate@email.com" },
    };

    await SendAsync(command);

    var result = await FindAsync<Contact>(command.Id);
    var addressResult = await FindAsync<Address>(result!.AddressId!);
    result.Should().NotBeNull();
    addressResult.Should().NotBeNull();
    result!.FirstName.Should().Be(command.FirstName);
    result.LastName.Should().Be(command.LastName);
    result.Position.Should().Be(command.Position);
    result.LineId.Should().Be(command.LineId);
    result.Remark.Should().Be(command.Remark);
    result.NickName.Should().Be(command.NickName);
    addressResult!.ProvinceId.Should().Be(command.ProvinceId);
    addressResult.DistrictId.Should().Be(command.DistrictId);
    addressResult.SubDistrictId.Should().Be(command.SubDistrictId);
    addressResult.Text.Should().Be(command.Address);
    addressResult.PostCode.Should().Be(command.PostCode);
    var emailResult = addressResult.Email.First(email => email.Equals(command.Email[0]));
    emailResult.Should().NotBeNull();
    foreach (var phoneNumber in addressResult!.PhoneNumbers)
    {
      var commandPhoneNumber = command.PhoneNumbers.First(p => p.Equals(phoneNumber));
      phoneNumber.Should().Be(commandPhoneNumber);
    }

  }

  [Test]
  public async Task ShouldUpdatedContactAndAddAddress()
  {
    var contact = new Contact
    {
      Id = new Guid("fe5c4115-1d80-47bd-9cba-db68c29806b9"),
      FirstName = "FirstNameAddAddress",
      LastName = "LastNameAndAddAddress",
      Position = "PositionAndAddAddress",
      LineId = "LineIdAddAddress",
      Remark = "RemarkAddAddress",
      AddressType = AddressType.Home,
      NickName = "NickNameAddAddress"
    };
    var command = new UpdateContactCommand()
    {
      Id = new Guid("fe5c4115-1d80-47bd-9cba-db68c29806b9"),
      FirstName = "John",
      LastName = "Chawrai",
      Position = "former",
      LineId = "John1999",
      Remark = "Remark",
      NickName = "John",
      AddressType = AddressType.Work,
      Address = "text",
      ProvinceId = "1",
      DistrictId = "1002",
      SubDistrictId = "100201",
      PostCode = "10300",
      PhoneNumbers = new string[] { "0950001567" },
      Email = new string[] { "AddAddress2@email.com" },
    };
    await AddAsync(contact);

    await SendAsync(command);
    var result = await FindAsync<Contact>(command.Id);
    var addressResult = await FindAsync<Address>(result!.AddressId!);
    result.Should().NotBeNull();
    addressResult.Should().NotBeNull();
    result!.FirstName.Should().Be(command.FirstName);
    result.LastName.Should().Be(command.LastName);
    result.Position.Should().Be(command.Position);
    result.LineId.Should().Be(command.LineId);
    result.Remark.Should().Be(command.Remark);
    result.NickName.Should().Be(command.NickName);
    addressResult!.ProvinceId.Should().Be(command.ProvinceId);
    addressResult.DistrictId.Should().Be(command.DistrictId);
    addressResult.SubDistrictId.Should().Be(command.SubDistrictId);
    addressResult.Text.Should().Be(command.Address);
    addressResult.PostCode.Should().Be(command.PostCode);
    var emailResult = addressResult.Email.First(email => email.Equals(command.Email[0]));
    emailResult.Should().NotBeNull();
    foreach (var phoneNumber in addressResult!.PhoneNumbers)
    {
      var commandPhoneNumber = command.PhoneNumbers.First(p => p.Equals(phoneNumber));
      phoneNumber.Should().Be(commandPhoneNumber);
    }
  }

  [Test]
  public async Task ShouldUpdatedContactNonAddress()
  {
    var contact = new Contact
    {
      Id = new Guid("2e756a5a-ee1c-411f-bd64-539ee2f89be3"),
      FirstName = "JoJo",
      LastName = "Chawban",
      Position = "house maker",
      LineId = "Jo2010",
      Remark = "RemarkAddAddress",
      AddressType = AddressType.Home,
      NickName = "jojo"
    };
    await AddAsync(contact);

    var command = new UpdateContactCommand()
    {
      Id = new Guid("2e756a5a-ee1c-411f-bd64-539ee2f89be3"),
      FirstName = "somchai",
      LastName = "Chawbanna",
      Position = "former",
      LineId = "Jo2011",
      Remark = "Remark",
      NickName = "Jony"
    };

    await SendAsync(command);

    var result = await FindAsync<Contact>(command.Id);
    result!.FirstName.Should().Be(command.FirstName);
    result.LastName.Should().Be(command.LastName);
    result.Position.Should().Be(command.Position);
    result.LineId.Should().Be(command.LineId);
    result.Remark.Should().Be(command.Remark);
    result.NickName.Should().Be(command.NickName);
  }

}