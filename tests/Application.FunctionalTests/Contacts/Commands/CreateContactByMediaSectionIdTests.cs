using PRManagement.Application.Common.Exceptions;
using PRManagement.Application.Common.Models;
using PRManagement.Application.Contacts.Commands.CreateContactByMediaSectionId;
using PRManagement.Application.Medias.Commands.CreateMedia;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.FunctionalTests.Contacts.Commands;
using static Testing;
public class CreateContactByMediaSectionIdTests : BaseTestFixture
{

  [Test]
  public async Task ShouldRequireMinimumFields()
  {
    var command = new CreateContactByMediaSectionIdCommand();

    await FluentActions.Invoking(() =>
        SendAsync(command)).Should().ThrowAsync<ValidationException>();
  }

  [Test]
  public async Task ShouldCreateContacts()
  {
    var mediaCommand = MockUpCreateMediaCommand();
    var media = await SendAsync(mediaCommand);
    var command = MockUpCreateContactByMediaSectionIdCommand(media.MediaSectionId.ToString());
    await SendAsync(command);
  }

  internal CreateMediaCommand MockUpCreateMediaCommand()
  {
    var platforms = new List<PlatformDto>()
    {
      new PlatformDto()
      {
        Name = "Contact multiple",
        Link = "https://www.contact-multiple.com",
        PlatformType = PlatformType.NEWSPAPER.ToString(),
      }
    };

    return new CreateMediaCommand
    {
      Name = "contact",
      Address = "Address",
      Email = new string[] { "email_contact_mul@email.com" },
      PhoneNumbers = new string[] { "0652934561" },
      ProvinceId = "1",
      DistrictId = "1001",
      SubDistrictId = "100101",
      PostCode = "10200",
      TypeOfMedia = TypeOfMedia.MAINSTREAM.ToString(),
      Platforms = platforms,
      SectionId = "56d22a41-6b16-4335-9ba2-dfeb63c5a830",
      SubSectionId = "90a6e746-a379-4603-b39c-c10409314445"
    };
  }

  internal CreateContactByMediaSectionIdCommand MockUpCreateContactByMediaSectionIdCommand(string mediaSectionId)
  {
    return new CreateContactByMediaSectionIdCommand()
    {
      MediaSectionId = mediaSectionId,
      Contacts = new List<ContactItem>() {
        new ContactItem() {
          FirstName = "chaitong ",
          LastName = "deemaina",
          Address = "Address",
          Position = "Position",
          LineId = "LineId",
          Remark = "Remark",
          AddressType = "Home",
          Email = new string[] { "chaitong@email.com" },
          PhoneNumbers = new string[] { "0915524567" },
          ProvinceId = "1",
          DistrictId = "1001",
          SubDistrictId = "100101",
          PostCode = "10200",
        },
         new ContactItem() {
          FirstName = "tongmee",
          LastName = "deenaja",
          Address = "Address",
          Position = "Position",
          LineId = "LineId",
          Remark = "Remark",
          AddressType = "Home",
          Email = new string[] { "tongmee@email.com" },
          PhoneNumbers = new string[] { "0925524569" },
          ProvinceId = "1",
          DistrictId = "1001",
          SubDistrictId = "100101",
          PostCode = "10200",
        },
      }
    };
  }
}