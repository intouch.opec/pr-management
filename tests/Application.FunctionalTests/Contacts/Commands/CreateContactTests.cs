using PRManagement.Application.Common.Models;
using PRManagement.Application.Contacts.Commands.CreateContact;
using PRManagement.Application.Medias.Commands.CreateMedia;
using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;
using ValidationException = PRManagement.Application.Common.Exceptions.ValidationException;

namespace PRManagement.Application.FunctionalTests.Contacts.Commands;
using static Testing;

public class CreateContactTests : BaseTestFixture
{
    [Test]
    public async Task ShouldRequireMinimumFields()
    {
        var command = new CreateContactCommand();

        await FluentActions.Invoking(() =>
            SendAsync(command)).Should().ThrowAsync<ValidationException>();
    }

    [Test]
    public async Task ShouldCreateMedia()
    {
        var commandMedia = MockUpCreateMediaCommand();
        var media = await SendAsync(commandMedia);
        var command = MockUpCreateContactCommand(media.MediaSectionId);

        var contact = await SendAsync(command);

        var item = await FindAsync<Contact>(contact.Id);
        var address = await FindAsync<Address>(item!.AddressId!);
        item.Should().NotBeNull();
        item!.Id.Should().Be(contact!.Id);
        item!.NickName.Should().Be(command.NickName);
        item!.LastName.Should().Be(command.LastName);
        item!.FirstName.Should().Be(command.FirstName);
        item!.Position.Should().Be(command.Position);
        item!.LineId.Should().Be(command.LineId);
        item!.Remark.Should().Be(command.Remark);
        address.Should().NotBeNull();
        address!.Text.Should().Be(command.Address);
        address!.DistrictId.Should().Be(command.DistrictId);
        address!.SubDistrictId.Should().Be(command.SubDistrictId);
        address!.ProvinceId.Should().Be(command.ProvinceId);
        address!.PostCode.Should().Be(command.PostCode);
        foreach (var phoneNumber in address!.PhoneNumbers)
        {
            var commandPhoneNumber = command.PhoneNumbers.First(p => p.Equals(phoneNumber));
            phoneNumber.Should().Be(commandPhoneNumber);
        }

        foreach (var email in address!.Email)
        {
            var commandEmail = command.Email.First(p => p.Equals(email));
            email.Should().Be(commandEmail);
        }
    }

    internal CreateMediaCommand MockUpCreateMediaCommand()
    {
        var platforms = new List<PlatformDto>()
        {
            new PlatformDto()
            {
                Name = "Create Contact",
                Link = "https://www.pr-platform.com",
                PlatformType = PlatformType.NEWSPAPER.ToString(),
            }
        };

        return new CreateMediaCommand
        {
            Name = "Create Contact",
            Address = "Address",
            Email = new string[] { "email@email.com", "email2@email.com" },
            PhoneNumbers = new string[] { "0651234561" },
            ProvinceId = "1",
            DistrictId = "1001",
            SubDistrictId = "100101",
            PostCode = "10200",
            TypeOfMedia = TypeOfMedia.MAINSTREAM.ToString(),
            Platforms = platforms,
            SectionId = "56d22a41-6b16-4335-9ba2-dfeb63c5a830",
            SubSectionId = "90a6e746-a379-4603-b39c-c10409314445"
        };
    }

    internal CreateContactCommand MockUpCreateContactCommand(Guid mediaSectionId)
    {
        return new CreateContactCommand
        {
            FirstName = "chai ",
            LastName = "jaidee",
            Address = "Address",
            Position = "Position",
            LineId = "LineId",
            Remark = "Remark",
            AddressType = "Home",
            Email = new string[] { "email@email.com", "email2@email.com" },
            PhoneNumbers = new string[] { "0655534567" },
            ProvinceId = "1",
            DistrictId = "1001",
            SubDistrictId = "100101",
            PostCode = "10200",
            MediaSectionId = mediaSectionId.ToString()
        };
    }

}