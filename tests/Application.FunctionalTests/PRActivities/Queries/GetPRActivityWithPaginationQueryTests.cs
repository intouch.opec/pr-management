using PRManagement.Application.PRActivities.Queries.GetPRActivityWithPagination;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.FunctionalTests.PRActivities.Queries;
using static Testing;

public class GetPRActivityWithPaginationQueryTests : BaseTestFixture
{
    [Test]
    public async Task ShouldBeGetPRActivityList()
    {
        var result = await SendAsync(new GetPRActivityWithPaginationQuery());

        foreach (var item in result.Items)
        {
            item.ClientName.Should().NotBeEmpty();
            item.SectionName.Should().NotBeEmpty();
            item.SubSectionName.Should().NotBeEmpty();
            item.Title.Should().NotBeEmpty();
            item.ClientId.Should().NotBeEmpty();
            item.SectionId.Should().NotBeEmpty();
            item.SubSectionId.Should().NotBeEmpty();
            item.OwnerId.Should().NotBeEmpty();
            item.OwnerName.Should().NotBeEmpty();
            item.WorkProgress.Should().NotBeEmpty();
        }
    }

    [Test]
    public async Task ShouldBeGetPRActivityDurationTypeShortList()
    {
        var query = new GetPRActivityWithPaginationQuery
        {
            DurationType = DurationPRActivityType.SHORT.ToString()
        };
        var result = await SendAsync(query);

        foreach (var item in result.Items)
        {
            item.ClientName.Should().NotBeEmpty();
            item.SectionName.Should().NotBeEmpty();
            item.SubSectionName.Should().NotBeEmpty();
            item.Title.Should().NotBeEmpty();
            item.ClientId.Should().NotBeEmpty();
            item.SectionId.Should().NotBeEmpty();
            item.SubSectionId.Should().NotBeEmpty();
            item.OwnerId.Should().NotBeEmpty();
            item.WorkProgress.Should().NotBeEmpty();
            item.OwnerName.Should().NotBeEmpty();
            item.DurationType.Should().Be(DurationPRActivityType.SHORT);
        }
    }

    [Test]
    public async Task ShouldBeGetPRActivityContractTypeAdhocList()
    {
        var query = new GetPRActivityWithPaginationQuery
        {
            ContractType = ContractType.ADHOC.ToString()
        };
        var result = await SendAsync(query);

        foreach (var item in result.Items)
        {
            item.ClientName.Should().NotBeEmpty();
            item.SectionName.Should().NotBeEmpty();
            item.SubSectionName.Should().NotBeEmpty();
            item.Title.Should().NotBeEmpty();
            item.ClientId.Should().NotBeEmpty();
            item.SectionId.Should().NotBeEmpty();
            item.SubSectionId.Should().NotBeEmpty();
            item.OwnerId.Should().NotBeEmpty();
            item.WorkProgress.Should().NotBeEmpty();
            item.OwnerName.Should().NotBeEmpty();
            item.ContractType.Should().Be(ContractType.ADHOC);
        }
    }

    [Test]
    public async Task ShouldBeGetPRActivityContentPRActivityTypePressReleaseList()
    {
        var query = new GetPRActivityWithPaginationQuery
        {
            ContentPRActivityType = ContentPRActivityType.PRESS_RELEASE.ToString()
        };
        var result = await SendAsync(query);

        foreach (var item in result.Items)
        {
            item.ClientName.Should().NotBeEmpty();
            item.SectionName.Should().NotBeEmpty();
            item.SubSectionName.Should().NotBeEmpty();
            item.Title.Should().NotBeEmpty();
            item.ClientId.Should().NotBeEmpty();
            item.SectionId.Should().NotBeEmpty();
            item.SubSectionId.Should().NotBeEmpty();
            item.OwnerId.Should().NotBeEmpty();
            item.WorkProgress.Should().NotBeEmpty();
            item.OwnerName.Should().NotBeEmpty();
            item.ContentPRActivityType.Should().Be(ContentPRActivityType.PRESS_RELEASE);
        }
    }

    [Test]
    public async Task ShouldBeGetPRActivityContentPRActivityFromStartedList()
    {
        var query = new GetPRActivityWithPaginationQuery()
        {
            FromReleased = DateTimeOffset.UtcNow,
            ToReleased = DateTimeOffset.UtcNow.AddDays(7),
        };
        var result = await SendAsync(query);

        foreach (var item in result.Items)
        {
            item.ClientName.Should().NotBeEmpty();
            item.SectionName.Should().NotBeEmpty();
            item.SubSectionName.Should().NotBeEmpty();
            item.Title.Should().NotBeEmpty();
            item.ClientId.Should().NotBeEmpty();
            item.SectionId.Should().NotBeEmpty();
            item.SubSectionId.Should().NotBeEmpty();
            item.OwnerId.Should().NotBeEmpty();
            item.OwnerName.Should().NotBeEmpty();
            item.WorkProgress.Should().NotBeEmpty();
        }
    }

    [Test]
    public async Task ShouldBeGetPRActivityContentPRActivityFromEndedList()
    {
        var query = new GetPRActivityWithPaginationQuery()
        {
            FromEnded = DateTimeOffset.UtcNow,
            ToEnded = DateTimeOffset.UtcNow.AddDays(7),
        };
        var result = await SendAsync(query);

        foreach (var item in result.Items)
        {
            item.ClientName.Should().NotBeEmpty();
            item.SectionName.Should().NotBeEmpty();
            item.SubSectionName.Should().NotBeEmpty();
            item.Title.Should().NotBeEmpty();
            item.ClientId.Should().NotBeEmpty();
            item.SectionId.Should().NotBeEmpty();
            item.SubSectionId.Should().NotBeEmpty();
            item.OwnerId.Should().NotBeEmpty();
            item.WorkProgress.Should().NotBeEmpty();
        }
    }

    [Test]
    public async Task ShouldBeGetPRActivityContentPRActivityFromStartedToEndedList()
    {
        var query = new GetPRActivityWithPaginationQuery()
        {
            FromEnded = DateTimeOffset.UtcNow,
            ToEnded = DateTimeOffset.UtcNow.AddDays(7),
        };
        var result = await SendAsync(query);

        foreach (var item in result.Items)
        {
            item.ClientName.Should().NotBeEmpty();
            item.SectionName.Should().NotBeEmpty();
            item.SubSectionName.Should().NotBeEmpty();
            item.Title.Should().NotBeEmpty();
            item.ClientId.Should().NotBeEmpty();
            item.SectionId.Should().NotBeEmpty();
            item.SubSectionId.Should().NotBeEmpty();
            item.OwnerId.Should().NotBeEmpty();
            item.OwnerName.Should().NotBeEmpty();
            item.WorkProgress.Should().NotBeEmpty();
        }
    }

}
