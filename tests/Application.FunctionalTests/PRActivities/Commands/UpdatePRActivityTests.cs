namespace PRManagement.Application.FunctionalTests.PRActivities.Commands;

using ValidationException = Application.Common.Exceptions.ValidationException;
using PRManagement.Application.PRActivities.Commands.UpdatePRActivity;
using static Testing;
using PRManagement.Domain.Enums;
using PRManagement.Domain.Entities;
using PRManagement.Application.Common.Extensions;
using PRManagement.Domain.Exceptions;
using PRManagement.Application.PRActivities.Commands.CreatePRActivity;

public class UpdatePRActivityTests : BaseTestFixture
{
    [Test]
    public async Task ShouldRequireMinimumFields()
    {
        var command = new UpdatePRActivityCommand();

        await FluentActions.Invoking(() =>
            SendAsync(command)).Should().ThrowAsync<ValidationException>();
    }

    [Test]
    public async Task ShouldBeUpdatePRActivity()
    {
        var mockup = await MockUpCreatePRActivity();
        var command = new UpdatePRActivityCommand
        {
            Id = mockup.ToString(),
            Title = "Annual Campaign 2",
            DurationType = DurationPRActivityType.LONG.ToString(),
            ContractType = ContractType.RETAINER.ToString(),
            ContentPRActivityType = ContentPRActivityType.SCOOP_NEWS.ToString(),
            Started = DateTimeOffset.UtcNow.ToUniversalTime(),
            Ended = DateTimeOffset.UtcNow.AddMonths(6).ToUniversalTime(),
            Released = DateTimeOffset.UtcNow.AddMonths(6).AddDays(1).ToUniversalTime(),
            WorkTasks = new List<string> {
                WorkTask.BRIEFING_MEETING.ToString(),
                WorkTask.CONFIRM_LOCATION.ToString(),
                WorkTask.UPLOAD_FILES.ToString(),
                WorkTask.SIGN_EVENT.ToString(),
                WorkTask.SEND_EMAIL_CLIENT.ToString(),
            },
            ClientId = "5f8fb285-2fa0-4974-9a80-6a6d6e46a4ed",
            SectionId = "56d22a41-6b16-4335-9ba2-dfeb63c5a830",
            SubSectionId = "90a6e746-a379-4603-b39c-c10409314445",
            OwnerId = "fbc382fe-5271-4ddd-bda8-4afdb839723d"
        };

        var result = await SendAsync(command);
        var prActivity = await FindAsync<PRActivity>(result.Id);
        var client = await FindAsync<Client>(command.ClientId.ToGuidOnly());
        prActivity.Should().NotBeNull();
        client.Should().NotBeNull();

        client!.ClientTypes.Length.Should().Be(1);

        prActivity!.Title.Should().Be(command.Title);
        prActivity!.DurationType.Should().Be(command.DurationType.ToEnum<DurationPRActivityType>());
        prActivity!.ContractType.Should().Be(command.ContractType.ToEnum<ContractType>());
        prActivity!.Started.Should().Be(command.Started);
        prActivity!.Ended.Should().Be(command.Ended);
        prActivity!.Released.Should().Be(command.Released);
        prActivity!.ClientId.Should().Be(command.ClientId.ToGuidOnly());
        prActivity!.SectionId.Should().Be(command.SectionId.ToGuidOnly());
        prActivity!.SubSectionId.Should().Be(command.SubSectionId.ToGuidOnly());

    }

    private async Task<Guid> MockUpCreatePRActivity()
    {
        var command = new CreatePRActivityCommand
        {
            Title = "Annual",
            DurationType = DurationPRActivityType.LONG.ToString(),
            ContentPRActivityType = ContentPRActivityType.PRESS_RELEASE.ToString(),
            ContractType = ContractType.RETAINER.ToString(),
            Started = DateTimeOffset.UtcNow.ToUniversalTime(),
            Ended = DateTimeOffset.UtcNow.AddMonths(6).ToUniversalTime(),
            Released = DateTimeOffset.UtcNow.AddMonths(6).AddDays(1).ToUniversalTime(),
            WorkTasks = new List<string> {
                WorkTask.BRIEFING_MEETING.ToString(),
                WorkTask.CONFIRM_LOCATION.ToString(),
                WorkTask.UPLOAD_FILES.ToString(),
                WorkTask.SIGN_EVENT.ToString(),
            },
            ClientId = "5f8fb285-2fa0-4974-9a80-6a6d6e46a4ed",
            SectionId = "56d22a41-6b16-4335-9ba2-dfeb63c5a830",
            SubSectionId = "90a6e746-a379-4603-b39c-c10409314445",
            OwnerId = "fbc382fe-5271-4ddd-bda8-4afdb839723d"
        };

        var result = await SendAsync(command);
        return result.Id;
    }

}
