namespace PRManagement.Application.FunctionalTests.PRActivities.Commands;

using PRManagement.Application.PRActivities.Commands.RemovePRActivity;
using PRManagement.Domain.Entities;
using static Testing;
using ValidationException = Application.Common.Exceptions.ValidationException;

public class RemovePRActivityTests : BaseTestFixture
{
    [Test]
    public async Task ShouldRequireMinimumFields()
    {
        var command = new RemovePRActivityCommand();

        await FluentActions.Invoking(() =>
            SendAsync(command)).Should().ThrowAsync<ValidationException>();
    }

    [Test]
    public async Task ShouldBeRemovePRActivity()
    {
        var command = new RemovePRActivityCommand()
        {
            Id = "e638a958-cabf-448d-bfec-734c280abd41",
            Remark = "Remark"
        };

        var result = await SendAsync(command);

        var entity = await FindAsync<PRActivity>(result.Id);

        entity.Should().BeNull();
    }
}
