using PRManagement.Application.Clients.Commands.CreateClient;
using PRManagement.Application.Clients.Commands.UpdateClient;
using PRManagement.Domain.Entities;
using ValidationException = PRManagement.Application.Common.Exceptions.ValidationException;

namespace PRManagement.Application.FunctionalTests.Contacts.Commands;
using static Testing;
public class UpdateClientTests : BaseTestFixture
{
    [Test]
    public async Task ShouldRequireMinimumFields()
    {
        var command = new UpdateClientCommand();

        await FluentActions.Invoking(() =>
            SendAsync(command)).Should().ThrowAsync<ValidationException>();
    }

    [Test]
    public async Task ShouldUpdateClient()
    {
        var commandCreate = MockUpCreateClientCommand();
        var client = await SendAsync(commandCreate);
        var command = MockUpUpdateClientCommand(client.Id);

        await SendAsync(command);
        var result = await FindAsync<Client>(client.Id);
        result.Should().NotBeNull();
        var resultAddress = await FindAsync<Address>(result!.AddressId!);
        resultAddress.Should().NotBeNull();
        result.Name.Should().Be(command.Name);
        resultAddress!.Text.Should().Be(command.Address);
        resultAddress!.ProvinceId.Should().Be(command.ProvinceId);
        resultAddress!.SubDistrictId.Should().Be(command.SubDistrictId);
        resultAddress!.DistrictId.Should().Be(command.DistrictId);
        resultAddress!.PostCode.Should().Be(command.PostCode);

        foreach (var email in resultAddress.Email)
            command.Email.First(x => x.Equals(email)).Should().NotBeNull();

        foreach (var phoneNumber in resultAddress.PhoneNumbers)
            command.PhoneNumbers.First(x => x.Equals(phoneNumber)).Should().NotBeNull();

    }

    [Test]
    public async Task ShouldRequireNameUnique()
    {
        var clientWillDuplicatedName = await SendAsync(MockUpCreateClientCommandWasDuplicatedName());
        var clientWillUpdated = await SendAsync(MockUpCreateClientCommandBeChanged());

        try
        {
            await SendAsync(MockUpUpdateClientCommandWasDuplicatedName(clientWillUpdated.Id));
        }
        catch (ValidationException result)
        {
            var error = result.Errors.First();
            var key = nameof(UpdateClientCommand.Name);
            error.Key.Should().Be(key);
            error.Value[0].Should().Be($"The '{nameof(UpdateClientCommand.Name)}' is already exits.");
        }
    }

    internal CreateClientCommand MockUpCreateClientCommand()
    {
        return new CreateClientCommand()
        {
            Name = "client name",
            Address = "address",
            PhoneNumbers = new string[] { "0651234122" },
            Email = new string[] { "client_test@gmail.com" },
            ProvinceId = "1",
            DistrictId = "1001",
            SubDistrictId = "100101",
            PostCode = "10200",
        };
    }

    internal UpdateClientCommand MockUpUpdateClientCommand(Guid id)
    {
        return new UpdateClientCommand()
        {
            Id = id,
            Name = "client update",
            Address = "address3",
            PhoneNumbers = new string[] { "0655122412" },
            Email = new string[] { "client_1@gmail.com" },
            ProvinceId = "1",
            DistrictId = "1002",
            SubDistrictId = "100102",
            PostCode = "10200",
        };
    }

    internal CreateClientCommand MockUpCreateClientCommandWasDuplicatedName()
    {
        return new CreateClientCommand()
        {
            Name = "client name duplicate",
            Address = "address",
            PhoneNumbers = new string[] { "0650934122" },
            Email = new string[] { "client_test@gmail.com" },
            ProvinceId = "1",
            DistrictId = "1001",
            SubDistrictId = "100101",
            PostCode = "10200",
        };
    }

    internal CreateClientCommand MockUpCreateClientCommandBeChanged()
    {
        return new CreateClientCommand()
        {
            Name = "client name 3",
            Address = "address",
            PhoneNumbers = new string[] { "0621564722" },
            Email = new string[] { "client_test@gmail.com" },
            ProvinceId = "1",
            DistrictId = "1001",
            SubDistrictId = "100101",
            PostCode = "10200",
        };
    }

    internal UpdateClientCommand MockUpUpdateClientCommandWasDuplicatedName(Guid id)
    {
        return new UpdateClientCommand()
        {
            Id = id,
            Name = "client name duplicate",
            Address = "address3",
            PhoneNumbers = new string[] { "0650022412" },
            Email = new string[] { "client_1@gmail.com" },
            ProvinceId = "1",
            DistrictId = "1002",
            SubDistrictId = "100102",
            PostCode = "10200",
        };
    }
}