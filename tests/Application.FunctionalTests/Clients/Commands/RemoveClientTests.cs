
namespace PRManagement.Application.FunctionalTests.Contacts.Commands;

using PRManagement.Application.Clients.Commands.CreateClient;
using PRManagement.Application.Clients.Commands.RemoveClient;
using static Testing;

public class RemoveClientTests : BaseTestFixture
{
    [Test]
    public async Task ShouldBeNotFoundContact()
    {
        var command = new RemoveClientCommand()
        {
            Id = Guid.NewGuid(),
            Remark = "Remaek"
        };

        await FluentActions.Invoking(() =>
          SendAsync(command)).Should().ThrowAsync<NotFoundException>();

    }

    internal CreateClientCommand MockUpCreateClientCommand()
    {
        return new CreateClientCommand
        {
            Name = "Client company",
            Address = "Address",
            Email = new string[] { "client_za@email.com" },
            PhoneNumbers = new string[] { "0612944567" },
            ProvinceId = "1",
            DistrictId = "1001",
            SubDistrictId = "100101",
            PostCode = "10200",
        };
    }

}