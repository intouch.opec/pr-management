using PRManagement.Application.Clients.Commands.CreateClient;
using PRManagement.Domain.Entities;
using ValidationException = PRManagement.Application.Common.Exceptions.ValidationException;

namespace PRManagement.Application.FunctionalTests.Contacts.Commands;
using static Testing;

public class CreateClientTests : BaseTestFixture
{
	[Test]
	public async Task ShouldRequireMinimumFields()
	{
		var command = new CreateClientCommand();

		await FluentActions.Invoking(() =>
			SendAsync(command)).Should().ThrowAsync<ValidationException>();
	}

	[Test]
	public async Task ShouldCreateClient()
	{
		var command = MockUpCreateClientCommand();
		var result = await SendAsync(command);
		var client = await FindAsync<Client>(result.Id);
		var address = await FindAsync<Address>(client!.AddressId!);

		result.Should().NotBeNull();

		client!.Name.Should().Be(command.Name);
		address!.DistrictId.Should().Be(command.DistrictId);
		address!.ProvinceId.Should().Be(command.ProvinceId);
		address!.SubDistrictId.Should().Be(command.SubDistrictId);
		address.PostCode.Should().Be(command.PostCode);
		foreach (var email in address.Email)
			command.Email.First(x => x.Equals(email)).Should().NotBeNull();

		foreach (var phoneNumber in address.PhoneNumbers)
			command.PhoneNumbers.First(x => x.Equals(phoneNumber)).Should().NotBeNull();
	}

	[Test]
	public async Task ShouldCreateClientRequireMinimumFields()
	{
		var command = MockUpCreateClientRequireMinimumFieldsCommand();
		var result = await SendAsync(command);
		var client = await FindAsync<Client>(result.Id);

		result.Should().NotBeNull();

		client!.Name.Should().Be(command.Name);
		client!.AddressId.Should().NotBeNull();
	}

	internal CreateClientCommand MockUpCreateClientCommand()
	{
		return new CreateClientCommand
		{
			Name = "Create PR Company",
			Address = "Address",
			Email = new string[] { "email_za@email.com", "emailzaza@email.com" },
			PhoneNumbers = new string[] { "0650934567" },
			ProvinceId = "1",
			DistrictId = "1001",
			SubDistrictId = "100101",
			PostCode = "10200",
		};
	}

	internal CreateClientCommand MockUpCreateClientRequireMinimumFieldsCommand()
	{
		return new CreateClientCommand
		{
			Name = "Create PR",
			Email = new string[] { "tew_za@email.com", "Etest@email.com" },
			PhoneNumbers = new string[] { "0651932567" },
		};
	}
}