using ValidationException = PRManagement.Application.Common.Exceptions.ValidationException;
using PRManagement.Application.GetPRActivityTaskComments.Queries.GetPRActivityTaskCommands;

namespace PRManagement.Application.FunctionalTests.PRActivityTaskComments.Commands;
using static Testing;

public class GetPRActivityTaskCommentTests : BaseTestFixture
{
    [Test]
    public async Task ShouldRequireValidGetPRActivityTaskComment()
    {
        var command = new GetPRActivityTaskCommentsQuery("");

        await FluentActions.Invoking(() =>
           SendAsync(command)).Should().ThrowAsync<ValidationException>();
    }

    [Test]
    public async Task ShouldBeGetPRActivityTask()
    {
        var command = new GetPRActivityTaskCommentsQuery("fe0fcc0a-70d9-4e03-962a-ace1e9ff5a3e");
        var result = await SendAsync(command);

        result.Should().NotBeEmpty();
    }
}
