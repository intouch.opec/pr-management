using ValidationException = PRManagement.Application.Common.Exceptions.ValidationException;

namespace PRManagement.Application.FunctionalTests.PRActivityTaskComments.Commands;

using PRManagement.Application.PRActivityTaskComments.Commands.CreatePRActivityTask;
using PRManagement.Domain.Entities;
using static Testing;

public class CreatePRActivityTaskTests : BaseTestFixture
{
    [Test]
    public async Task ShouldRequireValidPRActivityTask()
    {
        var command = new CreatePRActivityTaskCommentCommand();

        await FluentActions.Invoking(() =>
           SendAsync(command)).Should().ThrowAsync<ValidationException>();
    }

    [Test]
    public async Task ShouldBeCretePRActivityTask()
    {
        var command = new CreatePRActivityTaskCommentCommand()
        {
            PrActivityTaskId = "fe0fcc0a-70d9-4e03-962a-ace1e9ff5a3e",
            Content = "XXXXX"
        };

        var result = await SendAsync(command);

        var entity = await FindAsync<PRActivityTaskComment>(result.Id);

        entity!.Content.Should().Be(command.Content);
    }

}

