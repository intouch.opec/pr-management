using PRManagement.Application.PRActivityTaskComments.Commands.UpdatePRActivityTaskCommands;
using PRManagement.Domain.Entities;
using ValidationException = PRManagement.Application.Common.Exceptions.ValidationException;

namespace PRManagement.Application.FunctionalTests.PRActivityTaskComments.Commands;
using static Testing;

public class UpdatePRActivityTaskCommentTests : BaseTestFixture
{
    [Test]
    public async Task ShouldRequireValidUpdatePRActivityTaskCommentCommand()
    {
        var command = new UpdatePRActivityTaskCommentCommand();

        await FluentActions.Invoking(() =>
           SendAsync(command)).Should().ThrowAsync<ValidationException>();
    }

    [Test]
    public async Task ShouldBeUpdatePRActivityTaskComment()
    {
        var command = new UpdatePRActivityTaskCommentCommand()
        {
            Id = "78c26f90-e125-4383-bf39-1f730c83f01a",
            Content = "After Updated",
            PrActivityTaskId = "fe0fcc0a-70d9-4e03-962a-ace1e9ff5a3e"
        };

        var result = await SendAsync(command);
        var entity = await FindAsync<PRActivityTaskComment>(result.Id);
        entity.Should().NotBeNull();
        entity!.Content.Should().Be(command.Content);
    }

}
