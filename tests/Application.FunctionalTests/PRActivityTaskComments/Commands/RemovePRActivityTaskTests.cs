namespace PRManagement.Application.FunctionalTests.PRActivityTaskComments.Commands;
using PRManagement.Application.PRActivityTaskComments.Commands.RemovePRActivityTaskComments;
using PRManagement.Domain.Entities;
using static Testing;

public class RemovePRActivityTaskTests : BaseTestFixture
{
    [Test]
    public async Task ShouldBeRemovePRActivityTaskCommand()
    {
        var command = new RemovePRActivityTaskCommentCommand("8dbf84b9-2869-45e9-8f4b-30ab8c13628c", "1e95414f-af62-4035-87a9-4e4db3f4ad32");

        var result = await SendAsync(command);

        var entity = await FindAsync<PRActivityTaskComment>(result.Id);
        entity!.Should().BeNull();
    }
}
