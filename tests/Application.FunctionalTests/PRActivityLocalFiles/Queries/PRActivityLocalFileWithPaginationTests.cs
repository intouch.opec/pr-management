using PRManagement.Application.PRActivityLocalFiles.Queries.PRActivityLocalFileWithPagination;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.FunctionalTests.PRActivityLocalFiles.Queries;
using static Testing;

public class PRActivityLocalFileWithPaginationTests : BaseTestFixture
{
    [Test]
    public async Task ShouldGetPRActivityLocalFilesWithPagination()
    {
        var query = new PRActivityLocalFileWithPaginationQuery();
        var result = await SendAsync(query);
        
        result.Items.Should().NotBeEmpty();
        result.TotalCount.Should().BeGreaterThan(0);
    }

    [Test]
    public async Task ShouldFilterByFileName()
    {
        var query = new PRActivityLocalFileWithPaginationQuery
        {
            FileName = "test1.pdf",
            PageSize = 10,
            PageNumber = 1
        };

        var result = await SendAsync(query);
        
        result.Items.Should().NotBeEmpty();
        result.Items.Should().OnlyContain(x => x.FileName.Contains("test1.pdf", StringComparison.OrdinalIgnoreCase));
    }

    [Test]
    public async Task ShouldFilterByDateRange()
    {
        var startDate = DateTimeOffset.UtcNow.AddDays(-7);
        var endDate = DateTimeOffset.UtcNow;

        var query = new PRActivityLocalFileWithPaginationQuery
        {
            CreatedStart = startDate,
            CreatedEnd = endDate,
            PageSize = 10,
            PageNumber = 1
        };

        var result = await SendAsync(query);
        
        result.Items.Should().NotBeEmpty();
        result.Items.Should().OnlyContain(x => x.Created >= startDate && x.Created <= endDate);
    }

    [Test]
    public async Task ShouldSortByLastModifiedDescending()
    {
        var query = new PRActivityLocalFileWithPaginationQuery
        {
            Sort = SortType.DESC,
            SortBy = nameof(PRActivityLocalFileItem.LastModified),
            PageSize = 10,
            PageNumber = 1
        };

        var result = await SendAsync(query);
        
        result.Items.Should().NotBeEmpty();
        result.Items.Should().BeInDescendingOrder(x => x.LastModified);
    }

    [Test]
    public async Task ShouldSortByLastModifiedAscending()
    {
        var query = new PRActivityLocalFileWithPaginationQuery
        {
            Sort = SortType.ASC,
            SortBy = nameof(PRActivityLocalFileItem.LastModified),
            PageSize = 10,
            PageNumber = 1
        };

        var result = await SendAsync(query);
        
        result.Items.Should().NotBeEmpty();
        result.Items.Should().BeInAscendingOrder(x => x.LastModified);
    }

    [Test]
    public async Task ShouldReturnCorrectPageSize()
    {
        var pageSize = 5;
        var query = new PRActivityLocalFileWithPaginationQuery
        {
            PageSize = pageSize,
            PageNumber = 1
        };

        var result = await SendAsync(query);
        
        result.Items.Count().Should().BeLessOrEqualTo(pageSize);
    }

    [Test]
    public async Task ShouldReturnEmptyResultForNonExistentFileName()
    {
        var query = new PRActivityLocalFileWithPaginationQuery
        {
            FileName = "NonExistentFile123456.xyz",
            PageSize = 10,
            PageNumber = 1
        };

        var result = await SendAsync(query);
        
        result.Items.Should().BeEmpty();
        result.TotalCount.Should().Be(0);
    }
}
