using PRManagement.Application.PRActivityLocalFiles.Queries.GetPRActivityLocalFiles;
using ValidationException = PRManagement.Application.Common.Exceptions.ValidationException;

namespace PRManagement.Application.FunctionalTests.PRActivityLocalFiles.Queries;

using static Testing;

public class GetPRActivityLocalFilesTests : BaseTestFixture
{
    [Test]
    public async Task ShouldRequireValidId()
    {
        var query = new GetPRActivityLocalFilesQuery("");

        await FluentActions.Invoking(() =>
            SendAsync(query)).Should().ThrowAsync<ValidationException>();
    }

    [Test]
    public async Task ShouldReturnFileResponse()
    {
        var validId = "543f43e4-734b-4c40-839b-942c5bf05e2e";
        
        var query = new GetPRActivityLocalFilesQuery(validId);
        var result = await SendAsync(query);

        result.Should().NotBeNull();
        result.File.Should().NotBeNull();
        result.FileName.Should().NotBeNullOrEmpty();
        result.ContentType.Should().NotBeNullOrEmpty();
    }

    [Test]
    public async Task ShouldThrowNotFoundExceptionForInvalidId()
    {
        // Arrange
        var invalidId = "11111111-1111-1111-1111-xx";
        var query = new GetPRActivityLocalFilesQuery(invalidId);

        // Act & Assert
        await FluentActions.Invoking(() =>
            SendAsync(query)).Should().ThrowAsync<ValidationException>();
    }
}
