namespace PRManagement.Application.FunctionalTests.PRActivityLocalFiles.Commands;
using static Testing;

using ValidationException = Application.Common.Exceptions.ValidationException;
using Microsoft.AspNetCore.Http;
using System.Text;
using PRManagement.Domain.Entities;
using PRManagement.Application.PRActivityLocalFiles.Commands.UploadFiles;

public class UploadFilesCommandTests : BaseTestFixture
{
    [Test]
    public async Task ShouldUploadFile()
    {
        // Arrange
        var fileContent = "This is a test file content";
        var fileName = "test.txt";
        var stream = new MemoryStream(Encoding.UTF8.GetBytes(fileContent));

        var file = new FormFile(
            baseStream: stream,
            baseStreamOffset: 0,
            length: stream.Length,
            name: "file",
            fileName: fileName)
        {
            Headers = new HeaderDictionary(),
            ContentType = "text/plain"
        };

        var command = new UploadFilesCommand("9bdcd9ec-a1f6-4a59-9c31-dbbe17bd3c35")
        {
            File = file
        };

        // Act
        var result = await SendAsync(command);

        // Assert
        result.Should().NotBeNull();
        result.FileName.Should().Be(fileName);

        var uploadedFile = await FindAsync<PRActivityLocalFile>(result.Id);
        var localFile = await FindAsync<LocalFile>(uploadedFile!.LocalFileId!);
        uploadedFile.Should().NotBeNull();
        localFile!.FileName.Should().Be(fileName);
    }

    [Test]
    public async Task ShouldValidateFileSize()
    {
        // Arrange
        var largeFileContent = new byte[5 * 1024 * 1024]; // 5MB file
        var stream = new MemoryStream(largeFileContent);

        var file = new FormFile(
            baseStream: stream,
            baseStreamOffset: 0,
            length: stream.Length,
            name: "file",
            fileName: "large-file.txt")
        {
            Headers = new HeaderDictionary(),
            ContentType = "text/plain"
        };

        var command = new UploadFilesCommand("79ab5fe5-80d0-4447-8ea9-06619749431c")
        {
            File = file
        };

        // Act & Assert
        await FluentActions
            .Invoking(() => SendAsync(command))
            .Should()
            .ThrowAsync<ValidationException>();
    }

    [Test]
    public async Task ShouldValidateFileType()
    {
        // Arrange
        var stream = new MemoryStream(new byte[] { 0x00 });
        var file = new FormFile(
            baseStream: stream,
            baseStreamOffset: 0,
            length: stream.Length,
            name: "file",
            fileName: "test.exe")
        {
            Headers = new HeaderDictionary(),
            ContentType = "application/x-msdownload"
        };

        var command = new UploadFilesCommand("79ab5fe5-80d0-4447-8ea9-06619749431c")
        {
            File = file
        };

        // Act & Assert
        await FluentActions
            .Invoking(() => SendAsync(command))
            .Should()
            .ThrowAsync<ValidationException>();
    }

    [Test]
    public async Task ShouldHandleEmptyFile()
    {
        // Arrange
        var stream = new MemoryStream();
        var file = new FormFile(
            baseStream: stream,
            baseStreamOffset: 0,
            length: 0,
            name: "file",
            fileName: "empty.txt")
        {
            Headers = new HeaderDictionary(),
            ContentType = "text/plain"
        };

        var command = new UploadFilesCommand("79ab5fe5-80d0-4447-8ea9-06619749431c")
        {
            File = file
        };

        // Act & Assert
        await FluentActions
            .Invoking(() => SendAsync(command))
            .Should()
            .ThrowAsync<ValidationException>();
    }
}
