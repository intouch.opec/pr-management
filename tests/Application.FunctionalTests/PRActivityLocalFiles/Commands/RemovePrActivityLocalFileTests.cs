namespace PRManagement.Application.FunctionalTests.PRActivityLocalFiles.Commands;

using PRManagement.Application.PRActivityLocalFiles.Commands.RemovePRActivityLocalFiles;
using PRManagement.Domain.Entities;
using ValidationException = PRManagement.Application.Common.Exceptions.ValidationException;
using static Testing;

public class RemovePRActivityLocalFilesCommandTests : BaseTestFixture
{
    [Test]
    public async Task ShouldRemovePRActivityLocalFile()
    {
        // Arrange
        var now = DateTimeOffset.UtcNow;
        var entity = new PRActivityLocalFile
        {
            Id = Guid.NewGuid(),
            PRActivityId = new Guid("79ab5fe5-80d0-4447-8ea9-06619749431c"),
            LocalFile = new LocalFile
            {
                Id = Guid.NewGuid(),
                FileName = "test1xxx.pdf", 
                Path = "/path/to/test1.pdf",
                Size = "1024",
                ContentType = "application/pdf"
            },
            Created = now.AddDays(-5),
            LastModified = now.AddDays(-3)
        };
        await AddAsync(entity);

        var command = new RemovePRActivityLocalFilesCommand(entity.Id.ToString());
        var result = await SendAsync(command);
        
        result.Should().NotBeNull();
    }

    [Test]
    public async Task ShouldThrowNotFoundException_WhenFileDoesNotExist()
    {
        var command = new RemovePRActivityLocalFilesCommand(Guid.NewGuid().ToString());

        await FluentActions
            .Invoking(() => SendAsync(command))
            .Should()
            .ThrowAsync<ValidationException>();
    }
}
