using PRManagement.Application.MediaSections.Commands.UpdateMediaSection;
using PRManagement.Domain.Enums;
using PRManagement.Application.MediaSections.Commands.CreateMediaSection;
using ValidationException = PRManagement.Application.Common.Exceptions.ValidationException;
using PRManagement.Domain.Exceptions;
using PRManagement.Application.Common.Models;

namespace PRManagement.Application.FunctionalTests.MediaSections.Commands;
using static Testing;

public class UpdateMediaSectionTests : BaseTestFixture
{
    [Test]
    public async Task ShouldRequireValidUpdateMediaSection()
    {
        var command = new UpdateMediaSectionCommand(new Guid("26c42685-c10b-4d85-9e7f-95f9087ff33f"));

        await FluentActions.Invoking(() =>
            SendAsync(command)).Should().ThrowAsync<ValidationException>();
    }

    [Test]
    public async Task ShouldUpdateMediaSection()
    {
        var mediaSectionId = await CreateMediaSectionAsync();
        var command = MockUpUpdateMediaSectionCommand(mediaSectionId);

        var updatedMediaSection = await SendAsync(command);

        updatedMediaSection.Should().NotBeNull();
        updatedMediaSection.Id.Should().Be(mediaSectionId);
        updatedMediaSection.SectionId.Should().Be(new Guid(command.SectionId));
        updatedMediaSection.SubSectionId.Should().Be(new Guid(command.SubSectionId));
        updatedMediaSection.TypeOfMedia.Should().Be(command.TypeOfMedia.ToEnum<TypeOfMedia>());
    }

    [Test]
    public async Task ShouldFailWhenUpdatingToDuplicatedMediaSection()
    {
        var firstMediaSectionId = await CreateFailWhenUpdatingToDuplicatedMediaSectionAsync();
        var secondMediaSectionId = await CreateFailWhenUpdatingToDuplicatedMediaSectionSecondAsync();

        var command = MockUpWhenUpdatingToDuplicatedMediaSection(firstMediaSectionId);
        await SendAsync(command);

        var duplicateCommand = MockUpWhenUpdatingToDuplicatedMediaSection(secondMediaSectionId);

        await FluentActions.Invoking(() =>
            SendAsync(duplicateCommand)).Should().ThrowAsync<ValidationException>();
    }

    [Test]
    public async Task ShouldUpdateMediaSectionWhenAddressNull()
    {
        var mediaSectionId = await CreateMediaSectionAddressNullAsync();
        var command = MockUpUpdateMediaSectionAddressNullCommand(mediaSectionId);

        var updatedMediaSection = await SendAsync(command);

        updatedMediaSection.Should().NotBeNull();
        updatedMediaSection.Id.Should().Be(mediaSectionId);
        updatedMediaSection.SectionId.Should().Be(new Guid(command.SectionId));
        updatedMediaSection.SubSectionId.Should().Be(new Guid(command.SubSectionId));
        updatedMediaSection.TypeOfMedia.Should().Be(command.TypeOfMedia.ToEnum<TypeOfMedia>());
        foreach (var email in command.Email)
        {
            var resultEmail = updatedMediaSection.Email!.FirstOrDefault(x => x == email);
            email.Should().Be(resultEmail);
        }
        foreach (var phoneNumber in command.PhoneNumbers)
        {
            var resultPhoneNumber = updatedMediaSection.PhoneNumbers!.FirstOrDefault(x => x == phoneNumber);
            phoneNumber.Should().Be(resultPhoneNumber);
        }
    }

    internal UpdateMediaSectionCommand MockUpUpdateMediaSectionCommand(Guid mediaSectionId)
    {
        return new UpdateMediaSectionCommand(mediaSectionId)
        {
            MediaId = "01926ff9-4fe9-73fa-9e1d-f8364c7dbf7d",
            TypeOfMedia = TypeOfMedia.DIGITAL.ToString(),
            SectionId = "26c42685-c10b-4d85-9e7f-95f9087ff33f",
            SubSectionId = "8f9b8253-44da-4693-93a5-cb9770d63844",
            Email = new[] { "aa2@email.com" },
            PhoneNumbers = new[] { "0912041234" }
        };
    }

    internal UpdateMediaSectionCommand MockUpUpdateMediaSectionAddressNullCommand(Guid mediaSectionId)
    {
        return new UpdateMediaSectionCommand(mediaSectionId)
        {
            MediaId = "b7721fa7-966a-4c85-913e-578f9b76c651",
            TypeOfMedia = TypeOfMedia.DIGITAL.ToString(),
            SectionId = "56d22a41-6b16-4335-9ba2-dfeb63c5a830",
            SubSectionId = "90a6e746-a379-4603-b39c-c10409314445",
            Email = new[] { "23a2@email.com" },
            PhoneNumbers = new[] { "0912121224" }
        };
    }

    internal UpdateMediaSectionCommand MockUpWhenUpdatingToDuplicatedMediaSection(Guid mediaSectionId)
    {
        return new UpdateMediaSectionCommand(mediaSectionId)
        {
            MediaId = "d647cf67-d115-49f0-ab17-9c17d83a210b",
            TypeOfMedia = TypeOfMedia.DIGITAL.ToString(),
            SectionId = "26c42685-c10b-4d85-9e7f-95f9087ff33f",
            SubSectionId = "8f9b8253-44da-4693-93a5-cb9770d63844",
            Email = new[] { "aa2@email.com" },
            PhoneNumbers = new[] { "0912041234" }
        };
    }

    internal async Task<Guid> CreateMediaSectionAsync()
    {
        var platforms = new List<PlatformDto>()
        {
        new PlatformDto()
        {
            Name = "PR Company Platform 2",
            Link = "https://www.pr-platform.com",
            PlatformType = PlatformType.FACEBOOK.ToString(),
        },
        new PlatformDto()
        {
            Name = "PR Company 2",
            Link = "https://www.pr-platform.com",
            PlatformType = PlatformType.X.ToString(),
        }
        };

        var createCommand = new CreateMediaSectionCommand
        {
            MediaId = "1e967671-6a4f-4148-94f6-06d1d2052932",
            TypeOfMedia = TypeOfMedia.MAINSTREAM.ToString(),
            Platforms = platforms,
            SectionId = "26c42685-c10b-4d85-9e7f-95f9087ff33f",
            SubSectionId = "8f9b8253-44da-4693-93a5-cb9770d63844",
            PhoneNumbers = new[] { "0614341234" },
            Email = new[] { "aa1@email.com" }
        };

        var mediaSection = await SendAsync(createCommand);

        return mediaSection.MediaSectionId;
    }

    internal async Task<Guid> CreateMediaSectionAddressNullAsync()
    {
        var platforms = new List<PlatformDto>()
        {
        new PlatformDto()
        {
            Name = "PR Company Platform 2",
            Link = "https://www.pr-platform.com",
            PlatformType = PlatformType.FACEBOOK.ToString(),
        },
        new PlatformDto()
        {
            Name = "PR Company 2",
            Link = "https://www.pr-platform.com",
            PlatformType = PlatformType.X.ToString(),
        }
        };

        var createCommand = new CreateMediaSectionCommand
        {
            MediaId = "b7721fa7-966a-4c85-913e-578f9b76c651",
            TypeOfMedia = TypeOfMedia.MAINSTREAM.ToString(),
            Platforms = platforms,
            SectionId = "26c42685-c10b-4d85-9e7f-95f9087ff33f",
            SubSectionId = "8f9b8253-44da-4693-93a5-cb9770d63844"
        };

        var mediaSection = await SendAsync(createCommand);

        return mediaSection.MediaSectionId;
    }

    internal async Task<Guid> CreateFailWhenUpdatingToDuplicatedMediaSectionAsync()
    {
        var platforms = new List<PlatformDto>()
        {
        new PlatformDto()
        {
            Name = "PR Company Platform 2",
            Link = "https://www.pr-platform.com",
            PlatformType = PlatformType.FACEBOOK.ToString(),
        },
        new PlatformDto()
        {
            Name = "PR Company 2",
            Link = "https://www.pr-platform.com",
            PlatformType = PlatformType.X.ToString(),
        }
        };

        var createCommand = new CreateMediaSectionCommand
        {
            MediaId = "d647cf67-d115-49f0-ab17-9c17d83a210b",
            TypeOfMedia = TypeOfMedia.MAINSTREAM.ToString(),
            Platforms = platforms,
            SectionId = "26c42685-c10b-4d85-9e7f-95f9087ff33f",
            SubSectionId = "8f9b8253-44da-4693-93a5-cb9770d63844",
            PhoneNumbers = new[] { "0644341554" },
            Email = new[] { "aa1_1@email.com" }
        };

        var mediaSection = await SendAsync(createCommand);

        return mediaSection.MediaSectionId;
    }

    internal async Task<Guid> CreateFailWhenUpdatingToDuplicatedMediaSectionSecondAsync()
    {
        var platforms = new List<PlatformDto>()
        {
        new PlatformDto()
        {
            Name = "PR Company Platform 2",
            Link = "https://www.pr-platform.com",
            PlatformType = PlatformType.FACEBOOK.ToString(),
        },
        new PlatformDto()
        {
            Name = "PR Company 2",
            Link = "https://www.pr-platform.com",
            PlatformType = PlatformType.X.ToString(),
        }
        };

        var createCommand = new CreateMediaSectionCommand
        {
            MediaId = "c7bb5200-72a2-4fa7-bd7e-ffd302eef6c6",
            TypeOfMedia = TypeOfMedia.MAINSTREAM.ToString(),
            Platforms = platforms,
            SectionId = "26c42685-c10b-4d85-9e7f-95f9087ff33f",
            SubSectionId = "8f9b8253-44da-4693-93a5-cb9770d63844",
            PhoneNumbers = new[] { "0644341544" },
            Email = new[] { "aa1_10@email.com" }
        };

        var mediaSection = await SendAsync(createCommand);

        return mediaSection.MediaSectionId;
    }
}
