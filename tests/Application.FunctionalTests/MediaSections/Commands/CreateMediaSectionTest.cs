using PRManagement.Application.MediaSections.Commands.CreateMediaSection;
using PRManagement.Application.Common.Models;
using PRManagement.Domain.Enums;
using PRManagement.Domain.Exceptions;
using ValidationException = PRManagement.Application.Common.Exceptions.ValidationException;

namespace PRManagement.Application.FunctionalTests.Medias.Commands;
using static Testing;
public class CreateMediaSectionTests : BaseTestFixture
{

    [Test]
    public async Task ShouldRequireValidMediaSection()
    {
        var command = new CreateMediaSectionCommand();

        await FluentActions.Invoking(() =>
           SendAsync(command)).Should().ThrowAsync<ValidationException>();
    }

    [Test]
    public async Task ShouldCreateMediaSection()
    {
        var command = MockUpMediaCommand();

        var mediaSection = await SendAsync(command);

        mediaSection.Should().NotBeNull();
        mediaSection.Id.Should().NotBeEmpty();
        mediaSection.SectionId.Should().Be(new Guid(command.SectionId));
        mediaSection.SubSectionId.Should().Be(new Guid(command.SubSectionId));
        mediaSection.TypeOfMedia.Should().Be(command.TypeOfMedia.ToEnum<TypeOfMedia>());
    }

    [Test]
    public async Task ShouldCreateMediaCommandPlatformsDuplicatedType()
    {
        var command = MockUpMediaCommandPlatformsDuplicatedType();

        try
        {
            await SendAsync(command);
        }
        catch (ValidationException validateResult)
        {
            var errors = validateResult.Errors;
            var error = errors.First();
            error.Value[0].Should().Be("The 'Platform Type' is already exits.");
        }
    }

    [Test]
    public async Task ShouldBeFailWhenDuplicateSectionSubSectionMediaTypeAndMediaId()
    {
        var command = MockUpMediaCommandDuplicated();
        await SendAsync(command);
        var commandDup = MockUpMediaCommandDuplicated();
        try
        {
            await SendAsync(commandDup);
        }
        catch (ValidationException validateResult)
        {
            var errors = validateResult.Errors;
            var error = errors.First();
            error.Value[0].Should().Be("The MediaId , SectionId And SubSectionId combination is duplicated.");
        }
    }

    internal CreateMediaSectionCommand MockUpMediaCommand()
    {
        var platforms = new List<PlatformDto>()
        {
            new PlatformDto()
            {
                Name = "MockUpMediaCommand",
                Link = "https://www.pr-platform.com",
                PlatformType = PlatformType.RADIO.ToString(),
            }
        };

        return new CreateMediaSectionCommand
        {
            MediaId = "2541e16a-7fa1-4e81-bdc9-617e37d47750",
            TypeOfMedia = TypeOfMedia.MAINSTREAM.ToString(),
            Platforms = platforms,
            SectionId = "26c42685-c10b-4d85-9e7f-95f9087ff33f",
            SubSectionId = "8f9b8253-44da-4693-93a5-cb9770d63844",
            PhoneNumbers = new[] { "0912341234" },
            Email = new[] { "aaa@email.com" }
        };
    }

    internal CreateMediaSectionCommand MockUpMediaCommandDuplicated()
    {
        var platforms = new List<PlatformDto>()
        {
            new PlatformDto()
            {
                Name = "PR Company Platform 2",
                Link = "https://www.pr-platform.com",
                PlatformType = PlatformType.RADIO.ToString(),
            }
        };

        return new CreateMediaSectionCommand
        {
            MediaId = "2541e16a-7fa1-4e81-bdc9-617e37d47750",
            TypeOfMedia = TypeOfMedia.MAINSTREAM.ToString(),
            Platforms = platforms,
            SectionId = "26c42685-c10b-4d85-9e7f-95f9087ff33f",
            SubSectionId = "90a6e746-a379-4603-b39c-c10409314445"
        };
    }

    internal CreateMediaSectionCommand MockUpMediaCommandPlatformsDuplicatedType()
    {
        var platforms = new List<PlatformDto>()
        {
            new PlatformDto()
            {
                Name = "PR Company Platform 2",
                Link = "https://www.pr-platform.com",
                PlatformType = PlatformType.FACEBOOK.ToString(),
            },
            new PlatformDto()
            {
                Name = "PR Company 2",
                Link = "https://www.pr-platform.com",
                PlatformType = PlatformType.FACEBOOK.ToString(),
            }
        };

        return new CreateMediaSectionCommand
        {
            MediaId = "2541e16a-7fa1-4e81-bdc9-617e37d47750",
            TypeOfMedia = TypeOfMedia.DIGITAL.ToString(),
            Platforms = platforms,
            SectionId = "26c42685-c10b-4d85-9e7f-95f9087ff33f",
            SubSectionId = "90a6e746-a379-4603-b39c-c10409314445"
        };
    }

}