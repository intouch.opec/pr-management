using PRManagement.Application.Common.Models;
using PRManagement.Application.Medias.Commands.CreateMedia;
using PRManagement.Application.MediaSections.Queries.GetMediaById;
using PRManagement.Domain.Enums;
using PRManagement.Domain.Exceptions;

namespace PRManagement.Application.FunctionalTests.MediaSections.Queries;
using static Testing;
public class GetMediaSectionByIdTests : BaseTestFixture
{
  [Test]
  public async Task ShouldBeNotFound()
  {
    var query = new GetMediaSectionByIdQuery(Guid.NewGuid());
    var action = () => SendAsync(query);
    await action.Should().ThrowAsync<NotFoundException>();
  }

  [Test]
  public async Task ShouldReturnMediaSection()
  {
    var mockUpCommand = MockUpCreateMediaCommand();
    var resultMockUp = await SendAsync(mockUpCommand);
    var query = new GetMediaSectionByIdQuery(resultMockUp.MediaSectionId);

    var result = await SendAsync(query);
    result.Should().NotBeNull();
    result.Id.Should().Be(resultMockUp.MediaSectionId);
    result.Name.Should().Be(mockUpCommand.Name);
    result.TypeOfMedia.Should().Be(mockUpCommand.TypeOfMedia.ToEnum<TypeOfMedia>());
    result.SectionId.Should().Be(new Guid(mockUpCommand.SectionId));
    result.SubSectionId.Should().Be(new Guid(mockUpCommand.SubSectionId));

    result.Platforms.Count().Should().Be(mockUpCommand.Platforms!.Count());

    foreach (var item in result.Platforms)
    {
      var platform = mockUpCommand.Platforms!.FirstOrDefault(x => x.Name == item.Name);
      platform.Should().NotBeNull();
      platform!.Name.Should().Be(item.Name);
      platform.Link.Should().Be(item.Link);
      platform.PlatformType.Should().Be(item.PlatformType);
    }
  }

  internal CreateMediaCommand MockUpCreateMediaCommand()
  {
    var platforms = new List<PlatformDto>()
    {
      new PlatformDto()
      {
        Name = "NEWSPAPER today",
        Link = "https://www.pr-platform.com",
        PlatformType = PlatformType.NEWSPAPER.ToString(),
      },
      new PlatformDto()
      {
        Name = "MAGAZINE book",
        Link = "https://www.pr-platform.com",
        PlatformType = PlatformType.MAGAZINE.ToString(),
      }
    };

    return new CreateMediaCommand
    {
      Name = "MediaSectionById",
      Address = "Address",
      Email = new string[] { "MediaSectionById@email.com" },
      PhoneNumbers = new string[] { "0921034567" },
      ProvinceId = "1",
      DistrictId = "1001",
      SubDistrictId = "100101",
      PostCode = "10200",
      TypeOfMedia = TypeOfMedia.MAINSTREAM.ToString(),
      Platforms = platforms,
      SectionId = "56d22a41-6b16-4335-9ba2-dfeb63c5a830",
      SubSectionId = "90a6e746-a379-4603-b39c-c10409314445"
    };
  }

}