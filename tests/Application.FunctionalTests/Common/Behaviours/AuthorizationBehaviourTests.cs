using PRManagement.Application.Common.Behaviours;
using PRManagement.Application.Common.Security;
using PRManagement.Application.Common.Interfaces;
using MediatR;
using PRManagement.Application.Common.Exceptions;

namespace PRManagement.Application.FunctionalTests.Common.Behaviours;

public class AuthorizationBehaviourTests : BaseTestFixture
{
    private Mock<IUser> _user = null!;
    private Mock<IIdentityService> _identityService = null!;
    private AuthorizationBehaviour<TestRequest, TestResponse> _behaviour = null!;

    [SetUp]
    public void Setup()
    {
        _user = new Mock<IUser>();
        _identityService = new Mock<IIdentityService>();
        _behaviour = new AuthorizationBehaviour<TestRequest, TestResponse>(_user.Object, _identityService.Object);
    }

    private class TestRequest { }
    private class TestResponse { }

    [Test]
    public async Task Handle_WhenRequestHasNoAuthorizeAttribute_ShouldProceed()
    {
        // Arrange
        var request = new TestRequest();
        var response = new TestResponse();
        RequestHandlerDelegate<TestResponse> next = () => Task.FromResult(response);

        // Act
        var result = await _behaviour.Handle(request, next, CancellationToken.None);

        // Assert
        Assert.That(result, Is.EqualTo(response));
    }

    [Test]
    public void Handle_WhenUserNotAuthenticated_ShouldThrowUnauthorizedAccess()
    {
        // Arrange
        var request = new AuthorizedRequest();
        _user.Setup(x => x.Id).Returns(string.Empty);
        RequestHandlerDelegate<TestResponse> next = () => Task.FromResult(new TestResponse());

        // Act & Assert
        Assert.ThrowsAsync<UnauthorizedAccessException>(async () =>
            await _behaviour.Handle(request, next, CancellationToken.None));
    }

    [Test]
    public async Task Handle_WhenUserHasRequiredRole_ShouldProceed()
    {
        // Arrange
        var request = new AuthorizedRequest();
        var response = new TestResponse();
        _user.Setup(x => x.Id).Returns("user-1");
        _identityService.Setup(x => x.IsInRoleAsync("user-1", "TestRole"))
            .ReturnsAsync(true);
        RequestHandlerDelegate<TestResponse> next = () => Task.FromResult(response);

        // Act
        var result = await _behaviour.Handle(request, next, CancellationToken.None);

        // Assert
        Assert.That(result, Is.EqualTo(response));
    }

    [Test]
    public void Handle_WhenUserLacksRequiredRole_ShouldThrowForbiddenAccess()
    {
        // Arrange
        var request = new AuthorizedRequest();
        _user.Setup(x => x.Id).Returns("user-1");
        _identityService.Setup(x => x.IsInRoleAsync("user-1", "TestRole"))
            .ReturnsAsync(false);
        RequestHandlerDelegate<TestResponse> next = () => Task.FromResult(new TestResponse());

        // Act & Assert
        Assert.ThrowsAsync<ForbiddenAccessException>(async () =>
            await _behaviour.Handle(request, next, CancellationToken.None));
    }

    [Authorize(Roles = "TestRole")]
    private class AuthorizedRequest : TestRequest { }
}