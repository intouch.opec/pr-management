using PRManagement.Application.SubDistricts.Queries.GetSubDistrictOptions;

namespace PRManagement.Application.FunctionalTests.SubDistricts.Queries;
using static Testing;
public class GetSubDistrictOptionTests : BaseTestFixture
{

  [Test]
  public async Task ShouldReturnSubDistrict()
  {
    var query = new GetSubDistrictOptionsQuery();
    var options = await SendAsync(query);

    options.Should().NotBeNullOrEmpty();
    foreach (var item in options)
    {
      item.Id.Should().NotBeNullOrEmpty();
      item.Name.Should().NotBeNullOrEmpty();
      item.NameEN.Should().NotBeNullOrEmpty();
      item.NameTH.Should().NotBeNullOrEmpty();
    }
  }
}