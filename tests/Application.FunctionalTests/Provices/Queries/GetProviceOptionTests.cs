using PRManagement.Application.Provinces.Queries.GetProvinceOptions;

namespace PRManagement.Application.FunctionalTests.Provinces.Queries;
using static Testing;
public class GetProvinceOptionTests : BaseTestFixture
{
  [Test]
  public async Task ShouldReturnProvince()
  {
    var query = new GetProvinceOptionsQuery();
    var options = await SendAsync(query);

    options.Should().NotBeNullOrEmpty();
    foreach (var item in options)
    {
      item.Id.Should().NotBeNullOrEmpty();
      item.Name.Should().NotBeNullOrEmpty();
      item.NameEN.Should().NotBeNullOrEmpty();
      item.NameTH.Should().NotBeNullOrEmpty();
    }
  }
}