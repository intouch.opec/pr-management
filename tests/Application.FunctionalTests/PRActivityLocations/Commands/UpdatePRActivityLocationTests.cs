namespace PRManagement.Application.FunctionalTests.PRActivityEmails.Commands;

using ValidationException = Application.Common.Exceptions.ValidationException;
using static Testing;
using PRManagement.Application.PRActivityLocations.Commands.UpdatePRActivityLocation;
using PRManagement.Domain.Entities;

public class UpdatePRActivityLocationTests : BaseTestFixture
{
    [Test]
    public async Task ShouldRequireValidUpdatePRActivityLocation()
    {
        var command = new UpdatePRActivityLocationCommand();

        await FluentActions.Invoking(() =>
           SendAsync(command)).Should().ThrowAsync<ValidationException>();
    }

    [Test]
    public async Task ShouldUpdatePRActivityLocation()
    {
        var command = new UpdatePRActivityLocationCommand()
        {
            Text = "Test Location",
            UrlMap = "http://test.com",
            ProvinceId = "1",
            DistrictId = "1001",
            SubDistrictId = "100101",
            PostCode = "10200",
            Remark = "Test Remark",
            PRActivityId = "79ab5fe5-80d0-4447-8ea9-06619749431c",
        };

        var result = await SendAsync(command);
        var prActivityLocation = await FindAsync<PRActivityLocation>(result.Id);
        prActivityLocation!.Text.Should().Be(command.Text);
        prActivityLocation!.PostCode.Should().Be(command.PostCode);
        prActivityLocation!.UrlMap.Should().Be(command.UrlMap);
        prActivityLocation!.ProvinceId.Should().Be(command.ProvinceId);
        prActivityLocation!.DistrictId.Should().Be(command.DistrictId);
        prActivityLocation!.SubDistrictId.Should().Be(command.SubDistrictId);
        prActivityLocation!.Remark.Should().Be(command.Remark);
    }
}
