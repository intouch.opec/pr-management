namespace PRManagement.Application.FunctionalTests.PRActivityEmails.Commands;

using ValidationException = Application.Common.Exceptions.ValidationException;
using static Testing;
using PRManagement.Domain.Entities;
using PRManagement.Application.PRActivityLocations.Commands.CreatePRActivityLocation;

public class CreatePRActivityLocationTests : BaseTestFixture
{
    [Test]
    public async Task ShouldRequireValidCreatePRActivityLocation()
    {
        var command = new CreatePRActivityLocationCommand();

        await FluentActions.Invoking(() =>
           SendAsync(command)).Should().ThrowAsync<ValidationException>();
    }

    [Test]
    public async Task ShouldCreatePRActivityLocation()
    {
        var command = new CreatePRActivityLocationCommand
        {
            Text = "Test Location",
            UrlMap = "http://test.com",
            ProvinceId = "1",
            DistrictId = "1001",
            SubDistrictId = "100101",
            PostCode = "10200",
            Remark = "Test Remark",
            PRActivityId = "9bdcd9ec-a1f6-4a59-9c31-dbbe17bd3c35",
        };

        var result = await SendAsync(command);

        var prActivityLocation = await FindAsync<PRActivityLocation>(result.Id);
        prActivityLocation!.Text.Should().Be(command.Text);
        prActivityLocation!.PostCode.Should().Be(command.PostCode);
        prActivityLocation!.UrlMap.Should().Be(command.UrlMap);
        prActivityLocation!.ProvinceId.Should().Be(command.ProvinceId);
        prActivityLocation!.DistrictId.Should().Be(command.DistrictId);
        prActivityLocation!.SubDistrictId.Should().Be(command.SubDistrictId);
        prActivityLocation!.Remark.Should().Be(command.Remark);
    }
}
