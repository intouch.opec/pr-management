﻿using System.Data.Common;
using PRManagement.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using Respawn;
using Testcontainers.PostgreSql;
using Npgsql;
using DotNet.Testcontainers.Builders;
using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.FunctionalTests;

public class TestcontainersTestDatabase : ITestDatabase
{
    private readonly PostgreSqlContainer _container;
    private DbConnection _connection = null!;
    private string _connectionString = null!;
    private Respawner _respawner = null!;

    public TestcontainersTestDatabase()
    {
        _container = new PostgreSqlBuilder()
            .WithAutoRemove(true)
            .WithImage("postgres:16-alpine")
            .WithDatabase("db")
            .WithUsername("postgres")
            .WithPassword("postgres")
            .WithWaitStrategy(Wait.ForUnixContainer().UntilCommandIsCompleted("pg_isready"))
            .WithCleanUp(true)
            .Build();
    }

    public async Task InitialiseAsync()
    {
        await _container.StartAsync();
        _connectionString = _container.GetConnectionString();
        _connection = new NpgsqlConnection(_connectionString);

        var options = new DbContextOptionsBuilder<ApplicationDbContext>()
            .UseNpgsql(_connectionString)
            .EnableSensitiveDataLogging()
            .Options;

        var context = new ApplicationDbContext(options);
        await context.Database.MigrateAsync();

        await _connection.OpenAsync();

        _respawner = await Respawner.CreateAsync(_connection, new RespawnerOptions
        {
            DbAdapter = DbAdapter.Postgres,
            SchemasToInclude = new[] { "public" }
        });
        await SeedProvinces();
        await SeedDistricts();
        await SeedSubDistricts();
        await SeedSections(context);
        await SeedMedia(context);
        await SeedContact(context);
        await SeedMediaSection(context);
        await SeedPlatform(context);
        await SeedClient(context);
        await SeedUser(context);
        await SeedPRActivity(context);
        await SeedPRActivityBriefing(context);
        await SeedPRActivityEmail(context);
        await SeedPRActivityLocation(context);
        await SeedMonitorTaskWorker(context);
        await SeedPRActivityTask(context);
        await SeedPRActivityTaskComment(context);
        await SeedPRActivityLocalFiles(context);
        await SeedPRActivityEventJoiners(context);
        await SeedPRActivityEventJoiners(context);
        await SeedPRActivityEvents(context);
        
        // await SeedNew(context);

    }

    public DbConnection GetConnection()
    {
        return _connection;
    }

    public async Task ResetAsync()
    {
        await _respawner.ResetAsync(_connectionString);
    }

    public async Task DisposeAsync()
    {
        await _connection.DisposeAsync();
        await _container.DisposeAsync();
    }

    private async Task SeedProvinces()
    {
        var conn = new NpgsqlConnection(_connectionString);
        await conn.OpenAsync();
        using var provinceWriter = await conn.BeginBinaryImportAsync("COPY public.\"Provinces\" FROM STDIN (FORMAT BINARY);");

        using var reader = new StreamReader("/Users/intouchteeramawanit/works/PRManagement/csv/thai_provinces.csv");
        while (!reader.EndOfStream)
        {

            var line = await reader.ReadLineAsync();
            var values = line!.Split(',');
            await provinceWriter.StartRowAsync();
            await provinceWriter.WriteAsync(values[0], NpgsqlTypes.NpgsqlDbType.Text);
            await provinceWriter.WriteAsync(values[1], NpgsqlTypes.NpgsqlDbType.Text);
            await provinceWriter.WriteAsync(values[2], NpgsqlTypes.NpgsqlDbType.Text);

        }
        await provinceWriter.CompleteAsync();
        await conn.CloseAsync();
    }

    private async Task SeedDistricts()
    {
        var conn = new NpgsqlConnection(_connectionString);
        await conn.OpenAsync();
        using var districtWriter = await conn.BeginBinaryImportAsync("COPY public.\"Districts\" FROM STDIN (FORMAT BINARY);");

        using var reader2 = new StreamReader("/Users/intouchteeramawanit/works/PRManagement/csv/thai_amphures.csv");
        while (!reader2.EndOfStream)
        {
            var line = await reader2.ReadLineAsync();
            var values = line!.Split(',');
            await districtWriter.StartRowAsync();
            await districtWriter.WriteAsync(values[0], NpgsqlTypes.NpgsqlDbType.Text);
            await districtWriter.WriteAsync(values[1], NpgsqlTypes.NpgsqlDbType.Text);
            await districtWriter.WriteAsync(values[2], NpgsqlTypes.NpgsqlDbType.Text);
            await districtWriter.WriteAsync(values[3], NpgsqlTypes.NpgsqlDbType.Text);
        }
        await districtWriter.CompleteAsync();
        await conn.CloseAsync();

    }

    private async Task SeedSubDistricts()
    {
        var conn = new NpgsqlConnection(_connectionString);
        await conn.OpenAsync();
        using var subDistrictWriter = await conn.BeginBinaryImportAsync("COPY public.\"SubDistricts\" FROM STDIN (FORMAT BINARY);");
        using var subDistrictsCsv = new StreamReader("/Users/intouchteeramawanit/works/PRManagement/csv/thai_tambons.csv");
        while (!subDistrictsCsv.EndOfStream)
        {
            var line = await subDistrictsCsv.ReadLineAsync();
            var values = line!.Split(',');
            await subDistrictWriter.StartRowAsync();
            await subDistrictWriter.WriteAsync(values[0], NpgsqlTypes.NpgsqlDbType.Text);
            await subDistrictWriter.WriteAsync(values[2], NpgsqlTypes.NpgsqlDbType.Text);
            await subDistrictWriter.WriteAsync(values[3], NpgsqlTypes.NpgsqlDbType.Text);
            await subDistrictWriter.WriteAsync(values[1], NpgsqlTypes.NpgsqlDbType.Text);
            await subDistrictWriter.WriteAsync(values[4], NpgsqlTypes.NpgsqlDbType.Text);
        }
        await subDistrictWriter.CompleteAsync();

        await conn.CloseAsync();
    }

    private async Task SeedSections(ApplicationDbContext context)
    {
        var sections = new List<Section>()
        {
            new Section()
            {
                Id = new Guid("56d22a41-6b16-4335-9ba2-dfeb63c5a830"),
                Name = "General",
                FormulaForCalculations = new List<FormulaForCalculation>() {
                    new FormulaForCalculation() {
                        PriceOfPurchaseRequisition = 300000,
                        Tier = 1,
                        MinimumFollowers = 100000
                    },
                    new FormulaForCalculation() {
                        PriceOfPurchaseRequisition = 25000,
                        Tier = 2,
                        MinimumFollowers = 80000
                    },
                    new FormulaForCalculation() {
                        PriceOfPurchaseRequisition = 20000,
                        Tier = 3,
                        MinimumFollowers = 10000
                    }
                }
            },
             new Section()
            {
                Id = new Guid("26c42685-c10b-4d85-9e7f-95f9087ff33f"),
                Name = "Sport",
            }
        };
        await context.AddRangeAsync(sections);

        var subSections = new List<SubSection>()
        {
            new SubSection() {
                Id = new Guid("90a6e746-a379-4603-b39c-c10409314445"),
                Name = "Sport",
                SectionId = new Guid("56d22a41-6b16-4335-9ba2-dfeb63c5a830"),
            },
            new SubSection() {
                Id = new Guid("8f9b8253-44da-4693-93a5-cb9770d63844"),
                Name = "Sport",
                SectionId = new Guid("56d22a41-6b16-4335-9ba2-dfeb63c5a830"),
            },
        };
        await context.AddRangeAsync(subSections);

        await context.SaveChangesAsync();
    }

    private async Task SeedMedia(ApplicationDbContext context)
    {
        var medias = new List<Media>()
        {
            new Media()
            {
                Id = new Guid("2541e16a-7fa1-4e81-bdc9-617e37d47750"),
                Name = "Media",
            },
            new Media()
            {
                Id = new Guid("01926ff9-4fe9-73fa-9e1d-f8364c7dbf7d"),
                Name = "Media_2",
            },
            new Media()
            {
                Id = new Guid("1e967671-6a4f-4148-94f6-06d1d2052932"),
                Name = "Media_3",
            },
            new Media()
            {
                Id = new Guid("d647cf67-d115-49f0-ab17-9c17d83a210b"),
                Name = "Media_4",
            },
            new Media()
            {
                Id = new Guid("c7bb5200-72a2-4fa7-bd7e-ffd302eef6c6"),
                Name = "Media_5",
            },
            new Media()
            {
                Id = new Guid("b7721fa7-966a-4c85-913e-578f9b76c651"),
                Name = "Media_6",
            },
            new Media()
            {
                Id = new Guid("d2518c07-f5fd-475b-99f3-e425041b8864"),
                Name = "instagram",
            }
        };
        await context.AddRangeAsync(medias);

        await context.SaveChangesAsync();
    }

    private async Task SeedMediaSection(ApplicationDbContext context)
    {
        var section = await context.Sections.FirstOrDefaultAsync(e => e.Id.Equals(new Guid("56d22a41-6b16-4335-9ba2-dfeb63c5a830")));
        var subSection = await context.SubSections.FirstOrDefaultAsync(e => e.Id.Equals(new Guid("90a6e746-a379-4603-b39c-c10409314445")));
        var media = await context.Medias.FirstOrDefaultAsync(e => e.Id.Equals(new Guid("2541e16a-7fa1-4e81-bdc9-617e37d47750")));
        var mediaIn = await context.Medias.FirstOrDefaultAsync(e => e.Id.Equals(new Guid("d2518c07-f5fd-475b-99f3-e425041b8864")));
        var mediaSections = new List<MediaSection>()
        {
            new MediaSection() {
                Id = new Guid("226f9c96-ca00-4c9b-a6cc-bfe788dbd015"),
                TypeOfMedia = TypeOfMedia.DIGITAL,
                MediaId = media!.Id,
                SectionId = section!.Id,
                SubSectionId = subSection!.Id,
                // Platforms  = new List<Platform>(),
                Media = media!,
                Section = section!,
                SubSection = subSection!,
            },
            new MediaSection() {
                Id = new Guid("7ec3b4ec-7256-4174-a8a2-5f336606d5d2"),
                TypeOfMedia = TypeOfMedia.DIGITAL,
                MediaId = media!.Id,
                SectionId = section!.Id,
                SubSectionId = subSection!.Id,
                // Platforms  = new List<Platform>(),
                Media = mediaIn!,
                Section = section!,
                SubSection = subSection!,
            }
        };

        await context.MediaSections.AddRangeAsync(mediaSections);
        await context.SaveChangesAsync();
    }

    private async Task SeedPlatform(ApplicationDbContext context)
    {
        var platforms = new List<Platform>()
        {
            new Platform()
            {
                Id = new Guid("b93b1b82-22a4-413c-a6d7-80187074d621"),
                Name = "Platform B",
                Link = "https://platform-b.com",
                PRValue = 800.30m,
                ADValue = 1000.20m,
                Potential = 50,
                TierOder = 2,
                TierPotential = 60,
                PlatFormType = PlatformType.FACEBOOK,
                MediaSectionId = new Guid("226f9c96-ca00-4c9b-a6cc-bfe788dbd015"),
            },
            new Platform()
            {
                Id = new Guid("5b5f8b31-caf6-4c08-b51e-81eebdf5738a"),
                Name = "Platform C",
                Link = "https://platform-c.com",
                PRValue = 800.30m,
                ADValue = 1000.20m,
                Potential = 50,
                TierOder = 2,
                TierPotential = 60,
                PlatFormType = PlatformType.FACEBOOK,
                MediaSectionId = new Guid("7ec3b4ec-7256-4174-a8a2-5f336606d5d2"),
            },
            new Platform()
            {
                Id = new Guid("ae891406-d5e2-4ef4-902e-6148acab7c79"),
                Name = "Platform D",
                Link = "https://platform-D.com",
                PRValue = 800.30m,
                ADValue = 1000.20m,
                Potential = 50,
                TierOder = 2,
                TierPotential = 60,
                PlatFormType = PlatformType.FACEBOOK,
                MediaSectionId = new Guid("226f9c96-ca00-4c9b-a6cc-bfe788dbd015"),
            }
        };

        await context.Platforms.AddRangeAsync(platforms);
        await context.SaveChangesAsync();
    }

    private async Task SeedClient(ApplicationDbContext context)
    {
        var clients = new List<Client>()
        {
            new Client() {
                Id = new Guid("5f8fb285-2fa0-4974-9a80-6a6d6e46a4ed"),
                Name = "clients Name",
                PercentageOfFullField = 100,
            },
            new Client() {
                Id = new Guid("e3f4d3dd-edca-4723-913e-d4a272ba4af9"),
                Name = "clients 2",
                PercentageOfFullField = 100,
            },

        };
        await context.AddRangeAsync(clients);

        await context.SaveChangesAsync();
    }

    private async Task SeedUser(ApplicationDbContext context)
    {
        context.Users.Add(new ApplicationUser
        {
            Id = "fbc382fe-5271-4ddd-bda8-4afdb839723d",
            UserName = "testuser",
            Email = "testuser@testuser.com",
            NickName = "testy",
        });

        context.Users.Add(new ApplicationUser
        {
            Id = "7a305e45-421b-4097-bc69-bc0d6823f4dd",
            UserName = "testuserX",
            Email = "xxx@x.com",
            NickName = "xxxy",
        });

        context.Users.Add(new ApplicationUser
        {
            Id = "ef9aa0bc-b545-4bb4-a3c2-9ed75ed32350",
            UserName = "remove_boy",
            Email = "remove_boy@remove_boy.com",
            NickName = "remove boy",
        });
        await context.SaveChangesAsync();
    }

    private async Task SeedPRActivity(ApplicationDbContext context)
    {
        await context.PRActivities.AddRangeAsync(
            new PRActivity()
            {
                Id = new Guid("9bdcd9ec-a1f6-4a59-9c31-dbbe17bd3c35"),
                Title = "PRActivities",
                DurationType = DurationPRActivityType.LONG,
                ContentPRActivityType = ContentPRActivityType.EXCLUSIVE_INTERVIEW,
                ContractType = ContractType.ADHOC,
                Started = DateTimeOffset.UtcNow.AddDays(-30),
                Ended = DateTimeOffset.UtcNow.AddDays(30),
                Released = DateTimeOffset.UtcNow.AddDays(32),
                ClientId = new Guid("5f8fb285-2fa0-4974-9a80-6a6d6e46a4ed"),
                SectionId = new Guid("56d22a41-6b16-4335-9ba2-dfeb63c5a830"),
                SubSectionId = new Guid("8f9b8253-44da-4693-93a5-cb9770d63844"),
                OwnerId = "fbc382fe-5271-4ddd-bda8-4afdb839723d",
                WorkProgress = new List<WorkProgress>()
                {
                    new ()
                    {
                        WorkTask = WorkTask.BRIEFING_MEETING,
                    },
                    new ()
                    {
                        WorkTask = WorkTask.CONFIRM_LOCATION,
                    },
                    new ()
                    {
                        WorkTask = WorkTask.SEND_EMAIL_CLIENT,
                    },
                    new ()
                    {
                        WorkTask = WorkTask.SIGN_EVENT,
                    },
                }
            },
            new PRActivity()
            {
                Id = new Guid("df8f5ede-634e-488f-b44a-5dd5a023fefa"),
                Title = "PRActivities2",
                DurationType = DurationPRActivityType.LONG,
                ContentPRActivityType = ContentPRActivityType.EXCLUSIVE_INTERVIEW,
                ContractType = ContractType.ADHOC,
                Started = DateTimeOffset.UtcNow.AddDays(-30),
                Ended = DateTimeOffset.UtcNow.AddDays(30),
                Released = DateTimeOffset.UtcNow.AddDays(32),
                ClientId = new Guid("5f8fb285-2fa0-4974-9a80-6a6d6e46a4ed"),
                SectionId = new Guid("56d22a41-6b16-4335-9ba2-dfeb63c5a830"),
                SubSectionId = new Guid("8f9b8253-44da-4693-93a5-cb9770d63844"),
                OwnerId = "fbc382fe-5271-4ddd-bda8-4afdb839723d",
                WorkProgress = new List<WorkProgress>()
                {
                    new ()
                    {
                        WorkTask = WorkTask.BRIEFING_MEETING,
                    },
                    new ()
                    {
                        WorkTask = WorkTask.CONFIRM_LOCATION,
                    },
                    new ()
                    {
                        WorkTask = WorkTask.SEND_EMAIL_CLIENT,
                    },
                    new ()
                    {
                        WorkTask = WorkTask.SIGN_EVENT,
                    },
                }
            },
            new PRActivity()
            {
                Id = new Guid("e638a958-cabf-448d-bfec-734c280abd41"),
                Title = "PRActivities3",
                DurationType = DurationPRActivityType.LONG,
                ContentPRActivityType = ContentPRActivityType.EXCLUSIVE_INTERVIEW,
                ContractType = ContractType.ADHOC,
                Started = DateTimeOffset.UtcNow.AddDays(-30),
                Ended = DateTimeOffset.UtcNow.AddDays(30),
                Released = DateTimeOffset.UtcNow.AddDays(32),
                ClientId = new Guid("5f8fb285-2fa0-4974-9a80-6a6d6e46a4ed"),
                SectionId = new Guid("56d22a41-6b16-4335-9ba2-dfeb63c5a830"),
                SubSectionId = new Guid("8f9b8253-44da-4693-93a5-cb9770d63844"),
                OwnerId = "fbc382fe-5271-4ddd-bda8-4afdb839723d",
                WorkProgress = new List<WorkProgress>()
                {
                    new ()
                    {
                        WorkTask = WorkTask.BRIEFING_MEETING,
                    },
                    new ()
                    {
                        WorkTask = WorkTask.CONFIRM_LOCATION,
                    },
                    new ()
                    {
                        WorkTask = WorkTask.SEND_EMAIL_CLIENT,
                    },
                    new ()
                    {
                        WorkTask = WorkTask.SIGN_EVENT,
                    },
                }
            },
            new PRActivity
            {
                Title = "Campaign A",
                DurationType = DurationPRActivityType.LONG,
                ContentPRActivityType = ContentPRActivityType.PRESS_RELEASE,
                ContractType = ContractType.RETAINER,
                Started = DateTimeOffset.UtcNow.AddDays(-20),
                Ended = DateTimeOffset.UtcNow.AddDays(10),
                Released = DateTimeOffset.UtcNow.AddDays(14),
                ClientId = new Guid("5f8fb285-2fa0-4974-9a80-6a6d6e46a4ed"),
                SectionId = new Guid("56d22a41-6b16-4335-9ba2-dfeb63c5a830"),
                SubSectionId = new Guid("8f9b8253-44da-4693-93a5-cb9770d63844"),
                OwnerId = "fbc382fe-5271-4ddd-bda8-4afdb839723d",
                WorkProgress = new List<WorkProgress>()
                {
                    new ()
                    {
                        WorkTask = WorkTask.BRIEFING_MEETING,
                    },
                    new ()
                    {
                        WorkTask = WorkTask.CONFIRM_LOCATION,
                    },
                    new ()
                    {
                        WorkTask = WorkTask.SEND_EMAIL_CLIENT,
                    },
                    new ()
                    {
                        WorkTask = WorkTask.SIGN_EVENT,
                    },
                }
            },
            new PRActivity
            {
                Id = new Guid("79ab5fe5-80d0-4447-8ea9-06619749431c"),
                Title = "Event B",
                DurationType = DurationPRActivityType.EVENT,
                ContentPRActivityType = ContentPRActivityType.BLOGGER_DAY,
                ContractType = ContractType.ADHOC,
                Started = DateTimeOffset.UtcNow.AddDays(-20),
                Ended = DateTimeOffset.UtcNow.AddDays(10),
                Released = DateTimeOffset.UtcNow.AddDays(14),
                ClientId = new Guid("5f8fb285-2fa0-4974-9a80-6a6d6e46a4ed"),
                SectionId = new Guid("56d22a41-6b16-4335-9ba2-dfeb63c5a830"),
                SubSectionId = new Guid("8f9b8253-44da-4693-93a5-cb9770d63844"),
                OwnerId = "fbc382fe-5271-4ddd-bda8-4afdb839723d",
                WorkProgress = new List<WorkProgress>()
                {
                    new ()
                    {
                        WorkTask = WorkTask.BRIEFING_MEETING,
                    },
                    new ()
                    {
                        WorkTask = WorkTask.CONFIRM_LOCATION,
                    },
                    new ()
                    {
                        WorkTask = WorkTask.SEND_EMAIL_CLIENT,
                    },
                    new ()
                    {
                        WorkTask = WorkTask.SIGN_EVENT,
                    },
                }
            },
            new PRActivity
            {
                Title = "Short News C",
                DurationType = DurationPRActivityType.SHORT,
                ContentPRActivityType = ContentPRActivityType.CAPTION_NEWS,
                ContractType = ContractType.RETAINER,
                Started = DateTimeOffset.UtcNow.AddDays(-20),
                Ended = DateTimeOffset.UtcNow.AddDays(10),
                Released = DateTimeOffset.UtcNow.AddDays(14),
                ClientId = new Guid("5f8fb285-2fa0-4974-9a80-6a6d6e46a4ed"),
                SectionId = new Guid("56d22a41-6b16-4335-9ba2-dfeb63c5a830"),
                SubSectionId = new Guid("8f9b8253-44da-4693-93a5-cb9770d63844"),
                OwnerId = "fbc382fe-5271-4ddd-bda8-4afdb839723d",
                WorkProgress = new List<WorkProgress>()
                {
                    new ()
                    {
                        WorkTask = WorkTask.BRIEFING_MEETING,
                    },
                    new ()
                    {
                        WorkTask = WorkTask.CONFIRM_LOCATION,
                    },
                    new ()
                    {
                        WorkTask = WorkTask.SEND_EMAIL_CLIENT,
                    },
                    new ()
                    {
                        WorkTask = WorkTask.SIGN_EVENT,
                    },
                }
            }
        );
        await context.SaveChangesAsync();
    }

    private async Task SeedPRActivityBriefing(ApplicationDbContext context)
    {
        await context.PRActivityBriefings.AddRangeAsync(
            new PRActivityBriefing()
            {
                Id = new Guid("c29c25ac-0785-43f2-af73-753d1d30460f"),
                Content = "xxxx",
                PRActivityId = new Guid("79ab5fe5-80d0-4447-8ea9-06619749431c")
            }
        );
        await context.SaveChangesAsync();
    }

    private async Task SeedPRActivityEmail(ApplicationDbContext context)
    {
        await context.PRActivityEmails.AddRangeAsync(
            new PRActivityEmail()
            {
                Id = new Guid("c29c25ac-0785-43f2-af73-753d1d30460f"),
                Subject = "Subject",
                Content = "Content",
                PRActivityId = new Guid("79ab5fe5-80d0-4447-8ea9-06619749431c")
            }
        );
        await context.SaveChangesAsync();
    }

    private async Task SeedPRActivityLocation(ApplicationDbContext context)
    {
        await context.PRActivityLocations.AddRangeAsync(
            new PRActivityLocation()
            {
                Id = new Guid("3447ce4b-1778-4a15-b486-f9cd5550922b"),
                Text = "Test Location",
                UrlMap = "http://test.com",
                ProvinceId = "1",
                DistrictId = "1001",
                SubDistrictId = "100101",
                PostCode = "10200",
                Remark = "Test Remark",
                PRActivityId = new Guid("79ab5fe5-80d0-4447-8ea9-06619749431c")
            }
        );
        await context.SaveChangesAsync();
    }

    private async Task SeedMonitorTaskWorker(ApplicationDbContext context)
    {
        var PRActivity = await context.PRActivities
            .FirstAsync(e => e.Title.Equals("PRActivities2"));

        var platform = await context.Platforms
            .FirstAsync(e => e.Id.Equals(new Guid("b93b1b82-22a4-413c-a6d7-80187074d621")));

        await context.MonitorTaskWorkers
            .AddRangeAsync(
                new MonitorTaskWorker()
                {
                    Id = new Guid("e2f12e32-6da9-4986-9a47-a8cb4ee1e3da"),
                    Topic = "Topic2",
                    KeywordQuery = "KeywordQuery",
                    TaskWorkerStatus = TaskWorkerStatus.PENDING,
                    IsUrgent = true,
                    PRActivityId = PRActivity.Id
                },
                new MonitorTaskWorker()
                {
                    Id = new Guid("c71572df-3373-43f0-a1b1-048cd957f8a0"),
                    Topic = "Topic3",
                    KeywordQuery = "KeywordQuery",
                    TaskWorkerStatus = TaskWorkerStatus.PENDING,
                    IsUrgent = true,
                    PRActivityId = PRActivity.Id,
                    News = new List<New>(){
                        new New()
                        {
                            Id = new Guid("07fdd805-9750-4a97-9986-e949423e49db"),
                            Title = "Title",
                            Owner = "OwnerTitle",
                            Link = "Link",
                            Description = "Description",
                            Mapping = MappingType.PENDING,
                            PlatformType = PlatformType.INSTAGRAM,
                            PlatformId = platform.Id,
                            // MonitorTaskWorkerId = monitorTaskWorker.Id
                        },
                        new New()
                        {
                            Id = new Guid("9042743d-b62f-44be-ac3a-b783efc5c562"),
                            Title = "Title2",
                            Owner = "OwnerTitle2",
                            Link = "Link",
                            Description = "Description",
                            Mapping = MappingType.PENDING,
                            PlatformType = PlatformType.INSTAGRAM,
                            PlatformId = platform.Id,
                            // MonitorTaskWorkerId = monitorTaskWorker.Id
                        }
                    }
                }
            );
        await context.SaveChangesAsync();
    }

    private async Task SeedContact(ApplicationDbContext context)
    {
        await context.Contacts.AddRangeAsync(
            new Contact()
            {
                Id = new Guid("547a3b25-ad3d-48bb-b4c5-c4d545ff25dc"),
                FirstName = "FirstName Jossshn",
                LastName = "LastName Doe",
                NickName = "Johnny",
                Position = "Developer",
                LineId = "john_doe123",
                Remark = "Remarkable employee",
                AddressType = AddressType.Home,
                ContactType = ContactType.CONTACT,
                Address = new()
                {
                    Email = new[] { "example1xxx@example.com", "example2xxx@example.com" },
                    PhoneNumbers = new[] { "123-456-7890", "098-765-4321" },
                    Text = "Sample text for mock entity",
                    ProvinceId = "1",
                    DistrictId = "1001",
                    SubDistrictId = "100101",
                    PostCode = "10200",
                }
            },
            new Contact()
            {
                Id = new Guid("d4f7da72-b34b-48ee-ae36-747c627727b1"),
                FirstName = "FirstName XX",
                LastName = "LastName XX",
                NickName = "Johnny",
                Position = "DevelopXer",
                LineId = "john_doe123X",
                Remark = "Remarkable employee",
                AddressType = AddressType.Home,
                ContactType = ContactType.CONTACT,
                Address = new()
                {
                    Email = new[] { "example1x12@example.com", "123123@example.com" },
                    PhoneNumbers = new[] { "123-4X6-1890", "098-765-1321" },
                    Text = "Sample text for mock entity",
                    ProvinceId = "1",
                    DistrictId = "1001",
                    SubDistrictId = "100101",
                    PostCode = "10200",
                }
            },
            new Contact()
            {
                Id = new Guid("069a760a-3bdc-4f53-bb0f-eb9dd0eb2975"),
                FirstName = "ssss XX",
                LastName = "ssss XX",
                NickName = "Johnny",
                Position = "DevelqweXer",
                LineId = "john_doeqweX",
                Remark = "Remarkable employee",
                AddressType = AddressType.Home,
                ContactType = ContactType.CONTACT,
                Address = new()
                {
                    Email = new[] { "examp33@example.com" },
                    PhoneNumbers = new[] { "123-4X6-1090" },
                    Text = "Sample text for mock entity",
                    ProvinceId = "1",
                    DistrictId = "1001",
                    SubDistrictId = "100101",
                    PostCode = "10200",
                }
            },
            new Contact()
            {
                Id = new Guid("6ab0b664-f158-4b0c-897f-8c737329ea14"),
                FirstName = "sxsss XX",
                LastName = "sssxs XdX",
                NickName = "Johxxnny",
                Position = "DevelqweXedr",
                LineId = "john_doeqweX",
                Remark = "Remarkablde emplodyee",
                AddressType = AddressType.Home,
                ContactType = ContactType.CONTACT,
                Address = new()
                {
                    Email = new[] { "examp33@example.com" },
                    PhoneNumbers = new[] { "123-4X6-1090" },
                    Text = "Sample text for mock entity",
                    ProvinceId = "1",
                    DistrictId = "1001",
                    SubDistrictId = "100101",
                    PostCode = "10200",
                }
            }
        );
        await context.SaveChangesAsync();
    }

    private async Task SeedPRActivityTask(ApplicationDbContext context)
    {
        await context.PRActivityTasks.AddRangeAsync(
            new PRActivityTask()
            {
                Id = new Guid("fe0fcc0a-70d9-4e03-962a-ace1e9ff5a3e"),
                TaskWorkerStatus = TaskWorkerStatus.PENDING,
                DueDate = DateTimeOffset.UtcNow.AddDays(10),
                Description = "Description",
                WorkTask = WorkTask.BRIEFING_MEETING,
                PRActivityId = new Guid("79ab5fe5-80d0-4447-8ea9-06619749431c"),
                PRActivityTaskResponsiblePersons = new List<PRActivityTaskResponsiblePerson>()
                {
                    new ()
                    {
                        OwnerId = "fbc382fe-5271-4ddd-bda8-4afdb839723d",
                        Id = new Guid("bea2702f-ef6d-4b0f-a2a4-934bedb1d20b")
                    }
                }
            },
            new PRActivityTask()
            {
                Id = new Guid("b6aa2739-2891-4ceb-adf8-be2e6600a6fb"),
                TaskWorkerStatus = TaskWorkerStatus.PENDING,
                DueDate = DateTimeOffset.UtcNow.AddDays(99),
                Description = "Description X",
                WorkTask = WorkTask.CONFIRM_LOCATION,
                PRActivityId = new Guid("79ab5fe5-80d0-4447-8ea9-06619749431c"),
                PRActivityTaskResponsiblePersons = new List<PRActivityTaskResponsiblePerson>()
                {
                    new ()
                    {
                        OwnerId = "fbc382fe-5271-4ddd-bda8-4afdb839723d",
                        Id = new Guid("acfa21ef-4fc1-4579-a148-708111b5f169")
                    },
                    new ()
                    {
                        OwnerId = "fbc382fe-5271-4ddd-bda8-4afdb839723d",
                        Id = new Guid("914426fa-a1ef-4c3e-aad0-d913cd75460b")
                    },
                }
            },
            new PRActivityTask()
            {
                Id = new Guid("ac36a9bc-a257-4d50-a7f5-b753a4e7ee18"),
                TaskWorkerStatus = TaskWorkerStatus.PENDING,
                DueDate = DateTimeOffset.UtcNow.AddDays(10),
                Description = "REMOVE",
                WorkTask = WorkTask.SEND_EMAIL_CLIENT,
                PRActivityId = new Guid("79ab5fe5-80d0-4447-8ea9-06619749431c"),
                PRActivityTaskResponsiblePersons = new List<PRActivityTaskResponsiblePerson>()
                {
                    new ()
                    {
                        OwnerId = "fbc382fe-5271-4ddd-bda8-4afdb839723d",
                        Id = new Guid("46020dea-a4e4-45db-b05f-58c62ff0784c")
                    }
                }
            },
            new PRActivityTask()
            {
                Id = new Guid("1e95414f-af62-4035-87a9-4e4db3f4ad32"),
                TaskWorkerStatus = TaskWorkerStatus.PENDING,
                DueDate = DateTimeOffset.UtcNow.AddDays(10),
                Description = "REMOVE_ZA",
                PRActivityId = new Guid("79ab5fe5-80d0-4447-8ea9-06619749431c"),
                PRActivityTaskResponsiblePersons = new List<PRActivityTaskResponsiblePerson>()
                {
                    new ()
                    {
                        OwnerId = "fbc382fe-5271-4ddd-bda8-4afdb839723d",
                        Id = new Guid("85c76dee-5e98-4fd9-9418-9fe6e1c63ebb")
                    }
                }
            }
        );
        await context.SaveChangesAsync();
    }

    private async Task SeedPRActivityTaskComment(ApplicationDbContext context)
    {
        await context.PRActivityTaskComments.AddRangeAsync(
            new PRActivityTaskComment()
            {
                Id = new Guid("09a0d61c-2665-40ec-8cdf-660180e7dac0"),
                Content = "Content",
                PRActivityTaskId = new Guid("fe0fcc0a-70d9-4e03-962a-ace1e9ff5a3e"),
            },
            new PRActivityTaskComment()
            {
                Id = new Guid("7be3a854-8fa5-4f12-bb81-03a14d7fa296"),
                Content = "REmove",
                PRActivityTaskId = new Guid("fe0fcc0a-70d9-4e03-962a-ace1e9ff5a3e"),
            },
            new PRActivityTaskComment()
            {
                Id = new Guid("8dbf84b9-2869-45e9-8f4b-30ab8c13628c"),
                Content = "REmove",
                PRActivityTaskId = new Guid("1e95414f-af62-4035-87a9-4e4db3f4ad32"),
            },
              new PRActivityTaskComment()
              {
                  Id = new Guid("78c26f90-e125-4383-bf39-1f730c83f01a"),
                  Content = "Before Updated",
                  PRActivityTaskId = new Guid("fe0fcc0a-70d9-4e03-962a-ace1e9ff5a3e"),
              }
        );
        await context.SaveChangesAsync();
    }

    private async Task SeedPRActivityLocalFiles(ApplicationDbContext context)
    {
        var now = DateTimeOffset.UtcNow;

        await context.PRActivityLocalFiles.AddRangeAsync(
            new PRActivityLocalFile
            {
                Id = new Guid("543f43e4-734b-4c40-839b-942c5bf05e2e"),
                PRActivityId = new Guid("79ab5fe5-80d0-4447-8ea9-06619749431c"),
                LocalFile = new LocalFile
                {
                    Id = Guid.NewGuid(),
                    FileName = "test1.pdf", 
                    Path = "path/to/test1.pdf",
                    Size = "1024",
                    ContentType = "application/pdf"
                },
                Created = now.AddDays(-5),
                LastModified = now.AddDays(-3)
            },
            new PRActivityLocalFile
            {
                Id = Guid.NewGuid(),
                PRActivityId = new Guid("79ab5fe5-80d0-4447-8ea9-06619749431c"),
                LocalFile = new LocalFile
                {
                    Id = Guid.NewGuid(),
                    FileName = "document.docx",
                    Path = "/path/to/document.docx", 
                    Size = "2048",
                    ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                },
                Created = now.AddDays(-2),
                LastModified = now.AddDays(-1)
            },
            new PRActivityLocalFile
            {
                Id = Guid.NewGuid(),
                PRActivityId = new Guid("79ab5fe5-80d0-4447-8ea9-06619749431c"),
                LocalFile = new LocalFile
                {
                    Id = Guid.NewGuid(),
                    FileName = "image.jpg",
                    Path = "/path/to/image.jpg",
                    Size = "3072",
                    ContentType = "image/jpeg"
                },
                Created = now.AddDays(-10),
                LastModified = now.AddDays(-8)
            },
            new PRActivityLocalFile
            {
                Id = Guid.NewGuid(),
                PRActivityId = new Guid("79ab5fe5-80d0-4447-8ea9-06619749431c"),
                LocalFile = new LocalFile
                {
                    Id = Guid.NewGuid(),
                    FileName = "test2.pdf",
                    Path = "/path/to/test2.pdf",
                    Size = "4096",
                    ContentType = "application/pdf"
                },
                Created = now.AddDays(-1),
                LastModified = now
            }
        );

        await context.SaveChangesAsync();
    }

    private async Task SeedPRActivityEventJoiners(ApplicationDbContext context)
    {        
        await context.PRActivityEventJoiners.AddRangeAsync(new PRActivityEventJoiner
        {
            ContactId = new Guid("547a3b25-ad3d-48bb-b4c5-c4d545ff25dc"),
            PRActivityId = new Guid("9bdcd9ec-a1f6-4a59-9c31-dbbe17bd3c35"),
            JoinType = JoinType.WalkIn
        },
        new PRActivityEventJoiner
        {
            ContactId = new Guid("d4f7da72-b34b-48ee-ae36-747c627727b1"),
            PRActivityId = new Guid("9bdcd9ec-a1f6-4a59-9c31-dbbe17bd3c35"),
            JoinType = JoinType.WalkIn
        });

        await context.SaveChangesAsync();
    }

    private async Task SeedPRActivityEvents(ApplicationDbContext context)
    {
        await context.PRActivityEvents.AddRangeAsync(
            new PRActivityEvent()
            {
                Id = new Guid("6a8fc324-289a-45dc-a7af-ab623fd38c03"),
                EventCode = "EventCode",
                NumberOfJoiner = 0,
                EventDate = DateTimeOffset.UtcNow,
                PRActivityId = new Guid("9bdcd9ec-a1f6-4a59-9c31-dbbe17bd3c35"),
            }
        );
        await context.SaveChangesAsync();
    }
}
