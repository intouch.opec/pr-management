using PRManagement.Application.Sections.Commands.CreateSection;
using PRManagement.Domain.Entities;
using ValidationException = PRManagement.Application.Common.Exceptions.ValidationException;

namespace PRManagement.Application.FunctionalTests.Sections.Commands;
using static Testing;

public class CreateSectionTests : BaseTestFixture
{
    [Test]
    public async Task ShouldRequireMinimumFields()
    {
        var command = new CreateSectionCommand();

        await FluentActions.Invoking(() =>
            SendAsync(command))
                .Should()
                .ThrowAsync<ValidationException>();
    }

    [Test]
    public async Task ShouldCreateSection()
    {
        var command = new CreateSectionCommand
        {
            Name = "New Section",
            FormulaForCalculations = new List<FormulaForCalculation>
            {
                new FormulaForCalculation { Tier = 1, PriceOfPurchaseRequisition = 10000, MinimumFollowers = 100000 },
                new FormulaForCalculation { Tier = 2, PriceOfPurchaseRequisition = 5000, MinimumFollowers = 10000 },
                new FormulaForCalculation { Tier = 3, PriceOfPurchaseRequisition = 1000, MinimumFollowers = 1000 }
            }
        };

        var section = await SendAsync(command);

        var item = await FindAsync<Section>(section.Id);
        item.Should().NotBeNull();
        item!.Id.Should().Be(section.Id);
        item!.Name.Should().Be(command.Name);

        var formulaForCalculations = section.FormulaForCalculations.ToList();

        foreach (var formulaForCalculation in command.FormulaForCalculations)
        {
            var found = formulaForCalculations.Find(e => e.Tier == formulaForCalculation.Tier);
            found.Should().NotBeNull();
            found!.Tier.Should().Be(formulaForCalculation.Tier);
            found!.MinimumFollowers.Should().Be(formulaForCalculation.MinimumFollowers);
            found!.PriceOfPurchaseRequisition.Should().Be(formulaForCalculation.PriceOfPurchaseRequisition);
        }
    }

    [Test]
    public async Task ShouldHaveErrorWhenNameIsDuplicate()
    {
        var name = "Duplicate";
        var command = new CreateSectionCommand
        {
            Name = name,
            FormulaForCalculations = new List<FormulaForCalculation>
            {
                new FormulaForCalculation { Tier = 1, PriceOfPurchaseRequisition = 10000, MinimumFollowers = 100000 },
                new FormulaForCalculation { Tier = 2, PriceOfPurchaseRequisition = 5000, MinimumFollowers = 10000 },
                new FormulaForCalculation { Tier = 3, PriceOfPurchaseRequisition = 1000, MinimumFollowers = 1000 }
            }
        };

        await SendAsync(command);

        await FluentActions.Invoking(() =>
            SendAsync(new CreateSectionCommand
            {
                Name = name,
                FormulaForCalculations = []
            }))
            .Should()
            .ThrowAsync<ValidationException>();
    }
}
