using PRManagement.Application.Common.Models;
using PRManagement.Application.MediaSections.Commands.CreateMediaSection;
using PRManagement.Application.Sections.Commands.CreateSection;
using PRManagement.Application.Sections.Commands.UpdateSection;
using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;
using ValidationException = PRManagement.Application.Common.Exceptions.ValidationException;

namespace PRManagement.Application.FunctionalTests.Sections.Commands;
using static Testing;

public class UpdateSectionTests : BaseTestFixture
{
    [Test]
    public async Task ShouldRequireMinimumFields()
    {
        var command = new UpdateSectionCommand();

        await FluentActions.Invoking(() =>
            SendAsync(command))
                .Should()
                .ThrowAsync<ValidationException>();
    }

    [Test]
    public async Task ShouldUpdateSection()
    {
        var command = new CreateSectionCommand
        {
            Name = "New Section Update",
            FormulaForCalculations = new List<FormulaForCalculation>
            {
                new FormulaForCalculation { Tier = 1, PriceOfPurchaseRequisition = 10000, MinimumFollowers = 100000 },
                new FormulaForCalculation { Tier = 2, PriceOfPurchaseRequisition = 5000, MinimumFollowers = 10000 },
                new FormulaForCalculation { Tier = 3, PriceOfPurchaseRequisition = 1000, MinimumFollowers = 1000 }
            }
        };

        var section = await SendAsync(command);

        var updateSectionCommand = new UpdateSectionCommand
        {
            Id = section.Id.ToString(),
            Name = "Section Updated",
            FormulaForCalculations = new List<FormulaForCalculation>
            {
                new FormulaForCalculation { Tier = 1, PriceOfPurchaseRequisition = 5000, MinimumFollowers = 100000 },
                new FormulaForCalculation { Tier = 2, PriceOfPurchaseRequisition = 2500, MinimumFollowers = 10000 },
                new FormulaForCalculation { Tier = 3, PriceOfPurchaseRequisition = 500, MinimumFollowers = 1000 },
                new FormulaForCalculation { Tier = 4, PriceOfPurchaseRequisition = 250, MinimumFollowers = 500 }
            }
        };

        var result = await SendAsync(updateSectionCommand);

        var item = await FindAsync<Section>(result.Id);
        item.Should().NotBeNull();
        item!.Id.Should().Be(section.Id);
        item!.Name.Should().Be(updateSectionCommand.Name);

        var formulaForCalculations = item.FormulaForCalculations.ToList();

        foreach (var formulaForCalculation in updateSectionCommand.FormulaForCalculations)
        {
            var found = formulaForCalculations.Find(e => e.Tier == formulaForCalculation.Tier);
            found.Should().NotBeNull();
            found!.Tier.Should().Be(formulaForCalculation.Tier);
            found!.MinimumFollowers.Should().Be(formulaForCalculation.MinimumFollowers);
            found!.PriceOfPurchaseRequisition.Should().Be(formulaForCalculation.PriceOfPurchaseRequisition);
        }
    }

    [Test]
    public async Task ShouldUpdateSectionAndUpdatePlatform()
    {
        var command = new CreateSectionCommand
        {
            Name = "update platform",
            FormulaForCalculations = new List<FormulaForCalculation>
            {
                new FormulaForCalculation { Tier = 1, PriceOfPurchaseRequisition = 10000, MinimumFollowers = 100000 },
                new FormulaForCalculation { Tier = 2, PriceOfPurchaseRequisition = 5000, MinimumFollowers = 10000 },
                new FormulaForCalculation { Tier = 3, PriceOfPurchaseRequisition = 1000, MinimumFollowers = 1000 }
            }
        };

        var section = await SendAsync(command);

        var platforms = new List<PlatformDto>()
        {
            new PlatformDto()
            {
                Name = "MockUpMediaCommand",
                Link = "https://www.pr-platform.com",
                PlatformType = PlatformType.RADIO.ToString(),
                Potential = 5001
            }
        };

        var mediaCommand = new CreateMediaSectionCommand
        {
            MediaId = "2541e16a-7fa1-4e81-bdc9-617e37d47750",
            TypeOfMedia = TypeOfMedia.MAINSTREAM.ToString(),
            Platforms = platforms,
            SectionId = section.Id.ToString(),
            SubSectionId = "8f9b8253-44da-4693-93a5-cb9770d63844",
            PhoneNumbers = new[] { "0912341234" },
            Email = new[] { "aaa@email.com" }
        };

        var updateSectionCommand = new UpdateSectionCommand
        {
            Id = section.Id.ToString(),
            Name = "Platform Updated",
            FormulaForCalculations = new List<FormulaForCalculation>
            {
                new FormulaForCalculation { Tier = 1, PriceOfPurchaseRequisition = 5000, MinimumFollowers = 100000 },
                new FormulaForCalculation { Tier = 2, PriceOfPurchaseRequisition = 2500, MinimumFollowers = 10000 },
                new FormulaForCalculation { Tier = 3, PriceOfPurchaseRequisition = 500, MinimumFollowers = 1000 },
                new FormulaForCalculation { Tier = 4, PriceOfPurchaseRequisition = 250, MinimumFollowers = 500 }
            }
        };

        var result = await SendAsync(updateSectionCommand);

        var item = await FindAsync<Section>(result.Id);
        item.Should().NotBeNull();
        item!.Id.Should().Be(section.Id);
        item!.Name.Should().Be(updateSectionCommand.Name);

        var formulaForCalculations = item.FormulaForCalculations.ToList();

        foreach (var formulaForCalculation in updateSectionCommand.FormulaForCalculations)
        {
            var found = formulaForCalculations.Find(e => e.Tier == formulaForCalculation.Tier);
            found.Should().NotBeNull();
            found!.Tier.Should().Be(formulaForCalculation.Tier);
            found!.MinimumFollowers.Should().Be(formulaForCalculation.MinimumFollowers);
            found!.PriceOfPurchaseRequisition.Should().Be(formulaForCalculation.PriceOfPurchaseRequisition);
        }
    }
}
