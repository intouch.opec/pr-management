using PRManagement.Application.Sections.Queries.GetSectionOptions;

namespace PRManagement.Application.FunctionalTests.Sections.Queries;
using static Testing;
public class GetSectionOptionTests : BaseTestFixture
{

  [Test]
  public async Task ShouldReturnSections()
  {
    var query = new GetSectionOptionsQuery();
    var options = await SendAsync(query);

    options.Should().NotBeNullOrEmpty();
    foreach (var item in options)
    {
      item.Id.Should().NotBeNullOrEmpty();
      item.Name.Should().NotBeNullOrEmpty();
    }
  }
}