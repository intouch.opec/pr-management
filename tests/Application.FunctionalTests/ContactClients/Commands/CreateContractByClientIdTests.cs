using PRManagement.Application.Clients.Commands.CreateClient;
using PRManagement.Application.Contacts.Commands.CreateContact;
using PRManagement.Application.Contacts.Commands.CreateContactByMediaSectionId;
using PRManagement.Application.ContactClients.Commands.CreateContactByClientId;
using ValidationException = PRManagement.Application.Common.Exceptions.ValidationException;

namespace PRManagement.Application.FunctionalTests.ContactClients.Commands;
using static Testing;

public class CreateContactByClientIdTests : BaseTestFixture
{
    [Test]
    public async Task ShouldRequireMinimumFields()
    {
        var command = new CreateContactByClientIdCommand();

        await FluentActions.Invoking(() =>
            SendAsync(command)).Should().ThrowAsync<ValidationException>();
    }

    [Test]
    public async Task ShouldCreateContactClient()
    {
        var client = await SendAsync(MockUpCreateClientRequireMinimumFieldsCommand());
        var contact = await SendAsync(MockUpCreateContactCommand());

        var result = await SendAsync(MockUpCreateContactByClientIdCommand(client.Id, contact.Id));
        result.ClientId.Should().Be(client.Id);
    }

    internal CreateContactByClientIdCommand MockUpCreateContactByClientIdCommand(Guid clientId, Guid contactId)
    {
        return new CreateContactByClientIdCommand
        {
            ClientId = clientId.ToString(),
            Contacts = new List<ContactItem>
            {
                new ContactItem
                {
                    FirstName = "John",
                    LastName = "Doe",
                    NickName = "Johnny",
                    Address = "123 Main St, Springfield",
                    Email = new[] { "john.doe@example.com", "johnny.doe@example.com" },
                    PhoneNumbers = new[] { "065-456-7890", "065-654-3210" },
                    Position = "Manager",
                    Remark = "Preferred contact",
                    ProvinceId = "1",
                    DistrictId = "1001",
                    SubDistrictId = "100101",
                    PostCode = "10200",
                    LineId = "johnny_line",
                    AddressType = "Work"
                },
                new ContactItem
                {
                    FirstName = "Jane",
                    LastName = "Smith",
                    NickName = "Janey",
                    Address = "456 Elm St, Springfield",
                    Email = new[] { "jane.smith@example.com" },
                    PhoneNumbers = new[] { "081-654-0987" },
                    Position = "Developer",
                    Remark = "Always available",
                    ProvinceId = "1",
                    DistrictId = "1001",
                    SubDistrictId = "100101",
                    PostCode = "10200",
                    LineId = "janey_line",
                    AddressType = "Work"
                }
            },
            ContactIds = new List<string>
            {
                contactId.ToString()
            }
        };
    }

    internal CreateContactCommand MockUpCreateContactCommand()
    {
        return new CreateContactCommand
        {
            FirstName = "chaitongdee",
            LastName = "jaidee",
            Address = "Address",
            Position = "Position",
            LineId = "LineId",
            Remark = "Remark",
            AddressType = "Home",
            Email = new string[] { "email_1@email.com" },
            PhoneNumbers = new string[] { "0612534567" },
            ProvinceId = "1",
            DistrictId = "1001",
            SubDistrictId = "100101",
            PostCode = "10200",
        };
    }

    internal CreateClientCommand MockUpCreateClientRequireMinimumFieldsCommand()
    {
        return new CreateClientCommand
        {
            Name = "Client PR 01 88",
            Email = new string[] { "Client@email.com" },
            PhoneNumbers = new string[] { "0650032167" },
        };
    }
}