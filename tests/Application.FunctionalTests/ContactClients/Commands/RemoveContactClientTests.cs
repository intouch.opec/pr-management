using PRManagement.Application.Clients.Commands.CreateClient;
using PRManagement.Application.Contacts.Commands.CreateContactByMediaSectionId;
using PRManagement.Application.ContactClients.Commands.CreateContactByClientId;

namespace PRManagement.Application.FunctionalTests.ContactClients.Commands;

using PRManagement.Application.ContactClients.Commands.RemoveContactClient;
using PRManagement.Domain.Entities;
using static Testing;

public class RemoveContactClientTests : BaseTestFixture
{

    [Test]
    public async Task ShouldNotFoundContactClient()
    {
        var command = new RemoveContactClientCommand(new Guid());

        await FluentActions.Invoking(() =>
            SendAsync(command)).Should().ThrowAsync<NotFoundException>();
    }

    [Test]
    public async Task ShouldRemoveContactClient()
    {
        var client = await SendAsync(MockUpCreateClientRequireMinimumFieldsCommand());
        var contactIds = (await SendAsync(MockUpCreateContactByClientIdCommand(client.Id))).ContactIds;

        await SendAsync(new RemoveContactClientCommand(contactIds.First()));

        var result = await FindAsync<ContactClient>(contactIds.First());
        result.Should().BeNull();
    }

    internal CreateContactByClientIdCommand MockUpCreateContactByClientIdCommand(Guid clientId)
    {
        return new CreateContactByClientIdCommand
        {
            ClientId = clientId.ToString(),
            Contacts = new List<ContactItem>
        {
            new ContactItem
            {
                FirstName = "Alice",
                LastName = "Johnson",
                NickName = "Ally",
                Address = "789 Maple St, Springfield",
                Email = new[] { "alice.johnson@example.com", "ally.j@example.com" },
                PhoneNumbers = new[] { "065-123-4567", "065-987-6543" },
                Position = "CEO",
                Remark = "Important contact",
                ProvinceId = "2",
                DistrictId = "2001",
                SubDistrictId = "200101",
                PostCode = "10300",
                LineId = "ally_line",
                AddressType = "Home"
            },
            new ContactItem
            {
                FirstName = "Bob",
                LastName = "Williams",
                NickName = "Bobby",
                Address = "101 Oak St, Springfield",
                Email = new[] { "bob.williams@example.com" },
                PhoneNumbers = new[] { "083-123-4567" },
                Position = "CTO",
                Remark = "Tech lead",
                ProvinceId = "3",
                DistrictId = "3001",
                SubDistrictId = "300101",
                PostCode = "10400",
                LineId = "bobby_line",
                AddressType = "Home"
            }
        },
            ContactIds = { }
        };
    }

    internal CreateClientCommand MockUpCreateClientRequireMinimumFieldsCommand()
    {
        return new CreateClientCommand
        {
            Name = "client 0001111",
            Email = new string[] { "client_1@email.com" },
            PhoneNumbers = new string[] { "0650030067" },
        };
    }
}
