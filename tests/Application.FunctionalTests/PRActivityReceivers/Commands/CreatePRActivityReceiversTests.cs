namespace PRManagement.Application.FunctionalTests.PRActivityReceivers.Commands;

using ValidationException = Application.Common.Exceptions.ValidationException;
using static Testing;
using PRManagement.Application.PRActivityReceivers.Commands.CreatePRActivityReceivers;

public class CreatePRActivityReceiversTests : BaseTestFixture
{
    [Test]
    public async Task ShouldRequireValidCreatePRActivityReceivers()
    {
        var command = new CreatePRActivityReceiversCommand();

        await FluentActions.Invoking(() =>
           SendAsync(command)).Should().ThrowAsync<ValidationException>();
    }

    [Test]
    public async Task ShouldRequireValidNotFoundCreatePRActivityReceivers()
    {
        var command = new CreatePRActivityReceiversCommand()
        {
            ContactIds = new string[]
            {
                "1623215f-de36-4f62-848f-ed901ea673eb"
            },
            PRActivityId = "9bdcd9ec-a1f6-4a59-9c31-dbbe17bd3c35"
        };

        await FluentActions.Invoking(() =>
           SendAsync(command)).Should().ThrowAsync<ValidationException>();
    }

    [Test]
    public async Task ShouldBeCreatePRActivityReceivers()
    {
        var command = new CreatePRActivityReceiversCommand()
        {
            ContactIds = new string[]
            {
                "069a760a-3bdc-4f53-bb0f-eb9dd0eb2975"
            },
            PRActivityId = "9bdcd9ec-a1f6-4a59-9c31-dbbe17bd3c35"
        };

        await SendAsync(command);
    }
}
