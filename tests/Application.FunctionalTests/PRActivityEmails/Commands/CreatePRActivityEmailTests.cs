namespace PRManagement.Application.FunctionalTests.PRActivityEmails.Commands;

using ValidationException = Application.Common.Exceptions.ValidationException;
using static Testing;
using PRManagement.Application.PRActivityEmails.Commands.CreatePRActivityEmail;
using PRManagement.Domain.Entities;
using PRManagement.Application.Common.Extensions;

public class CreatePRActivityEmailTests : BaseTestFixture
{
    [Test]
    public async Task ShouldRequireValidCreatePRActivityEmail()
    {
        var command = new CreatePRActivityEmailCommand();

        await FluentActions.Invoking(() =>
           SendAsync(command)).Should().ThrowAsync<ValidationException>();
    }

    [Test]
    public async Task ShouldCreatePRActivityEmail()
    {
        var command = new CreatePRActivityEmailCommand()
        {
            Subject = "Subject",
            Content = "Content",
            PRActivityId = "9bdcd9ec-a1f6-4a59-9c31-dbbe17bd3c35"
        };

        var result = await SendAsync(command);

        var prActivityEmail = await FindAsync<PRActivityEmail>(result.Id);
        prActivityEmail!.Content.Should().Be(command.Content);
        prActivityEmail!.Subject.Should().Be(command.Subject);
        prActivityEmail!.PRActivityId.Should().Be(command.PRActivityId.ToGuidOnly());
    }
}
