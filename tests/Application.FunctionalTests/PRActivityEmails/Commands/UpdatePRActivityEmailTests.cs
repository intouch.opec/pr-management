namespace PRManagement.Application.FunctionalTests.PRActivityEmails.Commands;

using ValidationException = Application.Common.Exceptions.ValidationException;
using static Testing;
using PRManagement.Application.PRActivityEmails.Commands.UpdatePRActivityEmail;
using PRManagement.Domain.Entities;
using PRManagement.Application.Common.Extensions;

public class UpdatePRActivityEmailTests : BaseTestFixture
{
    [Test]
    public async Task ShouldRequireValidUpdatePRActivityEmail()
    {
        var command = new UpdatePRActivityEmailCommand();

        await FluentActions.Invoking(() =>
           SendAsync(command)).Should().ThrowAsync<ValidationException>();
    }

    [Test]
    public async Task ShouldUpdatePRActivityEmail()
    {
        var command = new UpdatePRActivityEmailCommand()
        {
            Subject = "UpdatePRActivityEmail",
            Content = "UpdatePRActivityEmailContent",
            PRActivityId = "79ab5fe5-80d0-4447-8ea9-06619749431c"
        };

        var result = await SendAsync(command);

        var prActivityEmail = await FindAsync<PRActivityEmail>(result.Id);
        prActivityEmail!.Content.Should().Be(command.Content);
        prActivityEmail!.Subject.Should().Be(command.Subject);
        prActivityEmail!.PRActivityId.Should().Be(command.PRActivityId.ToGuidOnly());
    }
}
