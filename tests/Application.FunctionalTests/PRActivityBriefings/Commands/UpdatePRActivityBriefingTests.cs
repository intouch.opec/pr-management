namespace PRManagement.Application.FunctionalTests.PRActivityBriefings.Commands;

using ValidationException = Application.Common.Exceptions.ValidationException;
using static Testing;
using PRManagement.Domain.Entities;
using PRManagement.Application.Common.Extensions;
using PRManagement.Application.PRActivityBriefings.Commands.UpdatePRActivityBriefing;

public class UpdatePRActivityBriefingTests : BaseTestFixture
{
    [Test]
    public async Task ShouldRequireValidCreatePRActivityBriefing()
    {
        var command = new UpdatePRActivityBriefingCommand();

        await FluentActions.Invoking(() =>
           SendAsync(command)).Should().ThrowAsync<ValidationException>();
    }

    [Test]
    public async Task ShouldCreatePRActivityBriefing()
    {
        var command = new UpdatePRActivityBriefingCommand()
        {
            Content = "SeedPRActivityBriefing",
            PRActivityId = "79ab5fe5-80d0-4447-8ea9-06619749431c"
        };

        var result = await SendAsync(command);
        result.Id.Should().NotBeEmpty();
        result.Content.Should().NotBeEmpty();
        result.PRActivityId.Should().NotBeEmpty();
        var prActivityBriefing = await FindAsync<PRActivityBriefing>(result.Id);
        prActivityBriefing!.Id.Should().Be(result.Id);
        prActivityBriefing!.Content.Should().Be(command.Content);
        prActivityBriefing!.PRActivityId.Should().Be(command.PRActivityId.ToGuidOnly());
    }
}
