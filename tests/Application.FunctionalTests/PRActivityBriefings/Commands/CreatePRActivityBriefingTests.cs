namespace PRManagement.Application.FunctionalTests.PRActivityBriefings.Commands;

using ValidationException = Application.Common.Exceptions.ValidationException;
using PRManagement.Application.PRActivityBriefings.Commands.CreatePRActivityBriefing;
using static Testing;
using PRManagement.Domain.Entities;
using PRManagement.Application.Common.Extensions;

public class CreatePRActivityBriefingTests : BaseTestFixture
{
    [Test]
    public async Task ShouldRequireValidCreatePRActivityBriefing()
    {
        var command = new CreatePRActivityBriefingCommand();

        await FluentActions.Invoking(() =>
           SendAsync(command)).Should().ThrowAsync<ValidationException>();
    }

    [Test]
    public async Task ShouldCreatePRActivityBriefing()
    {
        var command = new CreatePRActivityBriefingCommand()
        {
            Content = "xxxx",
            PRActivityId = "9bdcd9ec-a1f6-4a59-9c31-dbbe17bd3c35"
        };

        var result = await SendAsync(command);
        result.Id.Should().NotBeEmpty();
        result.Content.Should().NotBeEmpty();
        result.PRActivityId.Should().NotBeEmpty();
        var prActivityBriefing = await FindAsync<PRActivityBriefing>(result.Id);
        prActivityBriefing!.Id.Should().Be(result.Id);
        prActivityBriefing!.Content.Should().Be(command.Content);
        prActivityBriefing!.PRActivityId.Should().Be(command.PRActivityId.ToGuidOnly());
    }

}