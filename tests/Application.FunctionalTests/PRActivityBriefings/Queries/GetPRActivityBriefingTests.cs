namespace PRManagement.Application.FunctionalTests.PRActivityBriefings.Queries;

using static Testing;
using PRManagement.Application.PRActivityBriefings.Queries.GetPRActivityBriefing;

public class GetPRActivityBriefingTests : BaseTestFixture
{
    [Test]
    public async Task ShouldGetPRActivityBriefing()
    {
        var command = new GetPRActivityBriefingQuery("79ab5fe5-80d0-4447-8ea9-06619749431c");

        var prActivityBriefing = await SendAsync(command);
        prActivityBriefing.Id.Should().NotBeEmpty();
        prActivityBriefing.Content.Should().NotBeEmpty();
        prActivityBriefing.PRActivityId.Should().NotBeEmpty();
    }

}