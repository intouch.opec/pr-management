namespace PRManagement.Application.FunctionalTests.TaskWorkerTests.Commands;
using ValidationException = Application.Common.Exceptions.ValidationException;
using static Testing;
using PRManagement.Domain.Entities;
using PRManagement.Application.TaskWorkers.Commands.UpdateTaskWorker;
using PRManagement.Domain.Enums;

public class UpdateTaskWorkerTests : BaseTestFixture
{
    [Test]
    public async Task ShouldRequireMinimumFields()
    {
        var command = new UpdateTaskWorkerCommand();

        await FluentActions.Invoking(() =>
            SendAsync(command)).Should().ThrowAsync<ValidationException>();
    }

    [Test]
    public async Task ShouldBeUpdateTaskWorker()
    {
        var command = new UpdateTaskWorkerCommand()
        {
            Id = "e2f12e32-6da9-4986-9a47-a8cb4ee1e3da",
            Topic = "UpdateTaskWorker",
            KeywordQuery = "KeywordQuery",
            PRActivityId = "9bdcd9ec-a1f6-4a59-9c31-dbbe17bd3c35",
            TaskWorkerStatus = TaskWorkerStatus.DONE.ToString(),
            IsUrgent = false
        };

        var result = await SendAsync(command);

        var monitorTaskWorker = await FindAsync<MonitorTaskWorker>(result.Id);
        monitorTaskWorker!.Topic.Should().Be(command.Topic);
        monitorTaskWorker!.KeywordQuery.Should().Be(command.KeywordQuery);
        monitorTaskWorker!.IsUrgent.Should().Be(command.IsUrgent);
    }
}
