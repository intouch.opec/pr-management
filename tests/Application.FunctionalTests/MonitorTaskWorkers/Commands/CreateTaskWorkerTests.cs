namespace PRManagement.Application.FunctionalTests.TaskWorkerTests.Commands;
using ValidationException = Application.Common.Exceptions.ValidationException;
using PRManagement.Application.TaskWorkers.Commands.CreateTaskWorker;
using static Testing;
using PRManagement.Domain.Entities;

public class CreateTaskWorkerTests : BaseTestFixture
{
    [Test]
    public async Task ShouldRequireMinimumFields()
    {
        var command = new CreateTaskWorkerCommand();

        await FluentActions.Invoking(() =>
            SendAsync(command)).Should().ThrowAsync<ValidationException>();
    }

    [Test]
    public async Task HasNotPRActivityIdShouldBeCreateTaskWorker()
    {
        var command = new CreateTaskWorkerCommand()
        {
            Topic = "Topic",
            KeywordQuery = "KeywordQuery",
            PRActivityId = "9bdcd9ec-a1f6-4a59-9c31-dbbe17bd3c35",
            IsUrgent = false
        };

        var result = await SendAsync(command);

        var monitorTaskWorker = await FindAsync<MonitorTaskWorker>(result.Id);
        monitorTaskWorker!.Topic.Should().Be(command.Topic);
        monitorTaskWorker!.KeywordQuery.Should().Be(command.KeywordQuery);
        monitorTaskWorker!.IsUrgent.Should().Be(command.IsUrgent);
    }

    [Test]
    public async Task HasPRActivityIdShouldBeCreateTaskWorker()
    {
        var command = new CreateTaskWorkerCommand()
        {
            Topic = "Topic X",
            KeywordQuery = "KeywordQuery",
            PRActivityId = "9bdcd9ec-a1f6-4a59-9c31-dbbe17bd3c35",
            IsUrgent = false
        };

        var result = await SendAsync(command);

        var monitorTaskWorker = await FindAsync<MonitorTaskWorker>(result.Id);
        monitorTaskWorker!.Topic.Should().Be(command.Topic);
        monitorTaskWorker!.KeywordQuery.Should().Be(command.KeywordQuery);
        monitorTaskWorker!.IsUrgent.Should().Be(command.IsUrgent);
    }
}
