namespace PRManagement.Application.FunctionalTests.MonitorTaskWorkers.Queries;

using PRManagement.Application.TaskWorkers.Queries.GetTaskWorkerWithPagination;
using static Testing;

public class GetTaskWorkerWithPaginationTests : BaseTestFixture
{
    [Test]
    public async Task ShouldGetTaskWorkerWithPagination()
    {
        await SendAsync(new GetTaskWorkerWithPaginationQuery());
    }

    [Test]
    public async Task ShouldGetTaskWorkerWithPaginationPRActivityIdOnly()
    {
        var taskWorker = await SendAsync(new GetTaskWorkerWithPaginationQuery()
        {
            PRActivityId = "df8f5ede-634e-488f-b44a-5dd5a023fefa"
        });

        taskWorker.Items.Count().Should().Be(2);
    }
}
