using PRManagement.Application.Districts.Queries.GetDistrictOptions;

namespace PRManagement.Application.FunctionalTests.Districts.Queries;
using static Testing;
public class GetDistrictOptionTests : BaseTestFixture
{

  [Test]
  public async Task ShouldReturnDistricts()
  {
    var query = new GetDistrictOptionsQuery();
    var options = await SendAsync(query);

    options.Should().NotBeNullOrEmpty();
    foreach (var item in options)
    {
      item.Id.Should().NotBeNullOrEmpty();
      item.Name.Should().NotBeNullOrEmpty();
      item.NameEN.Should().NotBeNullOrEmpty();
      item.NameTH.Should().NotBeNullOrEmpty();
    }
  }
}