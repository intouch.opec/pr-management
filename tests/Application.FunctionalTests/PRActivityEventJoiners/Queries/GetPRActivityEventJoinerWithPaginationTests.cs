using PRManagement.Application.PRActivityEventJoiners.Queries.GetPRActivityEventJoinerWithPagination;
using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.FunctionalTests.PRActivityEventJoiners.Queries;

using static Testing;

public class GetPRActivityEventJoinerWithPaginationTests : BaseTestFixture
{
    [Test]
    public async Task ShouldReturnPaginatedList()
    {
        var query = new GetPRActivityEventJoinerWithPaginationQuery
        {
            PrActivityId = "9bdcd9ec-a1f6-4a59-9c31-dbbe17bd3c35",
            PageNumber = 1,
            PageSize = 10
        };

        // Act
        var result = await SendAsync(query);

        // Assert
        result.Items.Should().NotBeEmpty();
        result.TotalCount.Should().BeGreaterThan(0);
        result.TotalPages.Should().BeGreaterThan(0);
        result.HasNextPage.Should().BeFalse();
        result.HasPreviousPage.Should().BeFalse();
    }

    [Test]
    public async Task ShouldFilterByFirstName()
    {
        var query = new GetPRActivityEventJoinerWithPaginationQuery
        {
            PrActivityId = "9bdcd9ec-a1f6-4a59-9c31-dbbe17bd3c35",
            FirstName = "FirstName"
        };

        // Act
        var result = await SendAsync(query);

        // Assert
        result.Items.Should().NotBeEmpty();
        result.Items.Should().AllSatisfy(item => 
            item.FirstName.ToLower().Should().Contain(query.FirstName.ToLower()));
    }

    [Test]
    public async Task ShouldFilterByLastName()
    {
        var query = new GetPRActivityEventJoinerWithPaginationQuery
        {
            LastName = "Last",
            PrActivityId = "9bdcd9ec-a1f6-4a59-9c31-dbbe17bd3c35",
        };

        // Act
        var result = await SendAsync(query);

        // Assert
        result.Items.Should().NotBeEmpty();
        result.Items.Should().AllSatisfy(item => 
            item.LastName.ToLower().Should().Contain(query.LastName.ToLower()));
    }

    // [Test]
    // public async Task ShouldFilterByEmail()
    // {
    //     var query = new GetPRActivityEventJoinerWithPaginationQuery
    //     {
    //         PrActivityId = "9bdcd9ec-a1f6-4a59-9c31-dbbe17bd3c35",
    //         Email = "example1xxx@example.co"
    //     };

    //     // Act
    //     var result = await SendAsync(query);

    //     // Assert
    //     result.Items.Should().NotBeEmpty();
    //     result.Items.Should().AllSatisfy(item => 
    //         item.Email.Should().Contain(query.Email));
    // }

    [Test]
    public async Task ShouldSearchAcrossMultipleFields()
    {
        var query = new GetPRActivityEventJoinerWithPaginationQuery
        {
            Search = "John",
            PrActivityId = "9bdcd9ec-a1f6-4a59-9c31-dbbe17bd3c35",
        };

        // Act
        var result = await SendAsync(query);

        // Assert
        result.Items.Should().NotBeEmpty();
        result.Items.Should().Match(items => 
            items.All(item => 
                item.FirstName.Contains("John", StringComparison.OrdinalIgnoreCase) ||
                item.LastName.Contains("John", StringComparison.OrdinalIgnoreCase) ||
                item.NickName.Contains("John", StringComparison.OrdinalIgnoreCase)));
    }

    [Test]
    public async Task ShouldSortBySpecifiedField()
    {
       
        var query = new GetPRActivityEventJoinerWithPaginationQuery
        {
            PrActivityId = "9bdcd9ec-a1f6-4a59-9c31-dbbe17bd3c35",
            SortBy = nameof(GetContactItem.FirstName),
            Sort = SortType.ASC
        };

        // Act
        var result = await SendAsync(query);

        // Assert
        result.Items.Should().BeInAscendingOrder(x => x.FirstName);
    }

    [Test]
    public async Task ShouldDefaultSortByLastModified()
    {
        
        var query = new GetPRActivityEventJoinerWithPaginationQuery()
        {
            PrActivityId = "9bdcd9ec-a1f6-4a59-9c31-dbbe17bd3c35",
        };

        // Act
        var result = await SendAsync(query);

        // Assert
        result.Items.Should().BeInDescendingOrder(x => x.LastModified);
    }

}
