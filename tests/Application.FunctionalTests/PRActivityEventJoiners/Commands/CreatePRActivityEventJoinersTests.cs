using PRManagement.Application.PRActivityEventJoiners.Commands.CreatePRActivityEventJoiners;
using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;
using ValidationException = PRManagement.Application.Common.Exceptions.ValidationException;

namespace PRManagement.Application.FunctionalTests.PRActivityEventJoiners.Commands;

using static Testing;

public class CreatePRActivityEventJoinersTests : BaseTestFixture
{
    [Test]
    public async Task ShouldRequireValidPRActivityId()
    {
        var command = new CreatePRActivityEventJoinersCommand
        {
            PRActivityId = "",
            ContactId = "fe0fcc0a-70d9-4e03-962a-ace1e9ff5a3e"
        };

        await FluentActions.Invoking(() =>
            SendAsync(command)).Should().ThrowAsync<ValidationException>();
    }

    [Test]
    public async Task ShouldRequireValidContactId()
    {
        var command = new CreatePRActivityEventJoinersCommand
        {
            PRActivityId = "fe0fcc0a-70d9-4e03-962a-ace1e9ff5a3e",
            ContactId = ""
        };

        await FluentActions.Invoking(() =>
            SendAsync(command)).Should().ThrowAsync<ValidationException>();
    }

    [Test]
    public async Task ShouldCreatePRActivityEventJoinerAsWalkIn()
    {
        // Arrange
        var contactId = new Guid("6ab0b664-f158-4b0c-897f-8c737329ea14");
        var prActivityId = new Guid("9bdcd9ec-a1f6-4a59-9c31-dbbe17bd3c35");

        var command = new CreatePRActivityEventJoinersCommand
        {
            PRActivityId = prActivityId.ToString(),
            ContactId = contactId.ToString()
        };

        // Act
        var result = await SendAsync(command);

        // Assert
        var created = await FindAsync<PRActivityEventJoiner>(result.Id);

        created.Should().NotBeNull();
        created!.PRActivityId.Should().Be(prActivityId);
        created.ContactId.Should().Be(contactId);
        created.JoinType.Should().Be(JoinType.WalkIn);
    }

    [Test]
    public async Task ShouldCreatePRActivityEventJoinerAsBook()
    {
        // Arrange
        var contactId = new Guid("d4f7da72-b34b-48ee-ae36-747c627727b1");
        var prActivityId = new Guid("9bdcd9ec-a1f6-4a59-9c31-dbbe17bd3c35");
        
        // Create PRActivityReceiver first
        await AddAsync(new PRActivityReceiver
        {
            PRActivityId = prActivityId,
            ContactId = contactId
        });

        var command = new CreatePRActivityEventJoinersCommand
        {
            PRActivityId = prActivityId.ToString(),
            ContactId = contactId.ToString()
        };

        // Act
        var result = await SendAsync(command);

        // Assert
        var created = await FindAsync<PRActivityEventJoiner>(result.Id);

        created.Should().NotBeNull();
        created!.PRActivityId.Should().Be(prActivityId);
        created.ContactId.Should().Be(contactId);
        created.JoinType.Should().Be(JoinType.Book);
    }

    [Test]
    public async Task ShouldThrowNotFoundForNonExistentPRActivity()
    {
        var contactId = new Guid("547a3b25-ad3d-48bb-b4c5-c4d545ff25dc");

        var command = new CreatePRActivityEventJoinersCommand
        {
            PRActivityId = Guid.NewGuid().ToString(),
            ContactId = contactId.ToString()
        };

        await FluentActions.Invoking(() =>
            SendAsync(command)).Should().ThrowAsync<ValidationException>();
    }

}
