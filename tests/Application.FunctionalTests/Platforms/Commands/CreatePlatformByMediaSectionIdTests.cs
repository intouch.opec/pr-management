using PRManagement.Application.Common.Exceptions;
using PRManagement.Application.Common.Models;
using PRManagement.Application.Medias.Commands.CreateMedia;
using PRManagement.Application.MediaSections.Queries.GetMediaById;
using PRManagement.Application.Platforms.Commands.CreatePlatformByMediaSectionId;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.FunctionalTests.Platforms.Commands;
using static Testing;
public class CreatePlatformByMediaSectionIdTests : BaseTestFixture
{

    [Test]
    public async Task ShouldRequireValidCreatePlatformByMediaSectionId()
    {
        var command = new CreatePlatformByMediaSectionIdCommand();

        await FluentActions.Invoking(() =>
           SendAsync(command)).Should().ThrowAsync<ValidationException>();
    }

    [Test]
    public async Task ShouldCreatePlatforms()
    {
        var mediaCommand = MockUpMediaCommand();
        var media = await SendAsync(mediaCommand);
        var command = MockUpCreatePlatformByMediaSectionIdCommand(media.MediaSectionId);

        var result = await SendAsync(command);

        var query = new GetMediaSectionByIdQuery(result.MediaSectionId);
        var mediaSection = await SendAsync(query);
        var platformCount = mediaSection.Platforms.Count();
        var platformCommand = command.Platforms.First();
        result.Should().NotBeNull();
        platformCount.Should().Be(2);
        var platform = mediaSection.Platforms.FirstOrDefault(pl => pl.PlatformType.Equals(platformCommand.PlatformType));
        platform.Should().NotBeNull();
        platform!.Name.Should().Be(platformCommand.Name);
        platform.Link.Should().Be(platformCommand.Link);
        platform.PRValue.Should().Be(platformCommand.PRValue);
        platform.ADValue.Should().Be(platformCommand.ADValue);
        platform.Potential.Should().Be(platformCommand.Potential);
        platform.TierOder.Should().Be(platformCommand.TierOder);
        platform.TierPotential.Should().Be(platformCommand.TierPotential);
        platform.PlatformType.Should().Be(platformCommand.PlatformType);
    }

    internal CreatePlatformByMediaSectionIdCommand MockUpCreatePlatformByMediaSectionIdCommand(Guid id)
    {
        var platforms = new List<PlatformByMediaSectionIdItemDto>()
        {
        new PlatformByMediaSectionIdItemDto()
        {
            Name = "platform facebook",
            Link = "https://www.facebook.com/",
            PRValue = 100,
            ADValue = 100,
            Potential = 100,
            TierOder = 1,
            TierPotential = 1,
            PlatformType = PlatformType.FACEBOOK.ToString(),
        }
        };

        return new CreatePlatformByMediaSectionIdCommand
        {
            Id = id.ToString(),
            Platforms = platforms
        };
    }

    internal CreateMediaCommand MockUpMediaCommand()
    {
        var platforms = new List<PlatformDto>()
        {
        new PlatformDto()
        {
            Name = "mock create platforms",
            Link = "https://www.pr-platform.com",
            PlatformType = PlatformType.X.ToString(),
            PRValue = 100,
            ADValue = 100,
            Potential = 100,
            TierOder = 1,
            TierPotential = 1,
        }
        };

        return new CreateMediaCommand
        {
            Name = "CreatePlatformByMediaSectionId",
            Address = "Address",
            Email = new string[] { "email_5@email.com", "email_6@email.com" },
            PhoneNumbers = new string[] { "0651234523", "0851213567" },
            ProvinceId = "1",
            DistrictId = "1001",
            SubDistrictId = "100101",
            PostCode = "10200",
            TypeOfMedia = TypeOfMedia.DIGITAL.ToString(),
            Platforms = platforms,
            SectionId = "56d22a41-6b16-4335-9ba2-dfeb63c5a830",
            SubSectionId = "90a6e746-a379-4603-b39c-c10409314445"
        };
    }

}
