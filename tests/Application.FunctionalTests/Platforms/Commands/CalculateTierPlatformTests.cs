
namespace PRManagement.Application.FunctionalTests.Platforms.Commands;

using PRManagement.Application.Platforms.Commands.CalculateTierPlatform;
using static Testing;
public class CalculateTierPlatformTests : BaseTestFixture
{

  [Test]
  public async Task ShouldReturnFirstTier()
  {
    var command = MockUpCalculateTierPlatformCommandFirstTier();

    var result = await SendAsync(command);

    result.Should().NotBeNull();
    result.TierPotential.Should().Be(1);
    result.PRValue.Should().Be(300000);
  }

  [Test]
  public async Task ShouldReturnSecondTier()
  {
    var command = MockUpCalculateTierPlatformCommandSecondTier();

    var result = await SendAsync(command);

    result.Should().NotBeNull();
    result.TierPotential.Should().Be(2);
    result.PRValue.Should().Be(25000);
  }

  [Test]
  public async Task ShouldReturnThirdTier()
  {
    var command = MockUpCalculateTierPlatformCommandThirdTier();

    var result = await SendAsync(command);

    result.Should().NotBeNull();
    result.TierPotential.Should().Be(3);
    result.PRValue.Should().Be(20000);
  }

  private CalculateTierPlatformCommand MockUpCalculateTierPlatformCommandFirstTier()
  {
    return new CalculateTierPlatformCommand()
    {
      SectionId = "56d22a41-6b16-4335-9ba2-dfeb63c5a830",
      NumberOfFollower = 100001
    };
  }

  private CalculateTierPlatformCommand MockUpCalculateTierPlatformCommandSecondTier()
  {
    return new CalculateTierPlatformCommand()
    {
      SectionId = "56d22a41-6b16-4335-9ba2-dfeb63c5a830",
      NumberOfFollower = 80000
    };
  }

   private CalculateTierPlatformCommand MockUpCalculateTierPlatformCommandThirdTier()
  {
    return new CalculateTierPlatformCommand()
    {
      SectionId = "56d22a41-6b16-4335-9ba2-dfeb63c5a830",
      NumberOfFollower = 10000
    };
  }
}