using PRManagement.Application.Common.Models;
using PRManagement.Application.Medias.Commands.CreateMedia;
using PRManagement.Application.Platforms.Queries.GetPlatformWithPagination;
using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.FunctionalTests.Platforms.Queries;

using static Testing;
public class GetPlatformWithPaginationByMediaSectionIdTests : BaseTestFixture
{
    [Test]
    public async Task ShouldReturnPlatformWithPagination()
    {
        var commandMockUp = MockUpCreateMediaCommand();
        await SendAsync(commandMockUp);
        var query = new GetPlatformWithPaginationQuery();

        var result = await SendAsync(query);

        var count = await CountAsync<Platform>();
        result.Should().NotBeNull();
        result.TotalCount.Should().Be(count);
        result.Items.Should().NotBeEmpty();
    }

    [Test]
    public async Task ShouldReturnPlatformWithPaginationOnlyMedia()
    {
        var query = new GetPlatformWithPaginationQuery()
        {
            MediaName = "instagram"
        };

        var result = await SendAsync(query);

        result.Should().NotBeNull();
        result.Items.Should().NotBeEmpty();
        foreach (var item in result.Items)
        {
            item.MediaName.Should().Be("instagram");
        }
    }


    internal CreateMediaCommand MockUpCreateMediaCommand()
    {
        var platforms = new List<PlatformDto>()
        {
            new PlatformDto()
            {
                Name = "PlatformWithPagination",
                Link = "https://www.pr-platform.com",
                PlatformType = PlatformType.NEWSPAPER.ToString(),
            },
        new PlatformDto()
            {
                Name = "PlatformWithPagination",
                Link = "https://www.pr-platform.com",
                PlatformType = PlatformType.MAGAZINE.ToString(),
            }
        };

        return new CreateMediaCommand
        {
            Name = "PlatformWithPagination",
            Address = "Address",
            Email = new string[] { "email@email.com" },
            PhoneNumbers = new string[] { "0991934567" },
            ProvinceId = "1",
            DistrictId = "1001",
            SubDistrictId = "100101",
            PostCode = "10200",
            TypeOfMedia = TypeOfMedia.MAINSTREAM.ToString(),
            Platforms = platforms,
            SectionId = "56d22a41-6b16-4335-9ba2-dfeb63c5a830",
            SubSectionId = "90a6e746-a379-4603-b39c-c10409314445"
        };
    }

}