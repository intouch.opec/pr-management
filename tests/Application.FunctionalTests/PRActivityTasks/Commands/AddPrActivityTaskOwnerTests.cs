using ValidationException = PRManagement.Application.Common.Exceptions.ValidationException;

namespace PRManagement.Application.FunctionalTests.PRActivityTasks.Commands;

using PRManagement.Application.Common.Extensions;
using PRManagement.Application.PRActivityTasks.Commands.AddPrActivityTaskOwner;
using PRManagement.Domain.Entities;
using static Testing;

public class AddPrActivityTaskOwnerTests : BaseTestFixture
{
    [Test]
    public async Task ShouldRequireValidAddPrActivityTaskOwner()
    {
        var command = new AddPrActivityTaskOwnerCommand();

        await FluentActions.Invoking(() =>
           SendAsync(command)).Should().ThrowAsync<ValidationException>();
    }

    [Test]
    public async Task ShouldNotBeDuplicatePrActivityTaskOwner()
    {
        var command = new AddPrActivityTaskOwnerCommand()
        {
            PrActivityTaskId = "fe0fcc0a-70d9-4e03-962a-ace1e9ff5a3e",
            OwnerId = "fbc382fe-5271-4ddd-bda8-4afdb839723d",
        };

        await FluentActions.Invoking(
            () =>
                SendAsync(command))
           .Should()
           .ThrowAsync<ValidationException>();
    }

    [Test]
    public async Task ShouldBeAddPrActivityTaskOwner()
    {
        var command = new AddPrActivityTaskOwnerCommand()
        {
            PrActivityTaskId = "b6aa2739-2891-4ceb-adf8-be2e6600a6fb",
            OwnerId = "7a305e45-421b-4097-bc69-bc0d6823f4dd",
        };

        var result = await SendAsync(command);

        // var entity = await FindAsync<PRActivityTaskResponsiblePerson>(result.Id.ToUlId());

        // entity!.OwnerId.Should().Be(command.OwnerId);
        // entity!.PRActivityTaskId.Should().Be(command.PrActivityTaskId.ToGuidOnly());
    }
}
