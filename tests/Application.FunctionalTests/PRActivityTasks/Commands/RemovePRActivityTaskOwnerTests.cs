using ValidationException = PRManagement.Application.Common.Exceptions.ValidationException;
using PRManagement.Application.PRActivityTasks.Commands.RemovePRActivityTaskOwner;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.FunctionalTests.PRActivityTasks.Commands;
using static Testing;

public class RemovePRActivityTaskOwnerTests : BaseTestFixture
{
    [Test]
    public async Task ShouldRequireValidRemovePRActivityTaskOwner()
    {
        var command = new RemovePRActivityTaskOwnerCommand("", "");

        await FluentActions.Invoking(() =>
           SendAsync(command)).Should().ThrowAsync<ValidationException>();
    }

    [Test]
    public async Task ShouldRemovePRActivityTaskOwner()
    {
        var command = new RemovePRActivityTaskOwnerCommand("914426fa-a1ef-4c3e-aad0-d913cd75460b", "b6aa2739-2891-4ceb-adf8-be2e6600a6fb");

        var result = await SendAsync(command);

        var entity = await FindAsync<PRActivityTaskResponsiblePerson>(result.Id);

        entity.Should().BeNull();
    }
}
