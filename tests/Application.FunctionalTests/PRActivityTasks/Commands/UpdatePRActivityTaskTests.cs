using ValidationException = PRManagement.Application.Common.Exceptions.ValidationException;
using PRManagement.Application.PRActivityTasks.Commands.UpdatePRActivityTask;
using PRManagement.Domain.Enums;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.FunctionalTests.PRActivityTasks.Commands;
using static Testing;

public class UpdatePRActivityTaskTests : BaseTestFixture
{
    [Test]
    public async Task ShouldRequireValidUpdatePRActivityTask()
    {
        var command = new UpdatePRActivityTaskCommand();

        await FluentActions.Invoking(() =>
           SendAsync(command)).Should().ThrowAsync<ValidationException>();
    }

    [Test]
    public async Task ShouldBeUpdatePRActivityTask()
    {
        var command = new UpdatePRActivityTaskCommand()
        {
            Id = "fe0fcc0a-70d9-4e03-962a-ace1e9ff5a3e",
            WorkTask = WorkTask.BRIEFING_MEETING.ToString(),
            DueDate = DateTimeOffset.UtcNow.ToUniversalTime().AddDays(4),
            Description = "Update Description",
            TaskWorkerStatus = TaskWorkerStatus.PENDING.ToString()
        };

        var result = await SendAsync(command);

        var entity = await FindAsync<PRActivityTask>(result.Id);

        entity!.DueDate.Should().Be(command.DueDate);
        entity.Description.Should().Be(command.Description);
        
    }
}
