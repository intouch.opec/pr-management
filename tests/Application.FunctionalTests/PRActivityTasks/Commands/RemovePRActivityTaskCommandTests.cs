using ValidationException = PRManagement.Application.Common.Exceptions.ValidationException;
using PRManagement.Application.PRActivityTasks.Commands.RemovePRActivityTask;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.FunctionalTests.PRActivityTasks.Commands;
using static Testing;

public class RemovePRActivityTaskCommandTests : BaseTestFixture
{
    // [Test]
    // public async Task ShouldRequireValidRemovePRActivityTaskCommand()
    // {
    //     var command = new RemovePRActivityTaskCommand("");

    //     await FluentActions.Invoking(() =>
    //        SendAsync(command)).Should().ThrowAsync<ValidationException>();
    // }

    [Test]
    public async Task ShouldBeRemovePRActivityTaskCommand()
    {
        var command = new RemovePRActivityTaskCommand("ac36a9bc-a257-4d50-a7f5-b753a4e7ee18");

        var result = await SendAsync(command);

        var entity = await FindAsync<PRActivityTask>(result.Id);
        entity!.Should().BeNull();
    }
}