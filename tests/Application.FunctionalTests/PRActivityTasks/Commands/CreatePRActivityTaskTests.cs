using PRManagement.Application.Common.Extensions;
using PRManagement.Application.PRActivityTasks.Commands.CreatePRActivityTask;
using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;
using PRManagement.Domain.Exceptions;
using ValidationException = PRManagement.Application.Common.Exceptions.ValidationException;

namespace PRManagement.Application.FunctionalTests.PRActivityTasks.Commands;
using static Testing;

public class CreatePRActivityTaskTests : BaseTestFixture
{
    [Test]
    public async Task ShouldRequireValidCreatePRActivityTask()
    {
        var command = new CreatePRActivityTaskCommand();

        await FluentActions.Invoking(() =>
           SendAsync(command)).Should().ThrowAsync<ValidationException>();
    }

    [Test]
    public async Task ShouldBeCreatePRActivityTaskComment()
    {
        var command = new CreatePRActivityTaskCommand()
        {
            DueDate = DateTimeOffset.UtcNow.ToUniversalTime().AddDays(3),
            Description = "XXXX",
            TaskWorkerStatus = TaskWorkerStatus.NOW.ToString(),
            WorkTask = WorkTask.BRIEFING_MEETING.ToString(),
            PRActivityId = "9bdcd9ec-a1f6-4a59-9c31-dbbe17bd3c35",
            OwnerIds = new List<string>()
            {
                "fbc382fe-5271-4ddd-bda8-4afdb839723d"
            }
        };

        var result = await SendAsync(command);

        var entity = await FindAsync<PRActivityTask>(result.Id);

        entity!.DueDate.Should().Be(command.DueDate);
        entity!.WorkTask.Should().Be(command.WorkTask.ToEnum<WorkTask>());
        entity!.TaskWorkerStatus.Should().Be(command.TaskWorkerStatus.ToEnum<TaskWorkerStatus>());
        entity.Description.Should().Be(command.Description);
        entity.PRActivityId.Should().Be(command.PRActivityId.ToGuidOnly());

        result.Owners.Count().Should().Be(command.OwnerIds.Count());

        foreach (var item in result.Owners)
        {
            var person = await FindAsync<PRActivityTaskResponsiblePerson>(item.Id);
            var ownerId = command.OwnerIds.ToList().Find(e => e.Equals(person!.OwnerId));
            ownerId.Should().Be(item.UserId);
        }
    }
}
