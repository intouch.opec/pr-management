using ValidationException = PRManagement.Application.Common.Exceptions.ValidationException;
using PRManagement.Application.PRActivityTasks.Commands.UpdatePRActivityTaskCommentStatus;
using PRManagement.Domain.Enums;
using PRManagement.Domain.Entities;
using PRManagement.Domain.Exceptions;

namespace PRManagement.Application.FunctionalTests.PRActivityTasks.Commands;
using static Testing;

public class UpdatePRActivityTaskStatusTests : BaseTestFixture
{
    [Test]
    public async Task ShouldRequireValidUpdatePRActivityTaskStatusCommand()
    {
        var command = new UpdatePRActivityTaskStatusCommand();

        await FluentActions.Invoking(() =>
           SendAsync(command)).Should().ThrowAsync<ValidationException>();
    }

    [Test]
    public async Task ShouldBeUpdatePRActivityTaskStatusTo()
    {
        var command = new UpdatePRActivityTaskStatusCommand()
        {
            Id = "fe0fcc0a-70d9-4e03-962a-ace1e9ff5a3e",
            TaskWorkerStatus = TaskWorkerStatus.DONE.ToString()
        };

        var result = await SendAsync(command);

        var entity = await FindAsync<PRActivityTask>(result.Id);
        entity!.TaskWorkerStatus.Should().Be(command.TaskWorkerStatus.ToEnum<TaskWorkerStatus>());
    }
}
