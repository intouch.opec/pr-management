using ValidationException = PRManagement.Application.Common.Exceptions.ValidationException;
using PRManagement.Application.PRActivityTasks.Queries.GetPrActivityTasks;

namespace PRManagement.Application.FunctionalTests.PRActivityTasks.Queries;
using static Testing;

public class GetPrActivityTaskTests : BaseTestFixture
{
    [Test]
    public async Task ShouldRequireValidGetPrActivityTasksQuery()
    {
        var command = new GetPrActivityTasksQuery(string.Empty);

        await FluentActions.Invoking(() =>
           SendAsync(command)).Should().ThrowAsync<ValidationException>();
    }

    [Test]
    public async Task ShouldBeGetPrActivityTasksQuery()
    {
        var command = new GetPrActivityTasksQuery("79ab5fe5-80d0-4447-8ea9-06619749431c");

        var result = await SendAsync(command);

        result.Should().NotBeEmpty();
        foreach (var item in result)
        {
            item.Id.Should().NotBeEmpty();
            item.Description.Should().NotBeEmpty();
        }
    }
}
