namespace PRManagement.Application.FunctionalTests.news.Queries;

using PRManagement.Application.News.Queries.GetNewWithPagination;
using PRManagement.Application.TaskWorkers.Commands.CreateTaskWorker;
using static Testing;

public class GetNewWithPaginationTests : BaseTestFixture
{
    [Test]
    public async Task ShouldGetNewWithPagination()
    {
        var news = await SendAsync(new GetNewWithPaginationQuery());
        news.Items.Count().Should().Be(2);
    }

    [Test]
    public async Task ShouldGetNewWithPaginationFilter()
    {
        var command = new CreateTaskWorkerCommand()
        {
            Topic = "Topic xyz",
            KeywordQuery = "KeywordQuery",
            PRActivityId = "9bdcd9ec-a1f6-4a59-9c31-dbbe17bd3c35",
            IsUrgent = false
        };

        var result = await SendAsync(command);
        var news = await SendAsync(new GetNewWithPaginationQuery()
        {
            MonitorTaskWorkerId = result.Id.ToString(),
        });

        news.Items.Count().Should().Be(0);
    }

    [Test]
    public async Task ShouldGetNewWithPaginationFilterPRActivities2()
    {
        var news = await SendAsync(new GetNewWithPaginationQuery()
        {
            PRActivityId = "df8f5ede-634e-488f-b44a-5dd5a023fefa",
        });

        news.Items.Count().Should().Be(2);
    }
}
