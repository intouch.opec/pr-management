namespace PRManagement.Application.FunctionalTests.News.Commands;

using PRManagement.Application.Common.Exceptions;
using PRManagement.Application.News.Commands.MappingNew;
using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;
using static Testing;

public class MappingNewTests : BaseTestFixture
{
    [Test]
    public async Task ShouldRequireMinimumFields()
    {
        var command = new MappingNewCommand();

        await FluentActions.Invoking(() =>
            SendAsync(command)).Should().ThrowAsync<ValidationException>();
    }

    [Test]
    public async Task ShouldBeMappingNewToDone()
    {
        var command = new MappingNewCommand()
        {
            Mapping = MappingType.DONE.ToString(),
            Id = "9042743d-b62f-44be-ac3a-b783efc5c562",
            PlatformId = "5b5f8b31-caf6-4c08-b51e-81eebdf5738a"
        };

        var result = await SendAsync(command);
        var n = await FindAsync<New>(result.Id);

        n!.Mapping.Should().Be(MappingType.DONE);
        n.PlatformId.Should().Be(new Guid(command.PlatformId));
    }

    [Test]
    public async Task ShouldBeMappingNewToPending()
    {
        var command = new MappingNewCommand()
        {
            Mapping = MappingType.PENDING.ToString(),
            Id = "07fdd805-9750-4a97-9986-e949423e49db",
        };

        var result = await SendAsync(command);
        var n = await FindAsync<New>(result.Id);

        n!.Mapping.Should().Be(MappingType.PENDING);
    }

}
