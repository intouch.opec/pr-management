namespace PRManagement.Application.FunctionalTests.BookmarksGroups.Commands;
using PRManagement.Application.Common.Exceptions;
using PRManagement.Application.BookmarkGroups.Commands.UpdateBookmarkGroup;
using PRManagement.Application.FunctionalTests;
using static Testing;
using PRManagement.Application.Medias.Commands.CreateMedia;
using PRManagement.Domain.Enums;
using PRManagement.Application.Contacts.Commands.CreateContact;
using PRManagement.Application.BookmarkGroups.Commands.CreateBookmarkGroup;
using PRManagement.Domain.Entities;

public class UpdateBookmarkGroupTests : BaseTestFixture
{
    [Test]
    public async Task ShouldRequireMinimumFields()
    {
        var command = new UpdateBookmarkGroupCommand();

        await FluentActions.Invoking(() =>
            SendAsync(command)).Should().ThrowAsync<ValidationException>();
    }


    [Test]
    public async Task ShouldUpdateBookmarkGroup()
    {
        var mediaCommand = MockUpCreateMediaCommand();
        var media = await SendAsync(mediaCommand);
        var contactCommand = MockUpCreateContactCommand(media.MediaSectionId);
        var contact = await SendAsync(contactCommand);
        var commandBookmarkGroup = new CreateBookmarkGroupCommand()
        {
            Name = "IT 1",
            ContactIds = new[] { contact.Id.ToString() },
            MediaIds = new[] { media.Id.ToString() },
            Tags = new List<Tag>()
            {
                new Tag()
                {
                    Name = "TAG RED",
                    Color = "RED"
                },
                new Tag()
                {
                    Name = "TAG BLUE",
                    Color = "BLUE"
                },
            }
        };

        var createBookmarkGroup = await SendAsync(commandBookmarkGroup);

        var command = new UpdateBookmarkGroupCommand()
        {
            Id = createBookmarkGroup.Id.ToString(),
            Name = "Change Name",
            Tags = new List<Tag>()
            {
                new Tag()
                {
                    Name = "TAG GREEN",
                    Color = "GREEN"
                },
                new Tag()
                {
                    Name = "TAG COFFEE",
                    Color = "COFFEE"
                },
            }
        };

        var updateBookmarkGroup = await SendAsync(command);

        var bookmarkGroup = await FindAsync<BookmarkGroup>(updateBookmarkGroup.Id);
        bookmarkGroup!.Name.Should().Be(command.Name);
        foreach (var item in bookmarkGroup.Tags)
        {
            command.Tags.Should().Contain(e => e.Name.Equals(item.Name));
            command.Tags.Should().Contain(e => e.Color.Equals(item.Color));
        }
    }


    internal CreateMediaCommand MockUpCreateMediaCommand()
    {
        return new CreateMediaCommand
        {
            Name = "Media Jammy",
            Address = "Address",
            Email = new string[] { "Jammy@email.com" },
            PhoneNumbers = new string[] { "0251394567" },
            ProvinceId = "1",
            DistrictId = "1001",
            SubDistrictId = "100101",
            PostCode = "10200",
            TypeOfMedia = TypeOfMedia.DIGITAL.ToString(),
            SectionId = "56d22a41-6b16-4335-9ba2-dfeb63c5a830",
            SubSectionId = "90a6e746-a379-4603-b39c-c10409314445"
        };
    }

    internal CreateContactCommand MockUpCreateContactCommand(Guid mediaSectionId)
    {
        return new CreateContactCommand
        {
            FirstName = "UpdateBookmarkGroup",
            LastName = "mahamuu1",
            Address = "Address",
            Position = "Position",
            LineId = "LineId",
            Remark = "Remark",
            AddressType = "Home",
            Email = new string[] { "UpdateBookmarkGroup@email.com" },
            PhoneNumbers = new string[] { "0611124567" },
            ProvinceId = "1",
            DistrictId = "1001",
            SubDistrictId = "100101",
            PostCode = "10200",
            MediaSectionId = mediaSectionId.ToString()
        };
    }

    internal CreateMediaCommand MockUpCreateMediaHasAddressMediaSectionCommand()
    {
        return new CreateMediaCommand
        {
            Name = "IT",
            Address = "Address",
            Email = new string[] { "ITAddress@email.com" },
            PhoneNumbers = new string[] { "0651394567" },
            ProvinceId = "1",
            DistrictId = "1001",
            SubDistrictId = "100101",
            PostCode = "10200",
            TypeOfMedia = TypeOfMedia.DIGITAL.ToString(),
            SectionId = "56d22a41-6b16-4335-9ba2-dfeb63c5a830",
            SubSectionId = "90a6e746-a379-4603-b39c-c10409314445",
            MediaSectionEmail = new string[] { "ITAddress2@email.com" },
            MediaSectionPhoneNumbers = new string[] { "0911225567" },
        };
    }
}
