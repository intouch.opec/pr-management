namespace PRManagement.Application.FunctionalTests.Bookmarks.Commands;
using PRManagement.Application.Common.Exceptions;
using PRManagement.Application.Bookmarks.Commands.CreateBookmarkContacts;
using static Testing;
using PRManagement.Domain.Enums;
using PRManagement.Application.Medias.Commands.CreateMedia;
using PRManagement.Domain.Entities;
using PRManagement.Application.Contacts.Commands.CreateContact;
using PRManagement.Application.Bookmarks.Queries.GetBookmarkWithPagination;

public class CreateBookmarkContactsCommandTests : BaseTestFixture
{
    [Test]
    public async Task ShouldRequireMinimumFields()
    {
        var command = new CreateBookmarkContactsCommand();

        await FluentActions.Invoking(() =>
            SendAsync(command)).Should().ThrowAsync<ValidationException>();
    }

    [Test]
    public async Task ShouldCreateBookmarkGroup()
    {
        var mediaCommand = MockUpCreateMediaCommand();
        var media = await SendAsync(mediaCommand);
        var contactCommand = MockUpCreateContactCommand(media.MediaSectionId);
        var contact = await SendAsync(contactCommand);
        var command = new CreateBookmarkContactsCommand()
        {
            Name = "IT 999",
            SectionId = "56d22a41-6b16-4335-9ba2-dfeb63c5a830",
            SubSectionId = "90a6e746-a379-4603-b39c-c10409314445",
            Tags = new List<Tag>()
            {
                new Tag()
                {
                    Name = "TAG GREEN",
                    Color = "GREEN"
                },
                new Tag()
                {
                    Name = "TAG COFFEE",
                    Color = "COFFEE"
                },
            }
        };

        var response = await SendAsync(command);

        var bookMarkGroup = await FindAsync<BookmarkGroup>(response.Id);

        bookMarkGroup.Should().NotBeNull();
        bookMarkGroup!.Name.Should().Be(command.Name);
        bookMarkGroup.Tags.Count.Should().Be(command.Tags.Count);
        foreach (var item in bookMarkGroup.Tags)
        {
            var tag = bookMarkGroup.Tags.Find(e => e.Name.Equals(item.Name));
            item.Name.Should().Be(tag!.Name);
            item.Color.Should().Be(tag!.Color);
        }
        var bookmarks = await SendAsync(new GetBookmarkWithPaginationQuery()
            {
                BookMarkGroupId = bookMarkGroup.Id.ToString()
            });

        bookmarks.Items.Count.Should().NotBe(0);
        
    }

    internal CreateMediaCommand MockUpCreateMediaCommand()
    {
        return new CreateMediaCommand
        {
            Name = "Bookmark Contacts",
            Address = "Address",
            Email = new string[] { "BookmarkContacts@email.com" },
            PhoneNumbers = new string[] { "0652156767" },
            ProvinceId = "1",
            DistrictId = "1001",
            SubDistrictId = "100101",
            PostCode = "10200",
            TypeOfMedia = TypeOfMedia.DIGITAL.ToString(),
            SectionId = "56d22a41-6b16-4335-9ba2-dfeb63c5a830",
            SubSectionId = "90a6e746-a379-4603-b39c-c10409314445"
        };
    }

    internal CreateContactCommand MockUpCreateContactCommand(Guid mediaSectionId)
    {
        return new CreateContactCommand
        {
            FirstName = "BookmarkContacts",
            LastName = "BookmarkContacts",
            Address = "Address",
            Position = "Position",
            LineId = "LineId",
            Remark = "Remark",
            AddressType = "Home",
            Email = new string[] { "BookmarkContacts1@email.com" },
            PhoneNumbers = new string[] { "0613134367" },
            ProvinceId = "1",
            DistrictId = "1001",
            SubDistrictId = "100101",
            PostCode = "10200",
            MediaSectionId = mediaSectionId.ToString()
        };
    }

}
