namespace PRManagement.Application.FunctionalTests.BookmarksGroups.Commands;

using PRManagement.Application.BookmarkGroups.Commands.CreateBookmarkGroup;
using PRManagement.Application.Common.Exceptions;
using PRManagement.Application.Contacts.Commands.CreateContact;
using PRManagement.Application.Medias.Commands.CreateMedia;
using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;
using static Testing;

public class CreateBookmarkGroupTests : BaseTestFixture
{
    [Test]
    public async Task ShouldRequireMinimumFields()
    {
        var command = new CreateBookmarkGroupCommand();

        await FluentActions.Invoking(() =>
            SendAsync(command)).Should().ThrowAsync<ValidationException>();
    }

    [Test]
    public async Task ShouldCreateBookmarkGroup()
    {
        var mediaCommand = MockUpCreateMediaCommand();
        var media = await SendAsync(mediaCommand);
        var contactCommand = MockUpCreateContactCommand(media.MediaSectionId);
        var contact = await SendAsync(contactCommand);
        var command = new CreateBookmarkGroupCommand()
        {
            Name = "Bookmark T1",
            ContactIds = new[] { contact.Id.ToString() },
            MediaIds = new[] { media.Id.ToString() },
            Tags = new List<Tag>()
            {
                new Tag()
                {
                    Name = "TAG GREEN",
                    Color = "GREEN"
                },
                new Tag()
                {
                    Name = "TAG COFFEE",
                    Color = "COFFEE"
                },
            }
        };

        var response = await SendAsync(command);

        var bookMarkGroup = await FindAsync<BookmarkGroup>(response.Id);

        bookMarkGroup.Should().NotBeNull();
        bookMarkGroup!.Name.Should().Be(command.Name);
        bookMarkGroup.Tags.Count.Should().Be(command.Tags.Count);
        foreach (var item in bookMarkGroup.Tags)
        {
            var tag = bookMarkGroup.Tags.Find(e => e.Name.Equals(item.Name));
            item.Name.Should().Be(tag!.Name);
            item.Color.Should().Be(tag!.Color);
        }
    }

    [Test]
    public async Task ShouldCreateBookmarkGroupMediaSection()
    {
        var mediaCommand = MockUpCreateMediaHasAddressMediaSectionCommand();
        var media = await SendAsync(mediaCommand);
        var command = new CreateBookmarkGroupCommand()
        {
            Name = "Bookmark T2",
            MediaSectionIds = new[] { media.MediaSectionId.ToString() }
        };

        var response = await SendAsync(command);

        var bookMarkGroup = await FindAsync<BookmarkGroup>(response.Id);

        bookMarkGroup.Should().NotBeNull();
        bookMarkGroup!.Name.Should().Be(command.Name);
    }

    internal CreateMediaCommand MockUpCreateMediaCommand()
    {
        return new CreateMediaCommand
        {
            Name = "Media Name Jim",
            Address = "Address",
            Email = new string[] { "jim@email.com" },
            PhoneNumbers = new string[] { "0651394567" },
            ProvinceId = "1",
            DistrictId = "1001",
            SubDistrictId = "100101",
            PostCode = "10200",
            TypeOfMedia = TypeOfMedia.DIGITAL.ToString(),
            SectionId = "56d22a41-6b16-4335-9ba2-dfeb63c5a830",
            SubSectionId = "90a6e746-a379-4603-b39c-c10409314445"
        };
    }

    internal CreateContactCommand MockUpCreateContactCommand(Guid mediaSectionId)
    {
        return new CreateContactCommand
        {
            FirstName = "Porndee1",
            LastName = "mahamuu1",
            Address = "Address",
            Position = "Position",
            LineId = "LineId",
            Remark = "Remark",
            AddressType = "Home",
            Email = new string[] { "Porndee@email.com" },
            PhoneNumbers = new string[] { "0611134567" },
            ProvinceId = "1",
            DistrictId = "1001",
            SubDistrictId = "100101",
            PostCode = "10200",
            MediaSectionId = mediaSectionId.ToString()
        };
    }

    internal CreateMediaCommand MockUpCreateMediaHasAddressMediaSectionCommand()
    {
        return new CreateMediaCommand
        {
            Name = "Jim & pim",
            Address = "Address",
            Email = new string[] { "jim@email.com" },
            PhoneNumbers = new string[] { "0651394567" },
            ProvinceId = "1",
            DistrictId = "1001",
            SubDistrictId = "100101",
            PostCode = "10200",
            TypeOfMedia = TypeOfMedia.DIGITAL.ToString(),
            SectionId = "56d22a41-6b16-4335-9ba2-dfeb63c5a830",
            SubSectionId = "90a6e746-a379-4603-b39c-c10409314445",
            MediaSectionEmail = new string[] { "jinny@email.com" },
            MediaSectionPhoneNumbers = new string[] { "0911115567" },
        };
    }

}
