namespace PRManagement.Application.FunctionalTests.Platforms.Queries;

using PRManagement.Application.Users.Queries.GetUserWithPagination;
using static Testing;
public class GetUsersTests : BaseTestFixture
{
    [Test]
    public async Task ShouldReturnGetUsers()
    {
        var result = await SendAsync(new GetUsersQuery());
        result.Should().NotBeEmpty();

        result.Count().Should().Be(3);
    }
}
