using ValidationException = PRManagement.Application.Common.Exceptions.ValidationException;
using PRManagement.Application.WorkProgressList.Queries.GetWorkProgressByPrActivityId;

namespace PRManagement.Application.FunctionalTests.WorkProgressList.Queries;
using static Testing;

public class GetWorkProgressByPrActivityIdTests : BaseTestFixture
{
    [Test]
    public async Task ShouldRequireValidGetWorkProgressByPrActivityIdQuery()
    {
        var command = new GetWorkProgressByPrActivityIdQuery(string.Empty);

        await FluentActions.Invoking(() =>
           SendAsync(command)).Should().ThrowAsync<ValidationException>();
    }

    [Test]
    public async Task ShouldBeGetWorkProgressByPrActivityId()
    {
        var command = new GetWorkProgressByPrActivityIdQuery("79ab5fe5-80d0-4447-8ea9-06619749431c");

        var result = await SendAsync(command);
        result.SummaryWorkProgress.WorkProgressCount.Should().Be(4);
        result.SummaryWorkProgress.WorkProgressCompleteCount.Should().Be(0);
        result.SummaryWorkProgress.WorkProgressPercent.Should().Be(0);
        result.WorkProgress.Count().Should().Be(4);
    }
}
