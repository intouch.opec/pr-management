FROM mcr.microsoft.com/dotnet/sdk:8.0-alpine3.18 AS build
ARG TARGETARCH
WORKDIR /source

COPY ["src/Application/Application.csproj", "Application/"]
COPY ["src/Domain/Domain.csproj", "Domain/"]
COPY ["src/Infrastructure/Infrastructure.csproj", "Infrastructure/"]
COPY ["src/WebAPI/WebAPI.csproj", "WebAPI/"]
COPY ["Directory.Packages.props", "./"]
COPY ["Directory.Build.props", "./"]
RUN dotnet restore "./WebAPI/WebAPI.csproj"

COPY . ../
WORKDIR /src/WebAPI
RUN dotnet build "WebAPI.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish --no-restore -c Release -o /app/publish

FROM mcr.microsoft.com/dotnet/aspnet:8.0-alpine3.18
EXPOSE 8080
WORKDIR /app
COPY --from=publish /app/publish .
COPY excel-templates/contact-template.xlsx /excel-templates/contact-template.xlsx
ENTRYPOINT ["dotnet", "WebAPI.dll"]
