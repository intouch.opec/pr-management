﻿using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Constants;
using PRManagement.Infrastructure.Data;
using PRManagement.Infrastructure.Data.Interceptors;
using PRManagement.Infrastructure.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Configuration;
using PRManagement.Application.Common.Configurations;
using PRManagement.Infrastructure.GenerateExcels.LocalGenerateExcels;
using PRManagement.Infrastructure.EmailServices;
using System.Reflection;
using PRManagement.Domain.Entities;
using SMS_Management.Infrastructure.UploadFiles;

namespace Microsoft.Extensions.DependencyInjection;
public static class DependencyInjection
{
    public static IServiceCollection AddInfrastructureServices(this IServiceCollection services, IConfiguration configuration)
    {
        var connectionString = configuration.GetConnectionString("DefaultConnection");
        services.AddAutoMapper(Assembly.GetExecutingAssembly());

        Guard.Against.Null(connectionString, message: "Connection string 'DefaultConnection' not found.");

        services.AddScoped<ISaveChangesInterceptor, AuditableEntityInterceptor>();
        services.AddScoped<ISaveChangesInterceptor, DispatchDomainEventsInterceptor>();

        services.AddDbContext<ApplicationDbContext>((sp, options) =>
        {
            options.AddInterceptors(sp.GetServices<ISaveChangesInterceptor>());
            options.UseNpgsql(connectionString);
        });

        services.AddScoped<IApplicationDbContext>(provider => provider.GetRequiredService<ApplicationDbContext>());

        services.AddScoped<ApplicationDbContextInitialiser>();

        services
            .AddIdentityApiEndpoints<ApplicationUser>()
            .AddRoles<IdentityRole>()
            .AddEntityFrameworkStores<ApplicationDbContext>();

        services.AddAuthentication(IdentityConstants.BearerScheme);

        services.AddAuthorizationBuilder();

        services.AddSingleton(TimeProvider.System);
        services.AddTransient<IIdentityService, IdentityService>();
        services.AddTransient<IGenerateExcel, LocalGenerateExcelsService>();
        services.AddTransient<IMailService, OutLookEmailService>();
        services.AddTransient<IUploadFileService, UploadFileLocalService>();

        services.AddAuthorization(options =>
        {
            options.AddPolicy(Policies.CanPurge, policy => policy.RequireRole(Roles.Administrator));
            options.AddPolicy(Policies.CanManageMedia, policy => policy.RequireRole(Roles.PRTeam));
            options.AddPolicy(Policies.CanManageContact, policy => policy.RequireRole(Roles.ADTeam));
        });

        services.AddConfigurationServices<DefaultPlatFromCalculationConfiguration>(configuration);
        services.AddConfigurationServices<ExportExcelConfiguration>(configuration);
        services.AddConfigurationServices<SmtpServerConfiguration>(configuration);
        services.AddConfigurationServices<ApplicationSettingConfiguration>(configuration);

        return services;
    }

    public static IServiceCollection AddConfigurationServices<TConfig>(this IServiceCollection services, IConfiguration configuration)
        where TConfig : class
    {
        var configurationSection = configuration.GetSection(typeof(TConfig).Name);
        return services.Configure<TConfig>(configurationSection);
    }

}
