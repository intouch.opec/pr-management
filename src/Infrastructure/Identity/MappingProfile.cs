using AutoMapper;
using PRManagement.Application.Common.Models;
using PRManagement.Domain.Entities;

namespace PRManagement.Infrastructure.Identity;

public class MappingProfile
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<ApplicationUser, OptionItemBase>()
                .ForMember(dest => dest.Id, op => op.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, op => op.MapFrom(src => src.Email));

        }
    }
}
