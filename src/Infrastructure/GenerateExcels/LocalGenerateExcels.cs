using NPOI.XSSF.UserModel;
using PRManagement.Application.Common.Interfaces;

namespace PRManagement.Infrastructure.GenerateExcels.LocalGenerateExcels;

public class LocalGenerateExcelsService : IGenerateExcel
{

    public async Task<byte[]> GenerateExcelAsync(string templateFile, string header, IEnumerable<IEnumerable<string>> records)
    {
        using var templateStream = new FileStream(templateFile, FileMode.Open, FileAccess.Read, FileShare.Read, 4096, true);
        var workbook = new XSSFWorkbook(templateStream);
        var sheet = workbook.GetSheetAt(0);

        var rowHeader = sheet.CreateRow(0);
        rowHeader.CreateCell(0).SetCellValue(header);
        
        int rowIndex = 2;
        foreach (var record in records)
        {
            var row = sheet.CreateRow(rowIndex);
            int cellIndex = 0;
            foreach (var cellValue in record)
            {
                var cell = row.CreateCell(cellIndex);
                cell.SetCellValue(cellValue);
                cellIndex++;
            }
            rowIndex++;
        }
        using var memoryStream = new MemoryStream();
        workbook.Write(memoryStream);

        return await Task.FromResult(memoryStream.ToArray());
    }

}