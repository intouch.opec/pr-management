﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PRManagement.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class Initial3 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PRActivityEvent_PRActivities_PRActivityId",
                table: "PRActivityEvent");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PRActivityEvent",
                table: "PRActivityEvent");

            migrationBuilder.RenameTable(
                name: "PRActivityEvent",
                newName: "PRActivityEvents");

            migrationBuilder.RenameIndex(
                name: "IX_PRActivityEvent_PRActivityId",
                table: "PRActivityEvents",
                newName: "IX_PRActivityEvents_PRActivityId");

            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "PRActivityReceiver",
                type: "character varying(10)",
                maxLength: 10,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PRActivityEvents",
                table: "PRActivityEvents",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_PRActivityEvents_PRActivities_PRActivityId",
                table: "PRActivityEvents",
                column: "PRActivityId",
                principalTable: "PRActivities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PRActivityEvents_PRActivities_PRActivityId",
                table: "PRActivityEvents");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PRActivityEvents",
                table: "PRActivityEvents");

            migrationBuilder.DropColumn(
                name: "Code",
                table: "PRActivityReceiver");

            migrationBuilder.RenameTable(
                name: "PRActivityEvents",
                newName: "PRActivityEvent");

            migrationBuilder.RenameIndex(
                name: "IX_PRActivityEvents_PRActivityId",
                table: "PRActivityEvent",
                newName: "IX_PRActivityEvent_PRActivityId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PRActivityEvent",
                table: "PRActivityEvent",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_PRActivityEvent_PRActivities_PRActivityId",
                table: "PRActivityEvent",
                column: "PRActivityId",
                principalTable: "PRActivities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
