﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PRManagement.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class Initial2 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PRActivityEventJoiners",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    ContactId = table.Column<Guid>(type: "uuid", nullable: false),
                    PRActivityId = table.Column<Guid>(type: "uuid", nullable: false),
                    JoinType = table.Column<int>(type: "integer", nullable: false),
                    Created = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    CreatedBy = table.Column<string>(type: "text", nullable: true),
                    LastModified = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    LastModifiedBy = table.Column<string>(type: "text", nullable: true),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    Deleted = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PRActivityEventJoiners", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PRActivityEventJoiners_Contacts_ContactId",
                        column: x => x.ContactId,
                        principalTable: "Contacts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PRActivityEventJoiners_PRActivities_PRActivityId",
                        column: x => x.PRActivityId,
                        principalTable: "PRActivities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PRActivityEventJoiners_ContactId",
                table: "PRActivityEventJoiners",
                column: "ContactId");

            migrationBuilder.CreateIndex(
                name: "IX_PRActivityEventJoiners_PRActivityId",
                table: "PRActivityEventJoiners",
                column: "PRActivityId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PRActivityEventJoiners");
        }
    }
}
