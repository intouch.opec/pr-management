using System.Reflection;
using System.Security;
using System.Text;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Extensions.Options;
using PRManagement.Application.Common.Configurations;
using PRManagement.Application.Common.Interfaces;

namespace SMS_Management.Infrastructure.UploadFiles;

public class UploadFileLocalService : IUploadFileService
{
    private readonly ApplicationSettingConfiguration _applicationSettingConfiguration;

    public UploadFileLocalService(IOptions<ApplicationSettingConfiguration> applicationSettingConfiguration)
    {
        _applicationSettingConfiguration = applicationSettingConfiguration.Value;
    }

    public async Task<string> UploadFileAsync(string fileName, IFormFile file)
    {
        if (file == null || file.Length == 0)
            throw new ArgumentNullException(nameof(file));

        if (!Directory.Exists(_applicationSettingConfiguration.DirectoryPath))
            Directory.CreateDirectory(_applicationSettingConfiguration.DirectoryPath);

        var (filePath, fullPath) = GetUniqueFileName(fileName);

        using var stream = new FileStream(fullPath, FileMode.Create);
        await file.CopyToAsync(stream);

        return filePath;
    }

    public void ValidateFileAllow(IEnumerable<string> fileTypes, IFormFile file)
    {
        if (!fileTypes.Any(type => type.Equals(file.ContentType)))
            throw new ArgumentNullException(nameof(file));
    }

    internal (string, string) CheckOrCreateFolder()
    {
        var folderName = Guid.NewGuid().ToString().Substring(0, 4);
        var directoryPath = new StringBuilder(
            Path.Combine(
                _applicationSettingConfiguration.DirectoryPath,
                folderName)
            ).ToString();

        if (!Directory.Exists(directoryPath))
        {
            Directory.CreateDirectory(directoryPath);
        }

        return (folderName, directoryPath);
    }

    internal (string, string) GetUniqueFileName(string fileName)
    {
        var (folderName, fullPath) = CheckOrCreateFolder();
        var uniqueFileName = $"{Guid.NewGuid().ToString().Substring(0, 4)}{Path.GetExtension(fileName)}";
        fullPath = Path.Combine(fullPath, uniqueFileName);
        return (Path.Combine(folderName, uniqueFileName), fullPath);
    }

    public async Task<byte[]> GetFileAsync(string path)
    {
        var fullPath = Path.GetFullPath(Path.Combine(_applicationSettingConfiguration.DirectoryPath, path));
        if (fullPath.Contains(".."))
            throw new SecurityException("Invalid file path detected.");

        return await File.ReadAllBytesAsync(fullPath);
    }

    public string GetContentType(string fileName)
    {
        var provider = new FileExtensionContentTypeProvider();
        if (!provider.TryGetContentType(Path.Combine(
                    _applicationSettingConfiguration.DirectoryPath,
                    fileName),
                out string? contentType))
            contentType = "application/octet-stream";


        return contentType;
    }

    public void RemoveFile(string fileName)
    {
        var path = Path.Combine(_applicationSettingConfiguration.DirectoryPath, fileName);
        string fullPath = Path.GetFullPath(path);
        if (!fullPath.StartsWith(Path.GetFullPath(_applicationSettingConfiguration.DirectoryPath)) || fullPath.Contains(".."))
            throw new SecurityException("Invalid file path detected.");

        if (File.Exists(fullPath))
            File.Delete(fullPath);
        else
            throw new ArgumentNullException($"The file name {fileName} not found.");
    }

}