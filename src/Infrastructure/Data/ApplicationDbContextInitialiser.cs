﻿using PRManagement.Domain.Constants;
using PRManagement.Domain.Entities;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Text.Json;
using PRManagement.Domain.Exceptions;
using PRManagement.Domain.Enums;
using PRManagement.Application.Common.Extensions;
using System.Text.RegularExpressions;

namespace PRManagement.Infrastructure.Data;

public static class InitialiserExtensions
{
    public static async Task InitialiseDatabaseAsync(this WebApplication app, IConfiguration configuration)
    {

        using var scope = app.Services.CreateScope();

        var initialiser = scope.ServiceProvider.GetRequiredService<ApplicationDbContextInitialiser>();

        var isMigration = configuration.GetValue<bool>("ConnectionStrings:IsMigration");
        if (isMigration)
            await initialiser.InitialiseAsync();

        var isSeed = configuration.GetValue<bool>("ConnectionStrings:IsSeed");
        if (isSeed)
            await initialiser.SeedAsync(configuration.GetConnectionString("DefaultConnection"));
    }
}

public class ApplicationDbContextInitialiser
{
    private readonly ILogger<ApplicationDbContextInitialiser> _logger;
    private readonly ApplicationDbContext _context;
    private readonly UserManager<ApplicationUser> _userManager;
    private readonly RoleManager<IdentityRole> _roleManager;
    private readonly string _currentDirectory = Directory.GetCurrentDirectory().Replace("/src/WebAPI", "/csv");

    public ApplicationDbContextInitialiser(ILogger<ApplicationDbContextInitialiser> logger, ApplicationDbContext context, UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager)
    {
        _logger = logger;
        _context = context;
        _userManager = userManager;
        _roleManager = roleManager;
    }

    public async Task InitialiseAsync()
    {
        try
        {
            await _context.Database.MigrateAsync();
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "An error occurred while initialising the database.");
            throw;
        }
    }

    public async Task SeedAsync(string? connectionString)
    {
        try
        {
            await TrySeedAsync(connectionString);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "An error occurred while seeding the database.");
            throw;
        }
    }

    public async Task TrySeedAsync(string? connectionString)
    {
        var administratorRole = new IdentityRole(Roles.Administrator);
        var adTeamRole = new IdentityRole(Roles.ADTeam);
        var prTeamRole = new IdentityRole(Roles.PRTeam);

        if (await _roleManager.Roles.AllAsync(r => r.Name != administratorRole.Name))
            await _roleManager.CreateAsync(administratorRole);

        if (await _roleManager.Roles.AllAsync(r => r.Name != adTeamRole.Name))
            await _roleManager.CreateAsync(adTeamRole);

        if (await _roleManager.Roles.AllAsync(r => r.Name != prTeamRole.Name))
            await _roleManager.CreateAsync(prTeamRole);

        var administrator = new ApplicationUser { UserName = "administrator@localhost", Email = "administrator@localhost" };
        var administrator2 = new ApplicationUser { UserName = "Anyaporn@agatethai.com", Email = "Anyaporn@agatethai.com" };
        var administrator3 = new ApplicationUser { UserName = "Newsclipping@agatethai.com", Email = "Newsclipping@agatethai.com" };
        var administrator4 = new ApplicationUser { UserName = "Savittre@agatethai.com", Email = "Savittre@agatethai.com" };
        var administrator5 = new ApplicationUser { UserName = "Jirakul@agatethai.com", Email = "Jirakul@agatethai.com" };
        var administrator6 = new ApplicationUser { UserName = "Pattama@agatethai.com", Email = "Pattama@agatethai.com" };

        if (await _userManager.Users.AllAsync(u => u.UserName != administrator.UserName))
        {
            await _userManager.CreateAsync(administrator, "Administrator1!");
            if (!string.IsNullOrWhiteSpace(administratorRole.Name))
                await _userManager.AddToRolesAsync(administrator, new[] { administratorRole.Name });
        }

        if (await _userManager.Users.AllAsync(u => u.UserName != administrator2.UserName))
        {
            await _userManager.CreateAsync(administrator2, "Administrator1!");
            if (!string.IsNullOrWhiteSpace(administratorRole.Name))
                await _userManager.AddToRolesAsync(administrator2, new[] { administratorRole.Name });
        }

        if (await _userManager.Users.AllAsync(u => u.UserName != administrator3.UserName))
        {
            await _userManager.CreateAsync(administrator3, "Administrator1!");
            if (!string.IsNullOrWhiteSpace(administratorRole.Name))
                await _userManager.AddToRolesAsync(administrator3, new[] { administratorRole.Name });
        }

        if (await _userManager.Users.AllAsync(u => u.UserName != administrator4.UserName))
        {
            await _userManager.CreateAsync(administrator4, "Administrator1!");
            if (!string.IsNullOrWhiteSpace(administratorRole.Name))
                await _userManager.AddToRolesAsync(administrator4, new[] { administratorRole.Name });
        }

        if (await _userManager.Users.AllAsync(u => u.UserName != administrator5.UserName))
        {
            await _userManager.CreateAsync(administrator5, "Administrator1!");
            if (!string.IsNullOrWhiteSpace(administratorRole.Name))
                await _userManager.AddToRolesAsync(administrator5, new[] { administratorRole.Name });
        }

        if (await _userManager.Users.AllAsync(u => u.UserName != administrator6.UserName))
        {
            await _userManager.CreateAsync(administrator6, "Administrator1!");
            if (!string.IsNullOrWhiteSpace(administratorRole.Name))
                await _userManager.AddToRolesAsync(administrator6, new[] { administratorRole.Name });
        }
        // Default data
        // Seed, if necessary
        await AddLocation(connectionString);
        await AddSections();
        await AddSubSections();
        await AddMediasAsync(connectionString);
        await AddMediaSections(connectionString);
        await AddContactAsync(connectionString);
        await AddPlatform(connectionString);
        await UpdatePlatform();

        await SeedSections();
        await SeedMedia();
        await SeedContact();
        await SeedMediaSection();
        await SeedPlatform();
        await SeedClient();
        await SeedUser();
        await SeedPRActivity();
        await SeedPRActivityBriefing();
        await SeedPRActivityEmail();
        await SeedPRActivityLocation();
        await SeedMonitorTaskWorker();
    }

    public async Task<IEnumerable<string[]?>> ReadCsv(string filePath)
    {
        var rows = new List<string[]?>();

        using var reader = new StreamReader(filePath);
        while (!reader.EndOfStream)
        {
            var line = await reader.ReadLineAsync();
            var values = line!.Split(',');
            rows.Add(values);
        }
        return rows;
    }

    internal async Task AddLocation(string? connectionString)
    {
        using var conn = new NpgsqlConnection(connectionString);
        await conn.OpenAsync();
        if (!await _context.Provinces.AnyAsync())
        {
            using var provinceWriter = await conn.BeginBinaryImportAsync("COPY public.\"Provinces\" FROM STDIN (FORMAT BINARY);");
            var currentDirectory = Path.Combine(_currentDirectory, "thai_provinces.csv");

            using var reader = new StreamReader(currentDirectory);
            while (!reader.EndOfStream)
            {

                var line = await reader.ReadLineAsync();
                var values = line!.Split(',');
                await provinceWriter.StartRowAsync();
                await provinceWriter.WriteAsync(values[0], NpgsqlTypes.NpgsqlDbType.Text);
                await provinceWriter.WriteAsync(values[1], NpgsqlTypes.NpgsqlDbType.Text);
                await provinceWriter.WriteAsync(values[2], NpgsqlTypes.NpgsqlDbType.Text);

            }
            await provinceWriter.CompleteAsync();
        }

        if (!await _context.Districts.AnyAsync())
        {
            using var districtWriter = await conn.BeginBinaryImportAsync("COPY public.\"Districts\" FROM STDIN (FORMAT BINARY);");
            var currentDirectory = Path.Combine(_currentDirectory, "thai_amphures.csv");

            using var reader2 = new StreamReader(currentDirectory);
            while (!reader2.EndOfStream)
            {
                var line = await reader2.ReadLineAsync();
                var values = line!.Split(',');
                await districtWriter.StartRowAsync();
                await districtWriter.WriteAsync(values[0], NpgsqlTypes.NpgsqlDbType.Text);
                await districtWriter.WriteAsync(values[1], NpgsqlTypes.NpgsqlDbType.Text);
                await districtWriter.WriteAsync(values[2], NpgsqlTypes.NpgsqlDbType.Text);
                await districtWriter.WriteAsync(values[3], NpgsqlTypes.NpgsqlDbType.Text);
            }
            await districtWriter.CompleteAsync();
        }

        if (!await _context.SubDistricts.AnyAsync())
        {
            using var subDistrictWriter = await conn.BeginBinaryImportAsync("COPY public.\"SubDistricts\" FROM STDIN (FORMAT BINARY);");
            var currentDirectory = Path.Combine(_currentDirectory, "thai_tambons.csv");

            using var subDistrictsCsv = new StreamReader(currentDirectory);
            while (!subDistrictsCsv.EndOfStream)
            {
                var line = await subDistrictsCsv.ReadLineAsync();
                var values = line!.Split(',');
                await subDistrictWriter.StartRowAsync();
                await subDistrictWriter.WriteAsync(values[0], NpgsqlTypes.NpgsqlDbType.Text);
                await subDistrictWriter.WriteAsync(values[2], NpgsqlTypes.NpgsqlDbType.Text);
                await subDistrictWriter.WriteAsync(values[3], NpgsqlTypes.NpgsqlDbType.Text);
                await subDistrictWriter.WriteAsync(values[1], NpgsqlTypes.NpgsqlDbType.Text);
                await subDistrictWriter.WriteAsync(values[4], NpgsqlTypes.NpgsqlDbType.Text);
            }
            await subDistrictWriter.CompleteAsync();
        }
        await conn.CloseAsync();

    }

    internal async Task AddSections()
    {
        if (!await _context.Sections.AnyAsync())
        {
            var currentDirectory = Path.Combine(_currentDirectory, "sections.json");
            var jsonString = await File.ReadAllTextAsync(currentDirectory);
            var sectionJson = JsonSerializer.Deserialize<Dictionary<string, List<FormulaForCalculation>>>(jsonString);

            var sectionOptions = new List<Section>();

            foreach (var item in sectionJson!.ToList())
            {
                sectionOptions.Add(new()
                {
                    Name = item.Key,
                    FormulaForCalculations = item.Value
                });
            }

            await _context.Sections.AddRangeAsync(sectionOptions);
        }
    }

    internal async Task AddSubSections()
    {
        if (!await _context.SubSections.AnyAsync())
        {
            var currentDirectory = Path.Combine(_currentDirectory, "sub_sections.csv");
            using var reader2 = new StreamReader(currentDirectory);
            var subSectionOptions = new List<SubSection>();
            while (!reader2.EndOfStream)
            {
                var line = await reader2.ReadLineAsync();
                var values = line!.Split('|');
                var section = await _context.Sections.FirstOrDefaultAsync(sub => sub.Name.Equals(values[0]));
                if (section != null)
                    subSectionOptions.Add(new SubSection()
                    {
                        Name = values[1],
                        SectionId = section!.Id
                    });
            }

            await _context.SubSections.AddRangeAsync(subSectionOptions);

            await _context.SaveChangesAsync();
        }
    }

    internal async Task AddMediasAsync(string? connectionString)
    {
        if (await _context.Medias.AnyAsync()) return;
        using var conn = new NpgsqlConnection(connectionString);
        using var conn2 = new NpgsqlConnection(connectionString);
        await conn.OpenAsync();
        await conn2.OpenAsync();
        var provinces = await _context.Provinces.ToListAsync();
        var districts = await _context.Districts.ToListAsync();
        var subDistricts = await _context.SubDistricts.ToListAsync();
        var currentDirectory = Path.Combine(_currentDirectory, "data.csv");
        using var reader = new StreamReader(currentDirectory);
        using var mediaWriter = await conn.BeginBinaryImportAsync("COPY public.\"Medias\" FROM STDIN (FORMAT BINARY);");
        using var addressWriter = await conn2.BeginBinaryImportAsync("COPY public.\"Address\" FROM STDIN (FORMAT BINARY);");
        var medias = new Dictionary<string, int>();
        var dateNow = DateTimeOffset.UtcNow;

        while (!reader.EndOfStream)
        {
            var line = await reader.ReadLineAsync();
            var values = line!.Split('|');

            if (string.IsNullOrWhiteSpace(values[1]))
                continue;
            if (medias.TryGetValue(values[1], out var count))
            {
                medias[values[1]] = count + 1;
                continue;
            }

            if (values == null || values.Length < 8)
                continue;
            var addressId = Guid.NewGuid();
            var mediaId = Guid.NewGuid();
            var createdBy = "System";
            await addressWriter.StartRowAsync();
            var emails = string.IsNullOrWhiteSpace(values[8]) ? new List<string>() : new List<string>() { values[8] };
            var phoneNumbers = string.IsNullOrWhiteSpace(values[5]) ? new List<string>() : new List<string>() { values[5] };
            await addressWriter.WriteAsync(addressId, NpgsqlTypes.NpgsqlDbType.Uuid);
            await addressWriter.WriteAsync(emails.ToArray(), NpgsqlTypes.NpgsqlDbType.Array | NpgsqlTypes.NpgsqlDbType.Text);
            await addressWriter.WriteAsync(phoneNumbers.ToArray(), NpgsqlTypes.NpgsqlDbType.Array | NpgsqlTypes.NpgsqlDbType.Text);
            var provinceId = string.Empty;
            var districtId = string.Empty;
            var subDistrictId = string.Empty;
            var postCode = string.Empty;
            var patternReplace = @"(แขวง|เขต|จังหวัด|อำเภอ|ตำบล)\s*\w+";
            var textAddress = string.Empty;

            if (values.Length > 15 && !string.IsNullOrWhiteSpace(values[14]))
            {
                textAddress = Regex.Replace(values[14], patternReplace, string.Empty);

                foreach (var province in provinces)
                {
                    var match = Regex.Match(values[14], $"{province.NameTH}|{province.NameEN}");
                    if (match.Success)
                    {
                        textAddress = Regex.Replace(textAddress, $"{province.NameTH}|{province.NameEN}", string.Empty);
                        provinceId = province.Id;
                        continue;
                    }
                }

                if (string.IsNullOrWhiteSpace(districtId))
                {
                    foreach (var district in districts.Where(e => e.ProvinceId == provinceId))
                    {
                        var match = Regex.Match(values[14], $"{district.NameTH}|{district.NameEN}");
                        if (match.Success)
                        {
                            textAddress = Regex.Replace(textAddress, $"{district.NameTH}|{district.NameEN}", string.Empty);
                            districtId = district.Id;
                            continue;
                        }
                    }
                }

                foreach (var subDistrict in subDistricts.Where(e => e.DistrictId == districtId))
                {
                    var match = Regex.Match(values[14], $"{subDistrict.NameTH}|{subDistrict.NameEN}");
                    if (match.Success)
                    {
                        subDistrictId = subDistrict.Id;
                        postCode = subDistrict.PostCode;
                        textAddress = Regex.Replace(textAddress, $"{subDistrict.NameTH}|{subDistrict.NameEN}", string.Empty);
                        textAddress = Regex.Replace(textAddress, subDistrict.PostCode, string.Empty);
                        districtId = subDistrict.DistrictId;
                        continue;
                    }
                }

            }
            if (string.IsNullOrWhiteSpace(textAddress))
            { await addressWriter.WriteAsync(string.Empty, NpgsqlTypes.NpgsqlDbType.Text); }
            else
            {
                textAddress = Regex.Replace(textAddress, $"  ", string.Empty);
                textAddress = Regex.Replace(textAddress, $"\"", string.Empty);
                await addressWriter.WriteAsync(textAddress, NpgsqlTypes.NpgsqlDbType.Text);
            }
            if (string.IsNullOrWhiteSpace(postCode))
            { await addressWriter.WriteAsync(string.Empty, NpgsqlTypes.NpgsqlDbType.Text); }
            else { await addressWriter.WriteAsync(postCode, NpgsqlTypes.NpgsqlDbType.Text); }
            if (string.IsNullOrWhiteSpace(provinceId))
            { await addressWriter.WriteAsync(DBNull.Value, NpgsqlTypes.NpgsqlDbType.Text); }
            else { await addressWriter.WriteAsync(provinceId, NpgsqlTypes.NpgsqlDbType.Text); }
            if (string.IsNullOrWhiteSpace(districtId))
            { await addressWriter.WriteAsync(DBNull.Value, NpgsqlTypes.NpgsqlDbType.Text); }
            else { await addressWriter.WriteAsync(districtId, NpgsqlTypes.NpgsqlDbType.Text); }
            if (string.IsNullOrWhiteSpace(subDistrictId))
            { await addressWriter.WriteAsync(DBNull.Value, NpgsqlTypes.NpgsqlDbType.Text); }
            else { await addressWriter.WriteAsync(subDistrictId, NpgsqlTypes.NpgsqlDbType.Text); }
            await addressWriter.WriteAsync(dateNow, NpgsqlTypes.NpgsqlDbType.TimestampTz);
            await addressWriter.WriteAsync(createdBy, NpgsqlTypes.NpgsqlDbType.Text); // CreatedBy
            await addressWriter.WriteAsync(dateNow, NpgsqlTypes.NpgsqlDbType.TimestampTz); // LastModified
            await addressWriter.WriteAsync(createdBy, NpgsqlTypes.NpgsqlDbType.Text); // LastModifiedBy
            await addressWriter.WriteAsync(false, NpgsqlTypes.NpgsqlDbType.Boolean); // IsDeleted
            await addressWriter.WriteAsync(DBNull.Value, NpgsqlTypes.NpgsqlDbType.TimestampTz); // Deleted

            medias.TryAdd(values[1], 1);
            await mediaWriter.StartRowAsync();
            await mediaWriter.WriteAsync(mediaId, NpgsqlTypes.NpgsqlDbType.Uuid); // Id
            await mediaWriter.WriteAsync(values[1], NpgsqlTypes.NpgsqlDbType.Text); // Name
            await mediaWriter.WriteAsync(addressId, NpgsqlTypes.NpgsqlDbType.Uuid); // AddressId
            await mediaWriter.WriteAsync((double)0.0, NpgsqlTypes.NpgsqlDbType.Double); // PercentageOfFullField
            await mediaWriter.WriteAsync(RemoveNullBytes(string.Empty), NpgsqlTypes.NpgsqlDbType.Text); // RemarkDeleted
            await mediaWriter.WriteAsync(dateNow, NpgsqlTypes.NpgsqlDbType.TimestampTz); // Created
            await mediaWriter.WriteAsync(RemoveNullBytes("System"), NpgsqlTypes.NpgsqlDbType.Text); // CreatedBy
            await mediaWriter.WriteAsync(dateNow, NpgsqlTypes.NpgsqlDbType.TimestampTz); // LastModified
            await mediaWriter.WriteAsync(RemoveNullBytes("System"), NpgsqlTypes.NpgsqlDbType.Text); // LastModifiedBy
            await mediaWriter.WriteAsync(false, NpgsqlTypes.NpgsqlDbType.Boolean); // IsDeleted
            await mediaWriter.WriteAsync(DBNull.Value, NpgsqlTypes.NpgsqlDbType.TimestampTz); // Deleted

            dateNow = dateNow.AddSeconds(1);
        }

        await addressWriter.CompleteAsync();
        await conn2.CloseAsync();
        await mediaWriter.CompleteAsync();
        await conn.CloseAsync();
    }

    internal async Task AddMediaSections(string? connectionString)
    {
        if (await _context.MediaSections.AnyAsync()) return;
        using var conn = new NpgsqlConnection(connectionString);
        var currentDirectory = Path.Combine(_currentDirectory, "data.csv");
        await conn.OpenAsync();
        var mediaSectionWriter = await conn.BeginBinaryImportAsync("COPY public.\"MediaSections\" FROM STDIN (FORMAT BINARY);");
        using var reader = new StreamReader(currentDirectory);
        var medias = await _context.Medias.ToListAsync();
        var sections = await _context.Sections.ToListAsync();
        var subSections = await _context.SubSections.ToListAsync();
        var mediaSectionCreated = new Dictionary<string, bool>();
        var dateNow = DateTimeOffset.UtcNow;
        while (!reader.EndOfStream)
        {
            var line = await reader.ReadLineAsync();
            var values = line!.Split('|');
            var name = values[1].ToString();
            var sectionName = values[10].ToString();
            var subSectionName = values[11].ToString();
            var media = medias.First(e => e.Name.Equals(name));
            Section? section;
            try
            {
                section = sections.First(e => e.Name.Equals(sectionName));
            }
            catch (System.Exception)
            {
                throw;
            }

            SubSection? subSection;
            try
            {
                subSection = subSections.First(e => e.Name.Equals(subSectionName));
            }
            catch (System.Exception)
            {
                throw;
            }

            var key = $"{section!.Id}{subSection!.Id}{values[12].ToEnum<TypeOfMedia>()}{media!.Id}";
            if (!mediaSectionCreated.TryGetValue(key, out bool test2))
            {
                mediaSectionCreated.Add(key, true);
                try
                {
                    await mediaSectionWriter.StartRowAsync();
                    await mediaSectionWriter.WriteAsync(Guid.NewGuid(), NpgsqlTypes.NpgsqlDbType.Uuid); // Id
                    await mediaSectionWriter.WriteAsync((int)values[12].ToEnum<TypeOfMedia>(), NpgsqlTypes.NpgsqlDbType.Integer); // TypeOfMedia
                    await mediaSectionWriter.WriteAsync(media!.Id, NpgsqlTypes.NpgsqlDbType.Uuid); // MediaId
                    await mediaSectionWriter.WriteAsync(section!.Id, NpgsqlTypes.NpgsqlDbType.Uuid); // SectionId
                    await mediaSectionWriter.WriteAsync(subSection!.Id, NpgsqlTypes.NpgsqlDbType.Uuid); // SubSectionId
                    await mediaSectionWriter.WriteAsync(string.Empty, NpgsqlTypes.NpgsqlDbType.Text); // RemarkDeleted
                    await mediaSectionWriter.WriteAsync(3, NpgsqlTypes.NpgsqlDbType.Bigint); // TierPotential
                    await mediaSectionWriter.WriteAsync(DBNull.Value, NpgsqlTypes.NpgsqlDbType.Uuid); // AddressId
                    await mediaSectionWriter.WriteAsync(dateNow, NpgsqlTypes.NpgsqlDbType.TimestampTz); // Created
                    await mediaSectionWriter.WriteAsync("System", NpgsqlTypes.NpgsqlDbType.Text); // CreatedBy
                    await mediaSectionWriter.WriteAsync(dateNow, NpgsqlTypes.NpgsqlDbType.TimestampTz); // LastModified
                    await mediaSectionWriter.WriteAsync("System", NpgsqlTypes.NpgsqlDbType.Text); // LastModifiedBy
                    await mediaSectionWriter.WriteAsync(false, NpgsqlTypes.NpgsqlDbType.Boolean); // IsDeleted
                    await mediaSectionWriter.WriteAsync(DBNull.Value, NpgsqlTypes.NpgsqlDbType.TimestampTz); // Deleted
                }
                catch (NpgsqlException ex)
                {
                    // Log or inspect the current values being written
                    Console.WriteLine($"Error during binary import: {ex.Message}");
                    Console.WriteLine($"Current Values: {string.Join(",", values)}");
                    throw;
                }
            }
            dateNow = dateNow.AddSeconds(0.5);
        }

        await mediaSectionWriter.CompleteAsync();
        await conn.CloseAsync();
    }

    public async Task AddContactAsync(string? connectionString)
    {
        if (await _context.Contacts.AnyAsync()) return;
        using var conn = new NpgsqlConnection(connectionString);
        using var conn2 = new NpgsqlConnection(connectionString);
        await conn.OpenAsync();
        await conn2.OpenAsync();
        var currentDirectory = Path.Combine(_currentDirectory, "data.csv");
        using var reader = new StreamReader(currentDirectory);
        using var contactWriter = await conn.BeginBinaryImportAsync("COPY public.\"Contacts\" FROM STDIN (FORMAT BINARY);");
        using var addressWriter = await conn2.BeginBinaryImportAsync("COPY public.\"Address\" FROM STDIN (FORMAT BINARY);");
        var medias = await _context.Medias.ToListAsync();
        var sections = await _context.Sections.ToListAsync();
        var subSections = await _context.SubSections.ToListAsync();
        var mediaSections = await _context.MediaSections.ToListAsync();

        string createdBySanitized = RemoveNullBytes("System");
        string lastModifiedBySanitized = RemoveNullBytes("System");
        var conMediaSecCreated = new List<ContactMediaSection>();
        var contacts = new Dictionary<string, Guid>();
        while (!reader.EndOfStream)
        {
            var line = await reader.ReadLineAsync();
            var values = line!.Split('|');

            if (string.IsNullOrEmpty(values[2]) && string.IsNullOrEmpty(values[3]))
                continue;

            var firstName = string.Empty;
            var lastName = string.Empty;
            var fullName = values[2].Split(" ");
            var dateNow = DateTimeOffset.UtcNow;
            if (fullName.Length > 0)
                firstName = fullName[0];
            if (fullName.Length > 1)
                lastName = fullName[1];

            if (firstName.Equals("กองบรรณาธิการ"))
                firstName = $"{fullName[0]} {values[1]}";

            var email = new List<string>();
            var phoneNumbers = new List<string>();

            if (!string.IsNullOrWhiteSpace(values[9]))
                email.Add(values[9].Trim());

            if (!string.IsNullOrWhiteSpace(values[6]))
                phoneNumbers.Add(values[6]);

            string nickNameSanitized = RemoveNullBytes(values[3]);

            var key = $"{firstName}{lastName}{nickNameSanitized}";

            if (!contacts.TryGetValue(key, out var contactId))
            {
                contactId = Guid.NewGuid();
                var addressId = Guid.NewGuid();

                contacts.TryAdd(key, contactId);
                await addressWriter.StartRowAsync();
                await addressWriter.WriteAsync(addressId, NpgsqlTypes.NpgsqlDbType.Uuid);
                await addressWriter.WriteAsync(email.ToArray(), NpgsqlTypes.NpgsqlDbType.Array | NpgsqlTypes.NpgsqlDbType.Text);
                await addressWriter.WriteAsync(phoneNumbers.ToArray(), NpgsqlTypes.NpgsqlDbType.Array | NpgsqlTypes.NpgsqlDbType.Text);
                await addressWriter.WriteAsync(string.Empty, NpgsqlTypes.NpgsqlDbType.Text);
                await addressWriter.WriteAsync(string.Empty, NpgsqlTypes.NpgsqlDbType.Text);
                await addressWriter.WriteAsync(DBNull.Value, NpgsqlTypes.NpgsqlDbType.Text);
                await addressWriter.WriteAsync(DBNull.Value, NpgsqlTypes.NpgsqlDbType.Text);
                await addressWriter.WriteAsync(DBNull.Value, NpgsqlTypes.NpgsqlDbType.Text);
                await addressWriter.WriteAsync(dateNow, NpgsqlTypes.NpgsqlDbType.TimestampTz);
                await addressWriter.WriteAsync(createdBySanitized, NpgsqlTypes.NpgsqlDbType.Text); // CreatedBy
                await addressWriter.WriteAsync(dateNow, NpgsqlTypes.NpgsqlDbType.TimestampTz); // LastModified
                await addressWriter.WriteAsync(lastModifiedBySanitized, NpgsqlTypes.NpgsqlDbType.Text); // LastModifiedBy
                await addressWriter.WriteAsync(false, NpgsqlTypes.NpgsqlDbType.Boolean); // IsDeleted
                await addressWriter.WriteAsync(DBNull.Value, NpgsqlTypes.NpgsqlDbType.TimestampTz); // Deleted

                await contactWriter.StartRowAsync();
                await contactWriter.WriteAsync(contactId, NpgsqlTypes.NpgsqlDbType.Uuid);
                string firstNameSanitized = RemoveNullBytes(firstName);
                await contactWriter.WriteAsync(firstNameSanitized, NpgsqlTypes.NpgsqlDbType.Varchar);
                string lastNameSanitized = RemoveNullBytes(lastName);
                await contactWriter.WriteAsync(lastNameSanitized, NpgsqlTypes.NpgsqlDbType.Varchar);
                await contactWriter.WriteAsync(nickNameSanitized, NpgsqlTypes.NpgsqlDbType.Varchar);
                string positionSanitized = RemoveNullBytes(values[4]);
                await contactWriter.WriteAsync(positionSanitized, NpgsqlTypes.NpgsqlDbType.Varchar);
                string lineIdSanitized = RemoveNullBytes(values[7]);
                await contactWriter.WriteAsync(lineIdSanitized, NpgsqlTypes.NpgsqlDbType.Varchar);
                string remarkSanitized = RemoveNullBytes(values[1]);
                await contactWriter.WriteAsync(remarkSanitized, NpgsqlTypes.NpgsqlDbType.Text);
                await contactWriter.WriteAsync((int)AddressType.Home, NpgsqlTypes.NpgsqlDbType.Integer);
                await contactWriter.WriteAsync((int)ContactType.CONTACT, NpgsqlTypes.NpgsqlDbType.Integer);
                await contactWriter.WriteAsync(addressId, NpgsqlTypes.NpgsqlDbType.Uuid);
                await contactWriter.WriteAsync((double)0.0, NpgsqlTypes.NpgsqlDbType.Double);
                string remarkDeletedSanitized = RemoveNullBytes(string.Empty);
                await contactWriter.WriteAsync(remarkDeletedSanitized, NpgsqlTypes.NpgsqlDbType.Text);
                await contactWriter.WriteAsync(DBNull.Value, NpgsqlTypes.NpgsqlDbType.Uuid);
                await contactWriter.WriteAsync(dateNow, NpgsqlTypes.NpgsqlDbType.TimestampTz);
                await contactWriter.WriteAsync(createdBySanitized, NpgsqlTypes.NpgsqlDbType.Text);
                await contactWriter.WriteAsync(dateNow, NpgsqlTypes.NpgsqlDbType.TimestampTz);
                await contactWriter.WriteAsync(lastModifiedBySanitized, NpgsqlTypes.NpgsqlDbType.Text);
                await contactWriter.WriteAsync(false, NpgsqlTypes.NpgsqlDbType.Boolean);
                await contactWriter.WriteAsync(DBNull.Value, NpgsqlTypes.NpgsqlDbType.TimestampTz);
            }

            var name = values[1].ToString();
            var sectionName = values[10].ToString();
            var subSectionName = values[11].ToString();
            var media = medias.First(e => e.Name.Equals(name));
            var section = sections.First(e => e.Name.Equals(sectionName));

            var subSection = subSections.First(e => e.Name.Equals(subSectionName));

            var mediaSection = mediaSections.Find(e =>
                    e.SectionId.Equals(section.Id) &&
                    e.SubSectionId.Equals(subSection.Id) &&
                    values[12].ToEnum<TypeOfMedia>().Equals(e.TypeOfMedia) &&
                    e.MediaId.Equals(media!.Id));

            conMediaSecCreated.Add(new ContactMediaSection()
            {
                ContactId = contactId,
                MediaSectionId = mediaSection!.Id
            });

            dateNow = dateNow.AddSeconds(0.4);
        }

        await _context.ContactMediaSections.AddRangeAsync(conMediaSecCreated);

        await addressWriter.CompleteAsync();
        await contactWriter.CompleteAsync();

        await conn2.CloseAsync();
        await conn.CloseAsync();

        await _context.SaveChangesAsync();
    }

    public async Task AddPlatform(string? connectionString)
    {
        // Ensure the connection string is valid
        if (string.IsNullOrEmpty(connectionString))
            throw new ArgumentException("Connection string cannot be null or empty.", nameof(connectionString));

        if (await _context.Platforms.AnyAsync()) return;

        using var conn = new NpgsqlConnection(connectionString);
        using var conn2 = new NpgsqlConnection(connectionString);
        using var conn3 = new NpgsqlConnection(connectionString);
        await conn.OpenAsync();
        await conn2.OpenAsync();
        await conn3.OpenAsync();

        var dateNow = DateTimeOffset.UtcNow;

        using var platformWriter = await conn.BeginBinaryImportAsync("COPY public.\"Platforms\" FROM STDIN (FORMAT BINARY);");
        using var mediaWriter = await conn2.BeginBinaryImportAsync("COPY public.\"Medias\" FROM STDIN (FORMAT BINARY);");
        using var mediaSectionWriter = await conn3.BeginBinaryImportAsync("COPY public.\"MediaSections\" FROM STDIN (FORMAT BINARY);");

        var medias = await _context.Medias.ToListAsync();
        var sections = await _context.Sections.ToListAsync();
        var subSections = await _context.SubSections.ToListAsync();
        var mediaSections = await _context.MediaSections.ToListAsync();

        var currentDirectory = Path.Combine(_currentDirectory, "plateform.csv");
        if (!File.Exists(currentDirectory))
            throw new FileNotFoundException($"File not found: {currentDirectory}");

        using var reader = new StreamReader(currentDirectory);

        var mediaSectionCreates = new List<MediaSection>();
        var mediaSectionCreated = new Dictionary<string, Guid>();
        var mediaCreated = new Dictionary<string, Guid>();
        var now = DateTime.UtcNow;
        while (!reader.EndOfStream)
        {
            var line = await reader.ReadLineAsync();
            if (string.IsNullOrWhiteSpace(line)) continue;

            var values = line.Split('|');
            if (values.Length < 11)
                throw new InvalidOperationException("Invalid data format in CSV file.");

            var name = values[2];
            var sectionName = values[9];
            var subSectionName = values[10];

            var media = medias.FirstOrDefault(e => e.Name == name);

            var section = sections.FirstOrDefault(e => e.Name == sectionName)
                ?? throw new InvalidOperationException($"Section not found: {sectionName}");

            var subSection = subSections.FirstOrDefault(e => e.SectionId == section.Id && e.Name == "General")
                ?? subSections.FirstOrDefault(e => e.SectionId == section.Id && e.Name == subSectionName)
                ?? throw new InvalidOperationException($"SubSection not found: {subSectionName}");

            Guid mediaId;

            if (media == null)
            {
                if (!mediaCreated.TryGetValue(name, out mediaId))
                {
                    mediaId = Guid.NewGuid();

                    await mediaWriter.StartRowAsync();
                    await mediaWriter.WriteAsync(mediaId, NpgsqlTypes.NpgsqlDbType.Uuid);
                    await mediaWriter.WriteAsync(name, NpgsqlTypes.NpgsqlDbType.Text);
                    await mediaWriter.WriteAsync(DBNull.Value, NpgsqlTypes.NpgsqlDbType.Uuid);
                    await mediaWriter.WriteAsync(0.0, NpgsqlTypes.NpgsqlDbType.Double);
                    await mediaWriter.WriteAsync(string.Empty, NpgsqlTypes.NpgsqlDbType.Text);
                    await mediaWriter.WriteAsync(dateNow, NpgsqlTypes.NpgsqlDbType.TimestampTz);
                    await mediaWriter.WriteAsync("System", NpgsqlTypes.NpgsqlDbType.Text);
                    await mediaWriter.WriteAsync(dateNow, NpgsqlTypes.NpgsqlDbType.TimestampTz);
                    await mediaWriter.WriteAsync("System", NpgsqlTypes.NpgsqlDbType.Text);
                    await mediaWriter.WriteAsync(false, NpgsqlTypes.NpgsqlDbType.Boolean);
                    await mediaWriter.WriteAsync(DBNull.Value, NpgsqlTypes.NpgsqlDbType.TimestampTz);
                    mediaCreated.Add(name, mediaId);
                }
            }
            else
            {
                mediaId = media.Id;
            }
            Guid mediaSectionId;
            var typeOfMedia = values[6].ToEnum<TypeOfMedia>();
            var mediaSectionKey = $"{section.Id}_{subSection.Id}_{mediaId}_{values[6]}";
            var mediaSection = mediaSections.FirstOrDefault(ms =>
                ms.SectionId == section.Id &&
                ms.SubSectionId == subSection.Id &&
                ms.MediaId == mediaId &&
                typeOfMedia == ms.TypeOfMedia);

            if (mediaSection == null)
            {
                if (!mediaSectionCreated.TryGetValue(mediaSectionKey, out mediaSectionId))
                {
                    mediaSectionId = Guid.NewGuid();
                    await mediaSectionWriter.StartRowAsync();
                    await mediaSectionWriter.WriteAsync(Guid.NewGuid(), NpgsqlTypes.NpgsqlDbType.Uuid); // Id
                    await mediaSectionWriter.WriteAsync((int)values[6].ToEnum<TypeOfMedia>(), NpgsqlTypes.NpgsqlDbType.Integer); // TypeOfMedia
                    await mediaSectionWriter.WriteAsync(mediaId, NpgsqlTypes.NpgsqlDbType.Uuid); // MediaId
                    await mediaSectionWriter.WriteAsync(section!.Id, NpgsqlTypes.NpgsqlDbType.Uuid); // SectionId
                    await mediaSectionWriter.WriteAsync(subSection!.Id, NpgsqlTypes.NpgsqlDbType.Uuid); // SubSectionId
                    await mediaSectionWriter.WriteAsync(string.Empty, NpgsqlTypes.NpgsqlDbType.Text); // RemarkDeleted
                    await mediaSectionWriter.WriteAsync(0, NpgsqlTypes.NpgsqlDbType.Bigint); // TierPotential
                    await mediaSectionWriter.WriteAsync(DBNull.Value, NpgsqlTypes.NpgsqlDbType.Uuid); // AddressId
                    await mediaSectionWriter.WriteAsync(dateNow, NpgsqlTypes.NpgsqlDbType.TimestampTz); // Created
                    await mediaSectionWriter.WriteAsync("System", NpgsqlTypes.NpgsqlDbType.Text); // CreatedBy
                    await mediaSectionWriter.WriteAsync(dateNow, NpgsqlTypes.NpgsqlDbType.TimestampTz); // LastModified
                    await mediaSectionWriter.WriteAsync("System", NpgsqlTypes.NpgsqlDbType.Text); // LastModifiedBy
                    await mediaSectionWriter.WriteAsync(false, NpgsqlTypes.NpgsqlDbType.Boolean); // IsDeleted
                    await mediaSectionWriter.WriteAsync(DBNull.Value, NpgsqlTypes.NpgsqlDbType.TimestampTz); // Deleted
                    mediaSectionCreated.Add(mediaSectionKey, mediaSectionId);
                }
            }
            else
            {
                mediaSectionId = mediaSection.Id;
            }

            var platformType = values[7].ToEnum<PlatformType>();
            var mediaUrl = values[1];
            var mediaName = values[2];
            var platformName = values[7];
            var prValue = 0m;
            if (!string.IsNullOrWhiteSpace(values[11]))
                prValue = decimal.Parse(values[11]);

            var potential = 0L;
            try
            {
                if (!string.IsNullOrWhiteSpace(values[13]))
                    potential = long.Parse(values[13]);
            }
            catch (System.Exception)
            {

            }

            await platformWriter.StartRowAsync();
            await platformWriter.WriteAsync(Guid.NewGuid(), NpgsqlTypes.NpgsqlDbType.Uuid); // Id
            await platformWriter.WriteAsync(platformName, NpgsqlTypes.NpgsqlDbType.Varchar); // Name
            await platformWriter.WriteAsync(mediaUrl, NpgsqlTypes.NpgsqlDbType.Text); // Link
            await platformWriter.WriteAsync(prValue, NpgsqlTypes.NpgsqlDbType.Numeric); // PRValue
            await platformWriter.WriteAsync(0, NpgsqlTypes.NpgsqlDbType.Numeric); // ADValue
            await platformWriter.WriteAsync(potential, NpgsqlTypes.NpgsqlDbType.Bigint); // Potential
            await platformWriter.WriteAsync(0L, NpgsqlTypes.NpgsqlDbType.Bigint); // TierOrder
            await platformWriter.WriteAsync(0L, NpgsqlTypes.NpgsqlDbType.Bigint); // TierPotential
            await platformWriter.WriteAsync((int)platformType, NpgsqlTypes.NpgsqlDbType.Integer); // PlatformType
            await platformWriter.WriteAsync(mediaSectionId, NpgsqlTypes.NpgsqlDbType.Uuid); // MediaSectionId
            await platformWriter.WriteAsync(string.Empty, NpgsqlTypes.NpgsqlDbType.Varchar); // RemarkDeleted
            await platformWriter.WriteAsync(now, NpgsqlTypes.NpgsqlDbType.TimestampTz); // Created
            await platformWriter.WriteAsync("System", NpgsqlTypes.NpgsqlDbType.Text); // CreatedBy
            await platformWriter.WriteAsync(now, NpgsqlTypes.NpgsqlDbType.TimestampTz); // LastModified
            await platformWriter.WriteAsync("System", NpgsqlTypes.NpgsqlDbType.Text); // LastModifiedBy
            await platformWriter.WriteAsync(false, NpgsqlTypes.NpgsqlDbType.Boolean); // IsDeleted
            await platformWriter.WriteAsync(DBNull.Value, NpgsqlTypes.NpgsqlDbType.TimestampTz); // Deleted
            now = now.AddSeconds(1);
        }

        await mediaWriter.CompleteAsync();
        await conn2.CloseAsync();

        await mediaSectionWriter.CompleteAsync();
        await conn3.CloseAsync();

        await platformWriter.CompleteAsync();
        await conn.CloseAsync();
    }

    public async Task UpdatePlatform()
    {
        if (await _context.Platforms.AnyAsync())
        {
            var mediaSections = await _context.MediaSections
                .Include(ms => ms.Platforms)
                .AsTracking()
                .ToListAsync();

            for (int i = 0; i < mediaSections.Count; i++)
            {
                var platform = mediaSections[i].Platforms.OrderBy(e => e.TierPotential).FirstOrDefault(e => e.TierPotential != 0);
                if (platform != null)
                    mediaSections[i].TierPotential = platform.TierPotential;
            }

            _context.SaveChanges();
        }
    }

    string RemoveNullBytes(string input)
    {
        return input.Replace("\0", "");
    }


    private async Task SeedSections()
    {
        var sections = new List<Section>()
        {
            new Section()
            {
                Id = new Guid("56d22a41-6b16-4335-9ba2-dfeb63c5a830"),
                Name = "General-test",
                FormulaForCalculations = new List<FormulaForCalculation>() {
                    new FormulaForCalculation() {
                        PriceOfPurchaseRequisition = 300000,
                        Tier = 1,
                        MinimumFollowers = 100000
                    },
                    new FormulaForCalculation() {
                        PriceOfPurchaseRequisition = 25000,
                        Tier = 2,
                        MinimumFollowers = 80000
                    },
                    new FormulaForCalculation() {
                        PriceOfPurchaseRequisition = 20000,
                        Tier = 3,
                        MinimumFollowers = 10000
                    }
                }
            },
             new Section()
            {
                Id = new Guid("26c42685-c10b-4d85-9e7f-95f9087ff33f"),
                Name = "Sport",
            }
        };
        await _context.AddRangeAsync(sections);

        var subSections = new List<SubSection>()
        {
            new SubSection() {
                Id = new Guid("90a6e746-a379-4603-b39c-c10409314445"),
                Name = "Sport-test",
                SectionId = new Guid("56d22a41-6b16-4335-9ba2-dfeb63c5a830"),
            },
            new SubSection() {
                Id = new Guid("8f9b8253-44da-4693-93a5-cb9770d63844"),
                Name = "Sport-test-2",
                SectionId = new Guid("56d22a41-6b16-4335-9ba2-dfeb63c5a830"),
            },
        };
        await _context.AddRangeAsync(subSections);

        await _context.SaveChangesAsync();
    }

    private async Task SeedMedia()
    {
        var medias = new List<Media>()
        {
            new Media()
            {
                Id = new Guid("2541e16a-7fa1-4e81-bdc9-617e37d47750"),
                Name = "Media",
            },
            new Media()
            {
                Id = new Guid("01926ff9-4fe9-73fa-9e1d-f8364c7dbf7d"),
                Name = "Media_2",
            },
            new Media()
            {
                Id = new Guid("1e967671-6a4f-4148-94f6-06d1d2052932"),
                Name = "Media_3",
            },
            new Media()
            {
                Id = new Guid("d647cf67-d115-49f0-ab17-9c17d83a210b"),
                Name = "Media_4",
            },
            new Media()
            {
                Id = new Guid("c7bb5200-72a2-4fa7-bd7e-ffd302eef6c6"),
                Name = "Media_5",
            },
            new Media()
            {
                Id = new Guid("b7721fa7-966a-4c85-913e-578f9b76c651"),
                Name = "Media_6",
            },
            new Media()
            {
                Id = new Guid("d2518c07-f5fd-475b-99f3-e425041b8864"),
                Name = "instagram",
            }
        };
        await _context.AddRangeAsync(medias);

        await _context.SaveChangesAsync();
    }

    private async Task SeedMediaSection()
    {
        var section = await _context.Sections.FirstOrDefaultAsync(e => e.Id.Equals(new Guid("56d22a41-6b16-4335-9ba2-dfeb63c5a830")));
        var subSection = await _context.SubSections.FirstOrDefaultAsync(e => e.Id.Equals(new Guid("90a6e746-a379-4603-b39c-c10409314445")));
        var media = await _context.Medias.FirstOrDefaultAsync(e => e.Id.Equals(new Guid("2541e16a-7fa1-4e81-bdc9-617e37d47750")));
        var mediaIn = await _context.Medias.FirstOrDefaultAsync(e => e.Id.Equals(new Guid("d2518c07-f5fd-475b-99f3-e425041b8864")));
        var mediaSections = new List<MediaSection>()
        {
            new MediaSection() {
                Id = new Guid("226f9c96-ca00-4c9b-a6cc-bfe788dbd015"),
                TypeOfMedia = TypeOfMedia.DIGITAL,
                MediaId = media!.Id,
                SectionId = section!.Id,
                SubSectionId = subSection!.Id,
                // Platforms  = new List<Platform>(),
                Media = media!,
                Section = section!,
                SubSection = subSection!,
            },
            new MediaSection() {
                Id = new Guid("7ec3b4ec-7256-4174-a8a2-5f336606d5d2"),
                TypeOfMedia = TypeOfMedia.DIGITAL,
                MediaId = media!.Id,
                SectionId = section!.Id,
                SubSectionId = subSection!.Id,
                // Platforms  = new List<Platform>(),
                Media = mediaIn!,
                Section = section!,
                SubSection = subSection!,
            }
        };

        await _context.MediaSections.AddRangeAsync(mediaSections);
        await _context.SaveChangesAsync();
    }

    private async Task SeedPlatform()
    {
        var platforms = new List<Platform>()
        {
            new Platform()
            {
                Id = new Guid("b93b1b82-22a4-413c-a6d7-80187074d621"),
                Name = "Platform B",
                Link = "https://platform-b.com",
                PRValue = 800.30m,
                ADValue = 1000.20m,
                Potential = 50,
                TierOder = 2,
                TierPotential = 60,
                PlatFormType = PlatformType.FACEBOOK,
                MediaSectionId = new Guid("226f9c96-ca00-4c9b-a6cc-bfe788dbd015"),
            },
            new Platform()
            {
                Id = new Guid("5b5f8b31-caf6-4c08-b51e-81eebdf5738a"),
                Name = "Platform C",
                Link = "https://platform-c.com",
                PRValue = 800.30m,
                ADValue = 1000.20m,
                Potential = 50,
                TierOder = 2,
                TierPotential = 60,
                PlatFormType = PlatformType.FACEBOOK,
                MediaSectionId = new Guid("7ec3b4ec-7256-4174-a8a2-5f336606d5d2"),
            },
            new Platform()
            {
                Id = new Guid("ae891406-d5e2-4ef4-902e-6148acab7c79"),
                Name = "Platform D",
                Link = "https://platform-D.com",
                PRValue = 800.30m,
                ADValue = 1000.20m,
                Potential = 50,
                TierOder = 2,
                TierPotential = 60,
                PlatFormType = PlatformType.FACEBOOK,
                MediaSectionId = new Guid("226f9c96-ca00-4c9b-a6cc-bfe788dbd015"),
            }
        };

        await _context.Platforms.AddRangeAsync(platforms);
        await _context.SaveChangesAsync();
    }

    private async Task SeedClient()
    {
        var clients = new List<Client>()
        {
            new Client() {
                Id = new Guid("5f8fb285-2fa0-4974-9a80-6a6d6e46a4ed"),
                Name = "clients Name",
                PercentageOfFullField = 100,
            },
            new Client() {
                Id = new Guid("e3f4d3dd-edca-4723-913e-d4a272ba4af9"),
                Name = "clients 2",
                PercentageOfFullField = 100,
            },

        };
        await _context.AddRangeAsync(clients);

        await _context.SaveChangesAsync();
    }

    private async Task SeedUser()
    {
        _context.Users.Add(new ApplicationUser
        {
            Id = "fbc382fe-5271-4ddd-bda8-4afdb839723d",
            UserName = "testuser"
        });
        await _context.SaveChangesAsync();
    }

    private async Task SeedPRActivity()
    {
        await _context.PRActivities.AddRangeAsync(
            new PRActivity()
            {
                Id = new Guid("9bdcd9ec-a1f6-4a59-9c31-dbbe17bd3c35"),
                Title = "PRActivities",
                DurationType = DurationPRActivityType.LONG,
                ContentPRActivityType = ContentPRActivityType.EXCLUSIVE_INTERVIEW,
                ContractType = ContractType.ADHOC,
                Started = DateTimeOffset.UtcNow.AddDays(-30),
                Ended = DateTimeOffset.UtcNow.AddDays(30),
                Released = DateTimeOffset.UtcNow.AddDays(32),
                ClientId = new Guid("5f8fb285-2fa0-4974-9a80-6a6d6e46a4ed"),
                SectionId = new Guid("56d22a41-6b16-4335-9ba2-dfeb63c5a830"),
                SubSectionId = new Guid("8f9b8253-44da-4693-93a5-cb9770d63844"),
                OwnerId = "fbc382fe-5271-4ddd-bda8-4afdb839723d",
                WorkProgress = new List<WorkProgress>()
                {
                    new ()
                    {
                        WorkTask = WorkTask.BRIEFING_MEETING,
                    },
                    new ()
                    {
                        WorkTask = WorkTask.CONFIRM_LOCATION,
                    },
                    new ()
                    {
                        WorkTask = WorkTask.SEND_EMAIL_CLIENT,
                    },
                    new ()
                    {
                        WorkTask = WorkTask.SIGN_EVENT,
                    },
                }
            },
            new PRActivity()
            {
                Id = new Guid("df8f5ede-634e-488f-b44a-5dd5a023fefa"),
                Title = "PRActivities2",
                DurationType = DurationPRActivityType.LONG,
                ContentPRActivityType = ContentPRActivityType.EXCLUSIVE_INTERVIEW,
                ContractType = ContractType.ADHOC,
                Started = DateTimeOffset.UtcNow.AddDays(-30),
                Ended = DateTimeOffset.UtcNow.AddDays(30),
                Released = DateTimeOffset.UtcNow.AddDays(32),
                ClientId = new Guid("5f8fb285-2fa0-4974-9a80-6a6d6e46a4ed"),
                SectionId = new Guid("56d22a41-6b16-4335-9ba2-dfeb63c5a830"),
                SubSectionId = new Guid("8f9b8253-44da-4693-93a5-cb9770d63844"),
                OwnerId = "fbc382fe-5271-4ddd-bda8-4afdb839723d",
                WorkProgress = new List<WorkProgress>()
                {
                    new ()
                    {
                        WorkTask = WorkTask.BRIEFING_MEETING,
                    },
                    new ()
                    {
                        WorkTask = WorkTask.CONFIRM_LOCATION,
                    },
                    new ()
                    {
                        WorkTask = WorkTask.SEND_EMAIL_CLIENT,
                    },
                    new ()
                    {
                        WorkTask = WorkTask.SIGN_EVENT,
                    },
                }
            },
            new PRActivity()
            {
                Id = new Guid("e638a958-cabf-448d-bfec-734c280abd41"),
                Title = "PRActivities3",
                DurationType = DurationPRActivityType.LONG,
                ContentPRActivityType = ContentPRActivityType.EXCLUSIVE_INTERVIEW,
                ContractType = ContractType.ADHOC,
                Started = DateTimeOffset.UtcNow.AddDays(-30),
                Ended = DateTimeOffset.UtcNow.AddDays(30),
                Released = DateTimeOffset.UtcNow.AddDays(32),
                ClientId = new Guid("5f8fb285-2fa0-4974-9a80-6a6d6e46a4ed"),
                SectionId = new Guid("56d22a41-6b16-4335-9ba2-dfeb63c5a830"),
                SubSectionId = new Guid("8f9b8253-44da-4693-93a5-cb9770d63844"),
                OwnerId = "fbc382fe-5271-4ddd-bda8-4afdb839723d",
                WorkProgress = new List<WorkProgress>()
                {
                    new ()
                    {
                        WorkTask = WorkTask.BRIEFING_MEETING,
                    },
                    new ()
                    {
                        WorkTask = WorkTask.CONFIRM_LOCATION,
                    },
                    new ()
                    {
                        WorkTask = WorkTask.SEND_EMAIL_CLIENT,
                    },
                    new ()
                    {
                        WorkTask = WorkTask.SIGN_EVENT,
                    },
                }
            },
            new PRActivity
            {
                Title = "Campaign A",
                DurationType = DurationPRActivityType.LONG,
                ContentPRActivityType = ContentPRActivityType.PRESS_RELEASE,
                ContractType = ContractType.RETAINER,
                Started = DateTimeOffset.UtcNow.AddDays(-20),
                Ended = DateTimeOffset.UtcNow.AddDays(10),
                Released = DateTimeOffset.UtcNow.AddDays(14),
                ClientId = new Guid("5f8fb285-2fa0-4974-9a80-6a6d6e46a4ed"),
                SectionId = new Guid("56d22a41-6b16-4335-9ba2-dfeb63c5a830"),
                SubSectionId = new Guid("8f9b8253-44da-4693-93a5-cb9770d63844"),
                OwnerId = "fbc382fe-5271-4ddd-bda8-4afdb839723d",
                WorkProgress = new List<WorkProgress>()
                {
                    new ()
                    {
                        WorkTask = WorkTask.BRIEFING_MEETING,
                    },
                    new ()
                    {
                        WorkTask = WorkTask.CONFIRM_LOCATION,
                    },
                    new ()
                    {
                        WorkTask = WorkTask.SEND_EMAIL_CLIENT,
                    },
                    new ()
                    {
                        WorkTask = WorkTask.SIGN_EVENT,
                    },
                }
            },
            new PRActivity
            {
                Id = new Guid("79ab5fe5-80d0-4447-8ea9-06619749431c"),
                Title = "Event B",
                DurationType = DurationPRActivityType.EVENT,
                ContentPRActivityType = ContentPRActivityType.BLOGGER_DAY,
                ContractType = ContractType.ADHOC,
                Started = DateTimeOffset.UtcNow.AddDays(-20),
                Ended = DateTimeOffset.UtcNow.AddDays(10),
                Released = DateTimeOffset.UtcNow.AddDays(14),
                ClientId = new Guid("5f8fb285-2fa0-4974-9a80-6a6d6e46a4ed"),
                SectionId = new Guid("56d22a41-6b16-4335-9ba2-dfeb63c5a830"),
                SubSectionId = new Guid("8f9b8253-44da-4693-93a5-cb9770d63844"),
                OwnerId = "fbc382fe-5271-4ddd-bda8-4afdb839723d",
                WorkProgress = new List<WorkProgress>()
                {
                    new ()
                    {
                        WorkTask = WorkTask.BRIEFING_MEETING,
                    },
                    new ()
                    {
                        WorkTask = WorkTask.CONFIRM_LOCATION,
                    },
                    new ()
                    {
                        WorkTask = WorkTask.SEND_EMAIL_CLIENT,
                    },
                    new ()
                    {
                        WorkTask = WorkTask.SIGN_EVENT,
                    },
                }
            },
            new PRActivity
            {
                Title = "Short News C",
                DurationType = DurationPRActivityType.SHORT,
                ContentPRActivityType = ContentPRActivityType.CAPTION_NEWS,
                ContractType = ContractType.RETAINER,
                Started = DateTimeOffset.UtcNow.AddDays(-20),
                Ended = DateTimeOffset.UtcNow.AddDays(10),
                Released = DateTimeOffset.UtcNow.AddDays(14),
                ClientId = new Guid("5f8fb285-2fa0-4974-9a80-6a6d6e46a4ed"),
                SectionId = new Guid("56d22a41-6b16-4335-9ba2-dfeb63c5a830"),
                SubSectionId = new Guid("8f9b8253-44da-4693-93a5-cb9770d63844"),
                OwnerId = "fbc382fe-5271-4ddd-bda8-4afdb839723d",
                WorkProgress = new List<WorkProgress>()
                {
                    new ()
                    {
                        WorkTask = WorkTask.BRIEFING_MEETING,
                    },
                    new ()
                    {
                        WorkTask = WorkTask.CONFIRM_LOCATION,
                    },
                    new ()
                    {
                        WorkTask = WorkTask.SEND_EMAIL_CLIENT,
                    },
                    new ()
                    {
                        WorkTask = WorkTask.SIGN_EVENT,
                    },
                }
            }
        );
        await _context.SaveChangesAsync();
    }

    private async Task SeedPRActivityBriefing()
    {
        await _context.PRActivityBriefings.AddRangeAsync(
            new PRActivityBriefing()
            {
                Id = new Guid("c29c25ac-0785-43f2-af73-753d1d30460f"),
                Content = "xxxx",
                PRActivityId = new Guid("79ab5fe5-80d0-4447-8ea9-06619749431c")
            }
        );
        await _context.SaveChangesAsync();
    }

    private async Task SeedPRActivityEmail()
    {
        await _context.PRActivityEmails.AddRangeAsync(
            new PRActivityEmail()
            {
                Id = new Guid("c29c25ac-0785-43f2-af73-753d1d30460f"),
                Subject = "Subject",
                Content = "Content",
                PRActivityId = new Guid("79ab5fe5-80d0-4447-8ea9-06619749431c")
            }
        );
        await _context.SaveChangesAsync();
    }

    private async Task SeedPRActivityLocation()
    {
        await _context.PRActivityLocations.AddRangeAsync(
            new PRActivityLocation()
            {
                Id = new Guid("3447ce4b-1778-4a15-b486-f9cd5550922b"),
                Text = "Test Location",
                UrlMap = "http://test.com",
                ProvinceId = "1",
                DistrictId = "1001",
                SubDistrictId = "100101",
                PostCode = "10200",
                Remark = "Test Remark",
                PRActivityId = new Guid("79ab5fe5-80d0-4447-8ea9-06619749431c")
            }
        );
        await _context.SaveChangesAsync();
    }


    private async Task SeedMonitorTaskWorker()
    {
        var PRActivity = await _context.PRActivities
            .FirstAsync(e => e.Title.Equals("PRActivities2"));

        var platform = await _context.Platforms
            .FirstAsync(e => e.Id.Equals(new Guid("b93b1b82-22a4-413c-a6d7-80187074d621")));

        await _context.MonitorTaskWorkers
            .AddRangeAsync(
                new MonitorTaskWorker()
                {
                    Id = new Guid("e2f12e32-6da9-4986-9a47-a8cb4ee1e3da"),
                    Topic = "Topic2",
                    KeywordQuery = "KeywordQuery",
                    TaskWorkerStatus = TaskWorkerStatus.PENDING,
                    IsUrgent = true,
                    PRActivityId = PRActivity.Id
                },
                new MonitorTaskWorker()
                {
                    Id = new Guid("c71572df-3373-43f0-a1b1-048cd957f8a0"),
                    Topic = "Topic3",
                    KeywordQuery = "KeywordQuery",
                    TaskWorkerStatus = TaskWorkerStatus.PENDING,
                    IsUrgent = true,
                    PRActivityId = PRActivity.Id,
                    News = new List<New>(){
                        new New()
                        {
                            Id = new Guid("07fdd805-9750-4a97-9986-e949423e49db"),
                            Title = "Title",
                            Owner = "OwnerTitle",
                            Link = "Link",
                            Description = "Description",
                            Mapping = MappingType.PENDING,
                            PlatformType = PlatformType.INSTAGRAM,
                            PlatformId = platform.Id,
                        },
                        new New()
                        {
                            Id = new Guid("9042743d-b62f-44be-ac3a-b783efc5c562"),
                            Title = "Title2",
                            Owner = "OwnerTitle2",
                            Link = "Link",
                            Description = "Description",
                            Mapping = MappingType.PENDING,
                            PlatformType = PlatformType.INSTAGRAM,
                            PlatformId = platform.Id,
                        }
                    }
                }
            );
        await _context.SaveChangesAsync();
    }

    private async Task SeedNew()
    {
        await _context.News.AddRangeAsync(
            new New()
            {
                Id = new Guid("07fdd805-9750-4a97-9986-e949423e49db"),
                Title = "Title",
                Owner = "OwnerTitle",
                Link = "Link",
                Description = "Description",
                Mapping = MappingType.PENDING,
                PlatformType = PlatformType.INSTAGRAM,
                PlatformId = new Guid("b93b1b82-22a4-413c-a6d7-80187074d621"),
                MonitorTaskWorkerId = new Guid("c71572df-3373-43f0-a1b1-048cd957f8a0")
            },
            new New()
            {
                Id = new Guid("9042743d-b62f-44be-ac3a-b783efc5c562"),
                Title = "Title2",
                Owner = "OwnerTitle2",
                Link = "Link",
                Description = "Description",
                Mapping = MappingType.PENDING,
                PlatformType = PlatformType.INSTAGRAM,
                PlatformId = new Guid("b93b1b82-22a4-413c-a6d7-80187074d621"),
                MonitorTaskWorkerId = new Guid("c71572df-3373-43f0-a1b1-048cd957f8a0")
            }
        );
        await _context.SaveChangesAsync();
    }

    private async Task SeedContact()
    {
        await _context.Contacts.AddRangeAsync(
            new Contact()
            {
                Id = new Guid("547a3b25-ad3d-48bb-b4c5-c4d545ff25dc"),
                FirstName = "FirstName Jossshn",
                LastName = "LastName Doe",
                NickName = "Johnny",
                Position = "Developer",
                LineId = "john_doe123",
                Remark = "Remarkable employee",
                AddressType = AddressType.Home,
                ContactType = ContactType.CONTACT,
                Address = new()
                {
                    Email = new[] { "example1xxx@example.com", "example2xxx@example.com" },
                    PhoneNumbers = new[] { "123-456-7890", "098-765-4321" },
                    Text = "Sample text for mock entity",
                    ProvinceId = "1",
                    DistrictId = "1001",
                    SubDistrictId = "100101",
                    PostCode = "10200",
                }
            }
        );
        await _context.SaveChangesAsync();
    }

}
