﻿using System.Reflection;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace PRManagement.Infrastructure.Data;

public class ApplicationDbContext : IdentityDbContext<ApplicationUser>, IApplicationDbContext
{
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) { }

    public DbSet<Address> Address => Set<Address>();
    public DbSet<BookMark> BookMarks => Set<BookMark>();
    public DbSet<BookmarkGroup> BookmarkGroups => Set<BookmarkGroup>();
    public DbSet<Client> Clients => Set<Client>();
    public DbSet<ContactClient> ContactClients => Set<ContactClient>();
    public DbSet<Contact> Contacts => Set<Contact>();
    public DbSet<ContactMediaSection> ContactMediaSections => Set<ContactMediaSection>();
    public DbSet<PRActivityReceiver> EmailLogs => Set<PRActivityReceiver>();
    public DbSet<Media> Medias => Set<Media>();
    public DbSet<MediaSection> MediaSections => Set<MediaSection>();
    public DbSet<Platform> Platforms => Set<Platform>();
    public DbSet<Province> Provinces => Set<Province>();
    public DbSet<RegisterForm> RegisterForms => Set<RegisterForm>();
    public DbSet<District> Districts => Set<District>();
    public DbSet<SubDistrict> SubDistricts => Set<SubDistrict>();
    public DbSet<Section> Sections => Set<Section>();
    public DbSet<SubSection> SubSections => Set<SubSection>();
    public DbSet<New> News => Set<New>();
    public DbSet<LocalFile> LocalFiles => Set<LocalFile>();
    public DbSet<MonitorTaskWorker> MonitorTaskWorkers => Set<MonitorTaskWorker>();
    public DbSet<PRActivity> PRActivities => Set<PRActivity>();
    public DbSet<PRActivityBriefing> PRActivityBriefings => Set<PRActivityBriefing>();
    public DbSet<PRActivityEvent> PRActivityEvents => Set<PRActivityEvent>();
    public DbSet<PRActivityEmail> PRActivityEmails => Set<PRActivityEmail>();
    public DbSet<PRActivityLocation> PRActivityLocations => Set<PRActivityLocation>();
    public DbSet<PRActivityReceiver> PRActivityReceivers => Set<PRActivityReceiver>();
    public DbSet<PRActivityTask> PRActivityTasks => Set<PRActivityTask>();
    public DbSet<PRActivityTaskComment> PRActivityTaskComments => Set<PRActivityTaskComment>();
    public DbSet<PRActivityTaskResponsiblePerson> PRActivityTaskResponsiblePersons => Set<PRActivityTaskResponsiblePerson>();
    public DbSet<PRActivityLocalFile> PRActivityLocalFiles => Set<PRActivityLocalFile>();
    public DbSet<PRActivityEventJoiner> PRActivityEventJoiners => Set<PRActivityEventJoiner>();
    public DbSet<WorkProgress> WorkProgress => Set<WorkProgress>();
    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);
        builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
    }
}
