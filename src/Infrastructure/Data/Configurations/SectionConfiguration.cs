using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PRManagement.Domain.Entities;
using PRManagement.Infrastructure.ValueConversionExtensions;

namespace PRManagement.Infrastructure.Data.Configurations;

public class SectionConfiguration : IEntityTypeConfiguration<Section>
{
    public void Configure(EntityTypeBuilder<Section> builder)
    {
        builder.HasMany(e => e.MediaSections)
            .WithOne(e => e.Section)
            .HasForeignKey(e => e.SectionId);

        builder
            .Property(e => e.FormulaForCalculations)
            .HasJsonConversion();

        builder.HasMany(e => e.SubSections)
            .WithOne(e => e.Section)
            .HasForeignKey(e => e.SectionId);
    }
}
