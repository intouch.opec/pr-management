using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PRManagement.Domain.Entities;

namespace PRManagement.Infrastructure.Data.Configurations;

public class PRActivityTaskCommentConfiguration : IEntityTypeConfiguration<PRActivityTaskComment>
{
    public void Configure(EntityTypeBuilder<PRActivityTaskComment> builder)
    {
        builder.HasOne(e => e.PRActivityTask)
            .WithMany(e => e.PRActivityTaskComments)
            .HasForeignKey(e => e.PRActivityTaskId)
            .OnDelete(DeleteBehavior.Cascade);

        builder.HasOne(e => e.Owner)
            .WithMany(e => e.PRActivityTaskComments)
            .HasForeignKey(e => e.CreatedBy)
            .OnDelete(DeleteBehavior.Cascade);

        builder
            .HasQueryFilter(pr => !pr.PRActivityTask.IsDeleted);

         builder
            .HasQueryFilter(pr => !pr.IsDeleted);

    }
}
