using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PRManagement.Domain.Entities;

namespace PRManagement.Infrastructure.Data.Configurations;

public class ClientConfiguration : IEntityTypeConfiguration<Client>
{
    public void Configure(EntityTypeBuilder<Client> builder)
    {
        builder.HasOne(e => e.Address)
            .WithMany()
            .HasForeignKey(e => e.AddressId)
            .IsRequired(false);

        builder.HasMany(e => e.ContactClients)
            .WithOne(e => e.Client)
            .HasForeignKey(e => e.ClientId);

        builder
            .HasQueryFilter(b => !b.IsDeleted);

        builder.HasMany(e => e.PRActivities)
            .WithOne(e => e.Client)
            .HasForeignKey(e => e.ClientId)
            .OnDelete(DeleteBehavior.Cascade)
            .IsRequired(false);

        builder
            .HasQueryFilter(b => !b.IsDeleted);

    }
}
