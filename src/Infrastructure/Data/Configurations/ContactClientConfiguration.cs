
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PRManagement.Domain.Entities;

namespace PRManagement.Infrastructure.Data.Configurations;

public class ContactClientConfiguration : IEntityTypeConfiguration<ContactClient>
{
    public void Configure(EntityTypeBuilder<ContactClient> builder)
    {
        builder.HasOne(e => e.Contact)
            .WithMany(e => e.ContactClients)
            .HasForeignKey(e => e.ContactId);

        builder.HasOne(e => e.Client)
            .WithMany(e => e.ContactClients)
            .HasForeignKey(e => e.ClientId);

        builder
            .HasQueryFilter(b => !b.IsDeleted);
    }
}
