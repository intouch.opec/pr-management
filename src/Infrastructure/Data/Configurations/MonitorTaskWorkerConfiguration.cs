using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PRManagement.Domain.Entities;

namespace PRManagement.Infrastructure.Data.Configurations;

public class MonitorTaskWorkerConfiguration : IEntityTypeConfiguration<MonitorTaskWorker>
{
    public void Configure(EntityTypeBuilder<MonitorTaskWorker> builder)
    {
        builder.HasMany(e => e.News)
            .WithOne(e => e.MonitorTaskWorker)
            .HasForeignKey(e => e.MonitorTaskWorkerId)
            .OnDelete(DeleteBehavior.Cascade)
            .IsRequired(false);

        builder.HasOne(e => e.PRActivity)
            .WithMany(e => e.MonitorTaskWorkers)
            .HasForeignKey(e => e.PRActivityId)
            .OnDelete(DeleteBehavior.Cascade)
            .IsRequired(false);

        builder
            .HasQueryFilter(b => !b.IsDeleted);
    }
}
