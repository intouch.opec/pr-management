using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PRManagement.Domain.Entities;

namespace PRManagement.Infrastructure.Data.Configurations;

public class PRActivityLocalFileConfiguration : IEntityTypeConfiguration<PRActivityLocalFile>
{
    public void Configure(EntityTypeBuilder<PRActivityLocalFile> builder)
    {
        builder.HasOne(e => e.PRActivity)
            .WithMany(e => e.PRActivityLocalFiles)
            .HasForeignKey(e => e.PRActivityId)
            .OnDelete(DeleteBehavior.Cascade);

        builder.HasOne(e => e.LocalFile)
            .WithMany(e => e.PRActivityLocalFiles)
            .HasForeignKey(e => e.LocalFileId)
            .OnDelete(DeleteBehavior.Cascade);

        builder
            .HasQueryFilter(b => !b.IsDeleted);
    }
}
