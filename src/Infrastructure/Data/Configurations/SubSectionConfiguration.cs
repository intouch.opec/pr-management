using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PRManagement.Domain.Entities;

namespace PRManagement.Infrastructure.Data.Configurations;

public class SubSectionConfiguration : IEntityTypeConfiguration<SubSection>
{
    public void Configure(EntityTypeBuilder<SubSection> builder)
    {
        builder.HasMany(e => e.MediaSections)
            .WithOne(e => e.SubSection)
            .HasForeignKey(e => e.SubSectionId);
    }
}
