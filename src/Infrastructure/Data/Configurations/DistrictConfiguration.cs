using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PRManagement.Domain.Entities;

namespace PRManagement.Infrastructure.Data.Configurations;

public class DistrictConfiguration : IEntityTypeConfiguration<District>
{
    public void Configure(EntityTypeBuilder<District> builder)
    {
        builder.HasKey(e => e.Id);

        builder.HasOne(e => e.Province)
            .WithMany(e => e.Districts)
            .HasForeignKey(e => e.ProvinceId);
    }
}
