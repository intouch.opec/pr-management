using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PRManagement.Domain.Entities;

namespace PRManagement.Infrastructure.Data.Configurations;

public class ProvinceConfiguration : IEntityTypeConfiguration<Province>
{
    public void Configure(EntityTypeBuilder<Province> builder)
    {
        builder.HasKey(e => e.Id);

        builder.HasMany(e => e.Districts)
            .WithOne(e => e.Province)
            .HasForeignKey(e => e.ProvinceId);

        builder.HasMany(e => e.Address)
            .WithOne(e => e.Province)
            .HasForeignKey(e => e.ProvinceId);
    }
}
