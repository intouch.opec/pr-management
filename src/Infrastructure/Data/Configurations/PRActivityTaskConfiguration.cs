using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PRManagement.Domain.Entities;

namespace PRManagement.Infrastructure.Data.Configurations;

public class PRActivityTaskConfiguration : IEntityTypeConfiguration<PRActivityTask>
{
    public void Configure(EntityTypeBuilder<PRActivityTask> builder)
    {
        builder.HasOne(e => e.PRActivity)
            .WithMany(e => e.PRActivityTasks)
            .HasForeignKey(e => e.PRActivityId)
            .OnDelete(DeleteBehavior.Cascade);

        builder.HasMany(e => e.PRActivityTaskComments)
            .WithOne(e => e.PRActivityTask)
            .HasForeignKey(e => e.PRActivityTaskId)
            .OnDelete(DeleteBehavior.Cascade);

        builder
            .HasQueryFilter(pr => !pr.PRActivity!.IsDeleted);

        builder
            .HasQueryFilter(pr => !pr.IsDeleted);

    }
}
