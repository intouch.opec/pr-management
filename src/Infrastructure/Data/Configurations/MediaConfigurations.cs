using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PRManagement.Domain.Entities;

namespace PRManagement.Infrastructure.Data.Configurations;

public class MediaConfigurations : IEntityTypeConfiguration<Media>
{
    public void Configure(EntityTypeBuilder<Media> builder)
    {
        builder
            .HasIndex(b => new { b.Name, b.Deleted, b.IsDeleted })
            .IsUnique();

        builder.HasOne(e => e.Address)
            .WithOne(e => e.Media)
            .HasForeignKey<Media>(e => e.AddressId)
            .IsRequired(false);

        builder.HasMany(e => e.MediaSections)
            .WithOne(e => e.Media)
            .HasForeignKey(e => e.MediaId);

        builder
            .HasQueryFilter(b => !b.IsDeleted);
    }
}
