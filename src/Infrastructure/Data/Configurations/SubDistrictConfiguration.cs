using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PRManagement.Domain.Entities;

namespace PRManagement.Infrastructure.Data.Configurations;

public class SubDistrictConfiguration : IEntityTypeConfiguration<SubDistrict>
{
    public void Configure(EntityTypeBuilder<SubDistrict> builder)
    {
        builder.HasKey(e => e.Id);

        builder.HasOne(e => e.District)
            .WithMany(e => e.SubDistricts)
            .HasForeignKey(e => e.DistrictId);
    }
}
