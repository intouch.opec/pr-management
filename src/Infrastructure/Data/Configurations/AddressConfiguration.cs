using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PRManagement.Domain.Entities;

namespace PRManagement.Infrastructure.Data.Configurations;
public class AddressConfiguration : IEntityTypeConfiguration<Address>
{
    public void Configure(EntityTypeBuilder<Address> builder)
    {
        builder.HasOne(e => e.Province)
            .WithMany(e => e.Address)
            .HasForeignKey(e => e.ProvinceId)
            .IsRequired(false);

        builder.HasOne(e => e.District)
            .WithMany(e => e.Address)
            .HasForeignKey(e => e.DistrictId)
            .IsRequired(false);

        builder.HasOne(e => e.SubDistrict)
            .WithMany(e => e.Address)
            .HasForeignKey(e => e.SubDistrictId)
            .IsRequired(false);

        builder.HasOne(e => e.Contact)
            .WithOne(e => e.Address)
            .HasForeignKey<Contact>(m => m.AddressId)
            .IsRequired(false)
            .OnDelete(DeleteBehavior.Cascade)
            .IsRequired(false);

        builder.HasOne(e => e.Media)
            .WithOne(e => e.Address)
            .HasForeignKey<Media>(m => m.AddressId)
            .IsRequired(false)
            .OnDelete(DeleteBehavior.Cascade)
            .IsRequired(false);

        builder.HasOne(e => e.MediaSection)
            .WithOne(e => e.Address)
            .HasForeignKey<MediaSection>(m => m.AddressId)
            .IsRequired(false)
            .OnDelete(DeleteBehavior.Cascade)
            .IsRequired(false);
        
        builder
            .HasQueryFilter(b => !b.IsDeleted);
    }
}