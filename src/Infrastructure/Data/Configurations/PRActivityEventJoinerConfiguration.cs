using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PRManagement.Domain.Entities;

namespace PRManagement.Infrastructure.Data.Configurations;

public class PRActivityEventJoinerConfiguration : IEntityTypeConfiguration<PRActivityEventJoiner>
{
    public void Configure(EntityTypeBuilder<PRActivityEventJoiner> builder)
    {
        builder.HasOne(e => e.PRActivity)
            .WithMany(e => e.PRActivityEventJoiners)
            .OnDelete(DeleteBehavior.Cascade);
        
        builder.HasOne(e => e.Contact)
            .WithMany(e => e.PRActivityEventJoiners)
            .OnDelete(DeleteBehavior.Cascade);

        builder
            .HasQueryFilter(b => !b.IsDeleted);
    }
}
