
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PRManagement.Domain.Entities;

namespace PRManagement.Infrastructure.Data.Configurations;

public class WorkProgressConfiguration : IEntityTypeConfiguration<WorkProgress>
{
    public void Configure(EntityTypeBuilder<WorkProgress> builder)
    {
        builder.HasOne(e => e.PRActivity)
            .WithMany(e => e.WorkProgress)
            .HasForeignKey(e => e.PRActivityId)
            .OnDelete(DeleteBehavior.Cascade);

        builder
            .HasQueryFilter(pr => !pr.PRActivity!.IsDeleted);

    }
}
