using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PRManagement.Domain.Entities;

namespace PRManagement.Infrastructure.Data.Configurations;

public class LocalFileConfiguration : IEntityTypeConfiguration<LocalFile>
{
    public void Configure(EntityTypeBuilder<LocalFile> builder)
    {
        builder.HasMany(e => e.PRActivityLocalFiles)
            .WithOne(e => e.LocalFile)
            .HasForeignKey(e => e.LocalFileId)
            .OnDelete(DeleteBehavior.Cascade)
            .IsRequired(false);

        builder
            .HasQueryFilter(b => !b.IsDeleted);
    }
}
