using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PRManagement.Domain.Entities;
using PRManagement.Infrastructure.ValueConversionExtensions;

namespace PRManagement.Infrastructure.Data.Configurations;

public class RegisterFormConfiguration : IEntityTypeConfiguration<RegisterForm>
{
    public void Configure(EntityTypeBuilder<RegisterForm> builder)
    {
        builder.HasKey(e => e.Id);

        builder
            .Property(e => e.RegisterFields)
            .HasJsonConversion();
    }
}
