using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PRManagement.Domain.Entities;
using PRManagement.Infrastructure.ValueConversionExtensions;

namespace PRManagement.Infrastructure.Data.Configurations;

public class PRActivityReceiverConfiguration : IEntityTypeConfiguration<PRActivityReceiver>
{
    public void Configure(EntityTypeBuilder<PRActivityReceiver> builder)
    {
        builder
           .HasOne(e => e.PRActivity)
           .WithMany(e => e.PRActivityReceivers)
           .HasForeignKey(e => e.PRActivityId)
           .IsRequired()
           .OnDelete(DeleteBehavior.Cascade);

        builder
            .HasQueryFilter(pr => !pr.PRActivity!.IsDeleted);
    }
}
