using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PRManagement.Domain.Entities;

namespace PRManagement.Infrastructure.Data.Configurations;

public class PRActivityConfiguration : IEntityTypeConfiguration<PRActivity>
{
    public void Configure(EntityTypeBuilder<PRActivity> builder)
    {
        builder.HasOne(e => e.Client)
            .WithMany(e => e.PRActivities)
            .HasForeignKey(e => e.ClientId)
            .OnDelete(DeleteBehavior.Cascade);

        builder.HasOne(e => e.Section)
            .WithMany(e => e.PRActivities)
            .HasForeignKey(e => e.SectionId)
            .OnDelete(DeleteBehavior.Cascade);

        builder.HasOne(e => e.SubSection)
            .WithMany(e => e.PRActivities)
            .HasForeignKey(e => e.SubSectionId)
            .OnDelete(DeleteBehavior.Cascade);

        builder.HasOne(e => e.PRActivityBriefing)
            .WithOne(e => e.PRActivity)
            .HasForeignKey<PRActivityBriefing>(e => e.PRActivityId)
            .OnDelete(DeleteBehavior.Cascade)
            .IsRequired(false);

        builder.HasOne(e => e.PRActivityEmail)
            .WithOne(e => e.PRActivity)
            .HasForeignKey<PRActivityEmail>(e => e.PRActivityId)
            .OnDelete(DeleteBehavior.Cascade)
            .IsRequired(false);

        builder.HasOne(e => e.PRActivityLocation)
           .WithOne(e => e.PRActivity)
           .HasForeignKey<PRActivityLocation>(e => e.PRActivityId)
           .OnDelete(DeleteBehavior.Cascade)
           .IsRequired(false);

        builder.HasOne(e => e.PRActivityEvent)
            .WithOne(e => e.PRActivity)
            .HasForeignKey<PRActivityEvent>(e => e.PRActivityId)
            .OnDelete(DeleteBehavior.Cascade)
            .IsRequired(false);

        builder.HasMany(e => e.PRActivityReceivers)
            .WithOne(e => e.PRActivity)
            .HasForeignKey(e => e.PRActivityId)
            .OnDelete(DeleteBehavior.Cascade)
            .IsRequired(false);

        builder.HasMany(e => e.PRActivityLocalFiles)
            .WithOne(e => e.PRActivity)
            .HasForeignKey(e => e.PRActivityId)
            .OnDelete(DeleteBehavior.Cascade)
            .IsRequired(false);

        builder.HasMany(e => e.PRActivityTasks)
            .WithOne(e => e.PRActivity)
            .HasForeignKey(e => e.PRActivityId)
            .OnDelete(DeleteBehavior.Cascade);

        builder.HasMany(e => e.WorkProgress)
            .WithOne(e => e.PRActivity)
            .HasForeignKey(e => e.PRActivityId);

        builder.HasOne(e => e.Owner)
            .WithMany(e => e.PRActivities)
            .HasForeignKey(e => e.OwnerId);

        builder.HasMany(e => e.MonitorTaskWorkers)
            .WithOne(e => e.PRActivity)
            .HasForeignKey(e => e.PRActivityId);

        builder
            .HasQueryFilter(b => !b.IsDeleted);
    }
}