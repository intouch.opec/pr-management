using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PRManagement.Domain.Entities;
using PRManagement.Infrastructure.ValueConversionExtensions;

namespace PRManagement.Infrastructure.Data.Configurations;

public class BookMarkGroupConfiguration : IEntityTypeConfiguration<BookmarkGroup>
{
    public void Configure(EntityTypeBuilder<BookmarkGroup> builder)
    {

        builder.HasMany(e => e.BookMarks)
            .WithOne(e => e.BookmarkGroup)
            .HasForeignKey(e => e.BookmarkGroupId);

        builder
            .Property(e => e.Tags)
            .HasJsonConversion();

        builder
            .HasQueryFilter(b => !b.IsDeleted);
    }
}
