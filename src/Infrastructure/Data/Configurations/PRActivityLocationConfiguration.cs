using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PRManagement.Domain.Entities;

namespace PRManagement.Infrastructure.Data.Configurations;

public class PRActivityLocationConfiguration : IEntityTypeConfiguration<PRActivityLocation>
{
    public void Configure(EntityTypeBuilder<PRActivityLocation> builder)
    {
        builder.HasOne(e => e.PRActivity)
            .WithOne(e => e.PRActivityLocation)
            .HasForeignKey<PRActivityLocation>(e => e.PRActivityId)
            .OnDelete(DeleteBehavior.Cascade);

        builder.HasOne(e => e.Province)
            .WithMany(e => e.PRActivityLocations)
            .HasForeignKey(e => e.ProvinceId)
            .OnDelete(DeleteBehavior.Cascade);

        builder.HasOne(e => e.District)
            .WithMany(e => e.PRActivityLocations)
            .HasForeignKey(e => e.DistrictId)
            .OnDelete(DeleteBehavior.Cascade);

        builder.HasOne(e => e.SubDistrict)
            .WithMany(e => e.PRActivityLocations)
            .HasForeignKey(e => e.SubDistrictId)
            .OnDelete(DeleteBehavior.Cascade);
    }
}
