using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;

namespace PRManagement.Infrastructure.Data.Configurations;

public class BookMarkConfiguration : IEntityTypeConfiguration<BookMark>
{
    public void Configure(EntityTypeBuilder<BookMark> builder)
    {

        builder.HasOne(e => e.Address)
            .WithMany(e => e.BookMarks)
            .HasForeignKey(e => e.AddressId)
            .OnDelete(DeleteBehavior.Cascade)
            .IsRequired(false);

        builder.HasOne(e => e.BookmarkGroup)
            .WithMany(e => e.BookMarks)
            .HasForeignKey(e => e.BookmarkGroupId)
            .IsRequired()
            .OnDelete(DeleteBehavior.Cascade)
            .IsRequired(false);

        builder.HasQueryFilter(b => b.BookMarkType == BookMarkType.Media
            ? !b.Address!.Media!.IsDeleted
            : b.BookMarkType == BookMarkType.Contact
                ? !b.Address!.Contact!.IsDeleted
                : !b.Address!.MediaSection!.IsDeleted
        );

        builder
            .HasQueryFilter(b => !b.IsDeleted);
    }
}
