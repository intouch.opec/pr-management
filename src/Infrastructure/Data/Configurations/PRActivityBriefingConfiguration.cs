using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PRManagement.Domain.Entities;

namespace PRManagement.Infrastructure.Data.Configurations;

public class PRActivityBriefingConfiguration : IEntityTypeConfiguration<PRActivityBriefing>
{
    public void Configure(EntityTypeBuilder<PRActivityBriefing> builder)
    {
        builder.HasOne(e => e.PRActivity)
            .WithOne(e => e.PRActivityBriefing)
            .HasForeignKey<PRActivityBriefing>(e => e.PRActivityId)
            .OnDelete(DeleteBehavior.Cascade);
        
    }
}
