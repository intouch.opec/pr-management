using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PRManagement.Domain.Entities;

namespace PRManagement.Infrastructure.Data.Configurations;

public class NewConfiguration : IEntityTypeConfiguration<New>
{
    public void Configure(EntityTypeBuilder<New> builder)
    {

        builder.HasOne(e => e.MonitorTaskWorker)
            .WithMany(t => t.News)
            .HasForeignKey(e => e.MonitorTaskWorkerId)
            .OnDelete(DeleteBehavior.Cascade)
            .IsRequired(false);

        builder.HasOne(e => e.Platform)
            .WithMany(t => t.News)
            .HasForeignKey(e => e.PlatformId)
            .OnDelete(DeleteBehavior.Cascade)
            .IsRequired(false);

        builder
            .HasQueryFilter(b => !b.IsDeleted);
    }
}
