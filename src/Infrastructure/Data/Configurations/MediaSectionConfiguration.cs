using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PRManagement.Domain.Entities;

namespace PRManagement.Infrastructure.Data.Configurations;

public class MediaSectionConfiguration : IEntityTypeConfiguration<MediaSection>
{
    public void Configure(EntityTypeBuilder<MediaSection> builder)
    {
        builder.HasOne(e => e.Media)
            .WithMany(e => e.MediaSections)
            .HasForeignKey(e => e.MediaId);

        builder.HasMany(e => e.Platforms)
            .WithOne(e => e.MediaSection)
            .HasForeignKey(e => e.MediaSectionId);

        builder.HasOne(e => e.Section)
            .WithMany(e => e.MediaSections)
            .HasForeignKey(e => e.SectionId);

        builder.HasOne(e => e.SubSection)
            .WithMany(e => e.MediaSections)
            .HasForeignKey(e => e.SubSectionId);

        builder.HasOne(e => e.Address)
            .WithOne(e => e.MediaSection)
            .HasForeignKey<MediaSection>(e => e.AddressId)
            .IsRequired(false);

        builder
            .HasQueryFilter(b => !b.IsDeleted);
    }
}
