using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PRManagement.Domain.Entities;

namespace PRManagement.Infrastructure.Data.Configurations;

public class PlatformConfiguration : IEntityTypeConfiguration<Platform>
{
    public void Configure(EntityTypeBuilder<Platform> builder)
    {
        builder.HasOne(e => e.MediaSection)
            .WithMany(e => e.Platforms)
            .HasForeignKey(e => e.MediaSectionId);

        builder.HasMany(e => e.News)
            .WithOne(e => e.Platform)
            .HasForeignKey(e => e.PlatformId);

        builder
            .HasQueryFilter(b => !b.IsDeleted);
    }
}
