using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PRManagement.Domain.Entities;

namespace PRManagement.Infrastructure.Data.Configurations;

public class PRActivityEmailConfiguration : IEntityTypeConfiguration<PRActivityEmail>
{
    public void Configure(EntityTypeBuilder<PRActivityEmail> builder)
    {
        builder.HasOne(e => e.PRActivity)
            .WithOne(e => e.PRActivityEmail)
            .HasForeignKey<PRActivityEmail>(e => e.PRActivityId)
            .OnDelete(DeleteBehavior.Cascade);
    }
}
