using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PRManagement.Domain.Entities;

namespace PRManagement.Infrastructure.Data.Configurations;

public class ContactConfiguration : IEntityTypeConfiguration<Contact>
{
    public void Configure(EntityTypeBuilder<Contact> builder)
    {
        builder.Property(e => e.FirstName)
            .IsRequired();

        builder.Property(e => e.LastName)
            .IsRequired();

        builder.Property(e => e.NickName)
            .IsRequired();

        builder.HasMany(e => e.ContactMediaSections)
            .WithOne(e => e.Contact)
            .HasForeignKey(e => e.Id);

        builder.HasMany(e => e.PRActivityReceivers)
            .WithOne(e => e.Contact)
            .HasForeignKey(e => e.ContactId)
            .OnDelete(DeleteBehavior.Cascade)
            .IsRequired(false);

        builder
            .HasQueryFilter(b => !b.IsDeleted);
    }
}
