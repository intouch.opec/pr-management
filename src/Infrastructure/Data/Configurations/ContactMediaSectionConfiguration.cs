using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PRManagement.Domain.Entities;

namespace PRManagement.Infrastructure.Data.Configurations;

public class ContactMediaSectionConfiguration : IEntityTypeConfiguration<ContactMediaSection>
{
    public void Configure(EntityTypeBuilder<ContactMediaSection> builder)
    {
        builder.HasOne(e => e.Contact)
            .WithMany(e => e.ContactMediaSections)
            .HasForeignKey(e => e.ContactId);

        builder.HasOne(e => e.MediaSection)
            .WithMany(e => e.ContactMediaSections)
            .HasForeignKey(e => e.MediaSectionId);

        builder
            .HasQueryFilter(b => !b.IsDeleted);
    }
}
