using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PRManagement.Domain.Entities;
using PRManagement.Infrastructure.ValueConversionExtensions;

namespace PRManagement.Infrastructure.Data.Configurations;

public class PRActivityTaskResponsiblePersonConfiguration : IEntityTypeConfiguration<PRActivityTaskResponsiblePerson>
{
    public void Configure(EntityTypeBuilder<PRActivityTaskResponsiblePerson> builder)
    {
            
        builder.HasOne(e => e.PRActivityTask)
            .WithMany(e => e.PRActivityTaskResponsiblePersons)
            .HasForeignKey(e => e.PRActivityTaskId)
            .OnDelete(DeleteBehavior.Cascade);

        builder.HasOne(e => e.Owner)
            .WithMany(e => e.PRActivityTaskResponsiblePersons)
            .HasForeignKey(e => e.OwnerId)
            .OnDelete(DeleteBehavior.Cascade);

        builder
            .HasQueryFilter(pr => !pr.PRActivityTask!.IsDeleted);

        builder
            .HasQueryFilter(pr => !pr.IsDeleted);

    }
}
