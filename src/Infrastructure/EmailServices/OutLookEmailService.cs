using Microsoft.Extensions.Options;
using MimeKit;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.Common.Configurations;
using MailKit.Net.Smtp;

namespace PRManagement.Infrastructure.EmailServices;

public class OutLookEmailService : IMailService
{
    // private readonly string _smtpServer = "smtp.example.com";
    // private readonly int _smtpPort = 587;
    // private readonly string _username = "your-email@example.com";
    // private readonly string _password = "your-email-password";
    private readonly SmtpServerConfiguration _smtpServerConfiguration;

    public OutLookEmailService(IOptions<SmtpServerConfiguration> options)
    {
        _smtpServerConfiguration = options.Value;
    }

    public async Task SendEmailAsync(string toEmail, string subject, string body)
    {
        var message = new MimeMessage();
        message.From.Add(new MailboxAddress("Sender Name", "intouch.opec1993@outlook.com"));
        message.To.Add(new MailboxAddress("Recipient Name", toEmail));
        message.Subject = subject;

        var builder = new BodyBuilder { HtmlBody = body };
        message.Body = builder.ToMessageBody();

        // Add a custom header for read receipt
        message.Headers.Add("Disposition-Notification-To", "intouch.opec@gmail.com");

        using (var client = new SmtpClient())
        {
            await client.ConnectAsync("smtp.example.com", _smtpServerConfiguration.Port, false);
            await client.AuthenticateAsync("intouch.opec1993@outlook.com", "We.learn1993Qq");
            await client.SendAsync(message);
            await client.DisconnectAsync(true);
        }
    }
}
