using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PRManagement.Domain.Entities;

public class Section : BaseAuditableEntity
{
    [MaxLength(55)]
    public string Name { get; set; } = string.Empty;
    [Column(TypeName = "jsonb")]
    public List<FormulaForCalculation> FormulaForCalculations { get; set; } = new();

    public ICollection<SubSection>? SubSections { get; set; }
    public ICollection<MediaSection>? MediaSections { get; set; }
    public ICollection<PRActivity>? PRActivities { get; set; }
}

public class FormulaForCalculation
{
    public uint PriceOfPurchaseRequisition { get; set; }
    public byte Tier { get; set; }
    public uint MinimumFollowers { get; set; }
}
