namespace PRManagement.Domain.Entities;

public class ContactClient : BaseAuditableEntity
{
    public Guid ContactId { get; set; }
    public Guid ClientId { get; set; }
    public Contact? Contact { get; set; }
    public Client? Client { get; set; }
}
