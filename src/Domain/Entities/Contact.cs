using System.ComponentModel.DataAnnotations;
using PRManagement.Domain.Interfaces;

namespace PRManagement.Domain.Entities;

public class Contact : BaseAuditableEntity, IRemarkableEntity
{
    [MaxLength(75)]
    public string FirstName { get; set; } = string.Empty;
    [MaxLength(55)]
    public string LastName { get; set; } = string.Empty;
    [MaxLength(55)]
    public string NickName { get; set; } = string.Empty;
    [MaxLength(55)]
    public string Position { get; set; } = string.Empty;
    [MaxLength(55)]
    public string LineId { get; set; } = string.Empty;
    public string Remark { get; set; } = string.Empty;
    public AddressType? AddressType { get; set; }
    public ContactType ContactType { get; set; }
    public Guid? AddressId { get; set; }
    public double PercentageOfFullField { get; set; }

    public Address? Address { get; set; }
    public ICollection<ContactMediaSection>? ContactMediaSections { get; set; }
    public ICollection<ContactClient>? ContactClients { get; set; }
    public ICollection<PRActivityReceiver>? PRActivityReceivers { get; set; }
    public ICollection<PRActivityEventJoiner>? PRActivityEventJoiners { get; set; }
    [MaxLength(155)]
    public string RemarkDeleted { get; set; } = string.Empty;
}
