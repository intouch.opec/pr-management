using System.ComponentModel.DataAnnotations;
using PRManagement.Domain.Interfaces;

namespace PRManagement.Domain.Entities;

public class Company : BaseAuditableEntity, IRemarkableEntity
{
    [MaxLength(75)]
    public string Name { get; set; } = string.Empty;
    public Guid? AddressId { get; set; }
    [MaxLength(255)]
    public string RemarkDeleted { get; set; } = string.Empty;

    public Address? Address { get; set; }
}
