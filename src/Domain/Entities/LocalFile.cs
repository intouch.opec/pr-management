
using System.ComponentModel.DataAnnotations;

namespace PRManagement.Domain.Entities;

public class LocalFile : BaseAuditableEntity
{
    [MaxLength(155)]
    public string Path { get ; set; } = string.Empty;
    [MaxLength(155)]
    public string ContentType { get; set; } = string.Empty;
    public string Size { get; set; } = string.Empty;
    [MaxLength(255)]
    public string FileName { get; set; } = string.Empty;

    public ICollection<PRActivityLocalFile>? PRActivityLocalFiles { get; set; }
}
