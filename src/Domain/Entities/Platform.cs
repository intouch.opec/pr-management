using System.ComponentModel.DataAnnotations;
using PRManagement.Domain.Interfaces;

namespace PRManagement.Domain.Entities;
public class Platform : BaseAuditableEntity, IRemarkableEntity
{
    [MaxLength(55)]
    public string Name { get; set; } = string.Empty;
    public string Link { get; set; } = string.Empty;
    public decimal PRValue { get; set; }
    public decimal ADValue { get; set; }
    public uint Potential { get; set; }
    public uint TierOder { get; set; }
    public uint TierPotential { get; set; }
    public PlatformType PlatFormType { get; set; }
    public Guid MediaSectionId { get; set; }
    public MediaSection? MediaSection { get; set; }
    public ICollection<New>? News { get; set; }
    [MaxLength(155)]
    public string RemarkDeleted { get; set; } = string.Empty;
}
