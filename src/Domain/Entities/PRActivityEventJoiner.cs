namespace PRManagement.Domain.Entities;

public class PRActivityEventJoiner : BaseAuditableEntity
{
    public Guid ContactId { get; set; }
    public Guid PRActivityId { get; set; }
    public JoinType JoinType { get; set; }

    public Contact? Contact { get; set; }
    public PRActivity? PRActivity { get; set; }
}
