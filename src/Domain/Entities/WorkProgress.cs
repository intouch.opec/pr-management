
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace PRManagement.Domain.Entities;

public class WorkProgress
{
    [Key]
    [NotNull]
    public Guid Id { get; set; }
    public WorkTask WorkTask { get; set; }
    public TaskWorkerStatus TaskWorkerStatus { get; set; }
    public Guid PRActivityId { get; set; }
    public PRActivity? PRActivity { get; set; }
}
