using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using PRManagement.Domain.Interfaces;

namespace PRManagement.Domain.Entities;
public class BookmarkGroup : BaseAuditableEntity, IRemarkableEntity
{
    public string Name { get; set; } = string.Empty;
    [Column(TypeName = "jsonb")]
    public List<Tag> Tags { get; set; } = new List<Tag>();
    public ICollection<BookMark>? BookMarks { get; set; }
    [MaxLength(155)]
    public string RemarkDeleted { get; set; } = string.Empty;
}

public class Tag
{
    public string Name { get; set; } = string.Empty;
    public string Color { get; set; } = string.Empty;
}
