using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace PRManagement.Domain.Entities;

public class District
{
    [Key]
    [NotNull]
    [MaxLength(10)]
    public required string Id { get; set; } = string.Empty;
    [MaxLength(55)]
    public string NameTH { get; set; } = string.Empty;
    [MaxLength(55)]
    public string NameEN { get; set; } = string.Empty;
    [MaxLength(10)]
    public required string ProvinceId { get; set; }
    public required Province Province { get; set; }

    public ICollection<Address> Address { get; set; } = new List<Address>();
    public ICollection<SubDistrict> SubDistricts { get; set; } = new List<SubDistrict>();
    public ICollection<PRActivityLocation>? PRActivityLocations { get; set; }
}
