using System.ComponentModel.DataAnnotations;

namespace PRManagement.Domain.Entities;

public class SubSection : BaseAuditableEntity
{
    public required Guid SectionId { get; set; }
    public string Name { get; set; } = string.Empty;

    public Section? Section { get; set; }
    public ICollection<MediaSection>? MediaSections { get; set; }
    public ICollection<PRActivity>? PRActivities { get; set; }
}
