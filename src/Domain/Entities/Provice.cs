using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace PRManagement.Domain.Entities;
public class Province
{
    [Key]
    [NotNull]
    [MaxLength(10)]
    public required string Id { get; set; } = string.Empty;
    [MaxLength(45)]
    public string NameTH { get; set; } = string.Empty;
    [MaxLength(45)]
    public string NameEN { get; set; } = string.Empty;

    public ICollection<Address> Address { get; set; } = new List<Address>();
    public ICollection<District> Districts { get; set; } = new List<District>();
    public ICollection<PRActivityLocation>? PRActivityLocations { get; set; }
}
