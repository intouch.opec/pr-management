using System.ComponentModel.DataAnnotations;
using PRManagement.Domain.Interfaces;

namespace PRManagement.Domain.Entities;

public class PRActivityTask : BaseAuditableEntity, IRemarkableEntity
{
    public TaskWorkerStatus TaskWorkerStatus { get; set; }
    public WorkTask WorkTask { get; set; }
    public DateTimeOffset DueDate { get; set; }
    [MaxLength(255)]
    public string Description { get; set; } = string.Empty;
    [MaxLength(155)]
    public string RemarkDeleted { get; set; } = string.Empty;

    public Guid PRActivityId { get; set; }
    public PRActivity? PRActivity { get; set; } = null!;
    public ICollection<PRActivityTaskComment>? PRActivityTaskComments { get; set; } 
    public ICollection<PRActivityTaskResponsiblePerson>? PRActivityTaskResponsiblePersons { get; set; }
}
