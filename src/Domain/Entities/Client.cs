using System.ComponentModel.DataAnnotations;
using PRManagement.Domain.Interfaces;

namespace PRManagement.Domain.Entities;

public class Client : BaseAuditableEntity, IRemarkableEntity
{
    [MaxLength(155)]
    public string Name { get; set; } = string.Empty;
    public Guid? AddressId { get; set; }
    public double PercentageOfFullField { get; set; }
    public string[] ClientTypes { get; set; } = Array.Empty<string>();

    public Address? Address { get; set; }
    public ICollection<ContactClient>? ContactClients { get; set; }
    public ICollection<PRActivity>? PRActivities { get; set; }

    [MaxLength(155)]
    public string RemarkDeleted { get; set; } = string.Empty;
}
