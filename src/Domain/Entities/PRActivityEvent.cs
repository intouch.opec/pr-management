using System.ComponentModel.DataAnnotations;

namespace PRManagement.Domain.Entities;

public class PRActivityEvent : BaseAuditableEntity
{
    [MaxLength(10)]
    public string EventCode { get; set; } = string.Empty;
    public int NumberOfJoiner { get; set; }
    public DateTimeOffset EventDate { get; set; }
    public Guid PRActivityId { get; set; }

    public PRActivity? PRActivity { get; set; }
}
