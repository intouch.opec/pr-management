using System.ComponentModel.DataAnnotations;

namespace PRManagement.Domain.Entities;

public class PRActivityReceiver: BaseAuditableEntity
{
    [MaxLength(55)]
    public string Email { get; set; } = string.Empty;
    public DateTimeOffset? Sended { get; set; }
    public DateTimeOffset? Read { get; set; }
    public DateTimeOffset? OpenLink { get; set; }
    public Guid PRActivityId { get; set; }
    public Guid ContactId { get ; set; }
    public StatusReceiver Status { get; set; }
    public Contact Contact { get; set; } = null!;
    [MaxLength(10)]
    public string Code { get; set; } = string.Empty;
   
    public PRActivity? PRActivity { get; set; }
}
