using System.ComponentModel.DataAnnotations;
using PRManagement.Domain.Interfaces;

namespace PRManagement.Domain.Entities;

public class BookMark : BaseAuditableEntity, IRemarkableEntity
{
    public Guid BookmarkGroupId { get; set; }
    public Guid AddressId { get; set; }
    public BookMarkType BookMarkType { get; set; }
    public int[]? ActiveEmail { get; set; } = null;
    public BookmarkGroup? BookmarkGroup { get; set; }
    public Address? Address { get; set; }
    [MaxLength(155)]
    public string RemarkDeleted { get; set; } = string.Empty;
}
