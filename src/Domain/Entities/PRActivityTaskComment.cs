namespace PRManagement.Domain.Entities;

public class PRActivityTaskComment : BaseAuditableEntity
{
    public string Content { get; set; } = string.Empty;
    public Guid PRActivityTaskId { get; set; }
    
    public PRActivityTask PRActivityTask { get; set; } = null!;
    public ApplicationUser? Owner { get; set; } = null!;
}
