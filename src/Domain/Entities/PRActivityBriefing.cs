namespace PRManagement.Domain.Entities;

public class PRActivityBriefing : BaseAuditableEntity
{
    public string Content { get; set; } = string.Empty;
    public Guid PRActivityId { get; set; }

    public PRActivity? PRActivity { get; set; } = null!;
}
