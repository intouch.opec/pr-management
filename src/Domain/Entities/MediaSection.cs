using System.ComponentModel.DataAnnotations;
using PRManagement.Domain.Interfaces;

namespace PRManagement.Domain.Entities;

public class MediaSection : BaseAuditableEntity, IRemarkableEntity
{
    public TypeOfMedia TypeOfMedia { get; set; }
    public Guid MediaId { get; set; }
    public Guid SectionId { get; set; }
    public Guid SubSectionId { get; set; }

    public required Media Media { get; set; }
    public required Section Section { get; set; }
    public required SubSection SubSection { get; set; }
    public ICollection<Platform> Platforms { get; set; } = new List<Platform>();
    public ICollection<Contact>? Contacts { get; set; }
    public ICollection<ContactMediaSection>? ContactMediaSections { get; set; }
    public ICollection<New>? News { get; set; }

    [MaxLength(155)]
    public string RemarkDeleted { get; set; } = string.Empty;
    public uint TierPotential { get; set; } = 0;
    public Guid? AddressId { get; set; }

    public Address? Address { get; set; }
}
