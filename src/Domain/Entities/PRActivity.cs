using System.ComponentModel.DataAnnotations;
using PRManagement.Domain.Interfaces;

namespace PRManagement.Domain.Entities;

public class PRActivity : BaseAuditableEntity, IRemarkableEntity
{
    [MaxLength(55)]
    public string Title { get; set; } = string.Empty;
    public DurationPRActivityType DurationType { get; set; }
    public ContentPRActivityType ContentPRActivityType { get; set; }
    public ContractType ContractType { get; set; }
    public DateTimeOffset Started { get; set; }
    public DateTimeOffset Ended { get; set; }
    public DateTimeOffset Released { get; set; }
    public Guid ClientId { get; set; }
    public Guid SectionId { get; set; }
    public Guid SubSectionId { get; set; }
    public string OwnerId { get; set; } = string.Empty;
    public PrStatus PrStatus { get; set; }
    [MaxLength(155)]
    public string RemarkDeleted { get; set; } = string.Empty;

    public ApplicationUser? Owner { get; set; }
    public Client? Client { get; set; }
    public Section? Section { get; set; }
    public SubSection? SubSection { get; set; }
    public PRActivityBriefing? PRActivityBriefing { get; set; } = null;
    public PRActivityEmail? PRActivityEmail { get; set; } = null;
    public PRActivityEvent? PRActivityEvent { get; set; } = null;
    public PRActivityLocation? PRActivityLocation { get; set; }
    public ICollection<PRActivityLocalFile>? PRActivityLocalFiles { get; set; }
    public ICollection<PRActivityTask>? PRActivityTasks { get; set; }
    public ICollection<PRActivityReceiver>? PRActivityReceivers { get; set; }
    public ICollection<WorkProgress>? WorkProgress { get; set; } 
    public ICollection<MonitorTaskWorker>? MonitorTaskWorkers { get; set; }
    public ICollection<PRActivityEventJoiner>? PRActivityEventJoiners { get; set; }

}
