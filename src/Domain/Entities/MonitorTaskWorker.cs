
using System.ComponentModel.DataAnnotations;
using PRManagement.Domain.Interfaces;

namespace PRManagement.Domain.Entities;

public class MonitorTaskWorker : BaseAuditableEntity, IRemarkableEntity
{
    [MaxLength(55)]
    public string Topic { get; set; } = string.Empty;
    public string KeywordQuery { get; set; } = string.Empty;
    public TaskWorkerStatus TaskWorkerStatus { get; set; }
    public bool IsUrgent { get; set; }
    public Guid PRActivityId { get; set; }
    [MaxLength(125)]
    public string RemarkDeleted { get; set; } = string.Empty;

    public ICollection<New> News { get; set; } = new List<New>();
    public PRActivity? PRActivity { get; set; }
}
