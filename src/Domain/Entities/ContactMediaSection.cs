using System.ComponentModel.DataAnnotations;
using PRManagement.Domain.Interfaces;

namespace PRManagement.Domain.Entities;

public class ContactMediaSection : BaseAuditableEntity, IRemarkableEntity
{
    public Guid ContactId { get; set; }
    public Guid MediaSectionId { get; set; }
    public ContactStatus ContactStatus { get; set; }
    public Contact? Contact { get; set; }
    public MediaSection? MediaSection { get; set; }
    public string Remark { get; set; } = string.Empty;
    [MaxLength(155)]
    public string RemarkDeleted { get; set; } = string.Empty;
}
