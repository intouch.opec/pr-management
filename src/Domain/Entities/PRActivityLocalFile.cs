namespace PRManagement.Domain.Entities;

public class PRActivityLocalFile : BaseAuditableEntity
{
    public Guid? LocalFileId { get; set; }
    public Guid PRActivityId { get; set; }

    public LocalFile? LocalFile { get; set; }
    public PRActivity? PRActivity { get; set; }
}
