﻿using Microsoft.AspNetCore.Identity;

namespace PRManagement.Domain.Entities;

public class ApplicationUser : IdentityUser
{
    public string? NickName { get; set; }
    public ICollection<PRActivity>? PRActivities { get; set; }
    public ICollection<PRActivityTaskResponsiblePerson>? PRActivityTaskResponsiblePersons { get; set; }
    public ICollection<PRActivityTaskComment>? PRActivityTaskComments { get; set; }
}
