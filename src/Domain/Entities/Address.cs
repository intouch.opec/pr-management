using System.ComponentModel.DataAnnotations;

namespace PRManagement.Domain.Entities;

public class Address : BaseAuditableEntity
{
    public string[] Email { get; set; } = Array.Empty<string>();
    public string[] PhoneNumbers { get; set; } = Array.Empty<string>();
    public string Text { get; set; } = string.Empty;
    [MaxLength(5)]
    public string PostCode { get; set; } = string.Empty;
    public string? ProvinceId { get; set; }
    public string? DistrictId { get; set; }
    public string? SubDistrictId { get; set; }

    public Province? Province { get; set; }
    public District? District { get; set; }
    public SubDistrict? SubDistrict { get; set; }

    public ICollection<BookMark>? BookMarks { get; set; }
    public Contact? Contact { get; set; }
    public Media? Media { get; set; }
    public MediaSection? MediaSection { get; set; }
    public Company? Company { get; set; }
}
