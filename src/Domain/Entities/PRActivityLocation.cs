using System.ComponentModel.DataAnnotations;

namespace PRManagement.Domain.Entities;

public class PRActivityLocation : BaseAuditableEntity
{
    [MaxLength(255)]
    public string Text { get; set; } = string.Empty;
    [MaxLength(5)]
    public string PostCode { get; set; } = string.Empty;
    [MaxLength(1024)]
    public string UrlMap { get; set; } = string.Empty;
    public string? ProvinceId { get; set; }
    public string? DistrictId { get; set; }
    public string? SubDistrictId { get; set; }
    public Guid PRActivityId { get; set; }
    [MaxLength(1024)]
    public string Remark { get; set; } = string.Empty;

    public Province? Province { get; set; }
    public District? District { get; set; }
    public SubDistrict? SubDistrict { get; set; }
    public PRActivity? PRActivity { get; set; }
}
