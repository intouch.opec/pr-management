using System.ComponentModel.DataAnnotations;

namespace PRManagement.Domain.Entities;

public class PRActivityEmail : BaseAuditableEntity
{
    [MaxLength(255)]
    public string Subject { get; set; } = string.Empty;
    public string Content { get; set; } = string.Empty;
    public DateTimeOffset StartSent { get; set; }
    public DateTimeOffset SentFinished { get; set; }
    public Guid PRActivityId { get; set; }

    public PRActivity PRActivity { get; set; } = null!;
}
