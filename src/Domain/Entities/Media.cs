using System.ComponentModel.DataAnnotations;
using PRManagement.Domain.Interfaces;

namespace PRManagement.Domain.Entities;

public class Media : BaseAuditableEntity, IRemarkableEntity
{
    [MaxLength(75)]
    public string Name { get; set; } = string.Empty;
    public Guid? AddressId { get; set; }
    public double PercentageOfFullField { get; set; }

    public Address? Address { get; set; }
    public ICollection<MediaSection> MediaSections { get; set; } = new List<MediaSection>();
    [MaxLength(155)]
    public string RemarkDeleted { get; set; } = string.Empty;
}
