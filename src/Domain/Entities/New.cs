using System.ComponentModel.DataAnnotations;
using PRManagement.Domain.Interfaces;

namespace PRManagement.Domain.Entities;

public class New : BaseAuditableEntity, IRemarkableEntity
{
    [MaxLength(255)]
    public string Title { get; set; } = string.Empty;
    [MaxLength(55)]
    public string Owner { get; set; } = string.Empty;
    public string Link { get; set; } = string.Empty;
    public string Description { get; set; } = string.Empty;
    public MappingType Mapping { get; set; }
    public PlatformType? PlatformType { get; set; }
    public Guid? PlatformId { get; set; }
    public DateTimeOffset? PublicationDate { get; set; }
    public Guid MonitorTaskWorkerId { get; set; }
    
    public Platform? Platform { get; set; }
    public MonitorTaskWorker? MonitorTaskWorker { get; set; } = new MonitorTaskWorker();
    [MaxLength(155)]
    public string RemarkDeleted { get; set; } = string.Empty;
}
