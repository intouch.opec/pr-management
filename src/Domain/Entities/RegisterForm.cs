using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using PRManagement.Domain.Interfaces;

namespace PRManagement.Domain.Entities;

public class RegisterForm : BaseAuditableEntity, IRemarkableEntity
{
    [MaxLength(55)]
    public string Name { get; set; } = string.Empty;
    [MaxLength(255)]
    public string Detail { get; set; } = string.Empty;
    [MaxLength(155)]
    public string RemarkDeleted { get; set; } = string.Empty;
    [Column(TypeName = "jsonb")]
    public List<RegisterField> RegisterFields { get; set; } = new();
}

public class RegisterField
{
    public InputInformation InputType { get; set; }
    public bool IsRequired { get; set; } = false;
}
