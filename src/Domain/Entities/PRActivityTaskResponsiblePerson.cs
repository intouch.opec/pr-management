namespace PRManagement.Domain.Entities;

public class PRActivityTaskResponsiblePerson : BaseAuditableEntity
{
    public string OwnerId { get; set; } = string.Empty;
    public Guid PRActivityTaskId { get; set; }

    public ApplicationUser? Owner { get; set; }
    public PRActivityTask? PRActivityTask { get; set; }
}
