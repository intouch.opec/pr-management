using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace PRManagement.Domain.Entities;

public class SubDistrict
{
    [Key]
    [NotNull]
    [MaxLength(10)]
    public required string Id { get; set; } = string.Empty;
    [MaxLength(75)]
    public string NameTH { get; set; } = string.Empty;
    [MaxLength(55)]
    public string NameEN { get; set; } = string.Empty;
    [MaxLength(10)]
    public string PostCode { get; set; } = string.Empty;
    [MaxLength(10)]
    public required string DistrictId { get; set; }

    public required District District { get; set; }
    public ICollection<Address> Address { get; set; } = new List<Address>();
    public ICollection<PRActivityLocation>? PRActivityLocations { get; set; }
}
