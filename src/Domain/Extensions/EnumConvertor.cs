using System.Runtime.Serialization;

namespace PRManagement.Domain.Exceptions;

public static class EnumExtensions
{
    public static bool IsEnum<T>(this string value) where T : Enum
        => Enum.TryParse(typeof(T), value, out _);

    public static T? ToEnum<T>(this string value) where T : Enum
        => (T)Enum.Parse(typeof(T), value, true);

    public static T? ToEnumOrNull<T>(this string? value) where T : struct, Enum
    {
        if (string.IsNullOrWhiteSpace(value))
            return null;

        return Enum.TryParse(value, true, out T result) ? result : null;
    }

    public static IEnumerable<string> ToListString<TEnum>() where TEnum : struct, Enum
    {
        if (!typeof(TEnum).IsEnum)
            throw new ArgumentException($"Type '{typeof(TEnum).FullName}' is not an enum");

        return Enum.GetValues(typeof(TEnum))
            .Cast<TEnum>()
            .Select(e => e.ToString());
    }

    public static string GetEnumMemberValue<TEnum>(this TEnum enumValue)
    where TEnum : struct, Enum
    {
        var enumType = enumValue.GetType();
        var enumName = Enum.GetName(enumType, enumValue);

        var memberInfo = enumType.GetMember(enumName!)[0];
        var enumMemberAttribute = (EnumMemberAttribute?)Attribute.GetCustomAttribute(memberInfo, typeof(EnumMemberAttribute));

        return enumMemberAttribute!.Value!;
    }
}
