namespace PRManagement.Domain.Exceptions;

public static class ClassChecker
{
    public static bool AllPropertiesHaveValue<T>(T obj)
    {
        var type = typeof(T);
        var properties = type.GetProperties();

        foreach (var property in properties)
        {
            var value = property.GetValue(obj);

            if (value == null || value.Equals(GetDefault(property.PropertyType)))
            {
                return false;
            }
        }

        return true;
    }

    private static object? GetDefault(Type type)
      => type.IsValueType ? Activator.CreateInstance(type) : null;

}
