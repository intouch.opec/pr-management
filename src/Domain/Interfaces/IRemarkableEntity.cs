namespace PRManagement.Domain.Interfaces;

public interface IRemarkableEntity
{
  string RemarkDeleted { get; set; }
}