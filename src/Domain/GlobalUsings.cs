﻿global using PRManagement.Domain.Common;
global using PRManagement.Domain.Entities;
global using PRManagement.Domain.Enums;
global using PRManagement.Domain.Exceptions;
global using PRManagement.Domain.ValueObjects;