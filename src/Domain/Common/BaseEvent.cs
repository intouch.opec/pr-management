﻿using MediatR;

namespace PRManagement.Domain.Common;

public abstract class BaseEvent : INotification
{
}
