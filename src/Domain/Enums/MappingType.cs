using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace PRManagement.Domain.Enums;

[JsonConverter(typeof(JsonStringEnumConverter<MappingType>))]
public enum MappingType
{
    [EnumMember(Value = "PENDING")]
    PENDING,
    [EnumMember(Value = "DONE")]
    DONE,
    [EnumMember(Value = "MAP")]
    MAP
}
