using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace PRManagement.Domain.Enums;

[JsonConverter(typeof(JsonStringEnumConverter<ContactType>))]
public enum ContactType
{
    [EnumMember(Value = "Contact")]
    CONTACT,
    [EnumMember(Value = "Client")]
    CLIENT,
}
