using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace PRManagement.Domain.Enums;
[JsonConverter(typeof(JsonStringEnumConverter<BookMarkType>))]
public enum BookMarkType
{
    [EnumMember(Value = "Contact")]
    Contact,
    [EnumMember(Value = "Media")]
    Media,
    [EnumMember(Value = "MediaSection")]
    MediaSection,
}
