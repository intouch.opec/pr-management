namespace PRManagement.Domain.Enums;

public enum TierLevel
{
    None = 0,
    Low = 1,
    Medium = 2,
    High = 3
}
