using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace PRManagement.Domain.Enums;

[JsonConverter(typeof(JsonStringEnumConverter<StatusReceiver>))]
public enum StatusReceiver
{
    [EnumMember(Value = "Send")]
    SEND,
    [EnumMember(Value = "Confirm")]
    CONFIRM,
    [EnumMember(Value = "Process")]
    PROCESS
}
