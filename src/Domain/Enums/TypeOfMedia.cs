using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace PRManagement.Domain.Enums;

[JsonConverter(typeof(JsonStringEnumConverter<TypeOfMedia>))]
public enum TypeOfMedia
{
    [EnumMember(Value = "Mainstream")]
    MAINSTREAM,
    [EnumMember(Value = "Digital")]
    DIGITAL
}
