
using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace PRManagement.Domain.Enums;

[JsonConverter(typeof(JsonStringEnumConverter<ContentPRActivityType>))]
public enum ContentPRActivityType
{
    [EnumMember(Value = "Press Release")]
    PRESS_RELEASE,
    [EnumMember(Value = "Scoop News")]
    SCOOP_NEWS,
    [EnumMember(Value = "Caption News")]
    CAPTION_NEWS,
    [EnumMember(Value = "Short Content")]
    SHORT_CONTENT,
    [EnumMember(Value = "Photo Caption")]
    PHOTO_CAPTION,
    [EnumMember(Value = "Gossip News")]
    GOSSIP_NEWS,
    [EnumMember(Value = "Press Conference")]
    PRESS_CONFERENCE,
    [EnumMember(Value = "Media Visit")]
    MEDIA_VISIT,
    [EnumMember(Value = "Blogger Day")]
    BLOGGER_DAY,
    [EnumMember(Value = "Review")]
    REVIEW,
    [EnumMember(Value = "Exclusive Interview")]
    EXCLUSIVE_INTERVIEW,
    [EnumMember(Value = "Group Interview")]
    GROUP_INTERVIEW
}

public static class DurationLongActivity
{
    public static ContentPRActivityType[] ContentActivities = {
        ContentPRActivityType.PRESS_RELEASE,
        ContentPRActivityType.SCOOP_NEWS,
    };

    public static IEnumerable<string> GetMemberList()
    {
        return ContentActivities.Select(e => e.GetEnumMemberValue());
    }

}

public static class DurationShortActivity
{
    public static ContentPRActivityType[] ContentActivities = {
        ContentPRActivityType.CAPTION_NEWS,
        ContentPRActivityType.SHORT_CONTENT,
        ContentPRActivityType.PHOTO_CAPTION,
        ContentPRActivityType.GOSSIP_NEWS,
    };

    public static IEnumerable<string> GetMemberList()
    {
        return ContentActivities.Select(e => e.GetEnumMemberValue());
    }

}

public static class DurationEventActivity
{
    public static ContentPRActivityType[] ContentActivities = {
        ContentPRActivityType.PRESS_CONFERENCE,
        ContentPRActivityType.SCOOP_NEWS,
        ContentPRActivityType.CAPTION_NEWS,
        ContentPRActivityType.MEDIA_VISIT,
        ContentPRActivityType.BLOGGER_DAY,
        ContentPRActivityType.REVIEW,
        ContentPRActivityType.EXCLUSIVE_INTERVIEW,
        ContentPRActivityType.GROUP_INTERVIEW
    };

    public static IEnumerable<string> GetMemberList()
    {
        return ContentActivities.Select(e => e.GetEnumMemberValue());
    }

}


