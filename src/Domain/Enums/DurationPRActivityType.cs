
using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace PRManagement.Domain.Enums;

[JsonConverter(typeof(JsonStringEnumConverter<DurationPRActivityType>))]
public enum DurationPRActivityType
{
    [EnumMember(Value = "Long")]
    LONG,
    [EnumMember(Value = "Short")]
    SHORT,
    [EnumMember(Value = "Event")]
    EVENT
}
