using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace PRManagement.Domain.Enums;

[JsonConverter(typeof(JsonStringEnumConverter<TaskWorkerStatus>))]
public enum TaskWorkerStatus
{
    [EnumMember(Value = "PENDING")]
    PENDING,
    [EnumMember(Value = "NOW")]
    NOW,
    [EnumMember(Value = "DONE")]
    DONE,
}
