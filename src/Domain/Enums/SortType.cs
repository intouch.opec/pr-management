using System.Runtime.Serialization;

namespace PRManagement.Domain.Enums;

public enum SortType
{
    [EnumMember(Value = "DESC")]
    DESC,
    [EnumMember(Value = "ASC")]
    ASC
}
