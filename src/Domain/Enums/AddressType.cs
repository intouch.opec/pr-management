using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace PRManagement.Domain.Enums;

[JsonConverter(typeof(JsonStringEnumConverter<AddressType>))]
public enum AddressType
{
    [EnumMember(Value = "Work")]
    Work,
    [EnumMember(Value = "Home")]
    Home,
}
