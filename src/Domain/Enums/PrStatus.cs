using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace PRManagement.Domain.Enums;

[JsonConverter(typeof(JsonStringEnumConverter<PrStatus>))]
public enum PrStatus
{
    [EnumMember(Value = "Pending")]
    PENDING,
    [EnumMember(Value = "Process")]
    PROCESS,
    [EnumMember(Value = "Monitoring")]
    MONITORING,
    [EnumMember(Value = "Finish")]
    FINISH,
}
