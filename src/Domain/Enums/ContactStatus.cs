using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace PRManagement.Domain.Enums;

[JsonConverter(typeof(JsonStringEnumConverter<ContactStatus>))]
public enum ContactStatus
{
    [EnumMember(Value = "PART TIME")]
    PART_TIME,
    [EnumMember(Value = "FULL TIME")]
    FULL_TIME,
    [EnumMember(Value = "FORMER MEMBER")]
    FORMER_MEMBER
}
