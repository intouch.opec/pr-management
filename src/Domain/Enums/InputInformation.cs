using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace PRManagement.Domain.Enums;

[JsonConverter(typeof(JsonStringEnumConverter<InputInformation>))]
public enum InputInformation
{
    [EnumMember(Value = "FullName")]
    FullName,
    [EnumMember(Value = "CompanyName")]
    CompanyName,
    [EnumMember(Value = "PhoneNumber")]
    PhoneNumber,
    [EnumMember(Value = "Email")]
    Email,
    [EnumMember(Value = "LineID")]
    LineID,
    [EnumMember(Value = "Transportation")]
    Transportation,
    [EnumMember(Value = "NumberOfAttendees")]
    NumberOfAttendees,
    [EnumMember(Value = "DietaryRestrictions")]
    DietaryRestrictions,
    [EnumMember(Value = "FoodAllergies")]
    FoodAllergies,
    [EnumMember(Value = "ShirtSize")]
    ShirtSize,
    [EnumMember(Value = "Drinks")]
    Drinks,
}

public static class InputInformationContact
{
    public static IEnumerable<InputInformation> List = new List<InputInformation>()
    {
        InputInformation.FullName,
        InputInformation.CompanyName,
        InputInformation.PhoneNumber,
        InputInformation.Email,
        InputInformation.LineID,
    };

    public static IEnumerable<string> ListStrings
    {
        get
        {
            return List.Select(pl => pl.GetEnumMemberValue());
        }
    }

    public static uint Count { get => (uint)List.Count(); }

    public static bool IsInformationContact(InputInformation input) {
        return List.Contains(input);
    }
}

public static class InputInformationRegister
{
    public static IEnumerable<InputInformation> List = new List<InputInformation>()
    {
        InputInformation.Transportation,
        InputInformation.NumberOfAttendees,
        InputInformation.DietaryRestrictions,
        InputInformation.FoodAllergies,
        InputInformation.ShirtSize,
        InputInformation.Drinks,
    };

    public static IEnumerable<string> ListStrings
    {
        get
        {
            return List.Select(pl => pl.GetEnumMemberValue());
        }
    }
    
    public static uint Count { get => (uint)List.Count(); }
}
