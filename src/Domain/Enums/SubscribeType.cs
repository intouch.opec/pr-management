using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace PRManagement.Domain.Enums;

[JsonConverter(typeof(JsonStringEnumConverter<SubscribeType>))]
public enum SubscribeType
{
    [EnumMember(Value = "VISITOR")]
    VISITOR,
    [EnumMember(Value = "FOLLOWER")]
    FOLLOWER
}
