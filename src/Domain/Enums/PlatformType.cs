using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace PRManagement.Domain.Enums;

[JsonConverter(typeof(JsonStringEnumConverter<PlatformType>))]
public enum PlatformType
{
    [EnumMember(Value = "Facebook")]
    FACEBOOK,
    [EnumMember(Value = "Ig")]
    INSTAGRAM,
    [EnumMember(Value = "X")]
    X,
    [EnumMember(Value = "WEBSITE")]
    WEBSITE,
    [EnumMember(Value = "TIKTOK")]
    TIKTOK,
    [EnumMember(Value = "BLOG")]
    BLOG,
    [EnumMember(Value = "LINE_Today")]
    LINE_Today,
    [EnumMember(Value = "YOUTUBE")]
    YOUTUBE,
    [EnumMember(Value = "NEWSPAPER")]
    NEWSPAPER,
    [EnumMember(Value = "MAGAZINE")]
    MAGAZINE,
    [EnumMember(Value = "TV")]
    TV,
    [EnumMember(Value = "RADIO")]
    RADIO,
    [EnumMember(Value = "Blockdit")]
    BLOCKDIT,
}

public static class MainStreamPlatformType
{
    public static IEnumerable<PlatformType> PlatformTypes { get; set; } = new List<PlatformType>
    {
        PlatformType.NEWSPAPER,
        PlatformType.MAGAZINE,
        PlatformType.TV,
        PlatformType.RADIO
    };

    public static IEnumerable<string> PlatformTypeStrings
    {
        get
        {
            return PlatformTypes.Select(pl => pl.GetEnumMemberValue());
        }
    }
    public static uint Count { get => (uint)PlatformTypes.Count(); }
}

public static class DigitalPlatformType
{
    public static IEnumerable<PlatformType> PlatformTypes { get; set; } = new List<PlatformType>
    {
        PlatformType.FACEBOOK,
        PlatformType.INSTAGRAM,
        PlatformType.X,
        PlatformType.WEBSITE,
        PlatformType.TIKTOK,
        PlatformType.BLOG,
        PlatformType.LINE_Today,
        PlatformType.YOUTUBE,
    };

    public static IEnumerable<string> PlatformTypeStrings
    {
        get
        {
            return PlatformTypes.Select(pl => pl.GetEnumMemberValue());
        }
    }

    public static uint Count { get => (uint)PlatformTypes.Count(); }
}
