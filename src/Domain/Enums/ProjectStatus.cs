using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace PRManagement.Domain.Enums;

[JsonConverter(typeof(JsonStringEnumConverter<ProjectStatus>))]
public enum ProjectStatus
{
    [EnumMember(Value = "Draft")]
    DRAFT,
    [EnumMember(Value = "Pending")]
    PENDING,
    [EnumMember(Value = "In Progress")]
    IN_PROGRESS,
    [EnumMember(Value = "Completed")]
    COMPLETED,
    [EnumMember(Value = "On Hold")]
    ON_HOLD
}
