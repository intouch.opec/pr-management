using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace PRManagement.Domain.Enums;

[JsonConverter(typeof(JsonStringEnumConverter<JoinType>))]
public enum JoinType
{
    [EnumMember(Value = "WalkIn")]
    WalkIn,
    [EnumMember(Value = "Book")]
    Book,
}
