using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace PRManagement.Domain.Enums;
[JsonConverter(typeof(JsonStringEnumConverter<ContractType>))]
public enum ContractType
{
    [EnumMember(Value = "Adhoc")]
    ADHOC,
    [EnumMember(Value = "Retainer")]
    RETAINER
}
