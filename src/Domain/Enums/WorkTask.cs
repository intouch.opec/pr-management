using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace PRManagement.Domain.Enums;
[JsonConverter(typeof(JsonStringEnumConverter<WorkTask>))]
public enum WorkTask
{
    [EnumMember(Value = "Briefing Meeting")]
    BRIEFING_MEETING,
    [EnumMember(Value = "Confirm Location")]
    CONFIRM_LOCATION,
    [EnumMember(Value = "Upload Files")]
    UPLOAD_FILES,
    [EnumMember(Value = "Send Email Client")]
    SEND_EMAIL_CLIENT,
    [EnumMember(Value = "UpLoad Event")]
    UPLOAD_EVENT,
    [EnumMember(Value = "Sign Event")]
    SIGN_EVENT
}
