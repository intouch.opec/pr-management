﻿namespace PRManagement.Domain.Constants;

public abstract class Policies
{
    public const string CanPurge = nameof(CanPurge);
    public const string CanManageMedia = nameof(CanManageMedia);
    public const string CanManageContact = nameof(CanManageContact);
}