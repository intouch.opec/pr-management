namespace PRManagement.Domain.Constants;

public abstract class ValidatorMessages
{
    public const string InvalidFormat = "The '{PropertyName}' is invalid format.";
    public const string ShouldBeUnique = "The '{PropertyName}' is already exits.";
    public const string ShouldNotBeDuplicate = "The '{PropertyName}' is duplicate.";
    public const string InvalidArrayFormat = "The '{PropertyName}[{CollectionIndex}]' is invalid format.";
    public const string NotFound = "The '{PropertyName}' is not found.";
    public const string NotFoundArray = "The '{PropertyName}[{CollectionIndex}]' is invalid format.";
}
