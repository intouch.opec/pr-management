﻿namespace PRManagement.Domain.Constants;

public abstract class Roles
{
    public const string Administrator = nameof(Administrator);
    public const string PRTeam = nameof(PRTeam);
    public const string ADTeam = nameof(ADTeam);
}