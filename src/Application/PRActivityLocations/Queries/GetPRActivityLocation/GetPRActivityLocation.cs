﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.PRActivityLocations.Commands.CreatePRActivityLocation;

namespace PRManagement.Application.PRActivityLocations.Queries.GetPRActivityLocation;

public record GetPRActivityLocationQuery (string PRActivityId): IRequest<PRActivityLocationDto>;

public class GetPRActivityLocationQueryHandler : IRequestHandler<GetPRActivityLocationQuery, PRActivityLocationDto>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public GetPRActivityLocationQueryHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<PRActivityLocationDto> Handle(GetPRActivityLocationQuery request, CancellationToken cancellationToken)
    {
        var result = await _context.PRActivityLocations
            .AsNoTracking()
            .Include(e => e.SubDistrict)
            .Include(e => e.District)
            .Include(e => e.Province)
            .ProjectTo<PRActivityLocationDto>(_mapper.ConfigurationProvider)
            .FirstOrDefaultAsync(
                e => e.PRActivityId.Equals(request.PRActivityId.ToGuid()), 
                cancellationToken
            );

        Guard.Against.NotFound(request.PRActivityId, result);

        return result;
    }
}
