using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Constants;

namespace PRManagement.Application.PRActivityLocations.Commands.UpdatePRActivityLocation;

public class UpdatePRActivityLocationCommandValidator : AbstractValidator<UpdatePRActivityLocationCommand>
{
    private readonly IApplicationDbContext _context;

    public UpdatePRActivityLocationCommandValidator(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(e => e.ProvinceId)
            .MustAsync(_context.Provinces.HasProvince)
            .WithMessage(ValidatorMessages.NotFound);

        RuleFor(e => e.DistrictId)
            .MustAsync(_context.Districts.HasDistrict)
            .WithMessage(ValidatorMessages.NotFound);

        RuleFor(e => e.SubDistrictId)
            .MustAsync(_context.SubDistricts.HasSubDistrict)
            .WithMessage(ValidatorMessages.NotFound);

        RuleFor(e => e.Text)
            .NotEmpty()
            .MaximumLength(255);

        RuleFor(e => e.PostCode)
            .NotEmpty()
            .MaximumLength(5);

        RuleFor(e => e.UrlMap)
            .NotEmpty()
            .MaximumLength(1024);

        RuleFor(e => e.Remark)
            .MaximumLength(1024);
    }
}
