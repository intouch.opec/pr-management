﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.PRActivityLocations.Commands.CreatePRActivityLocation;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.PRActivityLocations.Commands.UpdatePRActivityLocation;

public record UpdatePRActivityLocationCommand : IRequest<PRActivityLocationDto>
{
    public string Text { get; set; } = string.Empty;
    public string PostCode { get; set; } = string.Empty;
    public string UrlMap { get; set; } = string.Empty;
    public string? ProvinceId { get; set; }
    public string? DistrictId { get; set; }
    public string? SubDistrictId { get; set; }
    public string PRActivityId { get; set; } = string.Empty;
    public string Remark { get; set; } = string.Empty;
}

public class UpdatePRActivityLocationCommandHandler : IRequestHandler<UpdatePRActivityLocationCommand, PRActivityLocationDto>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public UpdatePRActivityLocationCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<PRActivityLocationDto> Handle(UpdatePRActivityLocationCommand request, CancellationToken cancellationToken)
    {
        var entity = await _context.PRActivityLocations
            .AsTracking()
            .FirstOrDefaultAndRemoveCacheAsync(
                e => e.PRActivityId.Equals(request.PRActivityId.ToGuidOnly()),
                cancellationToken
            );

        Guard.Against.NotFound(entity!.PRActivityId, entity);

        _mapper.Map<UpdatePRActivityLocationCommand, PRActivityLocation>(request, entity);

        await _context.SaveChangesAsync(cancellationToken);

        return await _context.PRActivityLocations
            .AsNoTracking()
            .Include(e => e.SubDistrict)
            .Include(e => e.District)
            .Include(e => e.Province)
            .ProjectTo<PRActivityLocationDto>(_mapper.ConfigurationProvider)
            .FirstAsync(en => en.Id.Equals(entity.Id), cancellationToken);
    }
}
