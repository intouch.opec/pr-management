using PRManagement.Application.Common.Extensions;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.PRActivityLocations.Commands.UpdatePRActivityLocation;

public class MappingProfile
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<UpdatePRActivityLocationCommand, PRActivityLocation>()
                .ForMember(dest => dest.PRActivityId, op => op.MapFrom(src => src.PRActivityId.ToGuidOnly()));

        }
    }
}
