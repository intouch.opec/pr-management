using PRManagement.Application.Common.Extensions;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.PRActivityLocations.Commands.CreatePRActivityLocation;

public class MappingProfile
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<CreatePRActivityLocationCommand, PRActivityLocation>()
                .ForMember(dest => dest.PRActivityId, op => op.MapFrom(src => src.PRActivityId.ToGuidOnly()));

            CreateMap<PRActivityLocation, PRActivityLocationDto>()
                .ForMember(s => s.PostCode, op => op.MapFrom(o => o!.PostCode ?? string.Empty))
				.ForMember(s => s.ProvinceEN, op => op.MapFrom(o => o!.Province!.NameEN ?? string.Empty))
				.ForMember(s => s.ProvinceTH, op => op.MapFrom(o => o!.Province!.NameTH ?? string.Empty))
				.ForMember(s => s.DistrictEN, op => op.MapFrom(o => o!.District!.NameEN ?? string.Empty))
				.ForMember(s => s.DistrictTH, op => op.MapFrom(o => o!.District!.NameTH ?? string.Empty))
				.ForMember(s => s.SubDistrictEN, op => op.MapFrom(o => o!.SubDistrict!.NameEN ?? string.Empty))
				.ForMember(s => s.SubDistrictTH, op => op.MapFrom(o => o!.SubDistrict!.NameTH ?? string.Empty));
        }
    }
}
