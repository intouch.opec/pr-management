﻿using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.PRActivityLocations.Commands.CreatePRActivityLocation;

public record CreatePRActivityLocationCommand : IRequest<PRActivityLocationDto>
{
    public string Text { get; set; } = string.Empty;
    public string PostCode { get; set; } = string.Empty;
    public string UrlMap { get; set; } = string.Empty;
    public string? ProvinceId { get; set; }
    public string? DistrictId { get; set; }
    public string? SubDistrictId { get; set; }
    public string PRActivityId { get; set; } = string.Empty;
    public string Remark { get; set; } = string.Empty;
}

public class CreatePRActivityLocationCommandHandler : IRequestHandler<CreatePRActivityLocationCommand, PRActivityLocationDto>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public CreatePRActivityLocationCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<PRActivityLocationDto> Handle(CreatePRActivityLocationCommand request, CancellationToken cancellationToken)
    {
        var entity = _mapper.Map<PRActivityLocation>(request);

        await _context.PRActivityLocations.AddAsync(entity, cancellationToken);
        await _context.SaveChangesAsync(cancellationToken);
        
        return await _context.PRActivityLocations
            .AsNoTracking()
            .Include(e => e.SubDistrict)
            .Include(e => e.District)
            .Include(e => e.Province)
            .ProjectTo<PRActivityLocationDto>(_mapper.ConfigurationProvider)
            .FirstAsync(en => en.Id.Equals(entity.Id), cancellationToken);
    }
}
