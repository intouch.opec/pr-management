namespace PRManagement.Application.PRActivityLocations.Commands.CreatePRActivityLocation;

public class PRActivityLocationDto
{
    public Guid Id { get; set; }
    public string Text { get; set; } = string.Empty;
    public string PostCode { get; set; } = string.Empty;
    public string UrlMap { get; set; } = string.Empty;
    public string? ProvinceId { get; set; }
    public string? DistrictId { get; set; }
    public string? SubDistrictId { get; set; }
    public Guid PRActivityId { get; set; }
    public string Remark { get; set; } = string.Empty;
    public string SubDistrictEN { get; set; } = string.Empty;
    public string SubDistrictTH { get; set; } = string.Empty;
    public string DistrictEN { get; set; } = string.Empty;
    public string DistrictTH { get; set; } = string.Empty;
    public string? ProvinceEN { get; set; }
    public string? ProvinceTH { get; set; }
}
