﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.Common.Mappings;
using PRManagement.Application.Common.Models;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.ContactClients.Queries.GetContactWithPagination;

public record GetContactWithPaginationQuery : IRequest<PaginatedList<GetContactItem>>
{
    public Guid? ClientId { get; set; }
    public string? Email { get; set; } = string.Empty;
    public string? PhoneNumbers { get; set; } = string.Empty;
    public string? Search { get; set; } = string.Empty;
    public int? PageNumber { get; init; } = 1;
    public int? PageSize { get; init; } = 10;
    public SortType? Sort { get; set; }
    public string? SortBy { get; set; }
}

public class GetContactWithPaginationQueryHandler : IRequestHandler<GetContactWithPaginationQuery, PaginatedList<GetContactItem>>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public GetContactWithPaginationQueryHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<PaginatedList<GetContactItem>> Handle(GetContactWithPaginationQuery request, CancellationToken cancellationToken)
    {
        return await Search(request)
            .ProjectTo<GetContactItem>(_mapper.ConfigurationProvider)
            .OrderBy(request.SortBy ?? string.Empty, ascending: request.Sort)
            .PaginatedListAsync(request.PageNumber ?? 1, request.PageSize ?? 10, cancellationToken);
    }

    internal IQueryable Search(GetContactWithPaginationQuery request)
        => _context.ContactClients
            .Include(c => c.Contact)
            .ThenInclude(c => c!.Address)
            .AsNoTracking()
            .WhereEqual(e => e.ClientId, request.ClientId)
            .WhereLikeArray(e => e.Contact!.Address!.PhoneNumbers, request.PhoneNumbers)
            .WhereLikeArray(e => e.Contact!.Address!.Email, request.Email)
            .SearchOrMultiple(request.Search,
                e => e.Contact!.Address!.Contact!.FirstName,
                e => e.Contact!.Address!.Contact!.LastName,
                e => e.Contact!.Address!.Contact!.NickName,
                e => e.Contact!.Address!.Contact!.Position,
                e => e.Contact!.Address!.Contact!.LineId,
                e => e.Contact!.Address!.Media!.Name,
                e => e.Contact!.Address!.Text,
                e => e.Contact!.Address!.PhoneNumbers,
                e => e.Contact!.Address!.Email
            );
    
}
