using PRManagement.Domain.Entities;

namespace PRManagement.Application.ContactClients.Queries.GetContactWithPagination;

public class GetContactWithPaginationMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<ContactClient, GetContactItem>()
                .ForMember(e => e.FirstName, op => op.MapFrom(o => o.Contact!.FirstName ?? string.Empty))
                .ForMember(e => e.LastName, op => op.MapFrom(o => o.Contact!.LastName ?? string.Empty))
                .ForMember(e => e.NickName, op => op.MapFrom(o => o.Contact!.NickName ?? string.Empty))
                .ForMember(e => e.Email, op => op.MapFrom(o => o.Contact!.Address!.Email ?? Array.Empty<string>()))
                .ForMember(e => e.PhoneNumbers, op => op.MapFrom(o => o.Contact!.Address!.PhoneNumbers ?? Array.Empty<string>()))
                .ForMember(e => e.Position, op => op.MapFrom(o => o.Contact!.Position))
                .ForMember(e => e.LineId, op => op.MapFrom(o => o.Contact!.LineId))
                .ForMember(e => e.PercentageOfFullField, op => op.MapFrom(o => o.Contact!.PercentageOfFullField));
        }
    }
}