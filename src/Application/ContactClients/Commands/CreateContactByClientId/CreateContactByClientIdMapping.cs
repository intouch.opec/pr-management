using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.ContactClients.Commands.CreateContactByClientId;

public class CreateContactByClientIdMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<CreateContactByClientIdCommand, IEnumerable<Contact>>()
                .ConvertUsing<CreateContactByClientIdCommandToContactConverter>();

            CreateMap<CreateContactByClientIdCommand, IEnumerable<ContactClient>>()
                .ConvertUsing<CreateContactByClientIdCommandToContactClientConverter>();
        }
    }

    public class CreateContactByClientIdCommandToContactConverter : ITypeConverter<CreateContactByClientIdCommand, IEnumerable<Contact>>
    {
        private readonly IMapper _mapper;
        private readonly IApplicationDbContext _context;

        public CreateContactByClientIdCommandToContactConverter(IMapper mapper, IApplicationDbContext context)
        {
            _mapper = mapper;
            _context = context;
        }

        public IEnumerable<Contact> Convert(CreateContactByClientIdCommand source, IEnumerable<Contact> destination, ResolutionContext context)
        {
            var client = _context.Clients.FirstAsync(e => e.Id.Equals(source.ClientId.ToGuidOnly())).GetAwaiter().GetResult();
            return source.Contacts.Select((e) =>
            {
                var contact = _mapper.Map<Contact>(e);
                contact.ContactClients = new List<ContactClient>()
                {
                    new ContactClient()
                    {
                        Client = client
                    }
                };
                return contact;
            });
        }
    }

    public class CreateContactByClientIdCommandToContactClientConverter : ITypeConverter<CreateContactByClientIdCommand, IEnumerable<ContactClient>>
    {
        private readonly IMapper _mapper;

        public CreateContactByClientIdCommandToContactClientConverter(IMapper mapper)
        {
            _mapper = mapper;
        }

        public IEnumerable<ContactClient> Convert(CreateContactByClientIdCommand source, IEnumerable<ContactClient> destination, ResolutionContext context)
        {
            return source.ContactIds.Select((e) =>
            {
                return  new ContactClient()
                    {
                        ContactId = e.ToGuidOnly(),
                        ClientId = source.ClientId.ToGuidOnly(),
                    };
            });
        }
    }

}