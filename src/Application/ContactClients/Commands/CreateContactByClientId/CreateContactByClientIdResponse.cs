namespace PRManagement.Application.ContactClients.Commands.CreateContactByClientId;

public class CreateContactByClientIdResponse
{
    public Guid ClientId { get; set; }
    public IEnumerable<Guid> ContactIds { get; set; } = Enumerable.Empty<Guid>();
}
