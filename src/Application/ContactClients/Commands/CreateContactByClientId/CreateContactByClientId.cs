﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.Contacts.Commands.CreateContactByMediaSectionId;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.ContactClients.Commands.CreateContactByClientId;

public record CreateContactByClientIdCommand : IRequest<CreateContactByClientIdResponse>
{
    public string ClientId { get; set; } = string.Empty;
    public IEnumerable<ContactItem> Contacts { get; set; } = Enumerable.Empty<ContactItem>();
    public IEnumerable<string> ContactIds { get; set; } = Enumerable.Empty<string>();
}

public class CreateContactByClientIdCommandHandler : IRequestHandler<CreateContactByClientIdCommand, CreateContactByClientIdResponse>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public CreateContactByClientIdCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<CreateContactByClientIdResponse> Handle(CreateContactByClientIdCommand request, CancellationToken cancellationToken)
    {
        var contacts = _mapper.Map<IEnumerable<Contact>>(request);
        var contactClients = _mapper.Map<IEnumerable<ContactClient>>(request);
        await _context.Contacts.AddRangeAsync(contacts);
        await _context.ContactClients.AddRangeAsync(contactClients);
        await _context.SaveChangesAsync(cancellationToken);
        
        return new CreateContactByClientIdResponse
        {
            ClientId = new Guid(request.ClientId),
            ContactIds =  await _context.ContactClients
                .AsNoTracking()
                .Where(e => e.ClientId == request.ClientId.ToGuidOnly())
                .Select(e => e.Id)
                .ToListAsync(cancellationToken)
        };
    }
}
