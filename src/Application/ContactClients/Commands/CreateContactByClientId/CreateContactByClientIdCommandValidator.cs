using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Constants;
using PRManagement.Application.Contacts.Commands.CreateContactByMediaSectionId;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.ContactClients.Commands.CreateContactByClientId;

public class CreateContactByClientIdCommandValidator : AbstractValidator<CreateContactByClientIdCommand>
{
    private readonly IApplicationDbContext _context;

    public CreateContactByClientIdCommandValidator(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(e => e.ClientId)
            .StringIsGuid()
            .NotEmpty()
            .MustAsync(HasClient)
            .WithMessage(ValidatorMessages.NotFound);

        RuleForEach(e => e.ContactIds)
            .StringIsGuid()
            .DependentRules(() =>
            {
                RuleForEach(command => command.ContactIds)
                    .SetValidator((command) =>
                    {
                        var contactIds = _context.ContactClients
                            .AsNoTracking()
                            .Where(e => 
                                command.ContactIds.Select(e => e.ToGuid())
                                    .Contains(e.ClientId) &&
                                e.ClientId.Equals(command.ClientId.ToGuid()))
                            .Select(e => e.ContactId.ToString())
                            .AsEnumerable();

                        return new ContactHasClientContactValidator(contactIds);
                    });
            });

        RuleFor(e => e.ContactIds)
            .Must(BeRequiredContactIds)
            .WithMessage("The '{PropertyName}' is required.");

        RuleForEach(e => e.Contacts)

            .ChildRules(e =>
                e.RuleFor(c => c.FirstName)
                    .MustAsync(BeNameUniqueContact)
                    .WithMessage($"The {nameof(ContactItem.FirstName)} and {nameof(ContactItem.LastName)} is duplicated")
                    .NotEmpty())

            .ChildRules(e =>
                e.RuleFor(c => c.ProvinceId)
                    .MustAsync(HasProvince)
                    .WithMessage(ValidatorMessages.NotFound))

            .ChildRules(e =>
                e.RuleFor(c => c.DistrictId)
                    .MustAsync(HasDistrict)
                    .WithMessage(ValidatorMessages.NotFound))

            .ChildRules(e =>
                e.RuleFor(c => c.SubDistrictId)
                    .MustAsync(HasSubDistrict)
                    .WithMessage(ValidatorMessages.NotFound))

            .ChildRules(e =>
                e.RuleForEach(c => c.PhoneNumbers)
                    .NotEmpty())

            .ChildRules(e =>
                e.RuleFor(c => c.AddressType)
                    .MustEnum<ContactItem, AddressType>());

    }

    internal async Task<bool> BeNameUniqueContact(ContactItem source, string firstName, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(firstName) || string.IsNullOrWhiteSpace(source.LastName))
            return true;

        return !await _context.Contacts
          .WhereEqual(e => e.FirstName, firstName)
          .WhereEqual(e => e.LastName, source.LastName)
          .AnyAsync(cancellationToken);
    }

    internal async Task<bool> HasProvince(string? ProvinceId, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(ProvinceId))
            return true;

        return await _context.Provinces
            .AnyAsync(p => p.Id.Equals(ProvinceId), cancellationToken);
    }

    internal async Task<bool> HasDistrict(string? districtId, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(districtId))
            return true;

        return await _context.Districts
          .AnyAsync(p => p.Id.Equals(districtId), cancellationToken);
    }

    internal async Task<bool> HasSubDistrict(string? subDistrictId, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(subDistrictId))
            return true;

        return await _context.SubDistricts
            .AnyAsync(p => p.Id.Equals(subDistrictId), cancellationToken);
    }

    internal async Task<bool> HasClient(string? clientId, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(clientId))
            return true;

        return await _context.Clients
            .AnyAsync(p => p.Id.Equals(clientId.ToGuid()), cancellationToken);
    }

    internal bool BeRequiredContactIds(CreateContactByClientIdCommand request, IEnumerable<string> contactIds)
    {
        if (request.Contacts.Any())
            return true;
        return contactIds.Any();
    }

    internal class ContactHasClientContactValidator : AbstractValidator<string>
    {
        private readonly IEnumerable<string> _contactIds;

        public ContactHasClientContactValidator(IEnumerable<string> contactIds)
        {
            _contactIds = contactIds;

            RuleFor(pl => pl)
                .Must(HasContact)
                .WithMessage("The 'contactId' already exit.");
        }

        private bool HasContact(string contactId)
        {
            if (!Guid.TryParse(contactId, out _))
                return false;
            return !_contactIds.Contains(contactId);
        }
    }

}
