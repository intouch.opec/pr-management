﻿using PRManagement.Application.Common.Interfaces;

namespace PRManagement.Application.ContactClients.Commands.RemoveContactClient;

public record RemoveContactClientCommand(Guid Id) : IRequest<RemoveContactClientResponse>;

public class RemoveContactClientCommandHandler : IRequestHandler<RemoveContactClientCommand, RemoveContactClientResponse>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public RemoveContactClientCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<RemoveContactClientResponse> Handle(RemoveContactClientCommand request, CancellationToken cancellationToken)
    {
        var entity = await _context.ContactClients
            .AsTracking()
            .SingleOrDefaultAsync(e => e.Id == request.Id, cancellationToken);

        Guard.Against.NotFound(request.Id, entity);

        _context.ContactClients.Remove(entity);

        await _context.SaveChangesAsync(cancellationToken);

        return _mapper.Map<RemoveContactClientResponse>(entity);
    
    }
}
