using PRManagement.Domain.Entities;

namespace PRManagement.Application.ContactClients.Commands.RemoveContactClient;

public class RemoveContactClientMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<ContactClient, RemoveContactClientResponse>();
        }
    }
}
