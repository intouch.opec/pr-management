namespace PRManagement.Application.ContactClients.Commands.RemoveContactClient;

public class RemoveContactClientResponse
{
    public Guid Id { get; set; }
    public Guid ClientId { get; set; }
    public Guid ContactId { get; set; }
}