﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.Common.Models;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.Sections.Commands.UpdateSection;

public record UpdateSectionCommand : IRequest<SectionDto>
{
    public string Id { get; set; } = string.Empty;
    public string Name { get; set; } = string.Empty;
    public List<FormulaForCalculation> FormulaForCalculations { get; set; } = new();
}

public class UpdateSectionCommandHandler : IRequestHandler<UpdateSectionCommand, SectionDto>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public UpdateSectionCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<SectionDto> Handle(UpdateSectionCommand request, CancellationToken cancellationToken)
    {
        var entity = await _context.Sections
            .AsTracking()
            .Include(e => e.MediaSections!.Where(ms => ms != null))
            .ThenInclude(e => e.Platforms)
            .FirstOrDefaultAndRemoveCacheAsync(
                e => e.Id.Equals(request.Id.ToGuidOnly()),
                cancellationToken);

        Guard.Against.NotFound(request.Id.ToGuidOnly(), entity);

        _mapper.Map<UpdateSectionCommand, Section>(request, entity);

        await _context.SaveChangesAsync(cancellationToken);

        return _mapper.Map<SectionDto>(entity);
    }
}
