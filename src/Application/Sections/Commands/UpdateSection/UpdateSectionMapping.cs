using PRManagement.Application.Common.Extensions;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.Sections.Commands.UpdateSection;

public class UpdateSectionMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<UpdateSectionCommand, Section>()
                .ForMember(e => e.Id, op => op.MapFrom(o => o.Id.ToGuidOnly()))
                .ForMember(e => e.MediaSections, op => op.MapFrom<UpdateSectionCommandToMediaSectionsResolver>());

        }

        internal class UpdateSectionCommandToMediaSectionsResolver : IValueResolver<UpdateSectionCommand, Section, ICollection<MediaSection>?>
        {
            public ICollection<MediaSection>? Resolve(
                UpdateSectionCommand source,
                Section destination,
                ICollection<MediaSection>? member,
                ResolutionContext context)
            {
                if (destination.MediaSections == null || !destination.MediaSections!.Any())
                    return null;

                var mediaSections = destination.MediaSections.ToList();
                for (int i = 0; i < mediaSections.Count; i++)
                {
                    var platforms = mediaSections[i].Platforms.ToList();
                    var tierPotential = 0;
                    for (var l = 0; l < platforms.Count; l++)
                    {
                        platforms[l].TierPotential = FindTier(source.FormulaForCalculations, platforms[l].Potential);
                        if (platforms[l].TierPotential > tierPotential)
                            mediaSections[i].TierPotential = platforms[l].TierPotential;
                    }   
                }
                return mediaSections;
            }

            internal uint FindTier(List<FormulaForCalculation> records, uint numberOfFollower)
            {
                for (int i = 0; i < records.Count; i++)
                {
                    if (numberOfFollower <= records[i].MinimumFollowers)
                        return records[i].Tier;

                    if (records.Count == i + 1)
                        return records[i].Tier;

                }
                return records.LastOrDefault()!.Tier;
            }
        }
    }
}
