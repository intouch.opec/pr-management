using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Constants;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.Sections.Commands.UpdateSection;

public class UpdateSectionCommandValidator : AbstractValidator<UpdateSectionCommand>
{
    private readonly IApplicationDbContext _context;

    public UpdateSectionCommandValidator(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(v => v.Name)
            .MaximumLength(35)
            .MustAsync(BeUniqueSectionName)
            .WithMessage(ValidatorMessages.ShouldNotBeDuplicate)
            .NotEmpty();

        RuleFor(x => x.FormulaForCalculations)
            .Must(BeSortedByTierAndMinimumFollowers)
            .WithMessage("Tiers must be sorted in ascending order and MinimumFollowers in descending order.");

        RuleFor(x => x.Id)
            .StringIsGuid();

        RuleForEach(e => e.FormulaForCalculations)
            .ChildRules((formulaForCalculation) =>
              formulaForCalculation
                  .RuleFor(e => (int)e.MinimumFollowers)
                  .GreaterThan(0))
            .ChildRules((formulaForCalculation) =>
              formulaForCalculation
                  .RuleFor(e => (int)e.PriceOfPurchaseRequisition)
                  .GreaterThan(0))
            .ChildRules((formulaForCalculation) =>
              formulaForCalculation
                  .RuleFor(e => (int)e.Tier)
                  .GreaterThan(0));
    }

    internal async Task<bool> BeUniqueSectionName(UpdateSectionCommand command, string name, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(name))
            return true;

        return !await _context.Sections
          .AsNoTracking()
          .AnyAsync(p =>
                !p.Id.Equals(command.Id.ToGuidOnly()) &&
                p.Name.ToLower().Equals(name.ToLower()),
            cancellationToken);
    }

    internal bool BeSortedByTierAndMinimumFollowers(List<FormulaForCalculation> formulas)
    {
        for (int i = 1; i < formulas.Count; i++)
        {
            if (formulas[i].Tier <= formulas[i - 1].Tier)
                return false;

            if (formulas[i].MinimumFollowers >= formulas[i - 1].MinimumFollowers)
                return false;

        }
        return true;
    }
}
