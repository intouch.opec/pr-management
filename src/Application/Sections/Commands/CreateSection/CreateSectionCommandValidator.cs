using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Constants;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.Sections.Commands.CreateSection;

public class CreateSectionCommandValidator : AbstractValidator<CreateSectionCommand>
{
    private readonly IApplicationDbContext _context;

    public CreateSectionCommandValidator(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(v => v.Name)
            .MaximumLength(35)
            .MustAsync(BeUniqueSectionName)
            .WithMessage(ValidatorMessages.ShouldNotBeDuplicate)
            .NotEmpty();

        RuleFor(x => x.FormulaForCalculations)
            .Must(BeSortedByTierAndMinimumFollowers)
            .WithMessage("Tiers must be sorted in ascending order and MinimumFollowers in descending order.");

        RuleForEach(e => e.FormulaForCalculations)
            .ChildRules((formulaForCalculation) =>
                formulaForCalculation
                    .RuleFor(e => (int)e.MinimumFollowers)
                    .GreaterThan(0))
            .ChildRules((formulaForCalculation) =>
                formulaForCalculation
                    .RuleFor(e => (int)e.PriceOfPurchaseRequisition)
                    .GreaterThan(0))
            .ChildRules((formulaForCalculation) =>
                formulaForCalculation
                    .RuleFor(e => (int)e.Tier)
                    .GreaterThan(0));

    }

    internal async Task<bool> BeUniqueSectionName(string name, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(name))
            return true;

        return !await _context.Sections
          .AsNoTracking()
          .AnyAsync(p => p.Name.ToLower().Equals(name.ToLower()), cancellationToken);
    }

    internal bool BeSortedByTierAndMinimumFollowers(List<FormulaForCalculation> formulas)
    {
        for (int i = 1; i < formulas.Count; i++)
        {
            if (formulas[i].Tier <= formulas[i - 1].Tier)
                return false;

            if (formulas[i].MinimumFollowers >= formulas[i - 1].MinimumFollowers)
                return false;

        }
        return true;
    }
}
