using PRManagement.Domain.Entities;

namespace PRManagement.Application.Sections.Commands.CreateSection;

public record CreateSectionMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<CreateSectionCommand, Section>();
        }
    }
}