﻿using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.Common.Models;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.Sections.Commands.CreateSection;

public record CreateSectionCommand : IRequest<SectionDto>
{
    public string Name { get; set; } = string.Empty;
    public List<FormulaForCalculation> FormulaForCalculations { get; set; } = new();
}

public class CreateSectionCommandHandler : IRequestHandler<CreateSectionCommand, SectionDto>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public CreateSectionCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }
    public async Task<SectionDto> Handle(CreateSectionCommand request, CancellationToken cancellationToken)
    {
        var entity = _mapper.Map<Section>(request);

        await _context.Sections.AddAsync(entity);
        await _context.SaveChangesAsync(cancellationToken);

        return _mapper.Map<SectionDto>(entity);
    }
}
