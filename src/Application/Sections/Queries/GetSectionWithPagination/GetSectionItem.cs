using PRManagement.Domain.Entities;

namespace PRManagement.Application.Sections.Queries.GetSectionWithPagination;

public class GetSectionItem 
{
    public Guid Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public DateTimeOffset Created { get; set; }
    public string? CreatedBy { get; set; }
    public DateTimeOffset LastModified { get; set; }
    public string? LastModifiedBy { get; set; }
    public IEnumerable<FormulaForCalculation> FormulaForCalculations { get; set; } = Array.Empty<FormulaForCalculation>();
}
