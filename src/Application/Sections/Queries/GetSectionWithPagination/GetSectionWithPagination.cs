﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.Common.Mappings;
using PRManagement.Application.Common.Models;
using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.Sections.Queries.GetSectionWithPagination;

public record GetSectionWithPaginationQuery : IRequest<PaginatedList<GetSectionItem>>
{
    public string? Name { get; set; } = string.Empty;
   
    public int? PageNumber { get; init; } = 1;
    public int? PageSize { get; init; } = 10;
    public SortType? Sort { get; set; } = SortType.DESC;
    public string? SortBy { get; set; } = nameof(GetSectionItem.LastModified);

}

public class GetSectionWithPaginationQueryHandler : IRequestHandler<GetSectionWithPaginationQuery, PaginatedList<GetSectionItem>>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public GetSectionWithPaginationQueryHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<PaginatedList<GetSectionItem>> Handle(GetSectionWithPaginationQuery request, CancellationToken cancellationToken)
    {
         return await Search(request)
            .ProjectTo<GetSectionItem>(_mapper.ConfigurationProvider)
            .OrderBy(request.SortBy ?? nameof(GetSectionItem.Name), ascending: request.Sort ?? SortType.ASC)
            .PaginatedListAsync(request.PageNumber ?? 1, request.PageSize ?? 10, cancellationToken);
    }

    internal IQueryable<Section> Search(GetSectionWithPaginationQuery request)
    {
        return _context.Sections
            .AsNoTracking()
            .WhereLike(e => e.Name.ToLower(), request.Name?.ToLower());

    }
}
