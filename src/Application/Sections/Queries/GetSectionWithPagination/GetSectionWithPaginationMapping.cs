using PRManagement.Domain.Entities;

namespace PRManagement.Application.Sections.Queries.GetSectionWithPagination;

public class GetSectionWithPaginationMapping
{
	public sealed class Mapping : Profile
	{
		public Mapping()
		{
            CreateMap<Section, GetSectionItem>();
        }
    }
}
