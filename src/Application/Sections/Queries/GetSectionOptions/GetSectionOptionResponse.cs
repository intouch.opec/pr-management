using PRManagement.Application.Common.Models;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.Sections.Queries.GetSectionOptions;
public class GetSectionOptionsResponse : OptionItemBase
{
    public IEnumerable<FormulaForCalculation> FormulaForCalculations { get; set; } = Array.Empty<FormulaForCalculation>();
}
