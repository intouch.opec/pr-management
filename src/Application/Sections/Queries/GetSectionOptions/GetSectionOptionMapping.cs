
using PRManagement.Domain.Entities;

namespace PRManagement.Application.Sections.Queries.GetSectionOptions;
public class GetSectionOptionsMapping
{
	public sealed class Mapping : Profile
	{
		public Mapping()
		{
			CreateMap<Section, GetSectionOptionsResponse>()
				.ForMember(e => e.Id, op => op.MapFrom(o => o.Id.ToString() ?? string.Empty));
		}
	}
}