using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.Sections.Queries.GetSectionOptions;

public record GetSectionOptionsQuery : IRequest<IEnumerable<GetSectionOptionsResponse>>
{
    public string? Name { get; set; } = string.Empty;
    public string? MediaId { get; set; } = string.Empty;
    public SortType? Sort { get; set; } = SortType.DESC;
    public string? SortBy { get; set; } = nameof(GetSectionOptionsResponse.Name);
}

public class GetSectionOptionsQueryHandler : IRequestHandler<GetSectionOptionsQuery, IEnumerable<GetSectionOptionsResponse>>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public GetSectionOptionsQueryHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<IEnumerable<GetSectionOptionsResponse>> Handle(GetSectionOptionsQuery request, CancellationToken cancellationToken)
      => await Search(request)
        .OrderBy(request.SortBy ?? nameof(GetSectionOptionsResponse.Name), ascending: request.Sort ?? SortType.ASC)
        .ProjectTo<GetSectionOptionsResponse>(_mapper.ConfigurationProvider)
        .ToListAsync(cancellationToken);

    internal IQueryable<Section> Search(GetSectionOptionsQuery request)
    {
        var sections = _context.Sections.AsQueryable();

        if (!string.IsNullOrWhiteSpace(request.MediaId))
        {
            sections = sections
              .Include(e => e.MediaSections!.Where(ms => ms != null))
              .Where(e => e.MediaSections!
                    .Any(ms => ms.MediaId == request.MediaId.ToGuid()));
        }
        return sections.AsNoTracking()
            .WhereLike(e => e.Name.ToLower(), request.Name);
    }
}
