﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.PRActivityBriefings.Commands.CreatePRActivityBriefing;

namespace PRManagement.Application.PRActivityBriefings.Queries.GetPRActivityBriefing;

public record GetPRActivityBriefingQuery (string PRActivityId) : IRequest<PRActivityBriefingDto>;

public class GetPRActivityBriefingQueryHandler : IRequestHandler<GetPRActivityBriefingQuery, PRActivityBriefingDto>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public GetPRActivityBriefingQueryHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<PRActivityBriefingDto> Handle(GetPRActivityBriefingQuery request, CancellationToken cancellationToken)
    {
        var entity = await _context.PRActivityBriefings
           .ProjectTo<PRActivityBriefingDto>(_mapper.ConfigurationProvider)
           .FirstOrDefaultAsync(
               e => e.PRActivityId.Equals(request.PRActivityId.ToGuidOnly()),
               cancellationToken);

        Guard.Against.NotFound(request.PRActivityId, entity);

        return entity;
    }
}
