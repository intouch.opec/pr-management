using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Constants;

namespace PRManagement.Application.PRActivityBriefings.Commands.CreatePRActivityBriefing;

public class CreatePRActivityBriefingCommandValidator : AbstractValidator<CreatePRActivityBriefingCommand>
{
    private readonly IApplicationDbContext _context;

    public CreatePRActivityBriefingCommandValidator(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(e => e.PRActivityId)
            .MustAsync(HasPRActivity)
            .WithMessage(ValidatorMessages.NotFound)
            .NotEmpty();

        RuleFor(e => e.Content)
            .NotEmpty();
    }

    internal async Task<bool> HasPRActivity(string id, CancellationToken cancellationToken)
    {
        if (id.IsValidGuid())
            return true;

        return await _context.PRActivities
            .AsNoTracking()
            .AnyCacheAsync(
                src => src.Id.Equals(id.ToGuid()),
                cancellationToken);
    }
}
