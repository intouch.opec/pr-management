using PRManagement.Application.Common.Extensions;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.PRActivityBriefings.Commands.CreatePRActivityBriefing;

public class MappingProfile
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<CreatePRActivityBriefingCommand, PRActivityBriefing>()
                .ForMember(dest => dest.PRActivityId, op => op.MapFrom(src => src.PRActivityId.ToGuidOnly()));

            CreateMap<PRActivityBriefing, PRActivityBriefingDto>();
        }
    }
}
