namespace PRManagement.Application.PRActivityBriefings.Commands.CreatePRActivityBriefing;

public class PRActivityBriefingDto
{
    public Guid Id { get; set; }
    public string Content { get; set; } = string.Empty;
    public Guid PRActivityId { get; set; }
}
