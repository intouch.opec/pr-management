﻿using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.PRActivityBriefings.Commands.CreatePRActivityBriefing;

public record CreatePRActivityBriefingCommand : IRequest<PRActivityBriefingDto>
{
    public string Content { get; set; } = string.Empty;
    public string PRActivityId { get; set; } = string.Empty;
}

public class CreatePRActivityBriefingCommandHandler : IRequestHandler<CreatePRActivityBriefingCommand, PRActivityBriefingDto>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public CreatePRActivityBriefingCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<PRActivityBriefingDto> Handle(CreatePRActivityBriefingCommand request, CancellationToken cancellationToken)
    {
        var entity = _mapper.Map<PRActivityBriefing>(request);

        await _context.PRActivityBriefings.AddAsync(entity, cancellationToken);
        await _context.SaveChangesAsync(cancellationToken);

        return _mapper.Map<PRActivityBriefingDto>(entity);
    }
}
