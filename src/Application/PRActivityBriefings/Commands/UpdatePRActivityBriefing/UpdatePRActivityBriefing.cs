﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.PRActivityBriefings.Commands.CreatePRActivityBriefing;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.PRActivityBriefings.Commands.UpdatePRActivityBriefing;

public record UpdatePRActivityBriefingCommand : IRequest<PRActivityBriefingDto>
{
    public string Content { get; set; } = string.Empty;
    public string PRActivityId { get; set; } = string.Empty;
}

public class UpdatePRActivityBriefingCommandHandler : IRequestHandler<UpdatePRActivityBriefingCommand, PRActivityBriefingDto>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public UpdatePRActivityBriefingCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<PRActivityBriefingDto> Handle(UpdatePRActivityBriefingCommand request, CancellationToken cancellationToken)
    {
        var entity = await _context.PRActivityBriefings
            .AsTracking()
            .FirstOrDefaultAndRemoveCacheAsync(
                e => e.PRActivityId.Equals(request.PRActivityId.ToGuidOnly()),
                cancellationToken);

        Guard.Against.NotFound(request.PRActivityId, entity);

        _mapper.Map<UpdatePRActivityBriefingCommand, PRActivityBriefing>(request, entity);

        await _context.SaveChangesAsync(cancellationToken);

        return _mapper.Map<PRActivityBriefingDto>(entity);
    }
}
