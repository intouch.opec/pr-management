using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Constants;

namespace PRManagement.Application.PRActivityBriefings.Commands.UpdatePRActivityBriefing;

public class UpdatePRActivityBriefingCommandValidator : AbstractValidator<UpdatePRActivityBriefingCommand>
{
    private readonly IApplicationDbContext _context;

    public UpdatePRActivityBriefingCommandValidator(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(e => e.PRActivityId)
           .MustAsync(HasPRActivity)
           .WithMessage(ValidatorMessages.NotFound)
           .NotEmpty();

        RuleFor(e => e.Content)
            .NotEmpty();
    }

    internal async Task<bool> HasPRActivity(string prActivityId, CancellationToken cancellationToken)
      => !prActivityId.IsValidGuid() || await _context.PRActivities
          .AnyAsync(p => p.Id.Equals(new Guid(prActivityId)), cancellationToken);

}
