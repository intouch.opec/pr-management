using PRManagement.Application.Common.Extensions;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.PRActivityBriefings.Commands.UpdatePRActivityBriefing;

public class MappingProfile
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<UpdatePRActivityBriefingCommand, PRActivityBriefing>()
                .ForMember(dest => dest.PRActivityId, op => op.MapFrom(src => src.PRActivityId.ToGuidOnly()));

        }
    }
}
