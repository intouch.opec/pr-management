using PRManagement.Application.Common.Models;

namespace PRManagement.Application.Provinces.Queries.GetProvinceOptions;
public class GetProvinceOptionsResponse : OptionItemBase
{
    public string NameTH { get; set; } = string.Empty;
    public string NameEN { get; set; } = string.Empty;
}
