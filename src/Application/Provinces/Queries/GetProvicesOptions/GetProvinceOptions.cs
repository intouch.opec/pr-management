using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.Provinces.Queries.GetProvinceOptions;

public record GetProvinceOptionsQuery : IRequest<IEnumerable<GetProvinceOptionsResponse>>
{
    public string? Name { get; set; } = string.Empty;
}

public class GetProvinceOptionsQueryHandler : IRequestHandler<GetProvinceOptionsQuery, IEnumerable<GetProvinceOptionsResponse>>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public GetProvinceOptionsQueryHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<IEnumerable<GetProvinceOptionsResponse>> Handle(GetProvinceOptionsQuery request, CancellationToken cancellationToken)
      => await Search(request)
        .OrderBy(nameof(GetProvinceOptionsResponse.Name), ascending: SortType.ASC)
        .ProjectToListCacheAsync<Province, GetProvinceOptionsResponse>(_mapper.ConfigurationProvider, cancellationToken);

    internal IQueryable<Province> Search(GetProvinceOptionsQuery request)
    {
        return _context.Provinces
            .AsNoTracking()
            .WhereLike(e => e.NameTH, request.Name);
    }
}
