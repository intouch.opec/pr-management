using PRManagement.Domain.Entities;

namespace PRManagement.Application.Provinces.Queries.GetProvinceOptions;

public class GetProvinceOptionsMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<Province, GetProvinceOptionsResponse>()
              .ForMember(e => e.Name, op => op.MapFrom(o => o.NameTH ?? string.Empty));

        }
    }
}
