﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.WorkProgressList.Commands.UpdateWorkProgress;

public record UpdateWorkProgressCommand : IRequest<WorkProgressDto>
{
    public string Id { get; set; } = string.Empty;
    public string WorkTask { get; set; } = string.Empty;
    public TaskWorkerStatus TaskWorkerStatus { get; set; }
}

public class UpdateWorkProgressCommandHandler : IRequestHandler<UpdateWorkProgressCommand, WorkProgressDto>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public UpdateWorkProgressCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<WorkProgressDto> Handle(UpdateWorkProgressCommand request, CancellationToken cancellationToken)
    {
        var entity = await _context.WorkProgress
            .AsTracking()
            .FirstOrDefaultAsync(
                e => e.PRActivityId.Equals(request.Id.ToGuid()),
                cancellationToken);

        Guard.Against.NotFound(request.Id, entity);

        _mapper.Map<UpdateWorkProgressCommand, WorkProgress>(request, entity);

        await _context.SaveChangesAsync(cancellationToken);

        return _mapper.Map<WorkProgressDto>(entity);
    }
}
