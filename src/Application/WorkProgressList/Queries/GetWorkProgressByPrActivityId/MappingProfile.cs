using PRManagement.Application.WorkProgressList.Commands.UpdateWorkProgress;
using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.WorkProgressList.Queries.GetWorkProgressByPrActivityId;

public class MappingProfile
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<WorkProgress, WorkProgressDto>();

            CreateMap<IEnumerable<WorkProgressDto>, GetWorkProgressByPrActivityIdResponse>()
                .ForMember(dest => dest.WorkProgress, op => op.MapFrom(src => src))
                .ForMember(dest => dest.SummaryWorkProgress, op => op.MapFrom(src => new SummaryWorkProgress()
                {
                    WorkProgressCount = src.Count(),
                    WorkProgressCompleteCount = src.Count(e => e.TaskWorkerStatus.Equals(TaskWorkerStatus.DONE)),
                    WorkProgressPercent = (src.Count(e => e.TaskWorkerStatus.Equals(TaskWorkerStatus.DONE)) * 100) / (src.Count() > 0 ? src.Count() : 1)
                }));
        }
    }
}
