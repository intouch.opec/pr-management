using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Constants;

namespace PRManagement.Application.WorkProgressList.Queries.GetWorkProgressByPrActivityId;

public class GetWorkProgressByPrActivityIdQueryValidator : AbstractValidator<GetWorkProgressByPrActivityIdQuery>
{
    private readonly IApplicationDbContext _context;

    public GetWorkProgressByPrActivityIdQueryValidator(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(e => e.PRActivityId)
            .MustAsync(HasPRActivity)
            .WithMessage(ValidatorMessages.NotFound)
            .NotEmpty();
    }

    internal async Task<bool> HasPRActivity(string id, CancellationToken cancellationToken)
    {
        if (id.IsValidGuid())
            return true;

        return await _context.PRActivities
            .AsNoTracking()
            .AnyAsync(
                src => src.Id.Equals(id.ToGuid()),
                cancellationToken);
    }
}