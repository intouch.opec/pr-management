using PRManagement.Domain.Enums;

namespace PRManagement.Application.WorkProgressList.Commands.UpdateWorkProgress;

public class WorkProgressDto
{
    public Guid Id { get; set; }
    public WorkTask WorkTask { get; set; }
    public TaskWorkerStatus TaskWorkerStatus { get; set; }
}
