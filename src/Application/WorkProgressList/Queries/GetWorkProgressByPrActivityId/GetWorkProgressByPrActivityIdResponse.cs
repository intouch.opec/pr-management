using PRManagement.Application.WorkProgressList.Commands.UpdateWorkProgress;

namespace PRManagement.Application.WorkProgressList.Queries.GetWorkProgressByPrActivityId;

public class GetWorkProgressByPrActivityIdResponse
{
    public IEnumerable<WorkProgressDto> WorkProgress { get; set; } = Enumerable.Empty<WorkProgressDto>();
    public SummaryWorkProgress SummaryWorkProgress { get; set; } = new SummaryWorkProgress();
}

public class SummaryWorkProgress
{
    public int WorkProgressCount { get; set; }
    public int WorkProgressCompleteCount { get; set; }
    public int WorkProgressPercent { get; set; }
}
