﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.WorkProgressList.Commands.UpdateWorkProgress;

namespace PRManagement.Application.WorkProgressList.Queries.GetWorkProgressByPrActivityId;

public record GetWorkProgressByPrActivityIdQuery(string PRActivityId) : IRequest<GetWorkProgressByPrActivityIdResponse>;

public class GetWorkProgressByPrActivityIdQueryHandler : IRequestHandler<GetWorkProgressByPrActivityIdQuery, GetWorkProgressByPrActivityIdResponse>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public GetWorkProgressByPrActivityIdQueryHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<GetWorkProgressByPrActivityIdResponse> Handle(GetWorkProgressByPrActivityIdQuery request, CancellationToken cancellationToken)
    {
        var workProgress = await _context.WorkProgress
            .AsNoTracking()
            .Where(e => e.PRActivityId.Equals(request.PRActivityId.ToGuid()))
            .ProjectTo<WorkProgressDto>(_mapper.ConfigurationProvider)
            .ToListAsync(cancellationToken);

        Guard.Against.NullOrEmpty(workProgress);

        return _mapper.Map<GetWorkProgressByPrActivityIdResponse>(workProgress);
    }
}
