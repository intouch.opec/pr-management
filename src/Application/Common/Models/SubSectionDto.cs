using PRManagement.Domain.Entities;

namespace PRManagement.Application.Common.Models;

public class SubSectionDto
{
    public Guid Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public Guid SectionId { get; set; }
    public DateTimeOffset Created { get; set; }
    public string? CreatedBy { get; set; }
    public DateTimeOffset LastModified { get; set; }
    public string? LastModifiedBy { get; set; }
    public bool IsDeleted { get; set; } = false;
    public DateTimeOffset? Deleted { get; set; }
}
