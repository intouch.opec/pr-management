using PRManagement.Domain.Entities;

public class FileResponse
{
    public byte[] File { get; set; } = Array.Empty<byte>();
    public string FileName { get; set; } = string.Empty;
    public string ContentType { get; set; } = string.Empty;
}
