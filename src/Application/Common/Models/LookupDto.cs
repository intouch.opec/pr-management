﻿using PRManagement.Domain.Entities;

namespace PRManagement.Application.Common.Models;
public class LookupDto
{
    public string Id { get; init; } = string.Empty;
    public string? Title { get; init; }
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<Section, LookupDto>()
                .ForMember(o => o.Id, op => op.MapFrom(s => s.Id.ToString()))
                .ForMember(o => o.Title, op => op.MapFrom(s => s.Name));

            CreateMap<Platform, LookupDto>()
                .ForMember(o => o.Id, op => op.MapFrom(s => s.Id.ToString()))
                .ForMember(o => o.Title, op => op.MapFrom(s => s.Name));

            CreateMap<Section, SectionDto>();
            CreateMap<SubSection, SubSectionDto>();
        }
    }
}
