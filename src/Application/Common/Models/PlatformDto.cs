namespace PRManagement.Application.Common.Models;

public class PlatformDto
{
    public string Name { get; set; } = string.Empty;
    public string Link { get; set; } = string.Empty;
    public decimal PRValue { get; set; }
    public decimal ADValue { get; set; }
    public uint Potential { get; set; }
    public uint TierOder { get; set; }
    public uint TierPotential { get; set; }
    public string PlatformType { get; set; } = string.Empty;
}
