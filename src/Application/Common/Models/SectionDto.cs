using PRManagement.Domain.Entities;

namespace PRManagement.Application.Common.Models;

public class SectionDto
{
    public Guid Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public IEnumerable<FormulaForCalculation> FormulaForCalculations { get; set; } = Array.Empty<FormulaForCalculation>();
    public DateTimeOffset Created { get; set; }
    public string? CreatedBy { get; set; }
    public DateTimeOffset LastModified { get; set; }
    public string? LastModifiedBy { get; set; }
    public bool IsDeleted { get; set; } = false;
    public DateTimeOffset? Deleted { get; set; }
}
