namespace PRManagement.Application.Common.Models;

public class OptionItemBase
{
    public string Id { get; set; } = string.Empty;
    public string Name { get; set; } = string.Empty;
}
