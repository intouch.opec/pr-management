
using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;

namespace PRManagement.Application.Common.Mappings;

public class UserIdValueResolver<TSource, TDestination> : IValueResolver<TSource, TDestination, Guid>
{
    private readonly IUser _user;

    public UserIdValueResolver(IUser user)
    {
        _user = user;
    }

    public Guid Resolve(TSource source, TDestination destination, Guid member, ResolutionContext context)
        => _user.Id!.ToGuidOnly();

}
