using PRManagement.Application.Medias.Commands.CreateMedia;
using PRManagement.Application.Medias.Commands.UpdateMedia;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.Common.BusinessLogics;

public class CalculatePercentageOfFullFieldExtensions
{

    public static double CalculateCreateMediaCommand(CreateMediaCommand media)
    {
        return ((media.Email.Any() ? 1 : 0) +
            (media.PhoneNumbers.Any() ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(media.Name) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(media.Address) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(media.PostCode) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(media.ProvinceId) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(media.DistrictId) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(media.SubDistrictId) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(media.SectionId) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(media.SubSectionId) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(media.TypeOfMedia) ? 1 : 0) +
            (media.Platforms!.Any(e => e.Potential != 0) ? 1 : 0))
            / 12.0 * 100;
    }

    public static double CalculateUpdateMediaCommand(UpdateMediaCommand updateMedia, Media media)
    {
        return ((updateMedia.Email.Any() ? 1 : 0) +
            (updateMedia.PhoneNumbers.Any() ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(updateMedia.Name) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(updateMedia.Address) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(updateMedia.PostCode) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(updateMedia.ProvinceId) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(updateMedia.DistrictId) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(updateMedia.SubDistrictId) ? 1 : 0) +
            (media.MediaSections.Any() ? 1 : 0) +
            (media.MediaSections.Select(ms => ms.Platforms).SelectMany(e => e)
                .Any(e => e.Potential != 0) ? 1 : 0))
            / 10.0 * 100;
    }

    public static double CalculateMedia(Media media)
    {
        return ((media.Address?.Email.Any() == true ? 1 : 0) +
            (media.Address?.PhoneNumbers.Any() == true ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(media.Name) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(media.Address?.Text) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(media.Address?.PostCode) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(media.Address?.ProvinceId) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(media.Address?.DistrictId) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(media.Address?.SubDistrictId) ? 1 : 0) +
            (media.MediaSections.Any() ? 1 : 0) +
            (media.MediaSections.Select(ms => ms.Platforms).SelectMany(e => e)
                .Any(e => e.Potential != 0) ? 1 : 0))
            / 10.0 * 100;
    }
}
