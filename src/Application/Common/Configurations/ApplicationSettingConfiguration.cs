using Microsoft.Extensions.Configuration;

namespace PRManagement.Application.Common.Configurations;

public class ApplicationSettingConfiguration
{
    public string DirectoryPath { get; set; } = string.Empty;
    public int ValidateOptionCacheSeconds { get; set; } = 1000000;
    public int EntityCacheSeconds { get; set; }
}

internal static class ConfigurationHelperHelpers
{
    public static IConfiguration Config { get; set; } = null!;
}

public static class ConfigurationHelper
{
    public static void Initialize(IConfiguration Configuration)
    {
        ConfigurationHelperHelpers.Config = Configuration;
    }

    public static ApplicationSettingConfiguration ApplicationSettingConfiguration => new()
    {
        DirectoryPath = ConfigurationHelperHelpers.Config![$"{nameof(ApplicationSettingConfiguration)}:DirectoryPath"]!,
        ValidateOptionCacheSeconds = int.Parse(ConfigurationHelperHelpers.Config[$"{nameof(ApplicationSettingConfiguration)}:ValidateOptionCacheSeconds"]!),
        EntityCacheSeconds = int.Parse(ConfigurationHelperHelpers.Config[$"{nameof(ApplicationSettingConfiguration)}:EntityCacheSeconds"]!)
    };

}