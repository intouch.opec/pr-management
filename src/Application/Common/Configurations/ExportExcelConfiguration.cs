namespace PRManagement.Application.Common.Configurations;

public class ExportExcelConfiguration
{
    public string BookmarkTemplate { get; set; } = string.Empty;
    public string HeaderText { get; set; } = string.Empty;
}
