using PRManagement.Domain.Enums;
using System.Linq.Expressions;
using System.Reflection;

namespace PRManagement.Application.Common.Extensions;
public static class QueryableWhereExtensions
{
    internal static (PropertyInfo?, ParameterExpression?) GetPropertyByKey<TEntity>(string propertyName)
    {
        var property = typeof(TEntity).GetProperty(propertyName,
            BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
        var parameter = Expression.Parameter(typeof(TEntity), "entity");
        return (property, parameter);
    }

    public static IQueryable<TEntity> WhereLike<TEntity>(this IQueryable<TEntity> query,
        Expression<Func<TEntity, string>> propertySelector,
        string? value)
    {
        if (string.IsNullOrEmpty(value))
            return query;
        var parameter = propertySelector.Parameters.Single();
        var containsMethod = typeof(string).GetMethod("Contains", new[] { typeof(string) });
        var containsExpression = Expression.Call(propertySelector.Body, containsMethod!, Expression.Constant(value.ToLower()));
        var lambdaExpression = Expression.Lambda<Func<TEntity, bool>>(containsExpression, parameter);
        return query.Where(lambdaExpression);
    }

    public static IQueryable<TEntity> WhereArrayJoin<TEntity, TEntity2>(
      this IQueryable<TEntity> query,
      Expression<Func<TEntity, ICollection<TEntity2>>> propertySelector,
      Expression<Func<TEntity2, string>> propertySelector2,
      string? value)
    {
        if (string.IsNullOrEmpty(value))
            return query;

        var parameter = propertySelector.Parameters.Single();
        var containsMethod = typeof(string).GetMethod("Contains", new[] { typeof(string) });

        var body = Expression.Property(parameter, (propertySelector.Body as MemberExpression)!.Member.Name);
        var joinedArray = Expression.NewArrayInit(typeof(string), Expression.Call(body, "Select", null, propertySelector2));

        var containsExpression = Expression.Call(joinedArray, "Any", null, Expression.Lambda<Func<string, bool>>(
            Expression.Call(Expression.Constant(value), containsMethod!), propertySelector2.Parameters.Single()));

        var lambdaExpression = Expression.Lambda<Func<TEntity, bool>>(containsExpression, parameter);

        return query.Where(lambdaExpression);
    }

    public static IQueryable<TEntity> WhereEqual<TEntity, TValue>(this IQueryable<TEntity> query,
       Expression<Func<TEntity, TValue?>> propertySelector,
       TValue? value)
    {
        if (value is null)
            return query;

        if (typeof(TValue) == typeof(string) && string.IsNullOrEmpty(value!.ToString()))
            return query;

        var parameter = propertySelector.Parameters.Single();
        var equalsExpression = Expression.Equal(propertySelector.Body, Expression.Constant(value, typeof(TValue)));
        var lambdaExpression = Expression.Lambda<Func<TEntity, bool>>(equalsExpression, parameter);
        return query.Where(lambdaExpression);
    }

    public static IQueryable<TEntity> OrderBy<TEntity>(
        this IQueryable<TEntity> query,
        string propertyName,
        SortType? ascending)
    {
        if (string.IsNullOrEmpty(propertyName))
            return query;

        var (property, parameter) = GetPropertyByKey<TEntity>(propertyName);

        if (parameter is null || property is null)
            return query;

        var propertyAccess = Expression.MakeMemberAccess(parameter, (MemberInfo)property);
        var orderByExpression = Expression.Lambda(propertyAccess, parameter);
        var methodName = ascending?.Equals(SortType.ASC) ?? true ? "OrderBy" : "OrderByDescending";
        var orderByCallExpression = Expression.Call(
            typeof(Queryable),
            methodName,
            new Type[] { typeof(TEntity), property.PropertyType },
            query.Expression,
            Expression.Quote(orderByExpression));

        return query.Provider.CreateQuery<TEntity>(orderByCallExpression);
    }

    public static IQueryable<TEntity> WhereInDateRange<TEntity>(
        this IQueryable<TEntity> query,
        Expression<Func<TEntity, DateTimeOffset>> propertySelector,
        DateTimeOffset? startThen,
        DateTimeOffset? endDate)
    {
        var parameter = propertySelector.Parameters.Single();

        if (startThen is not null)
        {
            var greaterThenExpression = Expression.GreaterThan(propertySelector.Body, Expression.Constant(startThen));
            var greaterThanLambdaExpression = Expression.Lambda<Func<TEntity, bool>>(greaterThenExpression, parameter);
            query = query.Where(greaterThanLambdaExpression);
        }

        if (endDate is not null)
        {
            var lessThenExpression = Expression.LessThanOrEqual(propertySelector.Body, Expression.Constant(endDate));
            var lessThenLambdaExpression = Expression.Lambda<Func<TEntity, bool>>(lessThenExpression, parameter);
            query = query.Where(lessThenLambdaExpression);
        }

        return query;
    }

    public static IQueryable<TEntity> WhereRange<TEntity, TType>(this IQueryable<TEntity> query,
        Expression<Func<TEntity, TType>> propertySelector,
        TType? start,
        TType? end
    )
    {
        var parameter = propertySelector.Parameters.Single();

        if (start is not null)
        {
            var greaterThenExpression = Expression.GreaterThan(propertySelector.Body, Expression.Constant(start));
            var greaterThanLambdaExpression = Expression.Lambda<Func<TEntity, bool>>(greaterThenExpression, parameter);
            query = query.Where(greaterThanLambdaExpression);
        }

        if (end is not null)
        {
            var lessThenExpression = Expression.LessThanOrEqual(propertySelector.Body, Expression.Constant(end));
            var lessThenLambdaExpression = Expression.Lambda<Func<TEntity, bool>>(lessThenExpression, parameter);
            query = query.Where(lessThenLambdaExpression);
        }

        return query;
    }

    public static IQueryable<TEntity> WhereRange<TEntity, TType>(this IQueryable<TEntity> query,
        Expression<Func<TEntity, TType>> propertySelector,
        TType? start,
        TType? end
    ) where TType : struct
    {
        var parameter = propertySelector.Parameters.Single();

        if (start.HasValue)
        {
            // Ensure compatibility between nullable and non-nullable types
            var startConstant = Expression.Convert(Expression.Constant(start.Value), propertySelector.Body.Type);
            var greaterThanExpression = Expression.GreaterThanOrEqual(propertySelector.Body, startConstant);
            var greaterThanLambdaExpression = Expression.Lambda<Func<TEntity, bool>>(greaterThanExpression, parameter);
            query = query.Where(greaterThanLambdaExpression);
        }

        if (end.HasValue)
        {
            // Ensure compatibility between nullable and non-nullable types
            var endConstant = Expression.Convert(Expression.Constant(end.Value), propertySelector.Body.Type);
            var lessThanExpression = Expression.LessThanOrEqual(propertySelector.Body, endConstant);
            var lessThanLambdaExpression = Expression.Lambda<Func<TEntity, bool>>(lessThanExpression, parameter);
            query = query.Where(lessThanLambdaExpression);
        }

        return query;
    }


    public static IEnumerable<TEntity> WhereRange<TEntity, TType>(this IEnumerable<TEntity> query,
      Expression<Func<TEntity, TType>> propertySelector,
      TType? start,
      TType? end
    )
    {
        var parameter = propertySelector.Parameters.Single();

        if (start is not null)
        {
            var greaterThanExpression = Expression.GreaterThanOrEqual(propertySelector.Body, Expression.Constant(start));
            var greaterThanLambdaExpression = Expression.Lambda<Func<TEntity, bool>>(greaterThanExpression, parameter);
            query = query.Where(greaterThanLambdaExpression.Compile());
        }

        if (end is not null)
        {
            var lessThanOrEqualExpression = Expression.LessThanOrEqual(propertySelector.Body, Expression.Constant(end));
            var lessThanOrEqualLambdaExpression = Expression.Lambda<Func<TEntity, bool>>(lessThanOrEqualExpression, parameter);
            query = query.Where(lessThanOrEqualLambdaExpression.Compile());
        }

        return query;
    }

    public static IQueryable<TEntity> WhereRange<TEntity, TEntity2, TType>(
      this IQueryable<TEntity> query,
      Expression<Func<TEntity, ICollection<TEntity2>>> propertySelector,
      Expression<Func<TEntity2, TType>> propertySelector2,
      TType? start,
      TType? end
    )
    {
        var parameter = propertySelector.Parameters.Single();

        if (start is not null)
        {
            var comparisonExpression =
                Expression.GreaterThanOrEqual(propertySelector2.Body, Expression.Constant(start));
            var comparisonLambdaExpression = Expression.Lambda<Func<TEntity2, bool>>(comparisonExpression, propertySelector2.Parameters);
            var whereClause = Expression.Lambda<Func<TEntity, bool>>(
                Expression.Call(
                    typeof(Enumerable),
                    "Any",
                    new[] { typeof(TEntity2) },
                    propertySelector.Body,
                    comparisonLambdaExpression
                ),
                parameter
            );
            query = query.Where(whereClause);
        }

        if (end is not null)
        {
            var comparisonExpression =
                Expression.LessThanOrEqual(propertySelector2.Body, Expression.Constant(end));

            var comparisonLambdaExpression = Expression.Lambda<Func<TEntity2, bool>>(comparisonExpression, propertySelector2.Parameters);
            var whereClause = Expression.Lambda<Func<TEntity, bool>>(
                Expression.Call(
                    typeof(Enumerable),
                    "Any",
                    new[] { typeof(TEntity2) },
                    propertySelector.Body,
                    comparisonLambdaExpression
                ),
                parameter
            );
            query = query.Where(whereClause);
        }

        return query;
    }

    public static IQueryable<TEntity> WhereLikeArray<TEntity>(this IQueryable<TEntity> query,
      Expression<Func<TEntity, string[]>> propertySelector,
      string? value)
    {
        if (string.IsNullOrEmpty(value))
            return query;
        var parameter = propertySelector.Parameters.Single();
        var containsMethod = typeof(string).GetMethod("Contains", new[] { typeof(string) });
        var containsExpression = Expression.Call(propertySelector.Body, containsMethod!, Expression.Constant(value.ToLower()));
        var lambdaExpression = Expression.Lambda<Func<TEntity, bool>>(containsExpression, parameter);
        return query.Where(lambdaExpression);
    }

    public static IQueryable<TEntity> WhereEqualArray<TEntity, TEntity2>(
        this IQueryable<TEntity> query,
        Expression<Func<TEntity, ICollection<TEntity2>>> propertySelector,
        Expression<Func<TEntity2, Guid>> propertySelector2,
        Guid? value)
    {
        if (!value.HasValue)
            return query;

        var parameter = propertySelector.Parameters.Single();
        var collectionSelector = propertySelector.Body;

        var elementParameter = Expression.Parameter(typeof(TEntity2), "e");
        var elementProperty = Expression.Invoke(propertySelector2, elementParameter);
        var valueExpression = Expression.Constant(value.Value);

        var equalsMethod = typeof(Guid).GetMethod("Equals", new[] { typeof(Guid) });
        var equalsExpression = Expression.Call(elementProperty, equalsMethod!, valueExpression);

        var anyMethod = typeof(Enumerable).GetMethods()
            .Single(m => m.Name == "Any" && m.GetParameters().Length == 2)
            .MakeGenericMethod(typeof(TEntity2));

        var anyLambda = Expression.Lambda<Func<TEntity2, bool>>(equalsExpression, elementParameter);
        var anyCall = Expression.Call(anyMethod, collectionSelector, anyLambda);

        var lambda = Expression.Lambda<Func<TEntity, bool>>(anyCall, parameter);
        return query.Where(lambda);
    }

    public static IQueryable<TEntity> WhereEqualArray<TEntity, TEntity2, TValue>(
     this IQueryable<TEntity> query,
     Expression<Func<TEntity, ICollection<TEntity2>>> propertySelector,
     Expression<Func<TEntity2, TValue>> propertySelector2,
     TValue? value)
     where TValue : struct
    {
        if (!value.HasValue)
            return query;

        var parameter = propertySelector.Parameters.Single();
        var collectionSelector = propertySelector.Body;

        var elementParameter = Expression.Parameter(typeof(TEntity2), "e");
        var elementProperty = Expression.Invoke(propertySelector2, elementParameter);
        var valueExpression = Expression.Constant(value.Value);

        var equalsExpression = Expression.Equal(elementProperty, valueExpression);

        var anyMethod = typeof(Enumerable).GetMethods()
            .Single(m => m.Name == "Any" && m.GetParameters().Length == 2)
            .MakeGenericMethod(typeof(TEntity2));

        var anyLambda = Expression.Lambda<Func<TEntity2, bool>>(equalsExpression, elementParameter);
        var anyCall = Expression.Call(anyMethod, collectionSelector, anyLambda);

        var lambda = Expression.Lambda<Func<TEntity, bool>>(anyCall, parameter);
        return query.Where(lambda);
    }

    public static bool IsEnumType<T>()
    {
        return typeof(T).IsEnum;
    }

    internal static MemberInfo? GetMemberInfo<T, TProperty>(Expression<Func<T, TProperty>> expression)
    {
        if (expression.Body is MemberExpression memberExpression)
            return memberExpression.Member;
        throw new ArgumentException("Invalid expression");
    }

    public static IQueryable<TEntity> SearchOrMultiple<TEntity>(
      this IQueryable<TEntity> query,
      string? value,
      params Expression<Func<TEntity, object>>[] propertySelectors)
      where TEntity : class
    {
        if (string.IsNullOrEmpty(value) || propertySelectors == null || propertySelectors.Length == 0)
            return query;

        var normalizedValue = value.ToLower();
        var parameter = Expression.Parameter(typeof(TEntity), "e");
        var containsMethod = typeof(string).GetMethod("Contains", new[] { typeof(string) });
        var toLowerMethod = typeof(string).GetMethod("ToLower", Type.EmptyTypes);
        MethodInfo anyMethod = typeof(Enumerable).GetMethods()
            .First(m => m.Name == "Any" && m.GetParameters().Length == 2)
            .MakeGenericMethod(typeof(string));

        Expression? combinedExpression = null;

        foreach (var propertySelector in propertySelectors)
        {
            var propertyAccess = GetPropertyAccessExpression(propertySelector.Body, parameter);
            if (propertyAccess == null)
                continue;

            Expression containsExpression;
            if (propertyAccess.Type == typeof(string))
            {
                var toLowerExpression = Expression.Call(propertyAccess, toLowerMethod!);
                containsExpression = Expression.Call(toLowerExpression, containsMethod!, Expression.Constant(normalizedValue));
            }
            else if (typeof(IEnumerable<string>).IsAssignableFrom(propertyAccess.Type))
            {
                var arrayParameter = Expression.Parameter(typeof(string), "arrayElement");
                var arrayToLowerExpression = Expression.Call(arrayParameter, toLowerMethod!);
                var arrayContainsExpression = Expression.Call(arrayToLowerExpression, containsMethod!, Expression.Constant(normalizedValue));
                var arrayLambda = Expression.Lambda<Func<string, bool>>(arrayContainsExpression, arrayParameter);
                containsExpression = Expression.Call(anyMethod, propertyAccess, arrayLambda);
            }
            else if (typeof(IEnumerable<object>).IsAssignableFrom(propertyAccess.Type))
            {
                var itemParameter = Expression.Parameter(propertyAccess.Type.GetGenericArguments()[0], "item");
                var nestedPropertyAccess = GetPropertyAccessExpression(Expression.Convert(Expression.MakeMemberAccess(itemParameter, propertyAccess.Type.GetGenericArguments()[0].GetProperty("TagType")!), typeof(object)), itemParameter);
                if (nestedPropertyAccess.Type == typeof(string))
                {
                    var nestedToLowerExpression = Expression.Call(nestedPropertyAccess, toLowerMethod!);
                    var nestedContainsExpression = Expression.Call(nestedToLowerExpression, containsMethod!, Expression.Constant(normalizedValue));
                    var nestedLambda = Expression.Lambda(nestedContainsExpression, itemParameter);
                    containsExpression = Expression.Call(anyMethod, propertyAccess, nestedLambda);
                }
                else
                {
                    throw new NotSupportedException($"Type {nestedPropertyAccess.Type} is not supported.");
                }
            }
            else
            {
                throw new NotSupportedException($"Type {propertyAccess.Type} is not supported.");
            }

            combinedExpression = combinedExpression == null ?
                containsExpression :
                Expression.OrElse(combinedExpression, containsExpression);
        }

        if (combinedExpression == null)
            return query;

        var lambdaExpression = Expression.Lambda<Func<TEntity, bool>>(combinedExpression, parameter);
        return query.Where(lambdaExpression);
    }

    private static Expression GetPropertyAccessExpression(Expression expression, ParameterExpression parameter)
    {
        if (expression is UnaryExpression unaryExpression)
        {
            expression = unaryExpression.Operand;
        }

        if (expression is MemberExpression memberExpression)
        {
            return BuildMemberAccessExpression(memberExpression, parameter);
        }

        if (expression is MethodCallExpression methodCallExpression && IsSelectMethod(methodCallExpression))
        {
            var memberExpressionItem = methodCallExpression.Arguments[0] as MemberExpression;
            if (memberExpressionItem != null)
            {
                var collectionExpression = BuildMemberAccessExpression(memberExpressionItem, parameter);
                var lambda = methodCallExpression.Arguments[1] as LambdaExpression;
                if (lambda?.Body is MemberExpression subMemberExpression)
                {
                    var itemParameter = lambda.Parameters[0];
                    return Expression.Call(
                        typeof(Enumerable),
                        "Select",
                        new[] { itemParameter.Type, typeof(string) },
                        collectionExpression,
                        Expression.Lambda(subMemberExpression, itemParameter));
                }
            }
        }

        throw new ArgumentException("Invalid expression type");
    }

    private static Expression BuildMemberAccessExpression(MemberExpression memberExpression, ParameterExpression parameter)
    {
        var expressionStack = new Stack<MemberExpression>();

        while (memberExpression != null)
        {
            expressionStack.Push(memberExpression);
            if (memberExpression.Expression is MemberExpression innerMember)
            {
                memberExpression = innerMember;
            }
            else if (memberExpression.Expression is ParameterExpression)
            {
                break;
            }
            else
            {
                throw new ArgumentException("Expression is not supported");
            }
        }

        Expression finalExpression = parameter;
        while (expressionStack.Count > 0)
        {
            var currentMember = expressionStack.Pop();
            finalExpression = Expression.MakeMemberAccess(finalExpression, currentMember.Member);
        }

        return finalExpression;
    }

    private static bool IsSelectMethod(MethodCallExpression expression)
    {
        return expression.Method.DeclaringType == typeof(Enumerable) && expression.Method.Name == "Select";
    }

    public static IQueryable<TEntity> WhereNotContains<TEntity, TValue>(this IQueryable<TEntity> query,
        Expression<Func<TEntity, TValue?>> propertySelector,
        IEnumerable<TValue>? values)
    {
        if (values == null || !values.Any())
            return query;

        var parameter = propertySelector.Parameters.Single();
        var memberExpression = GetMemberExpression(propertySelector.Body);

        if (memberExpression == null || !(memberExpression.Member is System.Reflection.PropertyInfo))
            throw new ArgumentException("Invalid property selector.");

        var propertyInfo = (System.Reflection.PropertyInfo)memberExpression.Member;
        var propertyType = propertyInfo.PropertyType;

        if (propertyType != typeof(TValue))
            throw new ArgumentException("Property type mismatch.");

        var valuesArray = values.ToArray();
        var containsMethod = typeof(Enumerable).GetMethods()
            .First(m => m.Name == "Contains" && m.GetParameters().Length == 2)
            .MakeGenericMethod(typeof(TValue));

        var valuesExpression = Expression.Constant(valuesArray);
        var propertyExpression = Expression.Property(parameter, propertyInfo);
        var containsExpression = Expression.Call(containsMethod, valuesExpression, propertyExpression);
        var notContainsExpression = Expression.Not(containsExpression);

        var lambda = Expression.Lambda<Func<TEntity, bool>>(notContainsExpression, parameter);
        return query.Where(lambda);
    }

    private static MemberExpression GetMemberExpression(Expression expression)
    {
        if (expression is MemberExpression memberExpression)
            return memberExpression;

        if (expression is UnaryExpression unaryExpression && unaryExpression.Operand is MemberExpression)
            return (MemberExpression)unaryExpression.Operand;

        throw new ArgumentException("Invalid expression");
    }
}
