namespace PRManagement.Application.Common.Extensions;
public static class RuleBuilderExtension
{
    public static IRuleBuilderOptions<T, string> StringIsGuid<T>(this IRuleBuilder<T, string> ruleBuilder)
      => ruleBuilder.Must(value =>
        {
            if (string.IsNullOrWhiteSpace(value))
                return true;
            return value.IsValidGuid();
        })
        .WithMessage("The {PropertyName} must be a valid GUID.");

    public static IRuleBuilderOptions<T, string?> StringOrNullIsGuid<T>(this IRuleBuilder<T, string?> ruleBuilder)
        => ruleBuilder.Must(value =>
        {
            if (string.IsNullOrWhiteSpace(value))
                return true;
            return value.IsValidGuid();
        })
        .WithMessage("The {PropertyName} must be a valid GUID.");

    public static IRuleBuilderOptions<T, string> StringIsUrl<T>(this IRuleBuilder<T, string> ruleBuilder)
      => ruleBuilder.Must(value =>
        {
            if (string.IsNullOrWhiteSpace(value)) return true;
            return Uri.TryCreate(value, UriKind.Absolute, out _);
        }).WithMessage("The {PropertyName} must be a valid Url.");

    public static IRuleBuilderOptions<T, IEnumerable<TItem>?> MinimalItem<T, TItem>(this IRuleBuilder<T, IEnumerable<TItem>?> ruleBuilder, uint amount)
      => ruleBuilder
          .Must(p =>
            {
                if (p is null) return true;
                return p.Count() <= amount;
            })
          .WithMessage("The {PropertyName} should be less then " + amount + ".");

    public static IRuleBuilderOptions<T, string[]> MustNotContainDuplicates<T>(this IRuleBuilder<T, string[]> ruleBuilder)
      => ruleBuilder.Must((property) =>
        {
            return property.Distinct().Count() == property.Count();
        }).WithMessage("The {PropertyName} should not contain duplicate values.");

    public static IRuleBuilderOptions<T, string> MustEnum<T, TEnum>(this IRuleBuilder<T, string> ruleBuilder)
        where TEnum : Enum
    {
        var enumValues = Enum.GetValues(typeof(TEnum)).Cast<TEnum>().Select(e => e.ToString());
        return ruleBuilder.Must((rootObject, value, context) =>
        {
            if (string.IsNullOrEmpty(value))
                return true;
            return enumValues.Any(v => v.Equals(value)) || Enum.TryParse(typeof(TEnum), value, out _);
        }).WithMessage("The {PropertyName} must be one of the enum values." + string.Join(",", enumValues));
    }

    public static IRuleBuilderOptions<T, string> MustNotBeDuplicate<T>(this IRuleBuilder<T, string> ruleBuilder)
      => ruleBuilder.Must((property) =>
        {
            return property.Distinct().Count() == property.Count();
        }).WithMessage("The {PropertyName} should not contain duplicate values.");

    public static IRuleBuilderOptions<T, string> MustBeValidContains<T>(this IRuleBuilder<T, string> ruleBuilder, IEnumerable<string> allowedValues)
      => ruleBuilder.Must(value => allowedValues.Contains(value))
          .WithMessage("Invalid {PropertyName}. Allowed values are: " + string.Join(", ", allowedValues));

    public static IRuleBuilderOptions<T, string> StringIsUlid<T>(this IRuleBuilder<T, string> ruleBuilder)
      => ruleBuilder.Must(value =>
        {
            if (string.IsNullOrWhiteSpace(value))
                return true;
            return value.IsValidUlId();
        })
        .WithMessage("The {PropertyName} must be a valid UlId.");

}

