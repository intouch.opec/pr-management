using NUlid;

namespace PRManagement.Application.Common.Extensions;
public static class StringExtension
{
    const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    public static bool IsValidGuid(this string value)
    {
        if (Guid.TryParse(value, out _))
            return true;

        return false;
    }

    public static Guid? ToGuid(this string? value)
    {
        if (string.IsNullOrEmpty(value) || !value.IsValidGuid() || string.IsNullOrWhiteSpace(value))
            return null;

        return Guid.Parse(value);
    }

    public static Guid ToGuidOnly(this string value)
    {
        return Guid.Parse(value);
    }

    public static bool IsValidGuids(this IEnumerable<string> values)
    {
        if (values == null) return false;

        foreach (var value in values)
            if (!Guid.TryParse(value, out _))
                return false;

        return true;
    }

    public static IEnumerable<Guid> ToGuids(this IEnumerable<string> values)
    {
        return values.Select(Guid.Parse);
    }

    public static Ulid ToUlId(this string value)
    {
        return Ulid.Parse(value);
    }

    public static bool IsValidUlId(this string value)
    {
        if (Ulid.TryParse(value, out _))
            return true;

        return false;
    }

    public static Ulid ConvertGuidStringToUlid(this string guidString)
    {
        if (Guid.TryParse(guidString, out Guid guid))
            return ConvertGuidToUlid(guid);
        else
            throw new ArgumentException("Invalid GUID format", nameof(guidString));
    }

    public static Ulid ConvertGuidToUlid(this Guid guid)
    {
        var guidBytes = guid.ToByteArray();
        var ulidBytes = new byte[16];
        Array.Copy(guidBytes, ulidBytes, 16);
        return new Ulid(ulidBytes);
    }

    public static string GenerateRandomString(int length)
    {
        var random = new Random();
        return new string(Enumerable.Repeat(chars, length)
            .Select(s => s[random.Next(s.Length)]).ToArray());
    }
}