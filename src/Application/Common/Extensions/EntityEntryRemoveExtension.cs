using Microsoft.EntityFrameworkCore.ChangeTracking;
using PRManagement.Domain.Interfaces;

namespace PRManagement.Application.Common.Extensions;
public static class EntityEntryRemoveExtension
{
    public static EntityEntry<TEntity> RemoveAndAddRemark<TEntity>(this DbSet<TEntity> entityEntry, TEntity entry, string remark)
    where TEntity : class
    {
        if (entry is IRemarkableEntity remarkableEntity)
        {
            remarkableEntity.RemarkDeleted = remark;
        }

        return entityEntry.Remove(entry);
    }
}