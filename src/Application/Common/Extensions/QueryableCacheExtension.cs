using System.Linq.Expressions;
using System.Security.Cryptography;
using System.Text;
using Microsoft.Extensions.Caching.Memory;
using PRManagement.Application.Common.Configurations;

namespace PRManagement.Application.Common.Extensions;
public static class QueryCacheExtensions
{
    private static IMemoryCache _cache = new MemoryCache(new MemoryCacheOptions());
    private const int _absoluteExpirationSeconds = 1000;
    private readonly static ApplicationSettingConfiguration _applicationSettingConfiguration = ConfigurationHelper.ApplicationSettingConfiguration;
    internal static string GetCacheKey(IQueryable query)
    {
        var queryString = query.ToQueryString();
        var hash = SHA256.HashData(Encoding.UTF8.GetBytes(queryString));
        return Convert.ToBase64String(hash);
    }

    public static IEnumerable<T> FromCache<T>(this IQueryable<T> query)
    {
        var key = GetCacheKey(query);

        var result = _cache.GetOrCreate(key, cache =>
        {
            cache.AbsoluteExpiration = DateTimeOffset.Now.AddSeconds(_absoluteExpirationSeconds);
            return query;
        }) ?? Enumerable.Empty<T>();

        return result;
    }

    public static async Task<IEnumerable<T>> FromCacheAsync<T>(this IQueryable<T> query, CancellationToken cancellationToken = default)
    {
        var key = GetCacheKey(query);

        var result = await _cache.GetOrCreateAsync(key, cache =>
        {
            cache.AbsoluteExpiration = DateTimeOffset.Now.AddSeconds(_absoluteExpirationSeconds);
            return query.ToListAsync(cancellationToken);
        }) ?? Enumerable.Empty<T>();

        return result;
    }

    public static async Task<T?> FirstOrDefaultCacheAsync<T>(this IQueryable<T> query,
        Expression<Func<T, bool>> predicate,
        CancellationToken cancellationToken = default,
        int? expirationSecond = null)
    {
        var key = GetCacheKey(query.Where(predicate));
        var isFound = _cache.TryGetValue(key, out T? entity);
        if (isFound)
            return entity;

        entity = await query.FirstOrDefaultAsync(predicate, cancellationToken);

        if (entity is null)
            return entity;

        var result = await _cache.GetOrCreateAsync(key, cache =>
        {
            cache.AbsoluteExpiration = DateTimeOffset.Now.AddSeconds(expirationSecond ?? _applicationSettingConfiguration.EntityCacheSeconds);
            return Task.FromResult(entity);
        });
        return result;
    }

    public static async Task<T?> FirstOrDefaultCacheAsync<T>(this IQueryable<T> query,
       CancellationToken cancellationToken = default,
       int? expirationSecond = null)
    {
        var key = GetCacheKey(query);

        var isFound = _cache.TryGetValue(key, out T? entity);

        if (isFound)
            return entity;

        entity = await query.FirstOrDefaultAsync(cancellationToken);

        if (entity is null)
            return entity;

        var result = await _cache.GetOrCreateAsync(key, cache =>
        {
            cache.AbsoluteExpiration = DateTimeOffset.Now.AddSeconds(expirationSecond ?? _applicationSettingConfiguration.EntityCacheSeconds);
            return Task.FromResult(entity);
        });
        return result;
    }

    public static async Task<bool> AllCacheAsync<T>(this IQueryable<T> query,
        Expression<Func<T, bool>> predicate,
        CancellationToken cancellationToken = default)
    {
        var key = GetCacheKey(query.Where(predicate));

        var result = await _cache.GetOrCreateAsync(key, async cache =>
        {
            cache.AbsoluteExpiration = DateTimeOffset.Now.AddSeconds(_applicationSettingConfiguration.ValidateOptionCacheSeconds);
            return await query.AnyCacheAsync(predicate, cancellationToken);
        });

        return result;
    }

    public static async Task<bool> AnyCacheAsync<T>(this IQueryable<T> query,
        Expression<Func<T, bool>> predicate,
        CancellationToken cancellationToken = default)
    {
        var key = GetCacheKey(query.Where(predicate));
        var isFound = _cache.TryGetValue(key, out bool result);

        if (isFound)
            return true;

        result = await query.AnyAsync(predicate, cancellationToken);

        if (!result)
            return result;

        result = _cache.GetOrCreate(key, cache =>
        {
            cache.AbsoluteExpiration = DateTimeOffset.Now.AddSeconds(_applicationSettingConfiguration.ValidateOptionCacheSeconds);
            return result;
        });
        return result;
    }

    public static async Task<T?> FirstOrDefaultAndRemoveCacheAsync<T>(this IQueryable<T> query,
        Expression<Func<T, bool>> predicate,
        CancellationToken cancellationToken = default)
    {
        var key = GetCacheKey(query.Where(predicate));
        var entity = await query.FirstOrDefaultCacheAsync(predicate, cancellationToken);
        _cache.Remove(key);
        return entity;
    }

    public static async Task<IEnumerable<TDestination>> ProjectToListCacheAsync<T, TDestination>(this IQueryable<T> query, IConfigurationProvider configuration, CancellationToken cancellationToken = default)
    {
        var key = GetCacheKey(query);
        var result = await _cache.GetOrCreateAsync(key, async cache =>
        {
            cache.AbsoluteExpiration = DateTimeOffset.Now.AddSeconds(_applicationSettingConfiguration.ValidateOptionCacheSeconds);
            return await query
            .ProjectTo<TDestination>(configuration)
            .ToListAsync(cancellationToken);
        });
        return result!;
    }

    public static void RemoveCacheAsync<T>(this IQueryable<T> query,
        Expression<Func<T, bool>> predicate)
    {
        var key = GetCacheKey(query.Where(predicate));
        _cache.Remove(key);
    }

    public static void RemoveCache<T>(this IQueryable<T> query)
    {
        var key = GetCacheKey(query);
        _cache.Remove(key);
    }

    public static void Clear()
    {
        _cache.Dispose();
        _cache = new MemoryCache(new MemoryCacheOptions());
    }

    public static async Task<IEnumerable<T>> ToListCacheAsync<T>(this IQueryable<T> query, CancellationToken cancellationToken = default)
    {
        var key = GetCacheKey(query);
        var result = await _cache.GetOrCreateAsync(key, async cache =>
        {
            cache.AbsoluteExpiration = DateTimeOffset.Now.AddSeconds(_applicationSettingConfiguration.ValidateOptionCacheSeconds);
            return await query
                .ToListAsync(cancellationToken);
        });
        return result!;
    }
}