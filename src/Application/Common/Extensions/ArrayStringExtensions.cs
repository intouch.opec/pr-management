namespace PRManagement.Application.Common.Extensions;

public static class ArrayStringExtensions
{
    public static bool AreAllGuids(this IEnumerable<string> value)
     => value.All(item => item.IsValidGuid());
}
