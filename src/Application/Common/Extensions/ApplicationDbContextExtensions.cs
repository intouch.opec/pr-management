using PRManagement.Domain.Entities;

namespace PRManagement.Application.Common.Extensions;
public static class ApplicationDbContextExtensions
{
    public static async Task<bool> HasProvince(this DbSet<Province> entityEntry, string? id, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(id))
            return true;

        return await entityEntry.AnyCacheAsync(e => e.Id.Equals(id), cancellationToken);
    }

    public static async Task<bool> HasDistrict(this DbSet<District> entityEntry, string? id, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(id))
            return true;

        return await entityEntry.AnyCacheAsync(e => e.Id.Equals(id), cancellationToken);
    }

    public static async Task<bool> HasSubDistrict(this DbSet<SubDistrict> entityEntry, string? id, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(id))
            return true;

        return await entityEntry.AnyCacheAsync(e => e.Id.Equals(id), cancellationToken);
    }

    public static async Task<bool> HasPRActivity(this DbSet<PRActivity> entityEntry, string? id, CancellationToken cancellationToken)
    {
        if (!id!.IsValidGuid())
            return true;

        return await entityEntry.AnyCacheAsync(e => e.Id.Equals(id!.ToGuidOnly()), cancellationToken);
    }

    public static async Task<bool> HasPRActivityTask(this DbSet<PRActivityTask> entityEntry, string? id, CancellationToken cancellationToken)
    {
        if (!id!.IsValidGuid())
            return true;

        return await entityEntry.AnyCacheAsync(e => e.Id.Equals(id!.ToGuidOnly()), cancellationToken);
    }

    public static async Task<bool> HasPRActivityLocalFile(this DbSet<PRActivityLocalFile> entityEntry, string? id, CancellationToken cancellationToken)
    {
        if (!id!.IsValidGuid())
            return true;

        return await entityEntry.AnyCacheAsync(e => e.Id.Equals(id!.ToGuidOnly()), cancellationToken);
    }
}
