
using Microsoft.AspNetCore.Http;
namespace PRManagement.Application.Common.Interfaces;

public interface IUploadFileService
{
    Task<string> UploadFileAsync(string fileName, IFormFile file);
    Task<byte[]> GetFileAsync(string path);
    void RemoveFile(string fileName);
    string GetContentType(string fileName);
}
