﻿using PRManagement.Domain.Entities;

namespace PRManagement.Application.Common.Interfaces;

public interface IApplicationDbContext
{
    DbSet<Address> Address { get; }
    DbSet<BookMark> BookMarks { get; }
    DbSet<BookmarkGroup> BookmarkGroups { get; }
    DbSet<Contact> Contacts { get; }
    DbSet<Client> Clients { get; }
    DbSet<ContactClient> ContactClients { get; }
    DbSet<ContactMediaSection> ContactMediaSections { get; }
    DbSet<Media> Medias { get; }
    DbSet<MediaSection> MediaSections { get; }
    DbSet<Platform> Platforms { get; }
    DbSet<Section> Sections { get; }
    DbSet<Province> Provinces { get; }
    DbSet<RegisterForm> RegisterForms { get; }
    DbSet<District> Districts { get; }
    DbSet<SubDistrict> SubDistricts { get; }
    DbSet<SubSection> SubSections { get; }
    DbSet<New> News { get; }
    DbSet<LocalFile> LocalFiles { get; }
    DbSet<MonitorTaskWorker> MonitorTaskWorkers { get; }
    DbSet<PRActivity> PRActivities { get; }
    DbSet<PRActivityBriefing> PRActivityBriefings { get; }
    DbSet<PRActivityEvent> PRActivityEvents { get; }
    DbSet<PRActivityEmail> PRActivityEmails { get; }
    DbSet<PRActivityLocation> PRActivityLocations { get; }
    DbSet<PRActivityReceiver> PRActivityReceivers { get; }
    DbSet<PRActivityTask> PRActivityTasks { get; }
    DbSet<PRActivityTaskComment> PRActivityTaskComments { get; }
    DbSet<PRActivityTaskResponsiblePerson> PRActivityTaskResponsiblePersons { get; }
    DbSet<PRActivityLocalFile> PRActivityLocalFiles { get; }
    DbSet<PRActivityEventJoiner> PRActivityEventJoiners { get; }
    DbSet<WorkProgress> WorkProgress { get; }

    Task<int> SaveChangesAsync(CancellationToken cancellationToken);
}
