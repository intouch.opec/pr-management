namespace PRManagement.Application.Common.Interfaces;

public interface IMailService
{
    Task SendEmailAsync(string toEmail, string subject, string body);
}
