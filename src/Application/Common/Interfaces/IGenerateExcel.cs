namespace PRManagement.Application.Common.Interfaces;

public interface IGenerateExcel
{
    Task<byte[]> GenerateExcelAsync(string templateFile, string header, IEnumerable<IEnumerable<string>> records);
}
