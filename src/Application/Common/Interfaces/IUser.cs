﻿using System.Security.Claims;

namespace PRManagement.Application.Common.Interfaces;

public interface IUser
{
    string? Id { get; }
    ClaimsPrincipal? Principal { get; }
}
