using PRManagement.Application.Platforms.Commands.CalculateTierPlatform;

namespace PRManagement.Application.Common.Interfaces;

public interface ICalculateTierPlatform
{
    public CalculateTierPlatformResponse CalculateTierPlatformByFollower(uint numberOfFollower);
}
