using PRManagement.Application.PRActivityLocalFiles.Commands.UploadFiles;

namespace PRManagement.Application.PRActivityLocalFiles.Queries.PRActivityLocalFileWithPagination;

public class PRActivityLocalFileItem: UploadFileResponse
{
    public DateTimeOffset Created { get; set; }
    public string? CreatedBy { get; set; }
    public DateTimeOffset LastModified { get; set; }
    public string? LastModifiedBy { get; set; }
}
