﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.Common.Mappings;
using PRManagement.Application.Common.Models;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.PRActivityLocalFiles.Queries.PRActivityLocalFileWithPagination;

public record PRActivityLocalFileWithPaginationQuery : IRequest<PaginatedList<PRActivityLocalFileItem>>
{
    public string? FileName { get; set; }
    public DateTimeOffset? CreatedStart { get; set; }
    public DateTimeOffset? CreatedEnd { get; set; }
    public int? PageNumber { get; init; } = 1;
    public int? PageSize { get; init; } = 10;
    public SortType? Sort { get; set; } = SortType.DESC;
    public string? SortBy { get; set; } = nameof(PRActivityLocalFileItem.LastModified);
}

public class PRActivityLocalFileWithPaginationQueryHandler : IRequestHandler<PRActivityLocalFileWithPaginationQuery, PaginatedList<PRActivityLocalFileItem>>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public PRActivityLocalFileWithPaginationQueryHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<PaginatedList<PRActivityLocalFileItem>> Handle(PRActivityLocalFileWithPaginationQuery request, CancellationToken cancellationToken)
    {
        return await Search(request)
            .ProjectTo<PRActivityLocalFileItem>(_mapper.ConfigurationProvider)
            .OrderBy(request.SortBy ?? nameof(PRActivityLocalFileItem.LastModified), ascending: request.Sort ?? SortType.DESC)
            .PaginatedListAsync(request.PageNumber ?? 1, request.PageSize ?? 10, cancellationToken);
    
    }

    internal IQueryable Search(PRActivityLocalFileWithPaginationQuery request)
    {
        var query = _context.PRActivityLocalFiles
            .Include(e => e.LocalFile)
            .WhereLike(e => e.LocalFile!.FileName, request.FileName)
            .WhereRange(e => e.Created, request.CreatedStart, request.CreatedEnd);

        return query;
    }
}
