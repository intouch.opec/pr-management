using PRManagement.Domain.Entities;

namespace PRManagement.Application.PRActivityLocalFiles.Queries.PRActivityLocalFileWithPagination;

public class MappingProfile
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<PRActivityLocalFile, PRActivityLocalFileItem>()
                .ForMember(dest => dest.Id, op => op.MapFrom(src => src.PRActivityId))
                .ForMember(dest => dest.Path, op => op.MapFrom(src => src.LocalFile!.Path))
                .ForMember(dest => dest.Size, op => op.MapFrom(src => src.LocalFile!.Size))
                .ForMember(dest => dest.ContentType, op => op.MapFrom(src => src.LocalFile!.ContentType))
                .ForMember(dest => dest.FileName, op => op.MapFrom(src => src.LocalFile!.FileName));

        }
    }
}
