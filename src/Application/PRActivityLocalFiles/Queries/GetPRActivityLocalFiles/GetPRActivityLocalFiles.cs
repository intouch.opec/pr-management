﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;

namespace PRManagement.Application.PRActivityLocalFiles.Queries.GetPRActivityLocalFiles;

public record GetPRActivityLocalFilesQuery(string Id) : IRequest<FileResponse>;

public class GetPRActivityLocalFilesQueryHandler : IRequestHandler<GetPRActivityLocalFilesQuery, FileResponse>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public GetPRActivityLocalFilesQueryHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<FileResponse> Handle(GetPRActivityLocalFilesQuery request, CancellationToken cancellationToken)
    {
        var entity = await _context.PRActivityLocalFiles
            .Include(e => e.LocalFile)
            .Where(e => e.Id.Equals(request.Id.ToGuidOnly()))
            .FirstOrDefaultAsync(cancellationToken);

        Guard.Against.NotFound(entity!.Id, request);

        return _mapper.Map<FileResponse>(entity.LocalFile);
    }
}
