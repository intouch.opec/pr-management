using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.PRActivityLocalFiles.Queries.GetPRActivityLocalFiles;

public class MappingProfile
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<LocalFile, FileResponse>()
                .ForMember(e => e.File, op => op.MapFrom<LocalFileToFileResolver>())
                .ForMember(e => e.FileName, op => op.MapFrom(o => o.FileName))
                .ForMember(e => e.ContentType, op => op.MapFrom<LocalFileToContentTypeResolver>());
        }
    }

    internal class LocalFileToFileResolver : IValueResolver<LocalFile, FileResponse, byte[]>
    {
        private readonly IUploadFileService _uploadFileService;

        public LocalFileToFileResolver(IUploadFileService uploadFileService)
        {
            _uploadFileService = uploadFileService;
        }

        public byte[] Resolve(LocalFile source, FileResponse destination, byte[] member, ResolutionContext context)
        {
            var task = _uploadFileService.GetFileAsync(source.Path);
            task.Wait();
            return task.Result;
        }
    }

    internal class LocalFileToContentTypeResolver : IValueResolver<LocalFile, FileResponse, string>
    {
        private readonly IUploadFileService _uploadFileService;

        public LocalFileToContentTypeResolver(IUploadFileService uploadFileService)
        {
            _uploadFileService = uploadFileService;
        }

        public string Resolve(LocalFile source, FileResponse destination, string member, ResolutionContext context)
        {
            return _uploadFileService.GetContentType(source.Path);
        }
    }

}
