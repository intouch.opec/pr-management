using PRManagement.Application.Common.Extensions;

namespace PRManagement.Application.PRActivityLocalFiles.Queries.GetPRActivityLocalFiles;

public class GetPRActivityLocalFilesQueryValidator : AbstractValidator<GetPRActivityLocalFilesQuery>
{
    public GetPRActivityLocalFilesQueryValidator()
    {
        RuleFor(e => e.Id)
            .NotEmpty()
            .StringIsGuid();
    }
}
