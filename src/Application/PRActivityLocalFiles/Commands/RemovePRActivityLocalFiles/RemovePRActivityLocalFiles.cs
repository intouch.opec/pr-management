﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.PRActivityLocalFiles.Commands.UploadFiles;

namespace PRManagement.Application.PRActivityLocalFiles.Commands.RemovePRActivityLocalFiles;

public record RemovePRActivityLocalFilesCommand(string Id) : IRequest<UploadFileResponse>;

public class RemovePRActivityLocalFilesCommandHandler : IRequestHandler<RemovePRActivityLocalFilesCommand, UploadFileResponse>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public RemovePRActivityLocalFilesCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<UploadFileResponse> Handle(RemovePRActivityLocalFilesCommand request, CancellationToken cancellationToken)
    {
        var entity = await _context.PRActivityLocalFiles
            .FirstOrDefaultAsync(
                prActivity => prActivity.Id.Equals(request.Id.ToGuidOnly()),
                cancellationToken);

        Guard.Against.NotFound(entity!.Id, request);
        _context.PRActivityLocalFiles.Remove(entity);

        return _mapper.Map<UploadFileResponse>(entity);
    }
}
