using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Constants;

namespace PRManagement.Application.PRActivityLocalFiles.Commands.RemovePRActivityLocalFiles;

public class RemovePRActivityLocalFilesCommandValidator : AbstractValidator<RemovePRActivityLocalFilesCommand>
{
    private IApplicationDbContext _context { get; set; }

    public RemovePRActivityLocalFilesCommandValidator(IApplicationDbContext context)
    {
        _context = context;
        
        RuleFor(e => e.Id)
           .StringIsGuid()
           .NotEmpty()
           .MustAsync(_context.PRActivityLocalFiles.HasPRActivityLocalFile)
           .WithMessage(ValidatorMessages.NotFound);
    }
}