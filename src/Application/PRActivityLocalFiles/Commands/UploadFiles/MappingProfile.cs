using Microsoft.AspNetCore.Http;
using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.PRActivityLocalFiles.Commands.UploadFiles;

public class CreatePRActivityMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<UploadFilesCommand, PRActivityLocalFile>()
                .ForMember(dest => dest.LocalFile, op => op.MapFrom<LocalFileValueResolver>())
                .ForMember(dest => dest.PRActivityId, op => op.MapFrom(src => src.Id.ToGuidOnly()));

            CreateMap<PRActivityLocalFile, UploadFileResponse>()
                .ForMember(dest => dest.Id, op => op.MapFrom(src => src.PRActivityId))
                .ForMember(dest => dest.Path, op => op.MapFrom(src => src.LocalFile!.Path))
                .ForMember(dest => dest.Size, op => op.MapFrom(src => src.LocalFile!.Size))
                .ForMember(dest => dest.ContentType, op => op.MapFrom(src => src.LocalFile!.ContentType))
                .ForMember(dest => dest.FileName, op => op.MapFrom(src => src.LocalFile!.FileName));
        }
    }
}

public class LocalFileValueResolver : IValueResolver<UploadFilesCommand, PRActivityLocalFile, LocalFile?>
{
    private readonly IUploadFileService _uploadFileService;

    public LocalFileValueResolver(IUploadFileService uploadFileService)
    {
        _uploadFileService = uploadFileService;
    }

    public LocalFile? Resolve(UploadFilesCommand source, PRActivityLocalFile destination, LocalFile? member, ResolutionContext context)
    {
        return new LocalFile()
        {
            Path = UploadFileAsync(source.File)
                .GetAwaiter()
                .GetResult(),
            ContentType = source.File.ContentType,
            Size = $"{source.File.Length}",
            FileName = source.File.FileName
        };
    }

    public async Task<string> UploadFileAsync(IFormFile file)
    {
        return await _uploadFileService.UploadFileAsync(file.FileName, file);
    }

}
