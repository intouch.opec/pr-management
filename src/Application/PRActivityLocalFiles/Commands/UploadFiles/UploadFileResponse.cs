namespace PRManagement.Application.PRActivityLocalFiles.Commands.UploadFiles;

public class UploadFileResponse
{
    public Guid Id { get; set; }
    public string FileName { get; set; } = string.Empty;
    public string Path { get ; set; } = string.Empty;
    public string ContentType { get; set; } = string.Empty;
    public string Size { get; set; } = string.Empty;
    public Guid LocalFileId { get; set; }
    public Guid PRActivityId { get; set; }
}
