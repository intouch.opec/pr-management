using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Constants;

namespace PRManagement.Application.PRActivityLocalFiles.Commands.UploadFiles;

public class UploadFilesCommandValidator : AbstractValidator<UploadFilesCommand>
{
    private IApplicationDbContext _context { get; set; }
    private const int MaxFileSizeInMB = 2;
    private const int MaxFileSizeInBytes = MaxFileSizeInMB * 1024 * 1024; // 2MB in bytes

    public UploadFilesCommandValidator(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(e => e.Id)
           .StringIsGuid()
           .NotEmpty()
           .MustAsync(_context.PRActivities.HasPRActivity)
           .WithMessage(ValidatorMessages.NotFound);

        RuleFor(e => e.File)
            .Must(file => file.Length > 0)
            .WithMessage("File cannot be empty.")
            .NotNull()
            .NotEmpty()
            .WithMessage("File is required");

        When(e => e.File != null, () =>
        {
            RuleFor(e => e.File.Length)
                .Must(fileSize => fileSize <= MaxFileSizeInBytes)
                .WithMessage($"File size exceeds the limit of {MaxFileSizeInMB}MB");

            RuleFor(e => e.File.FileName)
                .NotEmpty()
                .WithMessage("Filename is required");

            RuleFor(e => Path.GetExtension(e.File.FileName).ToLower())
                .Must(ext => {
                    return AllowedFileExtensions.Contains(ext);
                })
                .WithMessage("Invalid file type. Allowed types are: " + string.Join(", ", AllowedFileExtensions));
        });
    }

    private static readonly string[] AllowedFileExtensions = new[]
    {
        ".pdf",
        ".doc",
        ".docx",
        ".xls",
        ".xlsx",
        ".jpg",
        ".jpeg",
        ".png",
        ".txt"
    };
}
