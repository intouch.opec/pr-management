﻿using Microsoft.AspNetCore.Http;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.PRActivityLocalFiles.Commands.UploadFiles;

public record UploadFilesCommand(string Id) : IRequest<UploadFileResponse>
{
    public required IFormFile File { get; set; }
}

public class UploadFilesCommandHandler : IRequestHandler<UploadFilesCommand, UploadFileResponse>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public UploadFilesCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<UploadFileResponse> Handle(UploadFilesCommand request, CancellationToken cancellationToken)
    {
        var entity = _mapper.Map<PRActivityLocalFile>(request);
        await _context.PRActivityLocalFiles.AddAsync(entity, cancellationToken);
        await _context.SaveChangesAsync(cancellationToken);
        return _mapper.Map<UploadFileResponse>(entity);
    }
}
