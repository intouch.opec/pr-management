namespace PRManagement.Application.PRActivityEmails.Commands.CreatePRActivityEmail;

public class PRActivityEmailDto
{
    public Guid Id { get; set; }
    public string Subject { get; set; } = string.Empty;
    public string Content { get; set; } = string.Empty;
    public DateTimeOffset StartSent { get; set; }
    public DateTimeOffset SentFinished { get; set; }
    public Guid PRActivityId { get; set; }
}
