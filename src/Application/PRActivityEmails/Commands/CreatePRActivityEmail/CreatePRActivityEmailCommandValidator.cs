
using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Constants;

namespace PRManagement.Application.PRActivityEmails.Commands.CreatePRActivityEmail;

public class CreatePRActivityEmailCommandValidator : AbstractValidator<CreatePRActivityEmailCommand>
{
    private readonly IApplicationDbContext _context;

    public CreatePRActivityEmailCommandValidator(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(e => e.Subject)
            .MaximumLength(255)
            .NotEmpty();

        RuleFor(e => e.Content)
            .NotEmpty();

        RuleFor(e => e.PRActivityId)
            .StringIsGuid()
            .NotEmpty()
            .MustAsync(_context.PRActivities.HasPRActivity)
            .WithMessage(ValidatorMessages.NotFound);
    }
}
