﻿using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.PRActivityEmails.Commands.CreatePRActivityEmail;

public record CreatePRActivityEmailCommand : IRequest<PRActivityEmailDto>
{
    public string Subject { get; set; } = string.Empty;
    public string Content { get; set; } = string.Empty;
    public string PRActivityId { get; set; } = string.Empty;
}

public class CreatePRActivityEmailCommandHandler : IRequestHandler<CreatePRActivityEmailCommand, PRActivityEmailDto>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public CreatePRActivityEmailCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<PRActivityEmailDto> Handle(CreatePRActivityEmailCommand request, CancellationToken cancellationToken)
    {
        var entity = _mapper.Map<PRActivityEmail>(request);

        await _context.PRActivityEmails.AddAsync(entity, cancellationToken);
        await _context.SaveChangesAsync(cancellationToken);

        return _mapper.Map<PRActivityEmailDto>(entity);
    }
}
