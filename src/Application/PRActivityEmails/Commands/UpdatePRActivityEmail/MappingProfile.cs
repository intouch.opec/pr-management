using PRManagement.Application.Common.Extensions;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.PRActivityEmails.Commands.UpdatePRActivityEmail;

public class MappingProfile
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<UpdatePRActivityEmailCommand, PRActivityEmail>()
                .ForMember(dest => dest.PRActivityId, op => op.MapFrom(src => src.PRActivityId.ToGuidOnly()));
            
            CreateMap<PRActivityEmail, UpdatePRActivityEmailCommand>();
        }
    }
}
