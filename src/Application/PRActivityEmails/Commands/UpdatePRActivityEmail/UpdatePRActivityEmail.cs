﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.PRActivityEmails.Commands.CreatePRActivityEmail;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.PRActivityEmails.Commands.UpdatePRActivityEmail;

public record UpdatePRActivityEmailCommand : IRequest<PRActivityEmailDto>
{
    public string Subject { get; set; } = string.Empty;
    public string Content { get; set; } = string.Empty;
    public string PRActivityId { get; set; } = string.Empty;
}

public class UpdatePRActivityEmailCommandHandler : IRequestHandler<UpdatePRActivityEmailCommand, PRActivityEmailDto>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public UpdatePRActivityEmailCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<PRActivityEmailDto> Handle(UpdatePRActivityEmailCommand request, CancellationToken cancellationToken)
    {
        var entity = await _context.PRActivityEmails
            .AsTracking()
            .FirstOrDefaultAndRemoveCacheAsync(
                e => e.PRActivityId.Equals(request.PRActivityId.ToGuidOnly()),
                cancellationToken
            );

        Guard.Against.NotFound(request.PRActivityId, entity);

        _mapper.Map<UpdatePRActivityEmailCommand, PRActivityEmail>(request, entity);

        await _context.SaveChangesAsync(cancellationToken);

        return _mapper.Map<PRActivityEmailDto>(entity);
    }
}
