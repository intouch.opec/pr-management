﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.PRActivityEmails.Commands.CreatePRActivityEmail;

namespace PRManagement.Application.PRActivityEmails.Queries.GetPRActivityEmail;

public record GetPRActivityEmailQuery(string PRActivityId) : IRequest<PRActivityEmailDto>;

public class GetPRActivityEmailQueryHandler : IRequestHandler<GetPRActivityEmailQuery, PRActivityEmailDto>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public GetPRActivityEmailQueryHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<PRActivityEmailDto> Handle(GetPRActivityEmailQuery request, CancellationToken cancellationToken)
    {
        var entity = await _context.PRActivityEmails
            .AsNoTracking()
            .ProjectTo<PRActivityEmailDto>(_mapper.ConfigurationProvider)
            .FirstOrDefaultAndRemoveCacheAsync(
                e => e.PRActivityId.Equals(request.PRActivityId.ToGuidOnly()),
                cancellationToken
            );

        Guard.Against.NotFound(request.PRActivityId, entity);

        return entity;
    }
}
