using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;

namespace PRManagement.Application.PRActivityReceivers.Commands.RemovePRActivityReceivers;

public class RemovePRActivityReceiversCommandValidator : AbstractValidator<RemovePRActivityReceiversCommand>
{
    private readonly IApplicationDbContext _context;

    public RemovePRActivityReceiversCommandValidator(IApplicationDbContext context)
    {
        _context = context;
        RuleForEach(e => e.Ids)
            .StringIsGuid()
            .DependentRules(() =>
            {
                RuleForEach(command => command.Ids)
                    .SetValidator((command) =>
                    {
                        var prActivityReceiverIds = _context.PRActivityReceivers
                            .AsNoTracking()
                            .Where(e => 
                                command.Ids.Select(e => e.ToGuid())
                                    .Contains(e.Id))
                            .Select(e => e.ContactId.ToString())
                            .AsEnumerable();

                        return new PRActivityReceiverValidator(prActivityReceiverIds);
                    });
            });
    }
}

internal class PRActivityReceiverValidator : AbstractValidator<string>
{
    private readonly IEnumerable<string> _prActivityReceiverIds;

    public PRActivityReceiverValidator(IEnumerable<string> prActivityReceiverIds)
    {
        _prActivityReceiverIds = prActivityReceiverIds;

        RuleFor(pl => pl)
            .Must(HasContact)
            .WithMessage("The 'Id' already exit.");
    }

    private bool HasContact(string contactId)
    {
        if (!Guid.TryParse(contactId, out _))
            return false;
        return !_prActivityReceiverIds.Contains(contactId);
    }
}
