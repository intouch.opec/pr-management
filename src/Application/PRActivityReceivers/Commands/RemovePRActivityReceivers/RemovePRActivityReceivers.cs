﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.PRActivityReceivers.Commands.CreatePRActivityReceivers;

namespace PRManagement.Application.PRActivityReceivers.Commands.RemovePRActivityReceivers;

public record RemovePRActivityReceiversCommand : IRequest<IEnumerable<PRActivityReceiverDto>>
{
    public IEnumerable<string> Ids { get; set; } = Enumerable.Empty<string>();
}

public class RemovePRActivityReceiversCommandHandler : IRequestHandler<RemovePRActivityReceiversCommand, IEnumerable<PRActivityReceiverDto>>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public RemovePRActivityReceiversCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<IEnumerable<PRActivityReceiverDto>> Handle(RemovePRActivityReceiversCommand request, CancellationToken cancellationToken)
    {
        var ids = request.Ids.Select(e => e.ToGuid());

        var entities = await _context.PRActivityReceivers
            .Where(e => ids.Contains(e.Id))
            .ToListCacheAsync(cancellationToken);

        _context.PRActivityReceivers.RemoveRange(entities);

        await _context.SaveChangesAsync(cancellationToken);

        return entities.Select(_mapper.Map<PRActivityReceiverDto>);
    }
}
