using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Constants;

namespace PRManagement.Application.PRActivityReceivers.Commands.CreatePRActivityReceivers;

public class CreatePRActivityReceiversCommandValidator : AbstractValidator<CreatePRActivityReceiversCommand>
{
    public IApplicationDbContext _context;

    public CreatePRActivityReceiversCommandValidator(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(src => src.ContactIds)
            .NotEmpty();

        RuleFor(e => e.PRActivityId)
            .StringIsGuid()
            .NotEmpty()
            .MustAsync(_context.PRActivities.HasPRActivity)
            .WithMessage(ValidatorMessages.NotFound);

        RuleForEach(src => src.ContactIds)
            .ChildRules(e =>
                e.RuleFor(contactId => contactId)
                    .StringIsGuid()
                    .NotEmpty()
            )
            .DependentRules(() =>
            {
                RuleForEach(command => command.ContactIds)
                    .SetValidator((command) =>
                    {
                        var contactIds = command.ContactIds.ToGuids();

                        var ids = _context.Contacts
                            .AsNoTracking()
                            .Where(e => contactIds.Contains(e.Id))
                            .Select(e => e.Id)
                            .AsEnumerable();

                        var receiverContactsIds = _context.PRActivityReceivers
                            .AsNoTracking()
                            .Where(e => contactIds.Contains(e.ContactId))
                            .Select(e => e.ContactId)
                            .AsEnumerable();

                        return new CreatePRActivityReceiversValidator(contactIds, ids, receiverContactsIds);
                    });
            });
    }
}

public class CreatePRActivityReceiversValidator : AbstractValidator<string>
{
    private readonly IEnumerable<Guid> _ids;
    private readonly IEnumerable<Guid> _contactIds;
    private readonly IEnumerable<Guid> _receiverContactsIds;

    public CreatePRActivityReceiversValidator(
        IEnumerable<Guid> ids,
        IEnumerable<Guid> contactIds,
        IEnumerable<Guid> receiverContactsIds
        )
    {
        _ids = ids;
        _contactIds = contactIds;
        _receiverContactsIds = receiverContactsIds;

        RuleFor(pl => pl)
            .Must(HasDuplicateContactId)
            .WithMessage($"The '{nameof(CreatePRActivityReceiversCommand.ContactIds)}' is duplicate.")
            .Must(HasContact)
            .WithMessage(ValidatorMessages.NotFound)
            .Must(HasDuplicateReceiverContactsId)
            .WithMessage(ValidatorMessages.ShouldNotBeDuplicate);
    }

    internal bool HasDuplicateContactId(string contactId)
    {
        return _ids.Where(i => i.Equals(contactId.ToGuid())).Count() == 1;
    }

    internal bool HasContact(string contactId)
    {
        return _contactIds.Any(i => i.Equals(contactId.ToGuid()));
    }

    internal bool HasDuplicateReceiverContactsId(string contactId)
    {
        return !_receiverContactsIds.Any(i => i.Equals(contactId.ToGuid()));
    }
}
