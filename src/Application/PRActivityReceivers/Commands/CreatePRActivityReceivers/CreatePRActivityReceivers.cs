﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.PRActivityReceivers.Commands.CreatePRActivityReceivers;

public record CreatePRActivityReceiversCommand : IRequest<IEnumerable<PRActivityReceiverDto>>
{
    public IEnumerable<string> ContactIds { get; set; } = Enumerable.Empty<string>();
    public string PRActivityId { get; set; } = string.Empty;
}

public class CreatePRActivityReceiversCommandHandler : IRequestHandler<CreatePRActivityReceiversCommand, IEnumerable<PRActivityReceiverDto>>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public CreatePRActivityReceiversCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<IEnumerable<PRActivityReceiverDto>> Handle(CreatePRActivityReceiversCommand request, CancellationToken cancellationToken)
    {
        var entities = await MapReceivers(request, cancellationToken);

        await _context
            .PRActivityReceivers
            .AddRangeAsync(entities, cancellationToken);
        await _context.SaveChangesAsync(cancellationToken);

        return _mapper.Map<IEnumerable<PRActivityReceiverDto>>(entities);
    }


    internal async Task<IEnumerable<PRActivityReceiver>> MapReceivers(
        CreatePRActivityReceiversCommand request, 
        CancellationToken cancellationToken)
    {
        var contacts = await _context
           .Contacts
           .Include(e => e.Address)
           .Where(e => request.ContactIds.ToGuids().Contains(e.Id))
           .ToListAsync(cancellationToken);

        return contacts
            .SelectMany(contact => contact.Address!.Email,
                (contact, email) => new PRActivityReceiver
                {
                    Email = email,
                    ContactId = contact.Id,
                    PRActivityId = request.PRActivityId.ToGuidOnly(),
                    Code = StringExtension.GenerateRandomString(10)
                }
            );
    }
}
