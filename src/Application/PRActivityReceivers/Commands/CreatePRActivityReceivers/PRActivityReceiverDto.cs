namespace PRManagement.Application.PRActivityReceivers.Commands.CreatePRActivityReceivers;

public class PRActivityReceiverDto
{
    public Guid Id { get; set; }
    public Guid ContactId { get ; set; }
    public string Code { get; set; } = string.Empty;
}
