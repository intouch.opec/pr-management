using PRManagement.Domain.Entities;

namespace PRManagement.Application.PRActivityReceivers.Commands.CreatePRActivityReceivers;

public class MappingProfile
{
    public sealed class Mapping : Profile
    {

        public Mapping()
        {
            CreateMap<PRActivityReceiver, PRActivityReceiverDto>();
        }
    }
}
