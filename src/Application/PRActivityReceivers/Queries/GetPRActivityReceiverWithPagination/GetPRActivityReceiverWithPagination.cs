﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.Common.Mappings;
using PRManagement.Application.Common.Models;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.PRActivityReceivers.Queries.GetPRActivityReceiverWithPagination;

public record GetPRActivityReceiverWithPaginationQuery : IRequest<PaginatedList<PRActivityReceiverItem>>
{
    public string PRActivityId { get; set; } = string.Empty;
    public string? Email { get; set; }
    public string? FirstName { get; set; }
    public string? LastName { get; set; }
    public string? NickName { get; set; }
    public string? PhoneNumber { get; set; }
    public DateTimeOffset? SendedStart { get; set; }
    public DateTimeOffset? SendedEnd { get; set; }
    public DateTimeOffset? ReadStart { get; set; }
    public DateTimeOffset? ReadEnd { get; set; }
    public DateTimeOffset? OpenLinkStart { get; set; }
    public DateTimeOffset? OpenLinkEnd { get; set; }
    public int? PageNumber { get; init; } = 1;
    public int? PageSize { get; init; } = 10;
    public SortType? Sort { get; set; }
    public string? SortBy { get; set; }
}

public class GetPRActivityReceiverWithPaginationQueryHandler : IRequestHandler<GetPRActivityReceiverWithPaginationQuery, PaginatedList<PRActivityReceiverItem>>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public GetPRActivityReceiverWithPaginationQueryHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<PaginatedList<PRActivityReceiverItem>> Handle(GetPRActivityReceiverWithPaginationQuery request, CancellationToken cancellationToken)
    {
        return await Search(request)
            .ProjectTo<PRActivityReceiverItem>(_mapper.ConfigurationProvider)
            .OrderBy(request.SortBy ?? string.Empty, ascending: request.Sort)
            .PaginatedListAsync(request.PageNumber ?? 1, request.PageSize ?? 10, cancellationToken);
    }

    internal IQueryable Search(GetPRActivityReceiverWithPaginationQuery request)
    {
        return _context.PRActivityReceivers
            .AsNoTracking()
            .Include(e => e.Contact)
            .ThenInclude(e => e.Address)
            .Where(pr => pr.PRActivityId.Equals(request.PRActivityId.ToGuid()))
            .WhereLike(e => e.Email.ToLower(), request.Email)
            .WhereLike(e => e.Contact.FirstName.ToLower(), request.FirstName)
            .WhereLike(e => e.Contact.LastName.ToLower(), request.LastName)
            .WhereLike(e => e.Contact.NickName.ToLower(), request.NickName)
            .WhereLikeArray(e => e.Contact!.Address!.PhoneNumbers, request.PhoneNumber)
            .WhereRange(e => e.Sended, request.SendedStart, request.SendedEnd)
            .WhereRange(e => e.Read, request.ReadStart, request.ReadEnd)
            .WhereRange(e => e.OpenLink, request.OpenLinkStart, request.OpenLinkEnd);
    }
}
