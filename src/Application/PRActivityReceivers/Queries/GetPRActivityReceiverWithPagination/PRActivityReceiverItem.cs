namespace PRManagement.Application.PRActivityReceivers.Queries.GetPRActivityReceiverWithPagination;

public class PRActivityReceiverItem
{
    public Guid Id { get; set; }
    public string Email { get; set; } = string.Empty;
    public DateTimeOffset? Sended { get; set; }
    public DateTimeOffset? Read { get; set; }
    public DateTimeOffset? OpenLink { get; set; }
    public string FirstName { get; set; } = string.Empty;
    public string LastName { get; set; } = string.Empty;
    public string NickName { get; set; } = string.Empty;
    public IEnumerable<string> FromMedias { get; set; } = Enumerable.Empty<string>();
    public IEnumerable<string> PhoneNumbers { get; set; } = Enumerable.Empty<string>();
}
