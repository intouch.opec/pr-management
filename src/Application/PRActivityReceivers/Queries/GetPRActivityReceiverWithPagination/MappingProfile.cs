using PRManagement.Application.Common.Extensions;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.PRActivityReceivers.Queries.GetPRActivityReceiverWithPagination;

public class MappingProfile
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<PRActivityReceiver, PRActivityReceiverItem>()
                .ForMember(dest => dest.NickName, op => op.MapFrom(src => src.Contact.NickName))
                .ForMember(dest => dest.LastName, op => op.MapFrom(src => src.Contact.LastName))
                .ForMember(dest => dest.FirstName, op => op.MapFrom(src => src.Contact.FirstName))
                .ForMember(dest => dest.PhoneNumbers, op => op.MapFrom(src => src.Contact.Address!.PhoneNumbers ?? Enumerable.Empty<string>()));
        }
    }
}
