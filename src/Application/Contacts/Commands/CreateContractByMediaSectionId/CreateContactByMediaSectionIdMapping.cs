using PRManagement.Application.Common.Extensions;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.Contacts.Commands.CreateContactByMediaSectionId;
public class CreateContactByMediaSectionIdMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<CreateContactByMediaSectionIdCommand, IEnumerable<Contact>>()
                .ConvertUsing<CreateContactByMediaSectionIdToContactConverter>();

            CreateMap<CreateContactByMediaSectionIdCommand, IEnumerable<ContactMediaSection>>()
                .ConvertUsing<CreateContactByMediaSectionIdToContactMediaSectionConverter>();

            CreateMap<ContactItem, Address>()
              .ForMember(s => s.ProvinceId, op => op.MapFrom(o => string.IsNullOrWhiteSpace(o.ProvinceId) ? null : o.ProvinceId))
              .ForMember(s => s.DistrictId, op => op.MapFrom(o => string.IsNullOrWhiteSpace(o.DistrictId) ? null : o.DistrictId))
              .ForMember(s => s.SubDistrictId, op => op.MapFrom(o => string.IsNullOrWhiteSpace(o.SubDistrictId) ? null : o.SubDistrictId));

            CreateMap<ContactItem, Contact>()
              .ForMember(s => s.Address, op => op.MapFrom<ContactItemAddressResolver>())
              .ForMember(s => s.PercentageOfFullField, op => op.MapFrom(o => CalculatePercentageOfFullField(o)));

            CreateMap<Contact, ContactResponseItem>()
              .ForMember(s => s.Email, op => op.MapFrom(o => o.Address!.Email ?? Array.Empty<string>()))
              .ForMember(s => s.PhoneNumbers, op => op.MapFrom(o => o.Address!.PhoneNumbers ?? Array.Empty<string>()))
              .ForMember(s => s.Address, op => op.MapFrom(o => o.Address!.Text ?? string.Empty))
              .ForMember(s => s.ProvinceId, op => op.MapFrom(o => o.Address!.ProvinceId ?? string.Empty))
              .ForMember(s => s.DistrictId, op => op.MapFrom(o => o.Address!.DistrictId ?? string.Empty))
              .ForMember(s => s.SubDistrictId, op => op.MapFrom(o => o.Address!.SubDistrictId ?? string.Empty))
              .ForMember(s => s.PostCode, op => op.MapFrom(o => o.Address!.PostCode ?? string.Empty));

            CreateMap<IEnumerable<Contact>, IEnumerable<ContactResponseItem>>();

        }
    }

    public class ContactItemAddressResolver : IValueResolver<ContactItem, Contact, Address?>
    {
        private readonly IMapper _mapper;

        public ContactItemAddressResolver(IMapper mapper)
        {
            _mapper = mapper;
        }

        public Address? Resolve(ContactItem source, Contact destination, Address? member, ResolutionContext context)
        {
            if (SomePropertiesAddressHaveValue(source))
                return null;

            return _mapper.Map<Address>(source);
        }

        public static bool SomePropertiesAddressHaveValue(ContactItem source)
        {
            if (source is null)
                return false;

            return !source.Email.Any() &&
                 !source.PhoneNumbers.Any() &&
                 string.IsNullOrEmpty(source.Address) &&
                 string.IsNullOrEmpty(source.PostCode) &&
                 string.IsNullOrEmpty(source.ProvinceId) &&
                 string.IsNullOrEmpty(source.DistrictId) &&
                 string.IsNullOrEmpty(source.SubDistrictId);
        }
    }

    public class CreateContactByMediaSectionIdToContactConverter : ITypeConverter<CreateContactByMediaSectionIdCommand, IEnumerable<Contact>>
    {
        private readonly IMapper _mapper;
        public CreateContactByMediaSectionIdToContactConverter(IMapper mapper)
        {
            _mapper = mapper;
        }
        public IEnumerable<Contact> Convert(CreateContactByMediaSectionIdCommand source, IEnumerable<Contact> destination, ResolutionContext context)
        {
            return source.Contacts.Select((e) =>
            {
                var contact = _mapper.Map<Contact>(e);
                contact.ContactMediaSections = new List<ContactMediaSection>()
              {
          new ContactMediaSection()
          {
            MediaSectionId = new Guid(source.MediaSectionId),
          }
              };
                return contact;
            });
        }
    }

    public class CreateContactByMediaSectionIdToContactMediaSectionConverter : ITypeConverter<CreateContactByMediaSectionIdCommand, IEnumerable<ContactMediaSection>>
    {
        public IEnumerable<ContactMediaSection> Convert(CreateContactByMediaSectionIdCommand source, IEnumerable<ContactMediaSection> destination, ResolutionContext context)
         => source.ContactIds
          .Select((contactId) => new ContactMediaSection()
          {
              ContactId = contactId.ToGuidOnly(),
              MediaSectionId = new Guid(source.MediaSectionId),
          });
    }

    public static double CalculatePercentageOfFullField(ContactItem contact)
    {
        return ((contact.Email.Any() ? 1 : 0) +
            (contact.PhoneNumbers.Any() ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(contact.FirstName) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(contact.LastName) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(contact.LineId) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(contact.Position) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(contact.NickName) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(contact.PostCode) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(contact.ProvinceId) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(contact.DistrictId) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(contact.SubDistrictId) ? 1 : 0))
            / 11.0 * 100;
    }

}