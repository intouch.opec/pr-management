using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.Contacts.Commands.CreateContactByMediaSectionId;

public class CreateContactByMediaSectionIdCommand : IRequest<CreateContactByMediaSectionIdResponse>
{
    public string MediaSectionId { get; set; } = string.Empty;
    public IEnumerable<ContactItem> Contacts { get; set; } = Enumerable.Empty<ContactItem>();
    public IEnumerable<string> ContactIds { get; set; } = Enumerable.Empty<string>();
}

public class CreateContactByMediaSectionIdCommandHandler :
IRequestHandler<CreateContactByMediaSectionIdCommand, CreateContactByMediaSectionIdResponse>
{
    private readonly IMapper _mapper;
    private readonly IApplicationDbContext _context;

    public CreateContactByMediaSectionIdCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<CreateContactByMediaSectionIdResponse> Handle(CreateContactByMediaSectionIdCommand request,
    CancellationToken cancellationToken)
    {
        var contacts = _mapper.Map<IEnumerable<Contact>>(request);
        var contactMediaSections = _mapper.Map<IEnumerable<ContactMediaSection>>(request);
        await _context.Contacts.AddRangeAsync(contacts);
        await _context.ContactMediaSections.AddRangeAsync(contactMediaSections);
        await _context.SaveChangesAsync(cancellationToken);
        return new CreateContactByMediaSectionIdResponse
        {
            MediaSectionId = new Guid(request.MediaSectionId)
        };
    }
}
