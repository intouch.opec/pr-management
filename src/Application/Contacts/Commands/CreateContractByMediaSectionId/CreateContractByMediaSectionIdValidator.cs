using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Constants;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.Contacts.Commands.CreateContactByMediaSectionId;
public class CreateContactByMediaSectionIdValidator : AbstractValidator<CreateContactByMediaSectionIdCommand>
{
    private readonly IApplicationDbContext _context;

    public CreateContactByMediaSectionIdValidator(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(e => e.MediaSectionId)
          .StringIsGuid()
          .NotEmpty()
          .MustAsync(HasMediaSection)
          .WithMessage(ValidatorMessages.NotFound);

        RuleForEach(e => e.ContactIds)
          .StringIsGuid();

        RuleFor(e => e.ContactIds)
          .Must(BeRequiredContactIds)
          .WithMessage("The '{PropertyName}' is required.");

        RuleFor(e => e.Contacts)
          .Must(BeRequiredContacts)
          .WithMessage("The '{PropertyName}' is required.");

        RuleForEach(e => e.ContactIds)
          .SetValidator((command) =>
          {
              var contactIds = _context.ContactMediaSections
              .Where(cms => cms.MediaSectionId.Equals(command.MediaSectionId.ToGuidOnly()) &&
                command.ContactIds.Select(e => e.ToGuidOnly())
                  .Contains(cms.ContactId))
              .Select(e => e.ContactId.ToString());
              return new BeDuplicateContactIdsValidator(contactIds);
          })
          .SetValidator((command) =>
          {
              var contactIds = _context.Contacts
              .Where(c => command.ContactIds.Contains(c.Id.ToString()))
              .Select(e => e.Id.ToString());
              return new HasContactIdsValidator(contactIds);
          })
          .When(command => command.ContactIds.Any() && command.ContactIds.AreAllGuids());

        RuleForEach(e => e.Contacts)

          .ChildRules(e =>
            e.RuleFor(c => c.FirstName)
              .MustAsync(BeNameUniqueContact)
              .WithMessage($"The {nameof(ContactItem.FirstName)} and {nameof(ContactItem.LastName)} is duplicated")
              .NotEmpty())

          .ChildRules(e =>
            e.RuleFor(c => c.ProvinceId)
              .MustAsync(HasProvince)
              .WithMessage(ValidatorMessages.NotFound))

          .ChildRules(e =>
            e.RuleFor(c => c.DistrictId)
              .MustAsync(HasDistrict)
              .WithMessage(ValidatorMessages.NotFound))

          .ChildRules(e =>
            e.RuleFor(c => c.SubDistrictId)
              .MustAsync(HasSubDistrict)
              .WithMessage(ValidatorMessages.NotFound))

          .ChildRules(e =>
            e.RuleForEach(c => c.PhoneNumbers)
              .Matches(Patterns.PhoneNumber)
              .WithMessage(ValidatorMessages.InvalidArrayFormat))

          .ChildRules(e =>
            e.RuleFor(c => c.PhoneNumbers)
              .MustAsync(BePhoneNumbersUniqueContact)
              .WithMessage($"The {nameof(ContactResponseItem.PhoneNumbers)} is duplicated."))

          .ChildRules(e =>
            e.RuleForEach(c => c.Email)
              .EmailAddress()
              .WithMessage(ValidatorMessages.InvalidArrayFormat))

          .ChildRules(e =>
            e.RuleFor(c => c.AddressType)
              .MustEnum<ContactItem, AddressType>());
    }

    internal async Task<bool> HasProvince(string? ProvinceId, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(ProvinceId))
            return true;

        return await _context.Provinces
            .AnyAsync(p => p.Id.Equals(ProvinceId), cancellationToken);
    }

    internal async Task<bool> HasDistrict(string? districtId, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(districtId))
            return true;

        return await _context.Districts
          .AnyAsync(p => p.Id.Equals(districtId), cancellationToken);
    }

    internal async Task<bool> HasSubDistrict(string? subDistrictId, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(subDistrictId))
            return true;

        return await _context.SubDistricts
            .AnyAsync(p => p.Id.Equals(subDistrictId), cancellationToken);
    }

    internal async Task<bool> BePhoneNumbersUniqueContact(string[] phoneNumbers, CancellationToken cancellationToken)
    {
        if (!phoneNumbers.Any())
            return true;

        return !await _context.Contacts
          .Include(e => e.Address)
          .AnyAsync(e => e.Address!.PhoneNumbers.Any(phoneNumber => phoneNumbers.Contains(phoneNumber)), cancellationToken);
    }

    internal async Task<bool> BeNameUniqueContact(ContactItem source, string firstName, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(firstName) || string.IsNullOrWhiteSpace(source.LastName))
            return true;

        return !await _context.Contacts
          .WhereEqual(e => e.FirstName, firstName)
          .WhereEqual(e => e.LastName, source.LastName)
          .AnyAsync(cancellationToken);
    }

    internal async Task<bool> HasMediaSection(string? mediaSectionId, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(mediaSectionId))
            return true;

        return await _context.MediaSections
            .AnyAsync(p => p.Id.Equals(mediaSectionId.ToGuid()), cancellationToken);
    }

    internal bool BeRequiredContactIds(CreateContactByMediaSectionIdCommand request, IEnumerable<string> contactIds)
    {
        if (request.Contacts.Any())
            return true;
        return contactIds.Any();
    }

    internal bool BeRequiredContacts(CreateContactByMediaSectionIdCommand request, IEnumerable<ContactItem> contacts)
    {
        if (request.ContactIds.Any())
            return true;
        return contacts.Any();
    }
}

public class BeDuplicateContactIdsValidator : AbstractValidator<string>
{
    private readonly IEnumerable<string> _contactIds;

    public BeDuplicateContactIdsValidator(IEnumerable<string> contactIds)
    {
        _contactIds = contactIds;
        RuleFor(e => e)
          .Must(BeUniqueContact)
          .WithMessage($"The '{nameof(CreateContactByMediaSectionIdCommand.ContactIds)}' is duplicate.");
    }

    private bool BeUniqueContact(string mediaSectionId)
    {
        if (!Guid.TryParse(mediaSectionId, out _))
            return false;
        return !_contactIds.Contains(mediaSectionId);
    }
}

public class HasContactIdsValidator : AbstractValidator<string>
{
    private readonly IEnumerable<string> _contactIds;

    public HasContactIdsValidator(IEnumerable<string> contactIds)
    {
        _contactIds = contactIds;

        RuleFor(e => e)
         .Must(HasContact)
         .WithMessage(ValidatorMessages.NotFound);

    }

    private bool HasContact(string contactId)
    {
        if (!Guid.TryParse(contactId, out _))
            return false;
        return _contactIds.Contains(contactId);
    }
}