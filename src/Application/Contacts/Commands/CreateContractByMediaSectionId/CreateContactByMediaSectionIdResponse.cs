namespace PRManagement.Application.Contacts.Commands.CreateContactByMediaSectionId;

public class CreateContactByMediaSectionIdResponse
{
    public Guid MediaSectionId { get; set; }
}

public class ContactResponseItem : ContactItem
{
    public Guid Id { get; set; }
}

public class ContactItem
{
    public string FirstName { get; set; } = string.Empty;
    public string LastName { get; set; } = string.Empty;
    public string NickName { get; set; } = string.Empty;
    public string Address { get; set; } = string.Empty;
    public string[] Email { get; set; } = Array.Empty<string>();
    public string[] PhoneNumbers { get; set; } = Array.Empty<string>();
    public string Position { get; set; } = string.Empty;
    public string Remark { get; set; } = string.Empty;
    public string? ProvinceId { get; set; }
    public string? DistrictId { get; set; }
    public string? SubDistrictId { get; set; }
    public string PostCode { get; set; } = string.Empty;
    public string LineId { get; set; } = string.Empty;
    public string AddressType { get; set; } = string.Empty;
}
