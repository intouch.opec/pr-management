namespace PRManagement.Application.Contacts.Commands.UpdateContact;

public class UpdateContactResponse
{
    public string FirstName { get; set; } = string.Empty;
    public string LastName { get; set; } = string.Empty;
    public string NickName { get; set; } = string.Empty;
    public string[] Email { get; set; } = Array.Empty<string>();
    public string[] PhoneNumbers { get; set; } = Array.Empty<string>();
    public string Position { get; set; } = string.Empty;
    public string Remark { get; set; } = string.Empty;
    public string LineId { get; set; } = string.Empty;
    public string Address { get; set; } = string.Empty;
    public double PercentageOfFullField { get; set; } = 0;
    public DateTimeOffset Created { get; set; }
    public string? CreatedBy { get; set; }
    public DateTimeOffset LastModified { get; set; }
    public string? LastModifiedBy { get; set; }
    public bool IsDeleted { get; set; } = false;
    public DateTimeOffset? Deleted { get; set; }
}
