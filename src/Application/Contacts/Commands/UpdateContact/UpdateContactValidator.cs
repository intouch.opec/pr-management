using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Constants;

namespace PRManagement.Application.Contacts.Commands.UpdateContact;

public class UpdateContactValidator : AbstractValidator<UpdateContactCommand>
{
    private readonly IApplicationDbContext _context;

    public UpdateContactValidator(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(v => v.FirstName)
          .MustAsync(BeNameUniqueContact)
          .WithMessage($"The {nameof(UpdateContactCommand.FirstName)} and {nameof(UpdateContactCommand.LastName)} is duplicated")
          .NotEmpty();

        RuleFor(e => e.ProvinceId)
          .MustAsync(HasProvince)
          .WithMessage(ValidatorMessages.NotFound);

        RuleFor(e => e.DistrictId)
          .MustAsync(HasDistrict)
          .WithMessage(ValidatorMessages.NotFound);

        RuleFor(e => e.SubDistrictId)
          .MustAsync(HasSubDistrict)
          .WithMessage(ValidatorMessages.NotFound);

        RuleFor(e => e.PhoneNumbers)
          .MustAsync(BePhoneNumbersUniqueContact)
          .WithMessage($"The {nameof(UpdateContactCommand.PhoneNumbers)} is duplicated.");

        RuleForEach(v => v.Email)
          .EmailAddress()
          .WithMessage(ValidatorMessages.InvalidArrayFormat);

    }

    internal async Task<bool> HasProvince(string? ProvinceId, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(ProvinceId))
            return true;

        return await _context.Provinces
            .AnyAsync(p => p.Id.Equals(ProvinceId), cancellationToken);
    }

    internal async Task<bool> HasDistrict(string? districtId, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(districtId))
            return true;

        return await _context.Districts
          .AnyAsync(p => p.Id.Equals(districtId), cancellationToken);
    }

    internal async Task<bool> HasSubDistrict(string? subDistrictId, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(subDistrictId))
            return true;

        return await _context.SubDistricts
            .AnyAsync(p => p.Id.Equals(subDistrictId), cancellationToken);
    }

    internal async Task<bool> BePhoneNumbersUniqueContact(UpdateContactCommand source, string[] phoneNumbers, CancellationToken cancellationToken)
    {
        if (!phoneNumbers.Any())
            return true;

        return !await _context.Contacts
          .Include(e => e.Address)
          .AsNoTracking()
          .AnyAsync(e =>
            e.Id != source.Id &&
            e.Address!.PhoneNumbers.Any(phoneNumber => phoneNumbers.Contains(phoneNumber)),
            cancellationToken);
    }

    internal async Task<bool> BeNameUniqueContact(UpdateContactCommand source, string firstName, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(firstName) || string.IsNullOrWhiteSpace(source.LastName))
            return true;

        return !await _context.Contacts
          .WhereEqual(e => e.FirstName, firstName)
          .WhereEqual(e => e.LastName, source.LastName)
          .Where(e => e.Id != source.Id)
          .AnyAsync(cancellationToken);
    }

}
