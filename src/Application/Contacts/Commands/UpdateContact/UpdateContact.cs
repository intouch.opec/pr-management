using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.Contacts.Commands.UpdateContact;

public class UpdateContactCommand : IRequest<UpdateContactResponse>
{
    public Guid Id { get; set; }
    public string FirstName { get; set; } = string.Empty;
    public string LastName { get; set; } = string.Empty;
    public string NickName { get; set; } = string.Empty;
    public string Address { get; set; } = string.Empty;
    public string[] Email { get; set; } = Array.Empty<string>();
    public string[] PhoneNumbers { get; set; } = Array.Empty<string>();
    public string Position { get; set; } = string.Empty;
    public string Remark { get; set; } = string.Empty;
    public string ProvinceId { get; set; } = string.Empty;
    public string DistrictId { get; set; } = string.Empty;
    public string SubDistrictId { get; set; } = string.Empty;
    public string PostCode { get; set; } = string.Empty;
    public string LineId { get; set; } = string.Empty;
    public AddressType? AddressType { get; set; }
}

public class UpdateContactCommandHandler : IRequestHandler<UpdateContactCommand, UpdateContactResponse>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public UpdateContactCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<UpdateContactResponse> Handle(UpdateContactCommand request, CancellationToken cancellationToken)
    {
        var entity = await _context.Contacts
          .Include(e => e.Address)
          .FirstOrDefaultAsync(e => e.Id == request.Id, cancellationToken);

        Guard.Against.NotFound(request.Id, entity);

        _mapper.Map<UpdateContactCommand, Contact>(request, entity);

        await _context.SaveChangesAsync(cancellationToken);

        return _mapper.Map<UpdateContactResponse>(entity);
    }
}
