using PRManagement.Domain.Entities;

namespace PRManagement.Application.Contacts.Commands.UpdateContact;
public class UpdateContactMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<UpdateContactCommand, Contact>()
              .ForMember(s => s.Id, s => s.Ignore())
              .ForMember(s => s.Address, op => op.MapFrom<UpdateContactCommandAddressResolver>())
              .ForMember(s => s.PercentageOfFullField, op => op.MapFrom(o => CalculatePercentageOfFullField(o)));

            CreateMap<UpdateContactCommand, Address>()
              .ForMember(s => s.ProvinceId, op => op.MapFrom(o => string.IsNullOrWhiteSpace(o.ProvinceId) ? null : o.ProvinceId))
              .ForMember(s => s.DistrictId, op => op.MapFrom(o => string.IsNullOrWhiteSpace(o.DistrictId) ? null : o.DistrictId))
              .ForMember(s => s.SubDistrictId, op => op.MapFrom(o => string.IsNullOrWhiteSpace(o.SubDistrictId) ? null : o.SubDistrictId))
              .ForMember(s => s.Text, op => op.MapFrom(o => o.Address))
              .ForMember(s => s.Id, op => op.Ignore());

            CreateMap<Contact, UpdateContactResponse>()
              .ForMember(s => s.Address, op => op.MapFrom(o => o.Address!.Text))
              .ForMember(s => s.PhoneNumbers, op => op.MapFrom(o => o.Address!.PhoneNumbers))
              .ForMember(s => s.Email, op => op.MapFrom(o => o.Address!.Email));

        }

        public class UpdateContactCommandAddressResolver : IValueResolver<UpdateContactCommand, Contact, Address?>
        {
            private readonly IMapper _mapper;

            public UpdateContactCommandAddressResolver(IMapper mapper)
            {
                _mapper = mapper;
            }

            public Address? Resolve(UpdateContactCommand source, Contact destination, Address? member, ResolutionContext context)
            {
                if (SomePropertiesAddressHaveValue(source))
                    return null;
                return _mapper.Map<Address>(source);
            }

            public static bool SomePropertiesAddressHaveValue(UpdateContactCommand source)
            {
                if (source is null)
                    return false;

                return !source.Email.Any() &&
                     !source.PhoneNumbers.Any() &&
                     string.IsNullOrEmpty(source.Address) &&
                     string.IsNullOrEmpty(source.PostCode) &&
                     string.IsNullOrEmpty(source.ProvinceId) &&
                     string.IsNullOrEmpty(source.DistrictId) &&
                     string.IsNullOrEmpty(source.SubDistrictId);
            }
        }
    }

    public static double CalculatePercentageOfFullField(UpdateContactCommand contact)
    {
        return ((contact.Email.Any() ? 1 : 0) +
            (contact.PhoneNumbers.Any() ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(contact.FirstName) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(contact.LastName) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(contact.LineId) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(contact.Position) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(contact.NickName) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(contact.PostCode) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(contact.ProvinceId) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(contact.DistrictId) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(contact.SubDistrictId) ? 1 : 0))
            / 11.0 * 100;
    }
}