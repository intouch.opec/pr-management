using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;
using PRManagement.Domain.Exceptions;

namespace PRManagement.Application.Contacts.Commands.CreateContact;

public class CreateContactMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<CreateContactCommand, Contact>()
              .ForMember(s => s.AddressType, op => op.MapFrom(o => o.AddressType.ToEnum<AddressType>()))
              .ForMember(s => s.ContactType, op => op.MapFrom(o => string.IsNullOrWhiteSpace(o.ContactType) ? ContactType.CONTACT : o.ContactType.ToEnum<ContactType>()))
              .ForMember(s => s.Address, op => op.MapFrom<CreateContactCommandAddressResolver>())
              .ForMember(s => s.ContactMediaSections, op => op.MapFrom<CreateContactCommandMediaSectionsResolver>())
              .ForMember(s => s.ContactClients, op => op.MapFrom<CreateContactCommandContactClientsResolver>())
              .ForMember(s => s.PercentageOfFullField, op => op.MapFrom(o => CalculatePercentageOfFullField(o)));

            CreateMap<CreateContactCommand, Address>()
              .ForMember(s => s.ProvinceId, op => op.MapFrom(o => string.IsNullOrWhiteSpace(o.ProvinceId) ? null : o.ProvinceId))
              .ForMember(s => s.DistrictId, op => op.MapFrom(o => string.IsNullOrWhiteSpace(o.DistrictId) ? null : o.DistrictId))
              .ForMember(s => s.SubDistrictId, op => op.MapFrom(o => string.IsNullOrWhiteSpace(o.SubDistrictId) ? null : o.SubDistrictId))
              .ForMember(s => s.Text, op => op.MapFrom(o => o.Address));

            CreateMap<Contact, CreateContactResponse>()
              .ForMember(s => s.Email, op => op.MapFrom(o => o.Address!.Email ?? Array.Empty<string>()))
              .ForMember(s => s.PhoneNumbers, op => op.MapFrom(o => o.Address!.PhoneNumbers ?? Array.Empty<string>()))
              .ForMember(s => s.Address, op => op.MapFrom(o => o.Address!.Text ?? string.Empty))
              .ForMember(s => s.ProvinceId, op => op.MapFrom(o => o.Address!.ProvinceId ?? string.Empty))
              .ForMember(s => s.DistrictId, op => op.MapFrom(o => o.Address!.DistrictId ?? string.Empty))
              .ForMember(s => s.SubDistrictId, op => op.MapFrom(o => o.Address!.SubDistrictId ?? string.Empty))
              .ForMember(s => s.PostCode, op => op.MapFrom(o => o.Address!.PostCode ?? string.Empty));

        }
    }

    public class CreateContactCommandMediaSectionsResolver : IValueResolver<CreateContactCommand, Contact, ICollection<ContactMediaSection>?>
    {

        public ICollection<ContactMediaSection>? Resolve(CreateContactCommand source, Contact destination, ICollection<ContactMediaSection>? member, ResolutionContext context)
        {
            if (string.IsNullOrWhiteSpace(source.MediaSectionId))
                return null;
            var contactMediaSection = new ContactMediaSection
            {
                MediaSectionId = new Guid(source.MediaSectionId),
                ContactStatus = ContactStatus.FULL_TIME
            };
            return new List<ContactMediaSection>()
            {
                contactMediaSection
            };
        }
    }

    public class CreateContactCommandContactClientsResolver : IValueResolver<CreateContactCommand, Contact, ICollection<ContactClient>?>
    {

        public ICollection<ContactClient>? Resolve(CreateContactCommand source, Contact destination, ICollection<ContactClient>? member, ResolutionContext context)
        {
            if (string.IsNullOrWhiteSpace(source.ClientId))
                return null;
            var contactClient = new ContactClient
            {
                ClientId = new Guid(source.ClientId),
            };
            return new List<ContactClient>()
            {
                contactClient
            };
        }
    }

    public class CreateContactCommandAddressResolver : IValueResolver<CreateContactCommand, Contact, Address?>
    {
        private readonly IMapper _mapper;

        public CreateContactCommandAddressResolver(IMapper mapper)
        {
            _mapper = mapper;
        }

        public Address? Resolve(CreateContactCommand source, Contact destination, Address? member, ResolutionContext context)
        {
            if (SomePropertiesAddressHaveValue(source))
                return null;

            return _mapper.Map<Address>(source);
        }

        public static bool SomePropertiesAddressHaveValue(CreateContactCommand source)
        {
            if (source is null)
                return false;

            return !source.Email.Any() &&
                !source.PhoneNumbers.Any() &&
                string.IsNullOrEmpty(source.Address) &&
                string.IsNullOrEmpty(source.PostCode) &&
                string.IsNullOrEmpty(source.ProvinceId) &&
                string.IsNullOrEmpty(source.DistrictId) &&
                string.IsNullOrEmpty(source.SubDistrictId);
        }
    }

    public static double CalculatePercentageOfFullField(CreateContactCommand contact)
    {
        return ((contact.Email.Any() ? 1 : 0) +
            (contact.PhoneNumbers.Any() ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(contact.FirstName) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(contact.LastName) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(contact.LineId) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(contact.Position) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(contact.NickName) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(contact.PostCode) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(contact.ProvinceId) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(contact.DistrictId) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(contact.SubDistrictId) ? 1 : 0))
            / 11.0 * 100;
    }

}