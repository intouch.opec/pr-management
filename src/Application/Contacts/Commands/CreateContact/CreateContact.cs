using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.Contacts.Commands.CreateContact;
public record CreateContactCommand : IRequest<CreateContactResponse>
{
    public string FirstName { get; set; } = string.Empty;
    public string LastName { get; set; } = string.Empty;
    public string NickName { get; set; } = string.Empty;
    public string Address { get; set; } = string.Empty;
    public string[] Email { get; set; } = Array.Empty<string>();
    public string[] PhoneNumbers { get; set; } = Array.Empty<string>();
    public string Position { get; set; } = string.Empty;
    public string Remark { get; set; } = string.Empty;
    public string? ProvinceId { get; set; }
    public string DistrictId { get; set; } = string.Empty;
    public string SubDistrictId { get; set; } = string.Empty;
    public string PostCode { get; set; } = string.Empty;
    public string LineId { get; set; } = string.Empty;
    public string AddressType { get; set; } = string.Empty;
    public string ContactType { get; set; } = string.Empty;
    public string MediaSectionId { get; set; } = string.Empty;
    public string ClientId { get; set; } = string.Empty;
}

public class CreateContactCommandHandler : IRequestHandler<CreateContactCommand, CreateContactResponse>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public CreateContactCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<CreateContactResponse> Handle(CreateContactCommand request, CancellationToken cancellationToken)
    {
        var entity = _mapper.Map<Contact>(request);
        await _context.Contacts.AddAsync(entity);
        await _context.SaveChangesAsync(cancellationToken);
        return _mapper.Map<CreateContactResponse>(entity);
    }
}
