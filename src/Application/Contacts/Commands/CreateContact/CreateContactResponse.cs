namespace PRManagement.Application.Contacts.Commands.CreateContact;

public class CreateContactResponse
{
    public Guid Id { get; set; }
    public string FirstName { get; set; } = string.Empty;
    public string LastName { get; set; } = string.Empty;
    public string NickName { get; set; } = string.Empty;
    public string Address { get; set; } = string.Empty;
    public string[] Email { get; set; } = Array.Empty<string>();
    public string[] PhoneNumbers { get; set; } = Array.Empty<string>();
    public string? ProvinceId { get; set; }
    public string? DistrictId { get; set; } = string.Empty;
    public string? SubDistrictId { get; set; } = string.Empty;
    public string PostCode { get; set; } = string.Empty;
    public decimal PercentageOfFullField { get; set; } = 0;
}
