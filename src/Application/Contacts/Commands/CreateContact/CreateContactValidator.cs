using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Constants;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.Contacts.Commands.CreateContact;
public class CreateContactValidator : AbstractValidator<CreateContactCommand>
{
    private readonly IApplicationDbContext _context;

    public CreateContactValidator(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(v => v.FirstName)
            .MustAsync(BeNameUniqueContact)
            .WithMessage($"The {nameof(CreateContactCommand.FirstName)} and {nameof(CreateContactCommand.LastName)} is duplicated")
            .NotEmpty();

        RuleFor(e => e.ProvinceId)
            .MustAsync(HasProvince)
            .WithMessage(ValidatorMessages.NotFound);

        RuleFor(e => e.DistrictId)
            .MustAsync(HasDistrict)
            .WithMessage(ValidatorMessages.NotFound);

        RuleFor(e => e.SubDistrictId)
            .MustAsync(HasSubDistrict)
            .WithMessage(ValidatorMessages.NotFound);

        RuleForEach(e => e.PhoneNumbers)
            .Matches(Patterns.PhoneNumber)
            .WithMessage(ValidatorMessages.InvalidArrayFormat)
            .Must((e, x) =>
                e.PhoneNumbers.Count(p => p.Equals(x)) < 2)
            .WithMessage(ValidatorMessages.ShouldNotBeDuplicate);

        RuleFor(e => e.PhoneNumbers)
            .MustAsync(BePhoneNumbersUniqueContact)
            .WithMessage($"The {nameof(CreateContactCommand.PhoneNumbers)} is duplicated.");

        RuleForEach(v => v.Email)
            .EmailAddress()
            .WithMessage(ValidatorMessages.InvalidArrayFormat)
            .Must((e, x) =>
                e.Email.Count(p => p.Equals(x)) < 2)
            .WithMessage(ValidatorMessages.ShouldNotBeDuplicate);

        RuleFor(e => e.MediaSectionId)
            .StringIsGuid()
            .MustAsync(HasMediaSection)
            .WithMessage(ValidatorMessages.NotFound);

        RuleFor(e => e.ClientId)
            .StringIsGuid();

        RuleFor(e => e.AddressType)
            .NotEmpty()
            .MustEnum<CreateContactCommand, AddressType>();

        RuleFor(e => e.ContactType)
            .MustEnum<CreateContactCommand, ContactType>();
    }

    internal async Task<bool> HasProvince(string? ProvinceId, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(ProvinceId))
            return true;

        return await _context.Provinces
            .AnyAsync(p => p.Id.Equals(ProvinceId), cancellationToken);
    }

    internal async Task<bool> HasDistrict(string? districtId, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(districtId))
            return true;

        return await _context.Districts
          .AnyAsync(p => p.Id.Equals(districtId), cancellationToken);
    }

    internal async Task<bool> HasSubDistrict(string? subDistrictId, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(subDistrictId))
            return true;

        return await _context.SubDistricts
            .AnyAsync(p => p.Id.Equals(subDistrictId), cancellationToken);
    }

    internal async Task<bool> BePhoneNumbersUniqueContact(string[] phoneNumbers, CancellationToken cancellationToken)
    {
        if (!phoneNumbers.Any())
            return true;

        return !await _context.Contacts
          .Include(e => e.Address)
          .WhereEqual(e => e.Address!.PhoneNumbers, phoneNumbers)
          .AnyAsync(cancellationToken);
    }

    internal async Task<bool> BeNameUniqueContact(CreateContactCommand source, string firstName, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(firstName) || string.IsNullOrWhiteSpace(source.LastName))
            return true;

        return !await _context.Contacts
          .WhereEqual(e => e.FirstName, firstName)
          .WhereEqual(e => e.LastName, source.LastName)
          .AnyAsync(cancellationToken);
    }

    internal async Task<bool> HasMediaSection(string? mediaSectionId, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(mediaSectionId))
            return true;

        return await _context.MediaSections
            .AnyAsync(p => p.Id.Equals(mediaSectionId.ToGuid()), cancellationToken);
    }

}