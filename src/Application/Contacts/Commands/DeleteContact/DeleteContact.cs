using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;

namespace PRManagement.Application.Contacts.Commands.DeleteContact;
public class DeleteContactCommand : IRequest<DeleteContactResponse>
{
    public Guid Id { get; set; }
    public string Remark { get; set; } = string.Empty;
}

public class DeleteContactCommandHandler : IRequestHandler<DeleteContactCommand, DeleteContactResponse>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public DeleteContactCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<DeleteContactResponse> Handle(DeleteContactCommand request, CancellationToken cancellationToken)
    {
        var entity = await _context.Contacts
          .Where(e => e.Id == request.Id)
          .SingleOrDefaultAsync(cancellationToken);

        Guard.Against.NotFound(request.Id, entity);

        _context.Contacts.RemoveAndAddRemark(entity, request.Remark);

        await _context.SaveChangesAsync(cancellationToken);

        return _mapper.Map<DeleteContactResponse>(entity);
    }

}