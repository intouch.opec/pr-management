using PRManagement.Domain.Entities;

namespace PRManagement.Application.Contacts.Commands.DeleteContact;
public class DeleteContactMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<Contact, DeleteContactResponse>();
        }
    }
}