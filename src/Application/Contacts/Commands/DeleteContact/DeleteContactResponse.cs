namespace PRManagement.Application.Contacts.Commands.DeleteContact;

public class DeleteContactResponse
{
    public Guid Id { get; set; }
}
