using PRManagement.Domain.Entities;

namespace PRManagement.Application.Contacts.Queries.GetContactWithPagination;

public class GetContactWithPaginationMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<Contact, GetContactWithPaginationResponse>()
              .ForMember(s => s.Email, op => op.MapFrom(o => o.Address!.Email ?? Array.Empty<string>()))
              .ForMember(s => s.PhoneNumbers, op => op.MapFrom(o => o.Address!.PhoneNumbers ?? Array.Empty<string>()))
              .ForMember(s => s.PercentageOfFullField, op => op.MapFrom(o => o.PercentageOfFullField == 0 ?
                 CalculatePercentageOfFullField(o) :
                 o.PercentageOfFullField));
        }
    }

    public static double CalculatePercentageOfFullField(Contact contact)
    {
        return ((contact.Address?.Email.Any() == true ? 1 : 0) +
            (contact.Address?.PhoneNumbers.Any() == true ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(contact.FirstName) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(contact.LastName) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(contact.LineId) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(contact.Position) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(contact.NickName) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(contact.Address?.PostCode) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(contact.Address?.ProvinceId) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(contact.Address?.DistrictId) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(contact.Address?.SubDistrictId) ? 1 : 0))
            / 11.0 * 100;
    }

}
