﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.Common.Mappings;
using PRManagement.Application.Common.Models;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.Contacts.Queries.GetContactAndMediaWithPagination;

public record GetContactAndMediaWithPaginationQuery : IRequest<PaginatedList<GetContactAndMediaWithPaginationResponse>>
{
    public string? FirstName { get; set; } = string.Empty;
    public string? LastName { get; set; } = string.Empty;
    public string? NickName { get; set; } = string.Empty;
    public string? Email { get; set; } = string.Empty;
    public string? PhoneNumber { get; set; } = string.Empty;
    public string? Position { get; set; } = string.Empty;
    public string? Search { get; set; } = string.Empty;
    public Guid? SectionId { get; set; }
    public Guid? SubSectionId { get; set; }
    public Guid? MediaId { get; set; }
    public bool? IsFullField { get; set; }

    public int? PageNumber { get; init; } = 1;
    public int? PageSize { get; init; } = 10;
    public SortType? Sort { get; set; } = SortType.DESC;
    public string? SortBy { get; set; } = nameof(GetContactAndMediaWithPaginationResponse.LastModified);
}

public class GetContactAndMediaWithPaginationQueryHandler : IRequestHandler<GetContactAndMediaWithPaginationQuery, PaginatedList<GetContactAndMediaWithPaginationResponse>>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public GetContactAndMediaWithPaginationQueryHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<PaginatedList<GetContactAndMediaWithPaginationResponse>> Handle(GetContactAndMediaWithPaginationQuery request, CancellationToken cancellationToken)
    {
        return await Search(request)
            .ProjectTo<GetContactAndMediaWithPaginationResponse>(_mapper.ConfigurationProvider)
            .OrderBy(request.SortBy ?? nameof(GetContactAndMediaWithPaginationResponse.LastModified), ascending: request.Sort ?? SortType.DESC)
            .PaginatedListAsync(request.PageNumber ?? 1, request.PageSize ?? 10, cancellationToken);
    }

    internal IQueryable Search(GetContactAndMediaWithPaginationQuery request)
    {
        var query = _context.Contacts
            .Include(c => c.Address)
            .Include(a => a.Address!.Province)
            .Include(a => a.Address!.District)
            .Include(a => a.Address!.SubDistrict)
            .Include(a => a.ContactMediaSections!.Where(cms => cms != null))
            .ThenInclude(cms => cms.MediaSection)
            .ThenInclude(ms => ms!.Media)
            .AsNoTracking()
            .WhereLike(e => e.FirstName.ToLower(), request.FirstName)
            .WhereLike(e => e.LastName.ToLower(), request.LastName)
            .WhereLike(e => e.NickName.ToLower(), request.NickName)
            .WhereLike(e => e.Position.ToLower(), request.Position)
            .WhereLikeArray(e => e.Address!.Email.Select(e => e.ToLower()).ToArray(), request.Email)
            .WhereLikeArray(e => e.Address!.PhoneNumbers.Select(e => e.ToLower()).ToArray(), request.PhoneNumber)
            .WhereEqualArray(e => e.ContactMediaSections!,
                            item => item.MediaSection!.SectionId,
                            request.SectionId)
            .WhereEqualArray(e => e.ContactMediaSections!,
                            item => item.MediaSection!.SubSectionId,
                            request.SubSectionId)
            .WhereEqualArray(e => e.ContactMediaSections!,
                            item => item.MediaSection!.MediaId,
                            request.MediaId)
            .SearchOrMultiple(request.Search,
                            e => e.FirstName,
                            e => e.LastName,
                            e => e.NickName,
                            e => e.Position,
                            e => e.Address!.Text,
                            e => e.Address!.PhoneNumbers,
                            e => e.Address!.Email,
                            e => e.ContactMediaSections!.Select(e => e.MediaSection!.Section.Name),
                            e => e.ContactMediaSections!.Select(e => e.MediaSection!.SubSection.Name)
            );

        if (request.IsFullField != null)
            query = query.Where(e => (bool)request.IsFullField ?
                e.PercentageOfFullField.Equals(100) :
                e.PercentageOfFullField != 100);

        return query;
    }

}
