using PRManagement.Application.Contacts.Queries.GetContactWithPagination;

namespace PRManagement.Application.Contacts.Queries.GetContactAndMediaWithPagination;

public class GetContactAndMediaWithPaginationResponse: GetContactWithPaginationResponse
{
    public IEnumerable<string> MediaNames { get; set; } = Enumerable.Empty<string>();
}
