using PRManagement.Domain.Entities;

namespace PRManagement.Application.Contacts.Queries.GetContactById;
public class GetContactByIdMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<Contact, GetContactByIdResponse>()
              .ForMember(s => s.LineId, op => op.MapFrom(o => o.LineId))
              .ForMember(s => s.Address, op => op.MapFrom(o => o.Address!.Text ?? string.Empty))
              .ForMember(s => s.Email, op => op.MapFrom(o => o.Address!.Email ?? Array.Empty<string>()))
              .ForMember(s => s.PhoneNumbers, op => op.MapFrom(o => o.Address!.PhoneNumbers ?? Array.Empty<string>()))
              .ForMember(s => s.ProvinceId, op => op.MapFrom(o => o.Address!.ProvinceId ?? string.Empty))
              .ForMember(s => s.DistrictId, op => op.MapFrom(o => o.Address!.DistrictId ?? string.Empty))
              .ForMember(s => s.SubDistrictId, op => op.MapFrom(o => o.Address!.SubDistrictId ?? string.Empty))
              .ForMember(s => s.PostCode, op => op.MapFrom(o => o.Address!.PostCode ?? string.Empty))
              .ForMember(s => s.ProvinceEN, op => op.MapFrom(o => o.Address!.Province!.NameEN ?? string.Empty))
              .ForMember(s => s.ProvinceTH, op => op.MapFrom(o => o.Address!.Province!.NameTH ?? string.Empty))
              .ForMember(s => s.DistrictEN, op => op.MapFrom(o => o.Address!.District!.NameEN ?? string.Empty))
              .ForMember(s => s.DistrictTH, op => op.MapFrom(o => o.Address!.District!.NameTH ?? string.Empty))
              .ForMember(s => s.SubDistrictEN, op => op.MapFrom(o => o.Address!.SubDistrict!.NameEN ?? string.Empty))
              .ForMember(s => s.SubDistrictTH, op => op.MapFrom(o => o.Address!.SubDistrict!.NameTH ?? string.Empty))
              .ForMember(e => e.Medias, op => op.MapFrom(o =>
                    o.ContactMediaSections!.Where(ms => ms != null)
                      .GroupBy(cms => cms.MediaSection!.Media)
                      .Select(ms => new ContactMediaSectionDto
                      {
                          Id = ms.Key.Id,
                          Name = ms.Key.Name,
                          Email = ms.Key.Address!.Email ?? Array.Empty<string>(),
                          PhoneNumbers = ms.Key.Address.PhoneNumbers ?? Array.Empty<string>(),
                          ProvinceId = ms.Key.Address.ProvinceId ?? string.Empty,
                          DistrictId = ms.Key.Address.DistrictId ?? string.Empty,
                          SubDistrictId = ms.Key.Address.SubDistrictId ?? string.Empty,
                          PostCode = ms.Key.Address.PostCode ?? string.Empty,
                          ProvinceEN = ms.Key.Address!.Province!.NameEN ?? string.Empty,
                          ProvinceTH = ms.Key.Address!.Province!.NameTH ?? string.Empty,
                          DistrictEN = ms.Key.Address!.District!.NameEN ?? string.Empty,
                          DistrictTH = ms.Key.Address!.District!.NameTH ?? string.Empty,
                          SubDistrictEN = ms.Key.Address!.SubDistrict!.NameEN ?? string.Empty,
                          SubDistrictTH = ms.Key.Address!.SubDistrict!.NameTH ?? string.Empty,
                          MediaSections = ms.Select(cms => new MediaSectionItem()
                          {
                              Id = cms.MediaSection!.Id,
                              SectionId = cms.MediaSection!.SectionId,
                              TypeOfMedia = cms.MediaSection!.TypeOfMedia,
                              SubSectionId = cms.MediaSection!.SubSectionId,
                              SectionName = cms.MediaSection!.Section.Name,
                              SubSectionName = cms.MediaSection!.SubSection.Name,
                          })
                        .OrderBy(e => e.SectionName)
                    })
                ))
            .ForMember(s => s.PercentageOfFullField, op => op.MapFrom(o => o.PercentageOfFullField == 0 ?
                CalculatePercentageOfFullField(o) :
                o.PercentageOfFullField));

        }
    }

    public static double CalculatePercentageOfFullField(Contact contact)
    {
        return ((contact.Address?.Email.Any() == true ? 1 : 0) +
            (contact.Address?.PhoneNumbers.Any() == true ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(contact.FirstName) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(contact.LastName) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(contact.LineId) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(contact.Position) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(contact.NickName) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(contact.Address?.PostCode) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(contact.Address?.ProvinceId) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(contact.Address?.DistrictId) ? 1 : 0) +
            (!string.IsNullOrWhiteSpace(contact.Address?.SubDistrictId) ? 1 : 0))
            / 11.0 * 100;
    }
}
