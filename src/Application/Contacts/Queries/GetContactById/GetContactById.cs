using PRManagement.Application.Common.Interfaces;

namespace PRManagement.Application.Contacts.Queries.GetContactById;

public record GetContactByIdQuery(Guid Id) : IRequest<GetContactByIdResponse>;

public class GetContactByIdQueryHandler : IRequestHandler<GetContactByIdQuery, GetContactByIdResponse>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public GetContactByIdQueryHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<GetContactByIdResponse> Handle(GetContactByIdQuery request, CancellationToken cancellationToken)
    {
        var entity = await _context.Contacts
            .Include(c => c.Address)
            .Include(a => a.Address!.Province)
            .Include(a => a.Address!.District)
            .Include(a => a.Address!.SubDistrict)
            .Include(c => c.ContactMediaSections!.Where(cm => cm != null))
            .ThenInclude(e => e.MediaSection)
            .ThenInclude(e => e!.Media)
            .ThenInclude(e => e!.Address)
            .ThenInclude(e => e!.Province)
            .Include(e => e.ContactMediaSections!.Where(cm => cm != null))
            .ThenInclude(e => e.MediaSection)
            .ThenInclude(e => e!.Section)
            .Include(e => e.ContactMediaSections!.Where(cm => cm != null))
            .ThenInclude(e => e.MediaSection)
            .ThenInclude(e => e!.SubSection)
            .AsNoTracking()
            .ProjectTo<GetContactByIdResponse>(_mapper.ConfigurationProvider)
            .SingleOrDefaultAsync(e => e.Id.Equals(request.Id));

        Guard.Against.NotFound(request.Id, entity);

        return entity;
    }
}