using PRManagement.Domain.Enums;

namespace PRManagement.Application.Contacts.Queries.GetContactById;

public class GetContactByIdResponse
{
    public Guid Id { get; set; }
    public string FirstName { get; set; } = string.Empty;
    public string LastName { get; set; } = string.Empty;
    public string NickName { get; set; } = string.Empty;
    public string Position { get; set; } = string.Empty;
    public string LineId { get; set; } = string.Empty;
    public string Address { get; set; } = string.Empty;
    public string[] Email { get; set; } = Array.Empty<string>();
    public string[] PhoneNumbers { get; set; } = Array.Empty<string>();
    public string? ProvinceId { get; set; }
    public string? ProvinceEN { get; set; }
    public string? ProvinceTH { get; set; }
    public string DistrictId { get; set; } = string.Empty;
    public string DistrictEN { get; set; } = string.Empty;
    public string DistrictTH { get; set; } = string.Empty;
    public string SubDistrictId { get; set; } = string.Empty;
    public string SubDistrictEN { get; set; } = string.Empty;
    public string SubDistrictTH { get; set; } = string.Empty;
    public string PostCode { get; set; } = string.Empty;
    public double PercentageOfFullField { get; set; } = 0;
    public AddressType? AddressType { get; set; }
    public IEnumerable<ContactMediaSectionDto> Medias { get; set; } = Enumerable.Empty<ContactMediaSectionDto>();
    public DateTimeOffset Created { get; set; }
    public string? CreatedBy { get; set; }
    public DateTimeOffset LastModified { get; set; }
    public string? LastModifiedBy { get; set; }
    public bool IsDeleted { get; set; } = false;
    public DateTimeOffset? Deleted { get; set; }
}

public class ContactMediaSectionDto
{
    public Guid Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public string Address { get; set; } = string.Empty;
    public string[] Email { get; set; } = Array.Empty<string>();
    public string[] PhoneNumbers { get; set; } = Array.Empty<string>();
    public string? ProvinceId { get; set; }
    public string? ProvinceEN { get; set; }
    public string? ProvinceTH { get; set; }
    public string DistrictId { get; set; } = string.Empty;
    public string DistrictEN { get; set; } = string.Empty;
    public string DistrictTH { get; set; } = string.Empty;
    public string SubDistrictId { get; set; } = string.Empty;
    public string SubDistrictEN { get; set; } = string.Empty;
    public string SubDistrictTH { get; set; } = string.Empty;
    public string PostCode { get; set; } = string.Empty;
    public IEnumerable<MediaSectionItem> MediaSections { get; set; } = Enumerable.Empty<MediaSectionItem>();
}

public class MediaSectionItem
{
    public Guid Id { get; set; }
    public Guid SectionId { get; set; }
    public Guid SubSectionId { get; set; }
    public string SectionName { get; set; } = string.Empty;
    public string SubSectionName { get; set; } = string.Empty;
    public TypeOfMedia TypeOfMedia { get; set; }
}
