using PRManagement.Domain.Entities;

namespace PRManagement.Application.Contacts.Queries.GetContactWithPaginationByMediaSectionId;
public class GetContactWithPaginationBySectionIdMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<ContactMediaSection, GetContactWithPaginationByMediaSectionIdResponse>()
              .ForMember(s => s.Id, op => op.MapFrom(o => o.ContactId))
              .ForMember(s => s.Email, op => op.MapFrom(o => o.Contact!.Address!.Email ?? Array.Empty<string>()))
              .ForMember(s => s.PhoneNumbers, op => op.MapFrom(o => o.Contact!.Address!.PhoneNumbers ?? Array.Empty<string>()))
              .ForMember(s => s.FirstName, op => op.MapFrom(o => o.Contact!.FirstName))
              .ForMember(s => s.LastName, op => op.MapFrom(o => o.Contact!.LastName))
              .ForMember(s => s.LineId, op => op.MapFrom(o => o.Contact!.LineId))
              .ForMember(s => s.NickName, op => op.MapFrom(o => o.Contact!.NickName))
              .ForMember(s => s.Position, op => op.MapFrom(o => o.Contact!.Position));

        }
    }
}
