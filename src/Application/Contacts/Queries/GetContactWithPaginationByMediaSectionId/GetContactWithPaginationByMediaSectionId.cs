using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.Common.Mappings;
using PRManagement.Application.Common.Models;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.Contacts.Queries.GetContactWithPaginationByMediaSectionId;

public class GetContactWithPaginationByMediaSectionIdQuery : IRequest<PaginatedList<GetContactWithPaginationByMediaSectionIdResponse>>
{
    public string? FirstName { get; set; } = string.Empty;
    public string? LastName { get; set; } = string.Empty;
    public string? NickName { get; set; } = string.Empty;
    public string? Email { get; set; } = string.Empty;
    public string? PhoneNumber { get; set; } = string.Empty;
    public string? Position { get; set; } = string.Empty;
    public string? LineId { get; set; } = string.Empty;
    public Guid? MediaSectionId { get; set; }
    public ContactType? ContactType { get; set; }

    public int? PageNumber { get; init; } = 1;
    public int? PageSize { get; init; } = 10;
    public SortType? Sort { get; set; }
    public string? SortBy { get; set; }
}

public class GetContactWithPaginationByMediaSectionIdQueryHandler : IRequestHandler<GetContactWithPaginationByMediaSectionIdQuery, PaginatedList<GetContactWithPaginationByMediaSectionIdResponse>>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public GetContactWithPaginationByMediaSectionIdQueryHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<PaginatedList<GetContactWithPaginationByMediaSectionIdResponse>> Handle(GetContactWithPaginationByMediaSectionIdQuery request, CancellationToken cancellationToken)
    {
        return await Search(request)
          .ProjectTo<GetContactWithPaginationByMediaSectionIdResponse>(_mapper.ConfigurationProvider)
          .OrderBy(request.SortBy ?? nameof(GetContactWithPaginationByMediaSectionIdResponse.LastModified), ascending: request.Sort ?? SortType.DESC)
          .PaginatedListAsync(request.PageNumber ?? 1, request.PageSize ?? 10, cancellationToken);
    }

    internal IQueryable Search(GetContactWithPaginationByMediaSectionIdQuery request)
      => _context.ContactMediaSections
        .Include(c => c.Contact)
        .Include(c => c.Contact!.Address)
        .Include(a => a.Contact!.Address!.Province)
        .Include(a => a.Contact!.Address!.District)
        .Include(a => a.Contact!.Address!.SubDistrict)
        .AsNoTracking()
        .Where(c => c.MediaSectionId.Equals(request.MediaSectionId))
        .Where(c => c.Contact != null)
        .WhereEqual(c => c.Contact!.ContactType, request.ContactType)
        .WhereLikeArray(e => e.Contact!.Address!.PhoneNumbers, request.PhoneNumber)
        .WhereLikeArray(e => e.Contact!.Address!.Email, request.Email)
        .WhereLike(e => e.Contact!.FirstName.ToLower(), request.FirstName)
        .WhereLike(e => e.Contact!.LastName.ToLower(), request.LastName)
        .WhereLike(e => e.Contact!.NickName.ToLower(), request.NickName)
        .WhereLike(e => e.Contact!.LineId.ToLower(), request.LineId)
        .WhereLike(e => e.Contact!.Position.ToLower(), request.Position);
}
