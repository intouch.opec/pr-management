using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.SubSections.Queries.GetSubSectionOptions;
public record GetSubSectionOptionsQuery : IRequest<IEnumerable<GetSubSectionOptionsResponse>>
{
    public Guid? SectionId { get; set; }
    public string? Name { get; set; }
    public string? MediaId { get; set; } = string.Empty;
}

public class GetSubSectionOptionsQueryHandler : IRequestHandler<GetSubSectionOptionsQuery, IEnumerable<GetSubSectionOptionsResponse>>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public GetSubSectionOptionsQueryHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<IEnumerable<GetSubSectionOptionsResponse>> Handle(GetSubSectionOptionsQuery request, CancellationToken cancellationToken)
      => await Search(request)
        .OrderBy(nameof(GetSubSectionOptionsResponse.Name), ascending: SortType.ASC)
        .ProjectTo<GetSubSectionOptionsResponse>(_mapper.ConfigurationProvider)
        .ToListAsync(cancellationToken);

    internal IQueryable<SubSection> Search(GetSubSectionOptionsQuery request)
    {
        var subSections = _context.SubSections
            .Include(ss => ss.Section)
            .AsQueryable();

        if (!string.IsNullOrWhiteSpace(request.MediaId))
        {
            subSections = subSections
              .Include(e => e.MediaSections!.Where(ms => ms != null))
              .Where(e => e.MediaSections!
                    .Any(ms => ms.MediaId == request.MediaId.ToGuid()));
        }
        return subSections
            .AsNoTracking()
            .WhereLike(e => e.Name.ToLower(), request.Name?.ToLower())
            .WhereEqual(e => e.SectionId, request.SectionId);
    }

}