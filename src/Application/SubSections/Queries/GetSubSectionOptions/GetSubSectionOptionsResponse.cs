using PRManagement.Application.Common.Models;

namespace PRManagement.Application.SubSections.Queries.GetSubSectionOptions;

public class GetSubSectionOptionsResponse : OptionItemBase
{
    public Guid SectionId { get; set; }
    public string SectionName { get; set; } = string.Empty;
}
