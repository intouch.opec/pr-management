using PRManagement.Domain.Entities;

namespace PRManagement.Application.SubSections.Queries.GetSubSectionOptions;
public class GetSectionOptionMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<SubSection, GetSubSectionOptionsResponse>()
                .ForMember(e => e.SectionName, op => op.MapFrom(o => o.Section!.Name ?? string.Empty));
        }
    }
}