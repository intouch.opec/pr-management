﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.Common.Mappings;
using PRManagement.Application.Common.Models;
using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.SubSections.Queries.GetSubSectionWithPagination;

public record GetSubSectionWithPaginationQuery : IRequest<PaginatedList<GetSubSectionItem>>
{
    public Guid? SectionId { get; set; }
    public string? Name { get; set; }
    public int? PageNumber { get; init; } = 1;
    public int? PageSize { get; init; } = 10;
    public SortType? Sort { get; set; } = SortType.DESC;
    public string? SortBy { get; set; } = nameof(GetSubSectionItem.LastModified);
}

public class GetSubSectionWithPaginationQueryHandler : IRequestHandler<GetSubSectionWithPaginationQuery, PaginatedList<GetSubSectionItem>>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public GetSubSectionWithPaginationQueryHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<PaginatedList<GetSubSectionItem>> Handle(GetSubSectionWithPaginationQuery request, CancellationToken cancellationToken)
    {
        return await Search(request)
            .ProjectTo<GetSubSectionItem>(_mapper.ConfigurationProvider)
            .OrderBy(request.SortBy ?? nameof(GetSubSectionItem.Name), ascending: request.Sort ?? SortType.ASC)
            .PaginatedListAsync(request.PageNumber ?? 1, request.PageSize ?? 10, cancellationToken);
    }

    internal IQueryable<SubSection> Search(GetSubSectionWithPaginationQuery request)
    {
        return _context.SubSections
            .Include(e => e.Section)
            .AsNoTracking()
            .WhereLike(e => e.Name.ToLower(), request.Name?.ToLower())
            .WhereEqual(e => e.SectionId, request.SectionId);

    }
}
