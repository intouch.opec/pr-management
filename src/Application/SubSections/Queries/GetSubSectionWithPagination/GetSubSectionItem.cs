namespace PRManagement.Application.SubSections.Queries.GetSubSectionWithPagination;

public class GetSubSectionItem
{
    public Guid Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public Guid SectionId { get; set; }
    public string SectionName { get; set; } = string.Empty;
    public DateTimeOffset Created { get; set; }
    public string? CreatedBy { get; set; }
    public DateTimeOffset LastModified { get; set; }
    public string? LastModifiedBy { get; set; }
}