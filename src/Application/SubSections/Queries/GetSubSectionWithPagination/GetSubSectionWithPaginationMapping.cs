using PRManagement.Domain.Entities;

namespace PRManagement.Application.SubSections.Queries.GetSubSectionWithPagination;

public class GetSubSectionWithPaginationMapping
{
	public sealed class Mapping : Profile
	{
		public Mapping()
		{
			CreateMap<SubSection, GetSubSectionItem>()
			.ForMember(e => e.SectionName, op => op.MapFrom(o => o.Section!.Name ?? string.Empty)); ;
		}
	}
}
