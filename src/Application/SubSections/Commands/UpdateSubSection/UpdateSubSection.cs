﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.Common.Models;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.SubSections.Commands.UpdateSubSection;

public record UpdateSubSectionCommand : IRequest<SubSectionDto>
{
    public string Id { get; set; } = string.Empty;
    public string Name { get; set; } = string.Empty;
    public string SectionId { get; set; } = string.Empty;
}

public class UpdateSubSectionCommandHandler : IRequestHandler<UpdateSubSectionCommand, SubSectionDto>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public UpdateSubSectionCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<SubSectionDto> Handle(UpdateSubSectionCommand request, CancellationToken cancellationToken)
    {
        var entity = await _context.SubSections
            .AsTracking()
            .FirstOrDefaultAndRemoveCacheAsync(e => e.Id == request.Id.ToGuidOnly(), cancellationToken);

        Guard.Against.NotFound(request.Id, entity);

        _mapper.Map<UpdateSubSectionCommand, SubSection>(request, entity);
        await _context.SaveChangesAsync(cancellationToken);

        return _mapper.Map<SubSectionDto>(entity);
    }
}
