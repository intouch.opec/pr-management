using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Constants;

namespace PRManagement.Application.SubSections.Commands.UpdateSubSection;

public class UpdateSubSectionCommandValidator : AbstractValidator<UpdateSubSectionCommand>
{
    private readonly IApplicationDbContext _context;

    public UpdateSubSectionCommandValidator(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(v => v.Name)
            .MaximumLength(15)
            .MustAsync(BeUniqueSubSectionName)
            .WithMessage(ValidatorMessages.ShouldNotBeDuplicate)
            .NotEmpty();

        RuleFor(v => v.SectionId)
            .NotEmpty()
            .StringIsGuid()
            .MustAsync(HasSection)
            .WithMessage(ValidatorMessages.NotFound);
    }

    internal async Task<bool> BeUniqueSubSectionName(UpdateSubSectionCommand command, string name, CancellationToken cancellationToken)
    {
        if (
            string.IsNullOrWhiteSpace(name) ||
            !command.SectionId.IsValidGuid()
        )
            return true;

        return !await _context.SubSections
            .AsNoTracking()
            .AnyAsync(p =>
                p.Id.Equals(command.Id.ToGuidOnly()) &&
                p.Name.ToLower().Equals(name.ToLower()) &&
                p.SectionId.Equals(command.SectionId.ToGuidOnly()),
                cancellationToken);
    }

    internal async Task<bool> HasSection(string id, CancellationToken cancellationToken)
    {
        if (
            string.IsNullOrWhiteSpace(id) ||
            !id.IsValidGuid()
        )
            return true;

        return await _context.Sections
            .AsNoTracking()
            .AnyAsync(
                p => p.Id.Equals(id.ToGuidOnly()),
                cancellationToken);
    }
}
