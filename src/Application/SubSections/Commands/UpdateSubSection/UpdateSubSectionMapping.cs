using PRManagement.Application.Common.Extensions;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.SubSections.Commands.UpdateSubSection;

public class UpdateSubSectionMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<UpdateSubSectionCommand, SubSection>()
                .ForMember(e => e.SectionId, op => op.MapFrom(o => o.SectionId.ToGuidOnly()));
        }
    }
}
