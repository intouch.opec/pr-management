using PRManagement.Application.Common.Extensions;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.SubSections.Commands.CreateSubSection;

public class CreateSubSectionMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<CreateSubSectionCommand, SubSection>()
                .ForMember(e => e.SectionId, op => op.MapFrom(o => o.SectionId.ToGuidOnly()));
        }
    }
}
