﻿using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.Common.Models;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.SubSections.Commands.CreateSubSection;

public record CreateSubSectionCommand : IRequest<SubSectionDto>
{
    public string Name { get; set; } = string.Empty;
    public string SectionId { get; set; } = string.Empty;
}

public class CreateSubSectionCommandHandler : IRequestHandler<CreateSubSectionCommand, SubSectionDto>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public CreateSubSectionCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<SubSectionDto> Handle(CreateSubSectionCommand request, CancellationToken cancellationToken)
    {
        var entity = _mapper.Map<SubSection>(request);

        await _context.SubSections.AddAsync(entity);

        await _context.SaveChangesAsync(cancellationToken);

        return _mapper.Map<SubSectionDto>(entity);
    }
}
