
using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.RegisterForms.Commands.CreateRegisterForm;
using PRManagement.Domain.Constants;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.RegisterForms.Commands.UpdateRegisterForm;

public class UpdateRegisterFormCommandValidator : AbstractValidator<UpdateRegisterFormCommand>
{
    private readonly IApplicationDbContext _context;

    public UpdateRegisterFormCommandValidator(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(v => v.Name)
            .MaximumLength(55)
            .MustAsync(BeUniqueRegisterFormName)
            .WithMessage(ValidatorMessages.ShouldNotBeDuplicate)
            .NotEmpty();

        RuleForEach(e => e.RegisterFields)
            .ChildRules(e =>
                e.RuleFor(c => c.InputType)
                    .MustEnum<RegisterFieldItem, InputInformation>()
                    .NotEmpty())
            .DependentRules(() =>
            {
                RuleForEach(command => command.RegisterFields)
                    .SetValidator((command, registerFields) =>
                    {
                        return new CreateRegisterFieldsValidator(command.RegisterFields.Select(rf => rf.InputType));
                    });
            });
    }

    internal async Task<bool> BeUniqueRegisterFormName(UpdateRegisterFormCommand command, string name, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(name) ||
            !command.Id.IsValidGuid())
            return true;

        return !await _context.RegisterForms
            .AsNoTracking()
            .AnyAsync(p =>
                !p.Id.Equals(command.Id.ToGuidOnly()) &&
                p.Name.ToLower().Equals(name.ToLower()),
                cancellationToken);
    }
}
