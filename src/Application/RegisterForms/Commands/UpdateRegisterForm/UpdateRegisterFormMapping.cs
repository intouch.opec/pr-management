using PRManagement.Application.RegisterForms.Commands.CreateRegisterForm;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.RegisterForms.Commands.UpdateRegisterForm;

public record UpdateRegisterFormMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<UpdateRegisterFormCommand, RegisterForm>();
            CreateMap<RegisterForm, RegisterFormDto>();
        }
    }
}