﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.RegisterForms.Commands.CreateRegisterForm;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.RegisterForms.Commands.UpdateRegisterForm;

public record UpdateRegisterFormCommand : IRequest<RegisterFormDto>
{
    public string Id { get; set; } = string.Empty;
    public string Name { get; set; } = string.Empty;
    public string Detail { get; set; } = string.Empty;
    public List<RegisterFieldItem> RegisterFields { get; set; } = new();
}

public class UpdateRegisterFormCommandHandler : IRequestHandler<UpdateRegisterFormCommand, RegisterFormDto>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public UpdateRegisterFormCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<RegisterFormDto> Handle(UpdateRegisterFormCommand request, CancellationToken cancellationToken)
    {
        var entity = await _context.RegisterForms
            .AsTracking()
            .FirstOrDefaultAndRemoveCacheAsync(
                e => e.Id.Equals(request.Id.ToGuidOnly()),
                cancellationToken);

        Guard.Against.NotFound(request.Id.ToGuidOnly(), entity);

        _mapper.Map<UpdateRegisterFormCommand, RegisterForm>(request, entity);

        await _context.SaveChangesAsync(cancellationToken);

        return _mapper.Map<RegisterFormDto>(entity);
    }
}
