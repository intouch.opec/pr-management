﻿using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.RegisterForms.Commands.CreateRegisterForm;

public record CreateRegisterFormCommand : IRequest<RegisterFormDto>
{
    public string Name { get; set; } = string.Empty;
    public string Detail { get; set; } = string.Empty;
    public List<RegisterFieldItem> RegisterFields { get; set; } = new();
}

public class RegisterFieldItem
{
    public string InputType { get; set; } = string.Empty;
    public bool IsRequired { get; set; } = false;
}

public class CreateRegisterFormCommandHandler : IRequestHandler<CreateRegisterFormCommand, RegisterFormDto>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public CreateRegisterFormCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<RegisterFormDto> Handle(CreateRegisterFormCommand request, CancellationToken cancellationToken)
    {
        var entity = _mapper.Map<RegisterForm>(request);

        await _context.RegisterForms.AddAsync(entity);
        await _context.SaveChangesAsync(cancellationToken);

        return _mapper.Map<RegisterFormDto>(entity);
    }
}
