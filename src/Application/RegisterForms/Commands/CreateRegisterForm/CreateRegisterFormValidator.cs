using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Constants;
using PRManagement.Application.Common.Extensions;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.RegisterForms.Commands.CreateRegisterForm;

public class CreateRegisterFormCommandValidator : AbstractValidator<CreateRegisterFormCommand>
{
    private readonly IApplicationDbContext _context;

    public CreateRegisterFormCommandValidator(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(v => v.Name)
           .MaximumLength(35)
           .MustAsync(BeUniqueRegisterFormName)
           .WithMessage(ValidatorMessages.ShouldNotBeDuplicate)
           .NotEmpty();

        RuleForEach(e => e.RegisterFields)
            .ChildRules(e =>
                e.RuleFor(c => c.InputType)
                    .MustEnum<RegisterFieldItem, InputInformation>()
                    .NotEmpty())
            .DependentRules(() =>
            {
                RuleForEach(command => command.RegisterFields)
                    .SetValidator((command, registerFields) =>
                    {
                        return new CreateRegisterFieldsValidator(command.RegisterFields.Select(rf => rf.InputType));
                    });
            });

    }

    internal async Task<bool> BeUniqueRegisterFormName(string name, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(name))
            return true;

        return !await _context.RegisterForms
          .AsNoTracking()
          .AnyAsync(p => p.Name.ToLower().Equals(name.ToLower()), cancellationToken);
    }

}

public class CreateRegisterFieldsValidator : AbstractValidator<RegisterFieldItem>
{
    private readonly IEnumerable<string> _inputs;

    public CreateRegisterFieldsValidator(IEnumerable<string> inputs)
    {
        _inputs = inputs;

        RuleFor(pl => pl)
            .Must(HasDuplicateInput)
            .WithMessage("The 'InputType' is duplicate.");
    }

    public bool HasDuplicateInput(RegisterFieldItem input)
    {
        return _inputs.Where(i => i.Equals(input.InputType)).Count() == 1;
    }
}
