using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;
using PRManagement.Domain.Exceptions;

namespace PRManagement.Application.RegisterForms.Commands.CreateRegisterForm;

public class CreateRegisterFormMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<CreateRegisterFormCommand, RegisterForm>();

            CreateMap<RegisterFieldItem, RegisterField>()
                .ForMember(s => s.InputType, op => op.MapFrom(o => o.InputType.ToEnum<InputInformation>()));

            CreateMap<RegisterForm, RegisterFormDto>();

            CreateMap<RegisterField, RegisterFieldItemDto>()
                .ForMember(s => s.IsRequired, op => op.MapFrom(o => o.IsRequired))
                .ForMember(s => s.InputType, op => op.MapFrom(o => o.InputType))
                .ForMember(s => s.Path, op => op.MapFrom(o => InputInformationContact.IsInformationContact(o.InputType) ? "Contact" : "Register"));

            CreateMap<RegisterField, RegisterFieldItem>()
                .ForMember(s => s.InputType, op => op.MapFrom(o => o.InputType.GetEnumMemberValue()));

        }
    }
}
