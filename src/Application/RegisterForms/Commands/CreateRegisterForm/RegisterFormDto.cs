using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.RegisterForms.Commands.CreateRegisterForm;

public class RegisterFormDto
{
    public Guid Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public string Detail { get; set; } = string.Empty;
    public IEnumerable<RegisterFieldItemDto> RegisterFields { get; set; } = Array.Empty<RegisterFieldItemDto>();
    public DateTimeOffset Created { get; set; }
    public string? CreatedBy { get; set; }
    public DateTimeOffset LastModified { get; set; }
    public string? LastModifiedBy { get; set; }
}

public class RegisterFieldItemDto
{
    public InputInformation InputType { get; set; }
    public bool IsRequired { get; set; } = false;
    public string Path { get; set; } = string.Empty;
}
