﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.RegisterForms.Commands.CreateRegisterForm;

namespace PRManagement.Application.RegisterForms.Queries.GetRegisterForm;

public record GetRegisterFormQuery(string Id): IRequest<RegisterFormDto>;

public class GetRegisterFormQueryHandler : IRequestHandler<GetRegisterFormQuery, RegisterFormDto>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public GetRegisterFormQueryHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<RegisterFormDto> Handle(GetRegisterFormQuery request, CancellationToken cancellationToken)
    {
        var entity = await _context.RegisterForms
            .AsTracking()
            .FirstOrDefaultCacheAsync(
                e => e.Id.Equals(request.Id.ToGuidOnly()),
                cancellationToken);

        Guard.Against.NotFound(request.Id.ToGuidOnly(), entity);

        return _mapper.Map<RegisterFormDto>(entity);
    }
}
