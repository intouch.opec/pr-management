using PRManagement.Application.Common.Extensions;

namespace PRManagement.Application.RegisterForms.Queries.GetRegisterForm;

public class GetRegisterFormQueryValidator : AbstractValidator<GetRegisterFormQuery>
{
    public GetRegisterFormQueryValidator()
    {
        RuleFor(v => v.Id)
            .StringIsGuid();
    }
}
