﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.Common.Models;
using PRManagement.Application.RegisterForms.Commands.CreateRegisterForm;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.RegisterForms.Queries.GetRegisterFormWithPagination;

public record GetRegisterFormWithPaginationQuery : IRequest<PaginatedList<RegisterFormDto>>
{
    public string? Name { get; set; }

    public int? PageNumber { get; init; } = 1;
    public int? PageSize { get; init; } = 10;
    public SortType? Sort { get; set; }
    public string? SortBy { get; set; }
}

public class GetRegisterFormWithPaginationQueryHandler : IRequestHandler<GetRegisterFormWithPaginationQuery, PaginatedList<RegisterFormDto>>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public GetRegisterFormWithPaginationQueryHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<PaginatedList<RegisterFormDto>> Handle(GetRegisterFormWithPaginationQuery request, CancellationToken cancellationToken)
    {
        var query = _context.RegisterForms
            .AsNoTracking()
            .WhereLike(e => e.Name.ToLower(), request.Name)
            .AsQueryable();

        var items = query
            .OrderBy(request.SortBy ?? string.Empty, ascending: request.Sort)
            .Skip(((request.PageNumber ?? 1) - 1) * (request.PageSize ?? 10))
            .Take(request.PageSize ?? 10)
            .AsEnumerable()
            .Select(e => new RegisterFormDto()
            {
                Id = e.Id,
                Name = e.Name,
                Detail = e.Detail,
                Created = e.Created,
                CreatedBy = e.CreatedBy,
                LastModified = e.LastModified,
                LastModifiedBy = e.LastModifiedBy,
                RegisterFields = e.RegisterFields.Select(rf => new RegisterFieldItemDto()
                {
                    InputType = rf.InputType,
                    IsRequired = rf.IsRequired,
                    Path = InputInformationContact.IsInformationContact(rf.InputType) ? "Contact" : "Register"
                }).ToList()
            }).ToList();

        var count = await query.CountAsync(cancellationToken);
        return new PaginatedList<RegisterFormDto>(items, count, request.PageNumber ?? 1, request.PageSize ?? 10);
    }

}
