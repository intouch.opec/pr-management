
using PRManagement.Domain.Entities;

namespace PRManagement.Application.RegisterForms.Queries.GetRegisterFormOption;

public class GetRegisterFormOption
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<RegisterForm, RegisterFormOption>();
        }
    }
}