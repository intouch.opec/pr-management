﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;

namespace PRManagement.Application.RegisterForms.Queries.GetRegisterFormOption;

public record GetRegisterFormOptionQuery : IRequest<IEnumerable<RegisterFormOption>>
{
    public string? Name { get; set; }
}

public class GetRegisterFormOptionQueryHandler : IRequestHandler<GetRegisterFormOptionQuery, IEnumerable<RegisterFormOption>>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public GetRegisterFormOptionQueryHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<IEnumerable<RegisterFormOption>> Handle(GetRegisterFormOptionQuery request, CancellationToken cancellationToken)
    {
        return await Search(request)
            .ProjectTo<RegisterFormOption>(_mapper.ConfigurationProvider)
            .ToListAsync(cancellationToken);
    }

    internal IQueryable Search(GetRegisterFormOptionQuery request)
    {
        return _context.RegisterForms
            .AsNoTracking()
            .WhereLike(e => e.Name.ToLower(), request.Name);
    }
}
