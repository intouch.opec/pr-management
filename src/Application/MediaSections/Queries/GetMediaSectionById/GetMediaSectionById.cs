using PRManagement.Application.Common.Interfaces;

namespace PRManagement.Application.MediaSections.Queries.GetMediaById;

public record GetMediaSectionByIdQuery(Guid Id) : IRequest<GetMediaSectionByIdResponse>;

public class GetMediaSectionByIdQueryHandler : IRequestHandler<GetMediaSectionByIdQuery, GetMediaSectionByIdResponse>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public GetMediaSectionByIdQueryHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<GetMediaSectionByIdResponse> Handle(GetMediaSectionByIdQuery request, CancellationToken cancellationToken)
    {
        var entity = await _context.MediaSections
          .Include(e => e.Section)
          .Include(e => e.SubSection)
          .Include(e => e.Media)
          .Include(e => e.Platforms)
          .Include(e => e.Address)
          .AsNoTracking()
          .ProjectTo<GetMediaSectionByIdResponse>(_mapper.ConfigurationProvider)
          .FirstOrDefaultAsync(e => e.Id.Equals(request.Id), cancellationToken);

        Guard.Against.NotFound(request.Id, entity);

        return entity;
    }
}