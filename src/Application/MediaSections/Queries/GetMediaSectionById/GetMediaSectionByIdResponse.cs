using PRManagement.Application.Common.Models;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.MediaSections.Queries.GetMediaById;
public class GetMediaSectionByIdResponse
{
    public Guid Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public TypeOfMedia? TypeOfMedia { get; set; }
    public Guid MediaId { get; set; }
    public Guid SectionId { get; set; }
    public Guid SubSectionId { get; set; }
    public string SectionName { get; set; } = string.Empty;
    public string SubSectionName { get; set; } = string.Empty;
    public IEnumerable<string> Email { get; set; } = Enumerable.Empty<string>();
    public IEnumerable<string> PhoneNumbers { get; set; } = Enumerable.Empty<string>();
    public IEnumerable<PlatformItem> Platforms { get; set; } = Enumerable.Empty<PlatformItem>();
    public DateTimeOffset Created { get; set; }
    public string? CreatedBy { get; set; }
    public DateTimeOffset LastModified { get; set; }
    public string? LastModifiedBy { get; set; }
}

public class PlatformItem : PlatformDto
{
    public Guid Id { get; set; }
    public DateTimeOffset Created { get; set; }
    public string? CreatedBy { get; set; }
    public DateTimeOffset LastModified { get; set; }
    public string? LastModifiedBy { get; set; }
}