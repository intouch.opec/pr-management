using PRManagement.Domain.Entities;

namespace PRManagement.Application.MediaSections.Queries.GetMediaById;

public class GetMediaSectionByIdMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<Platform, PlatformItem>();

            CreateMap<MediaSection, GetMediaSectionByIdResponse>()
                .ForMember(s => s.Id, op => op.MapFrom(o => o.Id))
                .ForMember(s => s.Name, op => op.MapFrom(o => o.Media.Name))
                .ForMember(s => s.TypeOfMedia, op => op.MapFrom(o => o.TypeOfMedia))
                .ForMember(s => s.MediaId, op => op.MapFrom(o => o.MediaId))
                .ForMember(s => s.SectionId, op => op.MapFrom(o => o.SectionId))
                .ForMember(s => s.SubSectionId, op => op.MapFrom(o => o.SubSectionId))
                .ForMember(s => s.SectionName, op => op.MapFrom(o => o.Section.Name))
                .ForMember(s => s.SubSectionName, op => op.MapFrom(o => o.SubSection.Name))
                .ForMember(s => s.Email, op => op.MapFrom(o => o.Address!.Email ?? Array.Empty<string>()))
                .ForMember(s => s.PhoneNumbers, op => op.MapFrom(o => o.Address!.PhoneNumbers ?? Array.Empty<string>()));
        }
    }
}
