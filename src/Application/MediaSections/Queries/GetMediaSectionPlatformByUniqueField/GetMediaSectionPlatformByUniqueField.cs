using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Enums;
using PRManagement.Domain.Exceptions;

namespace PRManagement.Application.MediaSections.Queries.GetMediaSectionPlatformByUniqueField;
public class GetMediaSectionByUniqueFieldQuery : IRequest<GetMediaSectionByUniqueFieldResponse>
{
    public string MediaId { get; set; } = string.Empty;
    public string TypeOfMedia { get; set; } = string.Empty;
    public string SectionId { get; set; } = string.Empty;
    public string SubSectionId { get; set; } = string.Empty;
}

public class GetMediaWithPaginationQueryHandler : IRequestHandler<GetMediaSectionByUniqueFieldQuery, GetMediaSectionByUniqueFieldResponse>
{
    private readonly IMapper _mapper;
    private readonly IApplicationDbContext _context;

    public GetMediaWithPaginationQueryHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<GetMediaSectionByUniqueFieldResponse> Handle(GetMediaSectionByUniqueFieldQuery request, CancellationToken cancellationToken)
    {
        var entity = await _context.MediaSections
            .Include(e => e.Platforms)
            .FirstOrDefaultAsync(ms => new Guid(request.MediaId).Equals(ms.MediaId) &&
                ms.SectionId.Equals(new Guid(request.SectionId)) &&
                ms.SubSectionId.Equals(new Guid(request.SubSectionId)) &&
                ms.TypeOfMedia.Equals(request.TypeOfMedia.ToEnum<TypeOfMedia>()), cancellationToken);

        Guard.Against.NotFound(request.MediaId!, entity);

        return _mapper.Map<GetMediaSectionByUniqueFieldResponse>(entity);
    }
}