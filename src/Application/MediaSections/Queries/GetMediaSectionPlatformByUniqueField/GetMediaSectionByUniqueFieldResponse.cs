using PRManagement.Application.Common.Models;

namespace PRManagement.Application.MediaSections.Queries.GetMediaSectionPlatformByUniqueField;

public class GetMediaSectionByUniqueFieldResponse
{
    public Guid Id { get; set; }
    public uint TierPotential { get; set; } = 0;
    public IEnumerable<PlatformItem> Platforms { get; set; } = Enumerable.Empty<PlatformItem>();
}

public class PlatformItem : PlatformDto
{
    public Guid Id { get; set; }
}