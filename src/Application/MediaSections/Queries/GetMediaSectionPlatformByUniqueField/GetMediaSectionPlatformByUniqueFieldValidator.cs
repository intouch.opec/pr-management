using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;

namespace PRManagement.Application.MediaSections.Queries.GetMediaSectionPlatformByUniqueField;

public class GetMediaSectionPlatformByUniqueFieldValidator : AbstractValidator<GetMediaSectionByUniqueFieldQuery>
{
    private readonly IApplicationDbContext _context;

    public GetMediaSectionPlatformByUniqueFieldValidator(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(e => e.MediaId)
          .StringIsGuid()
          .NotEmpty();

        RuleFor(e => e.SectionId)
          .StringIsGuid()
          .NotEmpty();

        RuleFor(e => e.SubSectionId)
          .StringIsGuid()
          .NotEmpty();

        RuleFor(e => e.TypeOfMedia)
          .NotNull()
          .NotEmpty();

    }
}
