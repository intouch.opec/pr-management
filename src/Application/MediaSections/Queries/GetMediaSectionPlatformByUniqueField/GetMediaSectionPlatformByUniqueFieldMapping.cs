using PRManagement.Domain.Entities;

namespace PRManagement.Application.MediaSections.Queries.GetMediaSectionPlatformByUniqueField;

public class GetMediaSectionPlatformByUniqueFieldMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<Platform, PlatformItem>();
            CreateMap<PlatformItem, Platform>();
            CreateMap<MediaSection, GetMediaSectionByUniqueFieldResponse>()
              .ForMember(e => e.Platforms, op => op.MapFrom<MediaSectionByUniqueFieldResponseResolver>());
        }
    }

    public class MediaSectionByUniqueFieldResponseResolver : IValueResolver<MediaSection, GetMediaSectionByUniqueFieldResponse, IEnumerable<PlatformItem>>
    {
        private readonly IMapper _mapper;
        public MediaSectionByUniqueFieldResponseResolver(IMapper mapper)
        {
            _mapper = mapper;
        }

        public IEnumerable<PlatformItem> Resolve(MediaSection source, GetMediaSectionByUniqueFieldResponse destination, IEnumerable<PlatformItem> member, ResolutionContext context)
        {
            if (source.Platforms is null) return Enumerable.Empty<PlatformItem>();
            return source.Platforms.Select(_mapper.Map<PlatformItem>);
        }
    }

}