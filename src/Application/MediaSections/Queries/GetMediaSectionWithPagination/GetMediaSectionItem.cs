using PRManagement.Domain.Enums;

namespace PRManagement.Application.MediaSections.Queries.GetMediaSectionWithPagination;

public class GetMediaSectionItem
{
	public Guid Id { get; set; }
	public string Name { get; set; } = string.Empty;
	public TypeOfMedia? TypeOfMedia { get; set; }
	public Guid MediaId { get; set; }
	public Guid SectionId { get; set; }
	public Guid SubSectionId { get; set; }
	public string SectionName { get; set; } = string.Empty;
	public string SubSectionName { get; set; } = string.Empty;
	public uint TierPotential { get; set; } = 0;
	public string[] Email { get; set;} = Array.Empty<string>();
    public string[] PhoneNumbers { get; set;} = Array.Empty<string>();
	public DateTimeOffset Created { get; set; }
	public string? CreatedBy { get; set; }
	public DateTimeOffset LastModified { get; set; }
	public string? LastModifiedBy { get; set; }
}
