using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.Common.Mappings;
using PRManagement.Application.Common.Models;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.MediaSections.Queries.GetMediaSectionWithPagination;

public class GetMediaSectionWithPaginationQuery : IRequest<PaginatedList<GetMediaSectionItem>>
{
    public string? Name { get; set; } = string.Empty;
    public TypeOfMedia? TypeOfMedia { get; set; }
    public string? MediaId { get; set; } = string.Empty;
    public string? SectionId { get; set; } = string.Empty;
    public string? SubSectionId { get; set; } = string.Empty;
    public uint? TierPotential { get; set; } = 0;

    public int? PageNumber { get; init; } = 1;
    public int? PageSize { get; init; } = 10;
    public SortType? Sort { get; set; }
    public string? SortBy { get; set; } = nameof(Name);
}

public class GetMediaSectionWithPaginationQueryHandler : IRequestHandler<GetMediaSectionWithPaginationQuery, PaginatedList<GetMediaSectionItem>>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public GetMediaSectionWithPaginationQueryHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<PaginatedList<GetMediaSectionItem>> Handle(GetMediaSectionWithPaginationQuery request, CancellationToken cancellationToken)
    {
        return await Search(request)
          .ProjectTo<GetMediaSectionItem>(_mapper.ConfigurationProvider)
          .OrderBy(request.SortBy ?? string.Empty, ascending: request.Sort)
          .PaginatedListAsync(request.PageNumber ?? 1, request.PageSize ?? 10, cancellationToken);
    }

    internal IQueryable Search(GetMediaSectionWithPaginationQuery request)
    {
        return _context.MediaSections
          .Include(e => e.Media)
          .Include(e => e.Section)
          .Include(e => e.SubSection)
          .Include(e => e.Address)
          .WhereLike(e => e.Media.Name.ToLower(), request.Name)
          .WhereEqual(e => e.TierPotential, request.TierPotential)
          .WhereEqual(e => e.TypeOfMedia, request.TypeOfMedia)
          .WhereEqual(e => e.MediaId, request.MediaId.ToGuid())
          .WhereEqual(e => e.SectionId, request.SectionId.ToGuid())
          .WhereEqual(e => e.SubSectionId, request.SubSectionId.ToGuid());
    }
}
