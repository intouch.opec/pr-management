using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.MediaSections.Commands.UpdateMediaSection;

public record UpdateMediaSectionCommand(Guid Id) : IRequest<UpdateMediaSectionResponse>
{
    public string MediaId { get; set; } = string.Empty;
    public string TypeOfMedia { get; set; } = string.Empty;
    public string SectionId { get; set; } = string.Empty;
    public string SubSectionId { get; set; } = string.Empty;
    public string[] Email { get; set; } = Array.Empty<string>();
    public string[] PhoneNumbers { get; set; } = Array.Empty<string>();
}

public class UpdateMediaSectionCommandHandler : IRequestHandler<UpdateMediaSectionCommand, UpdateMediaSectionResponse>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public UpdateMediaSectionCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<UpdateMediaSectionResponse> Handle(UpdateMediaSectionCommand request, CancellationToken cancellationToken)
    {
        var entity = await _context.MediaSections
            .Include(e => e.Section)
            .Include(e => e.SubSection)
            .Include(e => e.Address)
            .AsTracking()
            .FirstOrDefaultAsync(e =>
                e.Id.Equals(request.Id),
            cancellationToken);

        Guard.Against.NotFound(request.Id, entity);

        _mapper.Map<UpdateMediaSectionCommand, MediaSection>(request, entity);

        await _context.SaveChangesAsync(cancellationToken);

        return _mapper.Map<UpdateMediaSectionResponse>(entity);
    }
}