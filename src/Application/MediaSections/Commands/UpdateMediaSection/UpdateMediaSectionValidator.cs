using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Constants;
using PRManagement.Domain.Enums;
using PRManagement.Domain.Exceptions;

namespace PRManagement.Application.MediaSections.Commands.UpdateMediaSection;
public class UpdateMediaSectionValidator : AbstractValidator<UpdateMediaSectionCommand>
{
    private readonly IApplicationDbContext _context;

    public UpdateMediaSectionValidator(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(e => e.MediaId)
          .MustAsync(HasMedia)
          .WithMessage(ValidatorMessages.NotFound)
          .NotEmpty()
          .StringIsGuid()
          .MustAsync(BeUniqueMediaSection)
          .WithMessage($"The {nameof(UpdateMediaSectionCommand.MediaId)}, {nameof(UpdateMediaSectionCommand.SectionId)} and {nameof(UpdateMediaSectionCommand.SubSectionId)} combination is duplicated.");

        RuleFor(e => e.SectionId)
          .MustAsync(HasSection)
          .WithMessage(ValidatorMessages.NotFound)
          .StringIsGuid()
          .NotEmpty();

        RuleFor(e => e.SubSectionId)
          .MustAsync(HasSubSection)
          .WithMessage(ValidatorMessages.NotFound)
          .StringIsGuid()
          .NotEmpty();

        RuleFor(e => e.TypeOfMedia)
         .NotEmpty()
         .MustEnum<UpdateMediaSectionCommand, TypeOfMedia>();

    }

    internal async Task<bool> BeUniqueMediaSection(UpdateMediaSectionCommand request, string mediaId, CancellationToken cancellationToken)
      => (!Guid.TryParse(mediaId, out var mediaGuid) ||
        !Guid.TryParse(request.SectionId, out var sectionGuid) ||
        !Guid.TryParse(request.SubSectionId, out var subSectionGuid)) ||
        !await _context.MediaSections
          .AnyAsync(e => 
                !e.Id.Equals(request.Id) &&
                e.MediaId.Equals(mediaId.ToGuid()) &&
                e.TypeOfMedia.Equals(request.TypeOfMedia.ToEnum<TypeOfMedia>()) &&
                e.SectionId.Equals(request.SectionId.ToGuid()) &&
                e.SubSectionId.Equals(request.SubSectionId.ToGuid()),
                cancellationToken);

    internal async Task<bool> HasMedia(string mediaId, CancellationToken cancellationToken)
    => !mediaId.IsValidGuid() || await _context.Medias
          .AnyAsync(p => p.Id.Equals(mediaId.ToGuid()), cancellationToken);

    internal async Task<bool> HasSection(string sectionId, CancellationToken cancellationToken)
    => !sectionId.IsValidGuid() || await _context.Sections
         .AnyAsync(p => p.Id.Equals(sectionId.ToGuid()), cancellationToken);

    internal async Task<bool> HasSubSection(string subSectionId, CancellationToken cancellationToken)
    => !subSectionId.IsValidGuid() || await _context.SubSections
        .AnyAsync(p => p.Id.Equals(new Guid(subSectionId)), cancellationToken);

}