using PRManagement.Domain.Enums;

namespace PRManagement.Application.MediaSections.Commands.UpdateMediaSection;

public class UpdateMediaSectionResponse
{
    public Guid Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public TypeOfMedia TypeOfMedia { get; set; }
    public Guid MediaId { get; set; }
    public Guid SectionId { get; set; }
    public Guid SubSectionId { get; set; }
    public string SectionName { get; set; } = string.Empty;
    public string SubSectionName { get; set; } = string.Empty;
    public string[] Email { get; set; } = Array.Empty<string>();
    public string[] PhoneNumbers { get; set; } = Array.Empty<string>();

}
