using PRManagement.Application.Common.Extensions;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.MediaSections.Commands.UpdateMediaSection;

public class UpdateMediaSectionMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<UpdateMediaSectionCommand, MediaSection>()
                .ForMember(e => e.SectionId, op => op.MapFrom(o => o.SectionId.ToGuid()))
                .ForMember(e => e.SubSectionId, op => op.MapFrom(o => o.SubSectionId.ToGuid()))
                .ForMember(e => e.MediaId, op => op.MapFrom(o => o.MediaId.ToGuid()))
                .ForMember(e => e.Address, op => op.MapFrom<UpdateMediaSectionCommandAddressResolver>());

            CreateMap<UpdateMediaSectionCommand, Address?>();

            CreateMap<MediaSection, UpdateMediaSectionResponse>()
                .ForMember(s => s.Id, op => op.MapFrom(o => o.Id))
                .ForMember(s => s.Name, op => op.MapFrom(o => o.Media.Name))
                .ForMember(s => s.TypeOfMedia, op => op.MapFrom(o => o.TypeOfMedia))
                .ForMember(s => s.MediaId, op => op.MapFrom(o => o.MediaId))
                .ForMember(s => s.SectionId, op => op.MapFrom(o => o.SectionId))
                .ForMember(s => s.SubSectionId, op => op.MapFrom(o => o.SubSectionId))
                .ForMember(s => s.SectionName, op => op.MapFrom(o => o.Section.Name))
                .ForMember(s => s.SubSectionName, op => op.MapFrom(o => o.SubSection.Name))
                .ForMember(s => s.PhoneNumbers, op => op.MapFrom(o => o.Address!.PhoneNumbers))
                .ForMember(s => s.Email, op => op.MapFrom(o => o.Address!.Email));
        }
    }

    public class UpdateMediaSectionCommandAddressResolver : IValueResolver<UpdateMediaSectionCommand, MediaSection, Address?>
    {
        private readonly IMapper _mapper;

        public UpdateMediaSectionCommandAddressResolver(IMapper mapper)
        {
            _mapper = mapper;
        }

        public Address? Resolve(UpdateMediaSectionCommand source, MediaSection destination, Address? member, ResolutionContext context)
        {
            member = _mapper.Map<UpdateMediaSectionCommand, Address?>(source, destination.Address);
            if (destination.AddressId is not null)
                member!.Id = (Guid)destination.AddressId;
            else
                member!.Id = Guid.Empty;
            return member;
        }
    }

}
