using PRManagement.Domain.Enums;

namespace PRManagement.Application.MediaSections.Commands.CreateMediaSection;

public class CreateMediaSectionResponse
{
    public Guid Id { get; set; }
    public TypeOfMedia TypeOfMedia { get; set; }
    public Guid SectionId { get; set; }
    public Guid SubSectionId { get; set; }
    public Guid MediaSectionId { get; set; }
    public string[] Email { get; set; } = Array.Empty<string>();
    public string[] PhoneNumbers { get; set; } = Array.Empty<string>();
}
