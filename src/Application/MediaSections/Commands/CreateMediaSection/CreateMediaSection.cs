using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.Common.Models;
using PRManagement.Application.Medias.Commands.CreateMedia;
using PRManagement.Domain.Entities;
using ValidationException = PRManagement.Application.Common.Exceptions.ValidationException;

namespace PRManagement.Application.MediaSections.Commands.CreateMediaSection;

public record CreateMediaSectionCommand : IRequest<CreateMediaSectionResponse>
{
    public string MediaId { get; set; } = string.Empty;
    public string TypeOfMedia { get; set; } = string.Empty;
    public string SectionId { get; set; } = string.Empty;
    public string SubSectionId { get; set; } = string.Empty;
    public string[] Email { get; set; } = Array.Empty<string>();
    public string[] PhoneNumbers { get; set; } = Array.Empty<string>();
    public IEnumerable<PlatformDto>? Platforms { get; set; }
}

public class CreateMediaSectionCommandHandler : IRequestHandler<CreateMediaSectionCommand, CreateMediaSectionResponse>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public CreateMediaSectionCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<CreateMediaSectionResponse> Handle(CreateMediaSectionCommand request, CancellationToken cancellationToken)
    {
        ValidatePlatforms(request.Platforms);
        var entity = _mapper.Map<MediaSection>(request);

        await _context.MediaSections.AddRangeAsync(entity);
        await _context.SaveChangesAsync(cancellationToken);

        return _mapper.Map<CreateMediaSectionResponse>(entity);
    }

    internal void ValidatePlatforms(IEnumerable<PlatformDto>? platforms)
    {
        var validator = new CreatePlatformsValidator(platforms);
        var validationResults = validator.Validate(platforms);
        if (!validationResults.IsValid)
            throw new ValidationException(validationResults.Errors);
    }
}
