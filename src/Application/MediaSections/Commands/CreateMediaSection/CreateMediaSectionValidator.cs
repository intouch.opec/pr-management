using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.Common.Models;
using PRManagement.Domain.Constants;
using PRManagement.Domain.Enums;
using PRManagement.Domain.Exceptions;

namespace PRManagement.Application.MediaSections.Commands.CreateMediaSection;

public class CreateMediaSectionValidator : AbstractValidator<CreateMediaSectionCommand>
{
    private readonly IApplicationDbContext _context;
    private readonly string _messageDuplicated = $"The {nameof(CreateMediaSectionCommand.MediaId)} , {nameof(CreateMediaSectionCommand.SectionId)} And {nameof(CreateMediaSectionCommand.SubSectionId)} combination is duplicated.";

    public CreateMediaSectionValidator(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(e => e.MediaId)
          .MustAsync(HasMedia)
          .WithMessage(ValidatorMessages.NotFound)
          .NotEmpty()
          .StringIsGuid()
          .MustAsync(BeUniqueMediaSection)
          .WithMessage(_messageDuplicated);

        RuleFor(e => e.SectionId)
          .MustAsync(HasSection)
          .WithMessage(ValidatorMessages.NotFound)
          .StringIsGuid()
          .NotEmpty();

        RuleFor(e => e.SubSectionId)
          .MustAsync(HasSubSection)
          .WithMessage(ValidatorMessages.NotFound)
          .StringIsGuid()
          .NotEmpty();

        RuleFor(e => e.TypeOfMedia)
          .NotEmpty()
          .MustEnum<CreateMediaSectionCommand, TypeOfMedia>();

        RuleForEach(e => e.Platforms)
          .ChildRules(platforms => platforms
            .RuleFor(pt => pt.PlatformType)
              .MustEnum<PlatformDto, PlatformType>());

        RuleForEach(e => e.Platforms)
         .ChildRules(platforms => platforms
           .RuleFor(pt => pt.PlatformType)
             .MustBeValidContains(MainStreamPlatformType.PlatformTypeStrings))
          .When(e => e.TypeOfMedia.Equals(TypeOfMedia.MAINSTREAM.GetEnumMemberValue()));

        RuleForEach(e => e.Platforms)
         .ChildRules(platforms => platforms
           .RuleFor(pt => pt.PlatformType)
             .MustBeValidContains(DigitalPlatformType.PlatformTypeStrings))
          .When(e => e.TypeOfMedia.Equals(TypeOfMedia.DIGITAL.GetEnumMemberValue()));
    }

    internal async Task<bool> BeUniqueMediaSection(CreateMediaSectionCommand request, string mediaId,
      CancellationToken cancellationToken)
    {
        if (!request.TypeOfMedia.IsEnum<TypeOfMedia>())
            return true;

        return !await _context.MediaSections
          .AnyAsync(e =>
            e.TypeOfMedia.Equals(request.TypeOfMedia.ToEnum<TypeOfMedia>()) &&
            e.MediaId.Equals(new Guid(mediaId)) &&
            e.SectionId.Equals(new Guid(request.SectionId)) &&
            e.SubSectionId.Equals(new Guid(request.SubSectionId)), cancellationToken);
    }

    internal async Task<bool> HasSection(string sectionId, CancellationToken cancellationToken)
      => !sectionId.IsValidGuid() || await _context.Sections
          .AnyAsync(p => p.Id.Equals(new Guid(sectionId)), cancellationToken);

    internal async Task<bool> HasSubSection(string subSectionId, CancellationToken cancellationToken)
      => !subSectionId.IsValidGuid() || await _context.SubSections
          .AnyAsync(p => p.Id.Equals(new Guid(subSectionId)), cancellationToken);

    internal async Task<bool> HasMedia(string mediaId, CancellationToken cancellationToken)
      => !mediaId.IsValidGuid() || await _context.Medias
          .AnyAsync(p => p.Id.Equals(new Guid(mediaId)), cancellationToken);

}
