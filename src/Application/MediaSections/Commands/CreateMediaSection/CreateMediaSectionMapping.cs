using PRManagement.Application.Common.Extensions;
using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;
using PRManagement.Domain.Exceptions;

namespace PRManagement.Application.MediaSections.Commands.CreateMediaSection;
public class CreateMediaSectionMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<MediaSection, CreateMediaSectionResponse>()
                .ForMember(e => e.MediaSectionId, op => op.MapFrom(o => o.Id))
                .ForMember(e => e.PhoneNumbers, op => op.MapFrom(o => o.Address!.PhoneNumbers))
                .ForMember(e => e.Email, op => op.MapFrom(o => o.Address!.Email));

            CreateMap<CreateMediaSectionCommand, Address>();

            CreateMap<CreateMediaSectionCommand, MediaSection>()
                .ForMember(s => s.Address, op => op.MapFrom<CreateMediaSectionCommandAddressResolver>())
                .ForMember(s => s.Platforms, op => op.MapFrom<CreatePlatformCommandResolver>())
                .ForMember(e => e.SectionId, op => op.MapFrom(o => o.MediaId.IsValidGuid() ? new Guid(o.SectionId) : Guid.Empty))
                .ForMember(e => e.SubSectionId, op => op.MapFrom(o => o.MediaId.IsValidGuid() ? new Guid(o.SubSectionId) : Guid.Empty))
                .ForMember(e => e.MediaId, op => op.MapFrom(o => o.MediaId.IsValidGuid() ? new Guid(o.MediaId) : Guid.Empty))
                .ForMember(e => e.TypeOfMedia, op => op.MapFrom(o => o.TypeOfMedia.ToEnum<TypeOfMedia>()));

        }
    }

    public class CreateMediaSectionCommandAddressResolver : IValueResolver<CreateMediaSectionCommand, MediaSection, Address?>
    {
        private readonly IMapper _mapper;

        public CreateMediaSectionCommandAddressResolver(IMapper mapper)
        {
            _mapper = mapper;
        }

        public Address? Resolve(CreateMediaSectionCommand source, MediaSection destination, Address? member, ResolutionContext context)
        {
            if (SomePropertiesAddressHaveValue(source))
                return null;

            return _mapper.Map<Address>(source);
        }

        public static bool SomePropertiesAddressHaveValue(CreateMediaSectionCommand source)
        {
            if (source is null)
                return false;

            return !source.Email.Any() &&
                   !source.PhoneNumbers.Any();
        }
    }

    public class CreatePlatformCommandResolver :
        IValueResolver<CreateMediaSectionCommand, MediaSection, ICollection<Platform>>
    {
        private readonly IMapper _mapper;

        public CreatePlatformCommandResolver(IMapper mapper)
        {
            _mapper = mapper;
        }

        public ICollection<Platform> Resolve(CreateMediaSectionCommand source, MediaSection destination, ICollection<Platform> member, ResolutionContext context)
        {
            if (source.Platforms is null) return Array.Empty<Platform>();

            return source.Platforms.Select(_mapper.Map<Platform>).ToList();
        }
    }
}