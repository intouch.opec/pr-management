using PRManagement.Domain.Entities;

namespace PRManagement.Application.MediaSections.Commands.DeleteMediaSections;

public class DeleteMediaSectionsMapping : AbstractValidator<DeleteMediaSectionCommand>
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<MediaSection, DeleteMediaSectionResponse>();
        }
    }
}
