﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;

namespace PRManagement.Application.MediaSections.Commands.DeleteMediaSections;

public record DeleteMediaSectionCommand : IRequest<DeleteMediaSectionResponse>
{
    public Guid Id { get; set; }
    public string Remark { get; set; } = string.Empty;
}

public class DeleteMediaSectionCommandHandler : IRequestHandler<DeleteMediaSectionCommand, DeleteMediaSectionResponse>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public DeleteMediaSectionCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<DeleteMediaSectionResponse> Handle(DeleteMediaSectionCommand request, CancellationToken cancellationToken)
    {
        var entity = await _context.MediaSections
            .SingleOrDefaultAsync(e => e.Id.Equals(request.Id), cancellationToken);

        Guard.Against.NotFound(request.Id, entity);

        _context.MediaSections.RemoveAndAddRemark(entity, request.Remark);

        await _context.SaveChangesAsync(cancellationToken);

        return _mapper.Map<DeleteMediaSectionResponse>(entity);
    }
}
