namespace PRManagement.Application.MediaSections.Commands.DeleteMediaSections;

public class DeleteMediaSectionResponse
{
    public Guid Id { get; set; }
}
