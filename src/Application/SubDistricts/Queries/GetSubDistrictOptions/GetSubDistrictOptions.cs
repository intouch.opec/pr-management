using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.SubDistricts.Queries.GetSubDistrictOptions;

public record GetSubDistrictOptionsQuery : IRequest<IEnumerable<GetSubDistrictOptionsResponse>>
{
    public string? Name { get; set; } = string.Empty;
    public string? Id { get; set; } = string.Empty;
    public string? DistrictId { get; set; } = string.Empty;
    public string? ProvinceId { get; set; } = string.Empty;
    public string? PostCode { get; set; } = string.Empty;
}

public class GetSubDistrictOptionsQueryHandler : IRequestHandler<GetSubDistrictOptionsQuery, IEnumerable<GetSubDistrictOptionsResponse>>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public GetSubDistrictOptionsQueryHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<IEnumerable<GetSubDistrictOptionsResponse>> Handle(GetSubDistrictOptionsQuery request, CancellationToken cancellationToken)
      => await Search(request)
        .OrderBy(nameof(GetSubDistrictOptionsResponse.Name), ascending: SortType.ASC)
        .ProjectToListCacheAsync<SubDistrict, GetSubDistrictOptionsResponse>(_mapper.ConfigurationProvider, cancellationToken);

    internal IQueryable<SubDistrict> Search(GetSubDistrictOptionsQuery request)
    {
        return _context.SubDistricts
          .Include(e => e.District)
          .Include(e => e.District.Province)
          .AsNoTracking()
          .WhereLike(e => e.NameTH, request.Name)
          .WhereLike(e => e.PostCode, request.PostCode)
          .WhereEqual(e => e.DistrictId, request.DistrictId)
          .WhereEqual(e => e.Id, request.Id);
    }
}
