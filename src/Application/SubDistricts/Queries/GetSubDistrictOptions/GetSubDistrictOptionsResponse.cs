using PRManagement.Application.Common.Models;

namespace PRManagement.Application.SubDistricts.Queries.GetSubDistrictOptions;

public class GetSubDistrictOptionsResponse : OptionItemBase
{
    public string NameTH { get; set; } = string.Empty;
    public string NameEN { get; set; } = string.Empty;
    public string DistrictId { get; set; } = string.Empty;
    public string ProvinceId { get; set; } = string.Empty;
    public string DistrictName { get; set; } = string.Empty;
    public string ProvinceName { get; set; } = string.Empty;
    public string PostCode { get; set; } = string.Empty;
}
