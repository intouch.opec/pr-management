using PRManagement.Domain.Entities;


namespace PRManagement.Application.SubDistricts.Queries.GetSubDistrictOptions;
public class GetSubDistrictOptionsMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<SubDistrict, GetSubDistrictOptionsResponse>()
                .ForMember(e => e.Name, op => op.MapFrom(o => o.NameTH))
                .ForMember(e => e.ProvinceId, op => op.MapFrom(o => o.District.ProvinceId))
                .ForMember(e => e.DistrictName, op => op.MapFrom(o => o.District.NameTH))
                .ForMember(e => e.ProvinceName, op => op.MapFrom(o => o.District.Province.NameTH));
        }
    }
}