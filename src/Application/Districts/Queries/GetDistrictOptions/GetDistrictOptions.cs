using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.Districts.Queries.GetDistrictOptions;
public record GetDistrictOptionsQuery : IRequest<IEnumerable<GetDistrictOptionsResponse>>
{
    public string? Name { get; set; } = string.Empty;
    public string? ProvinceId { get; set; } = string.Empty;
    public string? Id { get; set; } = string.Empty;
}

public class GetDistrictOptionsQueryHandler : IRequestHandler<GetDistrictOptionsQuery, IEnumerable<GetDistrictOptionsResponse>>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public GetDistrictOptionsQueryHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<IEnumerable<GetDistrictOptionsResponse>> Handle(GetDistrictOptionsQuery request, CancellationToken cancellationToken)
      => await Search(request)
        .OrderBy(nameof(GetDistrictOptionsResponse.Name), ascending: SortType.ASC)
        .ProjectToListCacheAsync<District, GetDistrictOptionsResponse>(_mapper.ConfigurationProvider, cancellationToken);

    internal IQueryable<District> Search(GetDistrictOptionsQuery request)
    {
        return _context.Districts
          .AsNoTracking()
          .WhereLike(e => e.NameTH, request.Name)
          .WhereEqual(e => e.Id, request.Id)
          .WhereEqual(e => e.ProvinceId, request.ProvinceId);
    }

}
