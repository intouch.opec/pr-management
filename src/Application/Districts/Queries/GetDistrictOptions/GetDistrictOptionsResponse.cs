using PRManagement.Application.Common.Models;

namespace PRManagement.Application.Districts.Queries.GetDistrictOptions;

public class GetDistrictOptionsResponse : OptionItemBase
{
    public string NameTH { get; set; } = string.Empty;
    public string NameEN { get; set; } = string.Empty;
}
