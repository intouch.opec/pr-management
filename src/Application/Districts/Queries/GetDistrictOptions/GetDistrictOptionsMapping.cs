using PRManagement.Domain.Entities;

namespace PRManagement.Application.Districts.Queries.GetDistrictOptions;
public class GetDistrictOptionsMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<District, GetDistrictOptionsResponse>()
                .ForMember(e => e.Id, op => op.MapFrom(o => o.Id))
                .ForMember(e => e.Name, op => op.MapFrom(o => o.NameTH));
        }
    }
}