﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.PRActivityTaskComments.Commands.CreatePRActivityTask;

namespace PRManagement.Application.PRActivityTaskComments.Commands.RemovePRActivityTaskComments;

public record RemovePRActivityTaskCommentCommand(string Id, string PRActivityTaskId) : IRequest<PRActivityTaskCommentDto>;

public class RemovePRActivityTaskCommentsCommandHandler : IRequestHandler<RemovePRActivityTaskCommentCommand, PRActivityTaskCommentDto>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public RemovePRActivityTaskCommentsCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<PRActivityTaskCommentDto> Handle(RemovePRActivityTaskCommentCommand request, CancellationToken cancellationToken)
    {
        var entity = await _context.PRActivityTaskComments
            .AsTracking()
            .SingleOrDefaultAsync(e =>
                e.Id.Equals(request.Id.ToGuid()) &&
                e.PRActivityTaskId.Equals(request.PRActivityTaskId.ToGuid()),
                cancellationToken);

        Guard.Against.NotFound(request.Id, entity);

        _context.PRActivityTaskComments.Remove(entity);
        await _context.SaveChangesAsync(cancellationToken);

        return _mapper.Map<PRActivityTaskCommentDto>(entity);
    }
}
