using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Constants;

namespace PRManagement.Application.PRActivityTaskComments.Commands.CreatePRActivityTask;

public class CreatePRActivityTaskCommandValidator : AbstractValidator<CreatePRActivityTaskCommentCommand>
{
    public IApplicationDbContext _context;

    public CreatePRActivityTaskCommandValidator(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(e => e.PrActivityTaskId)
           .StringIsGuid()
           .NotEmpty()
           .MustAsync(_context.PRActivityTasks.HasPRActivityTask)
           .WithMessage(ValidatorMessages.NotFound);

        RuleFor(e => e.Content)
           .NotEmpty();
    }
}
