using PRManagement.Application.Common.Extensions;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.PRActivityTaskComments.Commands.CreatePRActivityTask;

public class MappingProfile
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<CreatePRActivityTaskCommentCommand, PRActivityTaskComment>()
                .ForMember(dest => dest.PRActivityTaskId, op => op.MapFrom(src => src.PrActivityTaskId.ToGuid()));

            CreateMap<PRActivityTaskComment, PRActivityTaskCommentDto>()
                .ForMember(dest => dest.OwnerName, op => op.MapFrom(src => src.Owner!.Email));

        }
    }

}
