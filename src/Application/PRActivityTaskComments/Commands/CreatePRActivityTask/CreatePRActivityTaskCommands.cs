﻿using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.PRActivityTaskComments.Commands.CreatePRActivityTask;

public record CreatePRActivityTaskCommentCommand : IRequest<PRActivityTaskCommentDto>
{
    public string PrActivityTaskId { get; set; } = string.Empty;
    public string Content { get; set; } = string.Empty;
}

public class CreatePRActivityTaskCommandHandler : IRequestHandler<CreatePRActivityTaskCommentCommand, PRActivityTaskCommentDto>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public CreatePRActivityTaskCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<PRActivityTaskCommentDto> Handle(CreatePRActivityTaskCommentCommand request, CancellationToken cancellationToken)
    {
        var entity = _mapper.Map<PRActivityTaskComment>(request);

        await _context.PRActivityTaskComments
            .AddAsync(entity, cancellationToken);
        await _context.SaveChangesAsync(cancellationToken);

        return await _context.PRActivityTaskComments
            .Include(e => e.Owner)
            .ProjectTo<PRActivityTaskCommentDto>(_mapper.ConfigurationProvider)
            .FirstAsync(e => e.Id == entity.Id,cancellationToken);
    }
}
