using PRManagement.Domain.Entities;

namespace PRManagement.Application.PRActivityTaskComments.Commands.UpdatePRActivityTaskCommands;

public class MappingProfile
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<UpdatePRActivityTaskCommentCommand, PRActivityTaskComment>()
                .ForMember(dest => dest.Id, op => op.Ignore());

        }
    }

}
