using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Constants;

namespace PRManagement.Application.PRActivityTaskComments.Commands.UpdatePRActivityTaskCommands;

public class UpdatePRActivityTaskCommandsCommandValidator : AbstractValidator<UpdatePRActivityTaskCommentCommand>
{
    public IApplicationDbContext _context;

    public UpdatePRActivityTaskCommandsCommandValidator(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(e => e.Content)
           .NotEmpty();
    }
}