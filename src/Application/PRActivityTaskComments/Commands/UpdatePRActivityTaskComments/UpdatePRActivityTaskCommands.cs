﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.PRActivityTaskComments.Commands.CreatePRActivityTask;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.PRActivityTaskComments.Commands.UpdatePRActivityTaskCommands;

public record UpdatePRActivityTaskCommentCommand : IRequest<PRActivityTaskCommentDto>
{
    public string Content { get; set; } = string.Empty;
    public string Id { get; set; } = string.Empty;
    public string PrActivityTaskId { get; set; } = string.Empty;
}

public class UpdatePRActivityTaskCommandsCommandHandler : IRequestHandler<UpdatePRActivityTaskCommentCommand, PRActivityTaskCommentDto>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public UpdatePRActivityTaskCommandsCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<PRActivityTaskCommentDto> Handle(UpdatePRActivityTaskCommentCommand request, CancellationToken cancellationToken)
    {
        var entity = await _context.PRActivityTaskComments
            .AsTracking()
            .SingleAsync(
                e => e.Id.Equals(request.Id.ToGuid()),
                cancellationToken);

        Guard.Against.NotFound(request.Id, entity);

        _mapper.Map<UpdatePRActivityTaskCommentCommand, PRActivityTaskComment>(request, entity);

        await _context.SaveChangesAsync(cancellationToken);

        return await _context.PRActivityTaskComments
            .Include(e => e.Owner)
            .ProjectTo<PRActivityTaskCommentDto>(_mapper.ConfigurationProvider)
            .FirstAsync(e => e.Id == entity.Id, cancellationToken);
    }
}
