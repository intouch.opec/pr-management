using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Constants;

namespace PRManagement.Application.GetPRActivityTaskComments.Queries.GetPRActivityTaskCommands;

public class GetPRActivityTaskCommandsQueryValidator : AbstractValidator<GetPRActivityTaskCommentsQuery>
{
    private readonly IApplicationDbContext _context;

    public GetPRActivityTaskCommandsQueryValidator(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(e => e.PRActivityTaskId)
           .StringIsGuid()
           .NotEmpty()
           .MustAsync(_context.PRActivityTasks.HasPRActivityTask)
           .WithMessage(ValidatorMessages.NotFound);

    }
}