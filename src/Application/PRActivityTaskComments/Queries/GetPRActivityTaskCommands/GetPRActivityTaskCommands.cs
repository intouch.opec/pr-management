﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.PRActivityTaskComments.Commands.CreatePRActivityTask;

namespace PRManagement.Application.GetPRActivityTaskComments.Queries.GetPRActivityTaskCommands;

public record GetPRActivityTaskCommentsQuery(string PRActivityTaskId) : IRequest<IEnumerable<PRActivityTaskCommentDto>>;

public class GetPRActivityTaskCommandsQueryHandler : IRequestHandler<GetPRActivityTaskCommentsQuery, IEnumerable<PRActivityTaskCommentDto>>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public GetPRActivityTaskCommandsQueryHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<IEnumerable<PRActivityTaskCommentDto>> Handle(GetPRActivityTaskCommentsQuery request, CancellationToken cancellationToken)
    {
        return await _context.PRActivityTaskComments
            .AsTracking()
            .Where(e => e.PRActivityTaskId.Equals(request.PRActivityTaskId.ToGuid()))
            .ProjectTo<PRActivityTaskCommentDto>(_mapper.ConfigurationProvider)
            .ToListAsync(cancellationToken);
    }
}
