using PRManagement.Domain.Entities;

namespace PRManagement.Application.PRActivities.Queries.GetPRActivityWithPagination;

public class MappingProfile
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<PRActivity, PRActivityItem>()
                .ForMember(dest => dest.ClientName, op => op.MapFrom(src => src.Client!.Name))
                .ForMember(dest => dest.SectionName, op => op.MapFrom(src => src.Section!.Name))
                .ForMember(dest => dest.SubSectionName, op => op.MapFrom(src => src.SubSection!.Name))
                .ForMember(dest => dest.WorkProgress, op => op.MapFrom(src => src.WorkProgress!.Select(wp => wp.WorkTask)))
                .ForMember(dest => dest.OwnerName, op => op.MapFrom(src => src.Owner!.Email));

        }
    }
}
