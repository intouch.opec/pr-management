﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.Common.Mappings;
using PRManagement.Application.Common.Models;
using PRManagement.Domain.Enums;
using PRManagement.Domain.Exceptions;

namespace PRManagement.Application.PRActivities.Queries.GetPRActivityWithPagination;

public record GetPRActivityWithPaginationQuery : IRequest<PaginatedList<PRActivityItem>>
{
    public string? Title { get; set; }
    public string? DurationType { get; set; }
    public string? ContractType { get; set; }
    public string? ContentPRActivityType { get; set; }
    public string? PrStatus { get; set; }
    public DateTimeOffset? FromStarted { get; set; }
    public DateTimeOffset? ToStarted { get; set; }
    public DateTimeOffset? FromEnded { get; set; }
    public DateTimeOffset? ToEnded { get; set; }
    public DateTimeOffset? FromReleased { get; set; }
    public DateTimeOffset? ToReleased { get; set; }
    public string? ClientId { get; set; }
    public string? SectionId { get; set; }
    public string? SubSectionId { get; set; }
    public string? OwnerId { get; set; }
    public int? PageNumber { get; init; } = 1;
    public int? PageSize { get; init; } = 10;
    public SortType? Sort { get; set; } = SortType.DESC;
    public string? SortBy { get; set; } = nameof(PRActivityItem.LastModified);
}

public class GetPRActivityWithPaginationQueryHandler : IRequestHandler<GetPRActivityWithPaginationQuery, PaginatedList<PRActivityItem>>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public GetPRActivityWithPaginationQueryHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<PaginatedList<PRActivityItem>> Handle(GetPRActivityWithPaginationQuery request, CancellationToken cancellationToken)
    {
        return await Search(request)
           .ProjectTo<PRActivityItem>(_mapper.ConfigurationProvider)
           .OrderBy(request.SortBy ?? string.Empty, ascending: request.Sort)
           .PaginatedListAsync(request.PageNumber ?? 1, request.PageSize ?? 10, cancellationToken);
    }

    internal IQueryable Search(GetPRActivityWithPaginationQuery request)
        => _context.PRActivities
            .Include(e => e.Section)
            .Include(e => e.SubSection)
            .Include(e => e.Client)
            .Include(e => e.WorkProgress)
            .Include(e => e.Owner)
            .AsNoTracking()
            .WhereLike(e => e.Title.ToLower(), request.Title)
            .WhereEqual(e => e.DurationType, request.DurationType.ToEnumOrNull<DurationPRActivityType>())
            .WhereEqual(e => e.ContractType, request.ContractType?.ToEnumOrNull<ContractType>())
            .WhereEqual(e => e.ContentPRActivityType, request.ContentPRActivityType?.ToEnumOrNull<ContentPRActivityType>())
            .WhereEqual(e => e.PrStatus, request.PrStatus?.ToEnumOrNull<PrStatus>())
            .WhereEqual(e => e.ClientId, request.ClientId.ToGuid())
            .WhereEqual(e => e.SectionId, request.SectionId.ToGuid())
            .WhereEqual(e => e.OwnerId, request.OwnerId)
            .WhereEqual(e => e.SubSectionId, request.SubSectionId.ToGuid())
            .WhereRange(e => e.Started, request.FromStarted, request.ToStarted)
            .WhereRange(e => e.Ended, request.FromEnded, request.ToEnded)
            .WhereRange(e => e.Released, request.FromReleased, request.ToReleased);
}
