using PRManagement.Application.PRActivities.Commands.CreatePRActivity;

namespace PRManagement.Application.PRActivities.Queries.GetPRActivityWithPagination;

public class PRActivityItem : PRActivityDto
{
    public string ClientName { get; set; } = string.Empty;
    public string SectionName { get; set; } = string.Empty;
    public string SubSectionName { get; set; } = string.Empty;
    public string OwnerName { get; set; } = string.Empty;
}