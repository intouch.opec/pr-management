﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.PRActivities.Queries.GetPRActivityWithPagination;

namespace PRManagement.Application.PRActivities.Queries.GetPRActivity;

public record GetPRActivityQuery(string Id) : IRequest<PRActivityItem>;

public class GetPRActivityQueryHandler : IRequestHandler<GetPRActivityQuery, PRActivityItem>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public GetPRActivityQueryHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<PRActivityItem> Handle(GetPRActivityQuery request, CancellationToken cancellationToken)
    {
         var entity = await _context.PRActivities
            .AsTracking()
            .SingleOrDefaultAsync(
                e => e.Id.Equals(request.Id.ToGuidOnly()),
                cancellationToken);

        Guard.Against.NotFound(request.Id.ToGuidOnly(), entity);

        return _mapper.Map<PRActivityItem>(entity);
    }
}
