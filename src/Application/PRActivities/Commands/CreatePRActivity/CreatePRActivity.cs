﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.PRActivities.Commands.CreatePRActivity;

public record CreatePRActivityCommand : IRequest<PRActivityDto>
{
    public string Title { get; set; } = string.Empty;
    public string DurationType { get; set; } = string.Empty;
    public string ContractType { get; set; } = string.Empty;
    public string ContentPRActivityType { get; set; } = string.Empty;
    public DateTimeOffset Started { get; set; }
    public DateTimeOffset Ended { get; set; }
    public DateTimeOffset Released { get; set; }
    public List<string> WorkTasks { get; set; } = new();

    public string ClientId { get; set; } = string.Empty;
    public string SectionId { get; set; } = string.Empty;
    public string SubSectionId { get; set; } = string.Empty;
    public string OwnerId { get; set; } = string.Empty;
}

public class CreatePRActivityCommandHandler : IRequestHandler<CreatePRActivityCommand, PRActivityDto>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public CreatePRActivityCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<PRActivityDto> Handle(CreatePRActivityCommand request, CancellationToken cancellationToken)
    {
        var entity = _mapper.Map<PRActivity>(request);
        var contractType = request.ContractType;

        var client = await _context.Clients
            .AsTracking()
            .SingleOrDefaultAsync(
                e => e.Id.Equals(request.ClientId.ToGuidOnly()) &&
                    !e.ClientTypes.Any(ct => ct.Equals(contractType)),
                cancellationToken);

        if (client != null)
            client.ClientTypes = client.ClientTypes
                .Concat(new[] { contractType })
                .ToArray();

        await _context.PRActivities.AddAsync(entity);
        await _context.SaveChangesAsync(cancellationToken);
        return _mapper.Map<PRActivityDto>(entity);
    }

}
