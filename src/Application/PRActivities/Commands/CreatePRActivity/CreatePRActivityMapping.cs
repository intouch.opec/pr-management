using PRManagement.Application.Common.Extensions;
using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;
using PRManagement.Domain.Exceptions;

namespace PRManagement.Application.PRActivities.Commands.CreatePRActivity;

public class CreatePRActivityMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {

            CreateMap<CreatePRActivityCommand, PRActivity>()
                .ForMember(dest => dest.DurationType, op => op.MapFrom(o => o.DurationType.ToEnum<DurationPRActivityType>()))
                .ForMember(dest => dest.ContractType, op => op.MapFrom(o => o.ContractType.ToEnum<ContractType>()))
                .ForMember(dest => dest.ContentPRActivityType, op => op.MapFrom(o => o.ContentPRActivityType.ToEnum<ContentPRActivityType>()))
                .ForMember(dest => dest.ClientId, op => op.MapFrom(o => o.ClientId.ToGuidOnly()))
                .ForMember(dest => dest.SectionId, op => op.MapFrom(o => o.SectionId.ToGuidOnly()))
                .ForMember(dest => dest.SubSectionId, op => op.MapFrom(o => o.SubSectionId.ToGuidOnly()))
                .ForMember(dest => dest.OwnerId, op => op.MapFrom(o => o.OwnerId.ToGuidOnly()))
                .ForMember(dest => dest.WorkProgress, op => op.MapFrom<WorkProgressValueResolver>())
                .ForMember(dest => dest.PrStatus, op => op.MapFrom(o => PrStatus.PENDING));

            CreateMap<PRActivity, PRActivityDto>()
                .ForMember(dest => dest.WorkProgress, op => op.MapFrom(o => o.WorkProgress!.Select(e => e.WorkTask.GetEnumMemberValue())));

        }
    }
}

public class WorkProgressValueResolver : IValueResolver<CreatePRActivityCommand, PRActivity, ICollection<WorkProgress>?>
{
    public ICollection<WorkProgress>? Resolve(CreatePRActivityCommand source, PRActivity destination, ICollection<WorkProgress>? member, ResolutionContext context)
    {
        return source.WorkTasks.Select(workTask => new WorkProgress()
        {
            WorkTask = workTask.ToEnum<WorkTask>(),
            TaskWorkerStatus = TaskWorkerStatus.PENDING
        }).ToList();
    }

}