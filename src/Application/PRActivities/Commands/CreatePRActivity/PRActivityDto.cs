using PRManagement.Domain.Enums;

namespace PRManagement.Application.PRActivities.Commands.CreatePRActivity;

public class PRActivityDto
{
    public Guid Id { get; set; }
    public string Title { get; set; } = string.Empty;
    public DurationPRActivityType DurationType { get; set; }
    public ContractType ContractType { get; set; }
    public ContentPRActivityType ContentPRActivityType { get; set; }
    public DateTimeOffset Started { get; set; }
    public DateTimeOffset Ended { get; set; }
    public DateTimeOffset Released { get; set; }
    public PrStatus PrStatus { get; set; }
    public Guid ClientId { get; set; }
    public Guid SectionId { get; set; }
    public Guid SubSectionId { get; set; }
    public string OwnerId { get; set; } = string.Empty;
    public IEnumerable<WorkTask> WorkProgress { get; set; } = Array.Empty<WorkTask>();

    public DateTimeOffset Created { get; set; }
    public string? CreatedBy { get; set; }
    public DateTimeOffset LastModified { get; set; }
    public string? LastModifiedBy { get; set; }
}
