using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Constants;
using PRManagement.Domain.Enums;
using PRManagement.Domain.Exceptions;

namespace PRManagement.Application.PRActivities.Commands.CreatePRActivity;

public class CreatePRActivityCommandValidator : AbstractValidator<CreatePRActivityCommand>
{
    private readonly IApplicationDbContext _context;
    private readonly IIdentityService _identityService;
    
    public CreatePRActivityCommandValidator(IApplicationDbContext context
    , IIdentityService identityService
    )
    {
        _context = context;
        _identityService = identityService;

        RuleFor(e => e.SectionId)
            .NotEmpty()
            .StringIsGuid()
            .MustAsync(HasSection)
            .WithMessage(ValidatorMessages.NotFound);

        RuleFor(e => e.OwnerId)
           .NotEmpty()
           .StringIsGuid()
           .MustAsync(HasUserAsync)
           .WithMessage(ValidatorMessages.NotFound);

        RuleFor(e => e.SubSectionId)
            .NotEmpty()
            .StringIsGuid()
            .MustAsync(HasSubSection)
            .WithMessage(ValidatorMessages.NotFound);

        RuleFor(e => e.DurationType)
            .NotEmpty()
            .MustEnum<CreatePRActivityCommand, DurationPRActivityType>();

        RuleFor(e => e.ContractType)
            .NotEmpty()
            .MustEnum<CreatePRActivityCommand, ContractType>();

        RuleFor(e => e.ContentPRActivityType)
            .NotEmpty()
            .MustEnum<CreatePRActivityCommand, ContentPRActivityType>()
            .Must(BeMatchDurationType);

        RuleForEach(e => e.WorkTasks)
            .ChildRules(e =>
                e.RuleFor(c => c).MustEnum<string, WorkTask>());

    }

    internal async Task<bool> HasSection(string sectionId, CancellationToken cancellationToken)
        => !sectionId.IsValidGuid() || await _context.Sections
            .AnyAsync(p => p.Id.Equals(sectionId.ToGuid()), cancellationToken);

    internal async Task<bool> HasSubSection(string subSectionId, CancellationToken cancellationToken)
        => !subSectionId.IsValidGuid() || await _context.SubSections
            .AnyAsync(p => p.Id.Equals(new Guid(subSectionId)), cancellationToken);

    internal bool BeMatchDurationType(CreatePRActivityCommand command, string value)
    {
        var canCheckType = string.IsNullOrEmpty(value) ||
            value.IsEnum<ContentPRActivityType>() ||
            string.IsNullOrEmpty(command.DurationType) ||
            command.DurationType.IsEnum<DurationPRActivityType>();

        if (canCheckType)
            return true;

        var contentPRActivityType = value.ToEnum<ContentPRActivityType>();
        var durationType = command.DurationType.ToEnum<DurationPRActivityType>();
        switch (durationType)
        {
            case DurationPRActivityType.LONG:
                return DurationLongActivity.ContentActivities.Contains(contentPRActivityType);
            case DurationPRActivityType.SHORT:
                return DurationShortActivity.ContentActivities.Contains(contentPRActivityType);
            case DurationPRActivityType.EVENT:
                return DurationEventActivity.ContentActivities.Contains(contentPRActivityType);
            default:
                return false;
        }
    }

    internal async Task<bool> HasUserAsync(string id, CancellationToken cancellationToken)
    {
        if (!id.IsValidGuid()) return true;

        return await _identityService.HasUserAsync(id);
    }

}
