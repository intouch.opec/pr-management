using PRManagement.Application.Common.Extensions;
using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;
using PRManagement.Domain.Exceptions;

namespace PRManagement.Application.PRActivities.Commands.UpdatePRActivity;

public class UpdatePRActivityMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<UpdatePRActivityCommand, PRActivity>()
                .ForMember(dest => dest.DurationType, op => op.MapFrom(o => o.DurationType.ToEnum<DurationPRActivityType>()))
                .ForMember(dest => dest.ContractType, op => op.MapFrom(o => o.ContractType.ToEnum<ContractType>()))
                .ForMember(dest => dest.ContentPRActivityType, op => op.MapFrom(o => o.ContentPRActivityType.ToEnum<ContentPRActivityType>()))
                .ForMember(dest => dest.ClientId, op => op.MapFrom(o => o.ClientId.ToGuidOnly()))
                .ForMember(dest => dest.SectionId, op => op.MapFrom(o => o.SectionId.ToGuidOnly()))
                .ForMember(dest => dest.SubSectionId, op => op.MapFrom(o => o.SubSectionId.ToGuidOnly()))
                .ForMember(dest => dest.OwnerId, op => op.MapFrom(o => o.OwnerId.ToGuidOnly()));

        }
    }
}
