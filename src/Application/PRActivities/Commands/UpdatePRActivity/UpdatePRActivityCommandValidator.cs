using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Constants;
using PRManagement.Domain.Enums;
using PRManagement.Domain.Exceptions;

namespace PRManagement.Application.PRActivities.Commands.UpdatePRActivity;

public class UpdatePRActivityCommandValidator : AbstractValidator<UpdatePRActivityCommand>
{
    private readonly IApplicationDbContext _context;

    public UpdatePRActivityCommandValidator(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(e => e.Id)
            .NotEmpty()
            .StringIsGuid()
            .MustAsync(HasPRActivity)
            .WithMessage(ValidatorMessages.NotFound);

        RuleFor(e => e.OwnerId)
            .NotEmpty()
            .StringIsGuid();

        RuleFor(e => e.SectionId)
            .NotEmpty()
            .StringIsGuid()
            .MustAsync(HasSection)
            .WithMessage(ValidatorMessages.NotFound);

        RuleFor(e => e.SubSectionId)
            .NotEmpty()
            .StringIsGuid()
            .MustAsync(HasSubSection)
            .WithMessage(ValidatorMessages.NotFound);

        RuleFor(e => e.DurationType)
            .NotEmpty()
            .MustEnum<UpdatePRActivityCommand, DurationPRActivityType>();

        RuleFor(e => e.ContractType)
            .NotEmpty()
            .MustEnum<UpdatePRActivityCommand, ContractType>();

        RuleForEach(e => e.WorkTasks)
            .NotEmpty()
            .ChildRules(e =>
                e.RuleFor(c => c).MustEnum<string, WorkTask>());

        RuleFor(e => e.ContentPRActivityType)
            .NotEmpty()
            .MustEnum<UpdatePRActivityCommand, ContentPRActivityType>()
            .Must(BeMatchDurationType);
    }

    internal async Task<bool> HasSection(string sectionId, CancellationToken cancellationToken)
        => !sectionId.IsValidGuid() || await _context.Sections
            .AnyAsync(p => p.Id.Equals(sectionId.ToGuid()), cancellationToken);

    internal async Task<bool> HasSubSection(string subSectionId, CancellationToken cancellationToken)
        => !subSectionId.IsValidGuid() || await _context.SubSections
            .AnyAsync(p => p.Id.Equals(new Guid(subSectionId)), cancellationToken);

    internal async Task<bool> HasPRActivity(string prActivityId, CancellationToken cancellationToken)
       => !prActivityId.IsValidGuid() || await _context.PRActivities
           .AnyAsync(p => p.Id.Equals(new Guid(prActivityId)), cancellationToken);

    internal bool BeMatchDurationType(UpdatePRActivityCommand command, string value)
    {
        var canCheckType = string.IsNullOrEmpty(value) ||
            value.IsEnum<ContentPRActivityType>() ||
            string.IsNullOrEmpty(command.DurationType) ||
            command.DurationType.IsEnum<DurationPRActivityType>();

        if (canCheckType)
            return true;

        var contentPRActivityType = value.ToEnum<ContentPRActivityType>();
        var durationType = command.DurationType.ToEnum<DurationPRActivityType>();
        switch (durationType)
        {
            case DurationPRActivityType.LONG:
                return DurationLongActivity.ContentActivities.Contains(contentPRActivityType);
            case DurationPRActivityType.SHORT:
                return DurationShortActivity.ContentActivities.Contains(contentPRActivityType);
            case DurationPRActivityType.EVENT:
                return DurationEventActivity.ContentActivities.Contains(contentPRActivityType);
            default:
                return false;
        }
    }
}