﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.PRActivities.Commands.CreatePRActivity;
using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;
using PRManagement.Domain.Exceptions;

namespace PRManagement.Application.PRActivities.Commands.UpdatePRActivity;

public record UpdatePRActivityCommand : IRequest<PRActivityDto>
{
    public string Id { get; set; } = string.Empty;
    public string Title { get; set; } = string.Empty;
    public string DurationType { get; set; } = string.Empty;
    public string ContractType { get; set; } = string.Empty;
    public string ContentPRActivityType { get; set; } = string.Empty;
    public DateTimeOffset Started { get; set; }
    public DateTimeOffset Ended { get; set; }
    public DateTimeOffset Released { get; set; }
    public List<string> WorkTasks { get; set; } = new();

    public string ClientId { get; set; } = string.Empty;
    public string SectionId { get; set; } = string.Empty;
    public string SubSectionId { get; set; } = string.Empty;
    public string OwnerId { get; set; } = string.Empty;
}

public class UpdatePRActivityCommandHandler : IRequestHandler<UpdatePRActivityCommand, PRActivityDto>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public UpdatePRActivityCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<PRActivityDto> Handle(UpdatePRActivityCommand request, CancellationToken cancellationToken)
    {
        var entity = await _context.PRActivities
            .AsTracking()
            .Include(e => e.WorkProgress)
            .SingleOrDefaultAsync(
                e => e.Id.Equals(request.Id.ToGuidOnly()),
                cancellationToken);

        Guard.Against.NotFound(request.Id.ToGuidOnly(), entity);

        _mapper.Map<UpdatePRActivityCommand, PRActivity>(request, entity);
        WorkProgressMapper(request, ref entity);
        await _context.SaveChangesAsync(cancellationToken);

        return _mapper.Map<PRActivityDto>(entity);
    }

    public void WorkProgressMapper(UpdatePRActivityCommand source, ref PRActivity destination)
    {
        if (destination.WorkProgress == null)
            destination.WorkProgress = new List<WorkProgress>();

        foreach (var item in destination.WorkProgress!.ToList() ?? [])
        {
            var isItemRemove = !source.WorkTasks
                .Select(wt => wt.ToEnumOrNull<WorkTask>())
                .Contains(item.WorkTask);

            if (isItemRemove)
                destination.WorkProgress!.Remove(item);

        }

        var existingWorkTasks = destination.WorkProgress
            .Select(wp => wp.WorkTask)
            .ToHashSet();

        foreach (var workTask in source.WorkTasks
            .Select(wt => wt.ToEnum<WorkTask>())
            .Where(wt => !existingWorkTasks.Contains(wt!)))
        {
            destination.WorkProgress.Add(new WorkProgress
            {
                WorkTask = workTask,
                TaskWorkerStatus = TaskWorkerStatus.PENDING,
                PRActivityId = source.Id.ToGuidOnly()
            });
        }
    }
}
