using PRManagement.Application.Common.Extensions;

namespace PRManagement.Application.PRActivities.Commands.RemovePRActivity;

public class RemovePRActivityCommandValidator : AbstractValidator<RemovePRActivityCommand>
{
    public RemovePRActivityCommandValidator()
    {

        RuleFor(command => command.Id)
            .StringIsGuid();

        RuleFor(command => command.Remark)
            .MaximumLength(155)
            .NotEmpty();
    }

   
}