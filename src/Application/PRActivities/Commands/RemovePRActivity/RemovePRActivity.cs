﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.PRActivities.Commands.CreatePRActivity;

namespace PRManagement.Application.PRActivities.Commands.RemovePRActivity;

public record RemovePRActivityCommand : IRequest<PRActivityDto>
{
    public string Id { get; set; } = string.Empty;
    public string Remark { get; set; } = string.Empty;
}

public class RemovePRActivityCommandHandler : IRequestHandler<RemovePRActivityCommand, PRActivityDto>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public RemovePRActivityCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<PRActivityDto> Handle(RemovePRActivityCommand request, CancellationToken cancellationToken)
    {
        var entity = await _context.PRActivities
            .AsTracking()
            .SingleOrDefaultAsync(e => e.Id.Equals(request.Id.ToGuid()), cancellationToken);

        Guard.Against.NotFound(request.Id, entity);

        _context.PRActivities.RemoveAndAddRemark(entity, request.Remark);

        await _context.SaveChangesAsync(cancellationToken);

        return _mapper.Map<PRActivityDto>(entity);
    }
}
