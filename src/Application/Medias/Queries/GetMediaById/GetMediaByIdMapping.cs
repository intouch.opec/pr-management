using PRManagement.Application.Medias.Queries.GetMediaWithPagination;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.Medias.Queries.GetMediaById;
public class GetMediaByIdMapping
{
	public sealed class Mapping : Profile
	{
		public Mapping()
		{
			CreateMap<Contact, ContactItem>()
				.ForMember(s => s.LineId, op => op.MapFrom(o => o.LineId))
				.ForMember(s => s.Address, op => op.MapFrom(o => o.Address!.Text ?? string.Empty))
				.ForMember(s => s.Email, op => op.MapFrom(o => o.Address!.Email ?? Array.Empty<string>()))
				.ForMember(s => s.PhoneNumbers, op => op.MapFrom(o => o.Address!.PhoneNumbers ?? Array.Empty<string>()))
				.ForMember(s => s.ProvinceId, op => op.MapFrom(o => o.Address!.ProvinceId ?? string.Empty))
				.ForMember(s => s.DistrictId, op => op.MapFrom(o => o.Address!.DistrictId ?? string.Empty))
				.ForMember(s => s.SubDistrictId, op => op.MapFrom(o => o.Address!.SubDistrictId ?? string.Empty))
				.ForMember(s => s.PostCode, op => op.MapFrom(o => o.Address!.PostCode ?? string.Empty))
				.ForMember(s => s.ProvinceEN, op => op.MapFrom(o => o.Address!.Province!.NameEN ?? string.Empty))
				.ForMember(s => s.ProvinceTH, op => op.MapFrom(o => o.Address!.Province!.NameTH ?? string.Empty))
				.ForMember(s => s.DistrictEN, op => op.MapFrom(o => o.Address!.District!.NameEN ?? string.Empty))
				.ForMember(s => s.DistrictTH, op => op.MapFrom(o => o.Address!.District!.NameTH ?? string.Empty))
				.ForMember(s => s.SubDistrictEN, op => op.MapFrom(o => o.Address!.SubDistrict!.NameEN ?? string.Empty))
				.ForMember(s => s.SubDistrictTH, op => op.MapFrom(o => o.Address!.SubDistrict!.NameTH ?? string.Empty));

			CreateMap<MediaSection, MediaSectionItemById>()
				.ForMember(s => s.Contacts, op => op.MapFrom(o => o.ContactMediaSections!.OrderBy(cms => cms.MediaSection!.Section.Name).Select(cms => cms.Contact)))
				.ForMember(s => s.Email, op => op.MapFrom(o => o.Address!.Email))
				.ForMember(s => s.PhoneNumbers, op => op.MapFrom(o => o.Address!.PhoneNumbers));

			CreateMap<Platform, PlatformItem>();

			CreateMap<Media, GetMediaByIdResponse>()
				.ForMember(s => s.Email, op => op.MapFrom(o => o.Address!.Email ?? Array.Empty<string>()))
				.ForMember(s => s.PhoneNumbers, op => op.MapFrom(o => o.Address!.PhoneNumbers ?? Array.Empty<string>()))
				.ForMember(s => s.Address, op => op.MapFrom(o => o.Address!.Text ?? string.Empty))
				.ForMember(s => s.ProvinceId, op => op.MapFrom(o => o.Address!.ProvinceId ?? string.Empty))
				.ForMember(s => s.DistrictId, op => op.MapFrom(o => o.Address!.DistrictId ?? string.Empty))
				.ForMember(s => s.SubDistrictId, op => op.MapFrom(o => o.Address!.SubDistrictId ?? string.Empty))
				.ForMember(s => s.PostCode, op => op.MapFrom(o => o.Address!.PostCode ?? string.Empty))
				.ForMember(s => s.ProvinceEN, op => op.MapFrom(o => o.Address!.Province!.NameEN ?? string.Empty))
				.ForMember(s => s.ProvinceTH, op => op.MapFrom(o => o.Address!.Province!.NameTH ?? string.Empty))
				.ForMember(s => s.DistrictEN, op => op.MapFrom(o => o.Address!.District!.NameEN ?? string.Empty))
				.ForMember(s => s.DistrictTH, op => op.MapFrom(o => o.Address!.District!.NameTH ?? string.Empty))
				.ForMember(s => s.SubDistrictEN, op => op.MapFrom(o => o.Address!.SubDistrict!.NameEN ?? string.Empty))
				.ForMember(s => s.SubDistrictTH, op => op.MapFrom(o => o.Address!.SubDistrict!.NameTH ?? string.Empty))
				.ForMember(s => s.MediaSections, op => op.MapFrom(o => o.MediaSections.OrderBy(ms => ms.Section.Name)))
				.ForMember(s => s.PercentageOfFullField, op => op.MapFrom(o => o.PercentageOfFullField.Equals(0) ? 
					CalculatePercentageOfFullField(o) : 
					o.PercentageOfFullField));
		}
	}

	public static double CalculatePercentageOfFullField(Media media)
	{
		return ((media.Address?.Email.Any() == true ? 1 : 0) +
				(media.Address?.PhoneNumbers.Any() == true ? 1 : 0) +
				(!string.IsNullOrWhiteSpace(media.Name) ? 1 : 0) +
				(!string.IsNullOrWhiteSpace(media.Address?.Text) ? 1 : 0) +
				(!string.IsNullOrWhiteSpace(media.Address?.PostCode) ? 1 : 0) +
				(!string.IsNullOrWhiteSpace(media.Address?.ProvinceId) ? 1 : 0) +
				(!string.IsNullOrWhiteSpace(media.Address?.DistrictId) ? 1 : 0) +
				(!string.IsNullOrWhiteSpace(media.Address?.SubDistrictId) ? 1 : 0))
				/ 8.0 * 100;
	}
}