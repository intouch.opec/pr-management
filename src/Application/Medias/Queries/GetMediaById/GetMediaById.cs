using PRManagement.Application.Common.Interfaces;

namespace PRManagement.Application.Medias.Queries.GetMediaById;
public record GetMediaByIdQuery(Guid Id) : IRequest<GetMediaByIdResponse>;

public class GetMediaByIdQueryHandler : IRequestHandler<GetMediaByIdQuery, GetMediaByIdResponse>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public GetMediaByIdQueryHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<GetMediaByIdResponse> Handle(GetMediaByIdQuery request, CancellationToken cancellationToken)
    {
        var entity = await _context.Medias
          .Include(e => e.Address)
          .Include(a => a.Address!.Province)
          .Include(a => a.Address!.District)
          .Include(a => a.Address!.SubDistrict)
          .Include(a => a.MediaSections)
          .ThenInclude(ms => ms.Section)
          .Include(a => a.MediaSections)
          .ThenInclude(ms => ms.SubSection)
          .Include(a => a.MediaSections)
          .ThenInclude(ms => ms.Platforms)
          .Include(a => a.MediaSections.Where(cms => cms != null))
          .ThenInclude(m => m.ContactMediaSections!.Where(cms => cms.Contact != null))
          .ThenInclude(cms => cms.Contact)
          .ThenInclude(c => c!.Address)
          .ThenInclude(a => a!.Province)
          .Include(e => e.MediaSections)
          .ThenInclude(ms => ms.ContactMediaSections!.Where(cms => cms.Contact != null))
          .ThenInclude(cms => cms.Contact)
          .ThenInclude(c => c!.Address)
          .ThenInclude(a => a!.District)
          .Include(e => e.MediaSections)
          .ThenInclude(ms => ms.ContactMediaSections!.Where(cms => cms.Contact != null))
          .ThenInclude(cms => cms.Contact)
          .ThenInclude(c => c!.Address)
          .ThenInclude(a => a!.SubDistrict)
          .AsNoTracking()
          .ProjectTo<GetMediaByIdResponse>(_mapper.ConfigurationProvider)
          .FirstOrDefaultAsync(e => e.Id.Equals(request.Id));

        Guard.Against.NotFound(request.Id, entity);

        return entity;
    }
}