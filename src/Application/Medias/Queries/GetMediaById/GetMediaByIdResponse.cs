using PRManagement.Application.Medias.Queries.GetMediaWithPagination;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.Medias.Queries.GetMediaById;
public class GetMediaByIdResponse
{
    public Guid Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public string Address { get; set; } = string.Empty;
    public string[] Email { get; set; } = Array.Empty<string>();
    public string[] PhoneNumbers { get; set; } = Array.Empty<string>();
    public string? ProvinceId { get; set; }
    public string? ProvinceEN { get; set; }
    public string? ProvinceTH { get; set; }
    public string DistrictId { get; set; } = string.Empty;
    public string DistrictEN { get; set; } = string.Empty;
    public string DistrictTH { get; set; } = string.Empty;
    public string SubDistrictId { get; set; } = string.Empty;
    public string SubDistrictEN { get; set; } = string.Empty;
    public string SubDistrictTH { get; set; } = string.Empty;
    public string PostCode { get; set; } = string.Empty;
    public double PercentageOfFullField { get; set; } = 0;
    public IEnumerable<MediaSectionItemById> MediaSections { get; set; } = Array.Empty<MediaSectionItemById>();
    public DateTimeOffset Created { get; set; }
    public string? CreatedBy { get; set; }
    public DateTimeOffset LastModified { get; set; }
    public string? LastModifiedBy { get; set; }
}

public class MediaSectionItemById
{
    public Guid Id { get; set; }
    public TypeOfMedia TypeOfMedia { get; set; }
    public Guid SectionId { get; set; }
    public string SectionName { get; set; } = string.Empty;
    public Guid SubSectionId { get; set; }
    public string SubSectionName { get; set; } = string.Empty;
    public IEnumerable<ContactItem> Contacts { get; set; } = Enumerable.Empty<ContactItem>();
    public IEnumerable<PlatformItem> Platforms { get; set; } = Enumerable.Empty<PlatformItem>();
    public IEnumerable<string> Email { get; set; } = Enumerable.Empty<string>();
    public IEnumerable<string> PhoneNumbers { get; set; } = Enumerable.Empty<string>();
}

public class ContactItem
{
    public Guid Id { get; set; }
    public string FirstName { get; set; } = string.Empty;
    public string LastName { get; set; } = string.Empty;
    public string NickName { get; set; } = string.Empty;
    public string Position { get; set; } = string.Empty;
    public string LineId { get; set; } = string.Empty;
    public string Address { get; set; } = string.Empty;
    public string[] Email { get; set; } = Array.Empty<string>();
    public string[] PhoneNumbers { get; set; } = Array.Empty<string>();
    public string? ProvinceId { get; set; }
    public string? ProvinceEN { get; set; }
    public string? ProvinceTH { get; set; }
    public string DistrictId { get; set; } = string.Empty;
    public string DistrictEN { get; set; } = string.Empty;
    public string DistrictTH { get; set; } = string.Empty;
    public string SubDistrictId { get; set; } = string.Empty;
    public string SubDistrictEN { get; set; } = string.Empty;
    public string SubDistrictTH { get; set; } = string.Empty;
    public string PostCode { get; set; } = string.Empty;
    public AddressType? AddressType { get; set; }
    public DateTimeOffset Created { get; set; }
    public string? CreatedBy { get; set; }
    public DateTimeOffset LastModified { get; set; }
    public string? LastModifiedBy { get; set; }
    public bool IsDeleted { get; set; } = false;
    public DateTimeOffset? Deleted { get; set; }
}