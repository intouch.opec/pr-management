using PRManagement.Domain.Entities;
using PRManagement.Application.Common.BusinessLogics;

namespace PRManagement.Application.Medias.Queries.GetMediaWithPagination;
public class GetMediaWithPaginationMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<Media, GetMediaWithPaginationResponse>()
                .ForMember(e => e.PostCode, op => op.MapFrom(o => o.Address!.PostCode ?? string.Empty))
                .ForMember(e => e.Address, op => op.MapFrom(o => o.Address!.Text ?? string.Empty))
                .ForMember(e => e.Email, op => op.MapFrom(o => o.Address!.Email ?? null))
                .ForMember(e => e.PhoneNumbers, op => op.MapFrom(o => o.Address!.PhoneNumbers ?? null))
                .ForMember(e => e.ProvinceId, op => op.MapFrom(o => o.Address!.ProvinceId ?? null))
                .ForMember(e => e.DistrictId, op => op.MapFrom(o => o.Address!.DistrictId ?? null))
                .ForMember(e => e.SubDistrictId, op => op.MapFrom(o => o.Address!.SubDistrictId ?? null))
                .ForMember(e => e.MediaSections, op => op.MapFrom(o =>
                    o.MediaSections.GroupBy(ms => ms.SectionId)
                    .Select(e => new MediaSectionItem()
                    {
                        SectionId = e.Key,
                        SectionName = e.First().Section.Name,
                        SubSections = e.GroupBy(ss => ss.SubSectionId)
                            .Select(ss => new SubSectionItem()
                            {
                                SubSectionId = ss.Key,
                                SubSectionName = ss.First().SubSection.Name,
                                TypeOfMedias = ss.GroupBy(e => e.TypeOfMedia).Select(me => me.Key),
                                Platforms = ss.SelectMany(s => s.Platforms.Select(pl => pl.PlatFormType))
                            })
                    })
                    ))
                .ForMember(e => e.PercentageOfFullField, op => op.MapFrom(o => o.PercentageOfFullField.Equals(0) ?
                    CalculatePercentageOfFullFieldExtensions.CalculateMedia(o) :
                    o.PercentageOfFullField));

            CreateMap<Platform, PlatformItem>();
            CreateMap<MediaSection, MediaSectionItem>();

        }
    }


}