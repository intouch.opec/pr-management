using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.Common.Mappings;
using PRManagement.Application.Common.Models;
using PRManagement.Domain.Enums;
using PRManagement.Domain.Exceptions;

namespace PRManagement.Application.Medias.Queries.GetMediaWithPagination;
public record GetMediaWithPaginationQuery : IRequest<PaginatedList<GetMediaWithPaginationResponse>>
{
    public string? Address { get; set; } = string.Empty;
    public string? Name { get; set; } = string.Empty;
    public string? Email { get; set; } = string.Empty;
    public string? PhoneNumber { get; set; } = string.Empty;
    public string? Search { get; set; } = string.Empty;
    public bool? IsFullField { get; set; }
    public DateTimeOffset? CreatedStart { get; set; }
    public DateTimeOffset? CreatedEnd { get; set; }
    public Guid? SectionId { get; set; }
    public Guid? SubSectionId { get; set; }
    public string? TypeOfMedia { get; set; } = string.Empty;
    public uint? TierPotential { get; set; }
    
    public int? PageNumber { get; init; } = 1;
    public int? PageSize { get; init; } = 10;
    public SortType? Sort { get; set; } = SortType.DESC;
    public string? SortBy { get; set; } = nameof(GetMediaWithPaginationResponse.LastModified);
}

public class GetMediaWithPaginationQueryHandler :
    IRequestHandler<GetMediaWithPaginationQuery, PaginatedList<GetMediaWithPaginationResponse>>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public GetMediaWithPaginationQueryHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<PaginatedList<GetMediaWithPaginationResponse>> Handle(GetMediaWithPaginationQuery request, CancellationToken cancellationToken)
    {
        return await Search(request)
          .ProjectTo<GetMediaWithPaginationResponse>(_mapper.ConfigurationProvider)
          .OrderBy(request.SortBy ?? nameof(GetMediaWithPaginationResponse.LastModified), ascending: request.Sort ?? SortType.DESC)
          .PaginatedListAsync(request.PageNumber ?? 1, request.PageSize ?? 10, cancellationToken);
    }

    internal IQueryable Search(GetMediaWithPaginationQuery request)
    {
        var query = _context.Medias
          .Include(e => e.Address)
          .Include(e => e.MediaSections.Where(ms => ms != null))
          .ThenInclude(ms => ms.Platforms)
          .Include(e => e.MediaSections.Where(ms => ms != null))
          .ThenInclude(ms => ms.Section)
          .Include(e => e.MediaSections.Where(ms => ms != null))
          .ThenInclude(ms => ms.SubSection)
          .Include(e => e.MediaSections.Where(ms => ms != null))
          .ThenInclude(ms => ms.Address)
          .AsNoTracking()
          .WhereRange(e => e.Created, request.CreatedStart, request.CreatedEnd)
          .WhereLikeArray(e => e.Address!.Email.Select(e => e.ToLower()).ToArray(), request.Email)
          .WhereLike(e => e.Address!.Text.ToLower(), request.Address)
          .WhereLike(e => e.Name.ToLower(), request.Name)
          .WhereLikeArray(e => e.Address!.PhoneNumbers, request.PhoneNumber)
          .WhereEqualArray(e => e.MediaSections,
            item => item.TierPotential,
            request.TierPotential)
          .WhereEqualArray(e => e.MediaSections,
            item => item.TypeOfMedia!,
            request.TypeOfMedia?.ToEnum<TypeOfMedia>())
          .WhereEqualArray(e => e.MediaSections!,
            item => item!.SectionId,
            request.SectionId)
          .WhereEqualArray(e => e.MediaSections!,
            item => item!.SubSectionId,
            request.SubSectionId)
          .SearchOrMultiple(request.Search,
            e => e.Name,
            e => e.Address!.Text,
            e => e.Address!.PhoneNumbers,
            e => e.Address!.Email,
            e => e.MediaSections.Select(ms => ms.Section.Name),
            e => e.MediaSections.Select(ms => ms.SubSection.Name)
          );

        if (request.IsFullField != null)
            query = query.Where(e => (bool)request.IsFullField ?
                e.PercentageOfFullField.Equals(100) :
                !e.PercentageOfFullField.Equals(100));

        return query;
    }
}
