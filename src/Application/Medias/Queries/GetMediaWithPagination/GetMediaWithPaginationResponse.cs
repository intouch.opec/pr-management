using PRManagement.Domain.Enums;

namespace PRManagement.Application.Medias.Queries.GetMediaWithPagination;
public class GetMediaWithPaginationResponse
{
    public Guid Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public string? Address { get; set; } = string.Empty;
    public IEnumerable<string> Email { get; set; } = Array.Empty<string>();
    public IEnumerable<string> PhoneNumbers { get; set; } = Array.Empty<string>();
    public IEnumerable<MediaSectionItem> MediaSections { get; set; } = Array.Empty<MediaSectionItem>();
    public string? PostCode { get; set; } = string.Empty;
    public string? ProvinceId { get; set; }
    public string? DistrictId { get; set; }
    public string? SubDistrictId { get; set; }
    public double PercentageOfFullField { get; set; }
    public DateTimeOffset Created { get; set; }
    public string? CreatedBy { get; set; }
    public DateTimeOffset LastModified { get; set; }
    public string? LastModifiedBy { get; set; }
}

public class PlatformItem
{
    public Guid Id { get; set; }
    public PlatformType? PlatformType { get; set; }
    public string Name { get; set; } = string.Empty;
}

public class MediaSectionItem
{
    public Guid SectionId { get; set; }
    public string SectionName { get; set; } = string.Empty;
    public IEnumerable<SubSectionItem> SubSections { get; set; } = Enumerable.Empty<SubSectionItem>();
}

public class SubSectionItem
{
    public Guid SubSectionId { get; set; }
    public string SubSectionName { get; set; } = string.Empty;
    public IEnumerable<TypeOfMedia> TypeOfMedias { get; set; } = Enumerable.Empty<TypeOfMedia>();
    public IEnumerable<PlatformType> Platforms { get; set; } = Enumerable.Empty<PlatformType>();
}