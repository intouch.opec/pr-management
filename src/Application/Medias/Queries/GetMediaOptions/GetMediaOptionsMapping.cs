using PRManagement.Domain.Entities;

namespace PRManagement.Application.Medias.Queries.GetMediaOptions;

public class GetMediaOptionsMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<Media, GetMediaOptionsResponse>();
        }
    }
}
