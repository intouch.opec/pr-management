using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.Medias.Queries.GetMediaOptions;
public class GetMediaOptionsQuery : IRequest<IEnumerable<GetMediaOptionsResponse>>
{
    public string Name { get; set; } = string.Empty;
}

public class GetMediaOptionsQueryHandle : IRequestHandler<GetMediaOptionsQuery, IEnumerable<GetMediaOptionsResponse>>
{
    private readonly IApplicationDbContext _context;
    public readonly IMapper _mapper;

    public GetMediaOptionsQueryHandle(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<IEnumerable<GetMediaOptionsResponse>> Handle(GetMediaOptionsQuery request, CancellationToken cancellationToken)
    {
        return await Search(request)
          .ProjectTo<GetMediaOptionsResponse>(_mapper.ConfigurationProvider)
          .ToListAsync(cancellationToken);
    }

    internal IQueryable<Media> Search(GetMediaOptionsQuery request)
    {
        return _context.Medias
          .Include(me => me.MediaSections)
          .AsNoTracking()
          .WhereLike(e => e.Name.ToLower(), request.Name.ToLower());
    }
}