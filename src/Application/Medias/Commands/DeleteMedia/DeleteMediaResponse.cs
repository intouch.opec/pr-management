namespace PRManagement.Application.Medias.Commands.DeleteMedia;

public class DeleteMediaResponse
{
    public Guid Id { get; set; }
}
