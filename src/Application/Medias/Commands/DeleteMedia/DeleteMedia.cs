using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;

namespace PRManagement.Application.Medias.Commands.DeleteMedia;

// [Authorize(Roles = Roles.ADTeam)]
// [Authorize(Policy = Policies.CanManageContact)]
public class DeleteMediaCommand : IRequest<DeleteMediaResponse>
{
    public Guid Id { get; set; }
    public string Remark { get; set; } = string.Empty;
}

public class DeleteMediaCommandHandler : IRequestHandler<DeleteMediaCommand, DeleteMediaResponse>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public DeleteMediaCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<DeleteMediaResponse> Handle(DeleteMediaCommand request, CancellationToken cancellationToken)
    {
        var entity = await _context.Medias
          .SingleOrDefaultAsync(e => e.Id.Equals(request.Id), cancellationToken);

        Guard.Against.NotFound(request.Id, entity);

        _context.Medias.RemoveAndAddRemark(entity, request.Remark);

        await _context.SaveChangesAsync(cancellationToken);

        return _mapper.Map<DeleteMediaResponse>(entity);
    }
}
