using PRManagement.Domain.Entities;

namespace PRManagement.Application.Medias.Commands.DeleteMedia;

public class DeleteMediaMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<Media, DeleteMediaResponse>();
        }
    }
}
