using PRManagement.Domain.Entities;
using PRManagement.Application.Common.BusinessLogics;

namespace PRManagement.Application.Medias.Commands.UpdateMedia;
public class UpdateMediaMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<UpdateMediaCommand, Media>()
              .ForMember(s => s.Id, s => s.Ignore())
              .ForMember(s => s.Address, op => op.MapFrom<UpdateMediaCommandAddressResolver>())
              .ForMember(s => s.PercentageOfFullField, op => op.MapFrom((o, s) => CalculatePercentageOfFullFieldExtensions.CalculateUpdateMediaCommand(o, s)));

            CreateMap<UpdateMediaCommand, Address>()
              .ForMember(s => s.Id, s => s.Ignore())
              .ForMember(s => s.Text, op => op.MapFrom(o => o.Address));

            CreateMap<Media, UpdateMediaResponse>()
              .ForMember(s => s.Email, op => op.MapFrom(o => o.Address!.Email ?? Array.Empty<string>()))
              .ForMember(s => s.PhoneNumbers, op => op.MapFrom(o => o.Address!.PhoneNumbers ?? Array.Empty<string>()))
              .ForMember(s => s.Address, op => op.MapFrom(o => o.Address!.Text ?? string.Empty))
              .ForMember(s => s.ProvinceId, op => op.MapFrom(o => o.Address!.ProvinceId ?? string.Empty))
              .ForMember(s => s.DistrictId, op => op.MapFrom(o => o.Address!.DistrictId ?? string.Empty))
              .ForMember(s => s.SubDistrictId, op => op.MapFrom(o => o.Address!.SubDistrictId ?? string.Empty))
              .ForMember(s => s.PostCode, op => op.MapFrom(o => o.Address!.PostCode ?? string.Empty));
        }

        public class UpdateMediaCommandAddressResolver : IValueResolver<UpdateMediaCommand, Media, Address?>
        {
            private readonly IMapper _mapper;

            public UpdateMediaCommandAddressResolver(IMapper mapper)
            {
                _mapper = mapper;
            }

            public Address? Resolve(UpdateMediaCommand source, Media destination, Address? member, ResolutionContext context)
            {
                if (SomePropertiesAddressHaveValue(source))
                {
                    member = _mapper.Map<UpdateMediaCommand, Address?>(source, destination.Address);
                    if (destination.AddressId is not null)
                        member!.Id = (Guid)destination.AddressId;
                    else
                        member!.Id = Guid.Empty;
                }
                return member;
            }

            public static bool SomePropertiesAddressHaveValue(UpdateMediaCommand source)
            {
                if (source is null)
                    return false;

                return source.Email.Any() ||
                       source.PhoneNumbers.Any() ||
                       !string.IsNullOrEmpty(source.Address) ||
                       !string.IsNullOrEmpty(source.PostCode) ||
                       !string.IsNullOrEmpty(source.ProvinceId) ||
                       !string.IsNullOrEmpty(source.DistrictId) ||
                       !string.IsNullOrEmpty(source.SubDistrictId);
            }
        }
    }

}
