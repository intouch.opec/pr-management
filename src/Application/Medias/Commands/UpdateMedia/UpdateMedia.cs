using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.Medias.Commands.UpdateMedia;
public record UpdateMediaCommand : IRequest<UpdateMediaResponse>
{
    public Guid Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public string Address { get; set; } = string.Empty;
    public string[] Email { get; set; } = Array.Empty<string>();
    public string[] PhoneNumbers { get; set; } = Array.Empty<string>();
    public string? ProvinceId { get; set; }
    public string? DistrictId { get; set; }
    public string? SubDistrictId { get; set; }
    public string PostCode { get; set; } = string.Empty;
}

public class UpdateMediaCommandHandler : IRequestHandler<UpdateMediaCommand, UpdateMediaResponse>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public UpdateMediaCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<UpdateMediaResponse> Handle(UpdateMediaCommand request, CancellationToken cancellationToken)
    {
        var entity = await _context.Medias
            .Include(e => e.Address)
            .Include(e => e.MediaSections)
            .ThenInclude(e => e.Platforms)
            .AsTracking()
            .FirstOrDefaultAsync(e => e.Id.Equals(request.Id), cancellationToken);

        Guard.Against.NotFound(request.Id, entity);

        _mapper.Map<UpdateMediaCommand, Media>(request, entity);

        await _context.SaveChangesAsync(cancellationToken);

        return _mapper.Map<UpdateMediaResponse>(entity);
    }
}
