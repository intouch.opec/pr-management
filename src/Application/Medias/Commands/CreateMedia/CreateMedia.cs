using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.Common.Models;
using PRManagement.Domain.Entities;
using ValidationException = PRManagement.Application.Common.Exceptions.ValidationException;

namespace PRManagement.Application.Medias.Commands.CreateMedia;

// [Authorize(Roles = Roles.ADTeam)]
// [Authorize(Policy = Policies.CanManageContact)]
public record CreateMediaCommand : IRequest<CreateMediaResponse>
{
    public string Name { get; set; } = string.Empty;
    public string Address { get; set; } = string.Empty;
    public string[] Email { get; set; } = Array.Empty<string>();
    public string[] PhoneNumbers { get; set; } = Array.Empty<string>();
    public string? ProvinceId { get; set; } = string.Empty;
    public string? DistrictId { get; set; } = string.Empty;
    public string? SubDistrictId { get; set; } = string.Empty;
    public string PostCode { get; set; } = string.Empty;
    public string SectionId { get; set; } = string.Empty;
    public string SubSectionId { get; set; } = string.Empty;
    public string TypeOfMedia { get; set; } = string.Empty;
    public IEnumerable<PlatformDto>? Platforms { get; set; }
    public string[] MediaSectionEmail { get; set; } = Array.Empty<string>();
    public string[] MediaSectionPhoneNumbers { get; set; } = Array.Empty<string>();
}

public class CreateMediaCommandHandler : IRequestHandler<CreateMediaCommand, CreateMediaResponse>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public CreateMediaCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<CreateMediaResponse> Handle(CreateMediaCommand request, CancellationToken cancellationToken)
    {
        ValidatePlatforms(request.Platforms);
        var entity = _mapper.Map<Media>(request);
        await _context.Medias.AddAsync(entity);
        await _context.SaveChangesAsync(cancellationToken);
        return _mapper.Map<CreateMediaResponse>(entity);
    }

    internal void ValidatePlatforms(IEnumerable<PlatformDto>? platforms)
    {
        if (platforms == null)
            return;
        var validator = new CreatePlatformsValidator(platforms);
        var validationResults = validator.Validate(platforms);
        if (!validationResults.IsValid)
            throw new ValidationException(validationResults.Errors);
    }
}
