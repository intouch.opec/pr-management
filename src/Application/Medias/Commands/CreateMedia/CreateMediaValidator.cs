using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.Common.Models;
using PRManagement.Domain.Constants;
using PRManagement.Domain.Enums;
using PRManagement.Domain.Exceptions;

namespace PRManagement.Application.Medias.Commands.CreateMedia;
public class CreatePlatformsValidator : AbstractValidator<IEnumerable<PlatformDto>?>
{
    private readonly IEnumerable<PlatformDto>? _platforms;
    public CreatePlatformsValidator(IEnumerable<PlatformDto>? platforms)
    {
        _platforms = platforms;

        RuleForEach(Platforms => Platforms)
            .ChildRules(platform => platform.RuleFor(pl => pl.PlatformType)
            .MustEnum<PlatformDto, PlatformType>()
            .Must(BeUniquePlatformType)
            .WithMessage(ValidatorMessages.ShouldBeUnique)
       );
    }

    internal bool BeUniquePlatformType(string platformType)
    {
        if (string.IsNullOrEmpty(platformType))
            return true;

        return _platforms!.Count(e => e.PlatformType!.Equals(platformType!)) < 2;
    }
}

public class StringDuplicateValidator : AbstractValidator<string[]>
{
    public StringDuplicateValidator()
    {
        RuleForEach(e => e)
          .Must((e, phoneNumber) => e.Count(p => p.Equals(phoneNumber)) > 1)
          .WithMessage("The {PropertyName} should not contain duplicate values.");

    }
}

public class CreateMediaValidator : AbstractValidator<CreateMediaCommand>
{
    private readonly IApplicationDbContext _context;
    public CreateMediaValidator(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(v => v.Name)
            .MaximumLength(35)
            .MustAsync(BeUniqueMediaName)
            .WithMessage(ValidatorMessages.ShouldNotBeDuplicate)
            .NotEmpty();

        RuleForEach(v => v.Email)
            .NotNull()
            .Must((e, x) =>
                e.Email.Where(p => !string.IsNullOrEmpty(p)).Count(p => p.Equals(x)) < 2)
            .WithMessage(ValidatorMessages.ShouldNotBeDuplicate)
            .EmailAddress()
            .WithMessage(ValidatorMessages.InvalidArrayFormat);

        RuleForEach(e => e.PhoneNumbers)
            .NotNull()
            .Must((e, x) =>
                e.PhoneNumbers.Where(p => !string.IsNullOrEmpty(p)).Count(p => p.Equals(x)) < 2)
            .WithMessage(ValidatorMessages.ShouldNotBeDuplicate);

        RuleFor(e => e.ProvinceId)
            .MustAsync(HasProvince)
            .WithMessage(ValidatorMessages.NotFound);

        RuleFor(e => e.DistrictId)
            .MustAsync(HasDistrict)
            .WithMessage(ValidatorMessages.NotFound);

        RuleFor(e => e.SubDistrictId)
            .MustAsync(HasSubDistrict)
            .WithMessage(ValidatorMessages.NotFound);

        RuleFor(e => e.PostCode)
            .MaximumLength(5)
            .MinimumLength(0)
            .When(postCode => postCode is not null);

        RuleFor(e => e.SectionId)
            .StringIsGuid()
            .MustAsync(HasSection)
            .WithMessage(ValidatorMessages.NotFound);

        RuleFor(e => e.SubSectionId)
            .StringIsGuid()
            .MustAsync(HasSubSection)
            .WithMessage(ValidatorMessages.NotFound);

        RuleFor(e => e.Name)
            .MustAsync(BeUniqueMediaSection)
            .WithMessage($"The {nameof(CreateMediaCommand.Name)} , {nameof(CreateMediaCommand.SectionId)} And {nameof(CreateMediaCommand.SubSectionId)} combination is duplicated.");

        RuleFor(e => e.TypeOfMedia)
            .MustEnum<CreateMediaCommand, TypeOfMedia>();

        RuleForEach(e => e.Platforms)
          .ChildRules((platform) =>
            platform.RuleFor(pl => pl.Link)
              .NotEmpty())
          .ChildRules(platform =>
            platform.RuleFor(pl => pl.Name)
              .NotEmpty()
              .MaximumLength(35))
          .ChildRules(platform =>
              platform.RuleFor(pl => pl.PlatformType)
                .MustBeValidContains(DigitalPlatformType.PlatformTypeStrings)
                .NotNull())
          .When(e => e.TypeOfMedia.Equals(TypeOfMedia.DIGITAL.GetEnumMemberValue()));

        RuleForEach(e => e.Platforms)
          .ChildRules((platform) =>
            platform.RuleFor(pl => pl.PlatformType)
              .NotEmpty()
              .MustBeValidContains(MainStreamPlatformType.PlatformTypeStrings))
         .When(e => e.TypeOfMedia.Equals(TypeOfMedia.MAINSTREAM.GetEnumMemberValue()));

        RuleForEach(e => e.Platforms)
          .ChildRules(platform =>
            platform.RuleFor(pl => pl.Link)
              .StringIsUrl());

        RuleFor(e => e.Platforms)
          .MinimalItem(DigitalPlatformType.Count)
          .When(e => e.TypeOfMedia.Equals(TypeOfMedia.DIGITAL.GetEnumMemberValue()));

        RuleFor(e => e.Platforms)
          .MinimalItem(MainStreamPlatformType.Count)
          .When(e => e.TypeOfMedia.Equals(TypeOfMedia.MAINSTREAM.GetEnumMemberValue()));
    }

    internal async Task<bool> HasProvince(string? ProvinceId, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(ProvinceId))
            return true;

        return await _context.Provinces
            .AnyAsync(p => p.Id.Equals(ProvinceId), cancellationToken);
    }

    internal async Task<bool> HasDistrict(string? districtId, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(districtId))
            return true;

        return await _context.Districts
          .AsNoTracking()
          .AnyAsync(p => p.Id.Equals(districtId), cancellationToken);
    }

    internal async Task<bool> HasSubDistrict(string? subDistrictId, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(subDistrictId))
            return true;

        return await _context.SubDistricts
          .AsNoTracking()
          .AnyAsync(p => p.Id.Equals(subDistrictId), cancellationToken);
    }

    internal async Task<bool> HasSection(string sectionId, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(sectionId))
            return true;

        return await _context.Sections
          .AsNoTracking()
          .AnyAsync(p => p.Id.Equals(new Guid(sectionId)), cancellationToken);
    }

    internal async Task<bool> HasSubSection(string sectionSubId, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(sectionSubId))
            return true;

        return await _context.SubSections
          .AsNoTracking()
          .AnyAsync(p => p.Id.Equals(new Guid(sectionSubId)), cancellationToken);
    }

    internal async Task<bool> BeUniqueMediaSection(CreateMediaCommand source, string name, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(source.SectionId) ||
            string.IsNullOrWhiteSpace(source.SubSectionId) ||
            !source.TypeOfMedia.IsEnum<TypeOfMedia>())
            return true;

        return !await _context.MediaSections
            .Include(ms => ms.Media)
            .AsNoTracking()
            .AnyAsync(ms =>
              ms.Media.Name.Equals(name) &&
              ms.TypeOfMedia.Equals(source.TypeOfMedia.ToEnum<TypeOfMedia>()) &&
              ms.SectionId.Equals(new Guid(source.SectionId)) &&
              ms.SubSectionId.Equals(new Guid(source.SubSectionId)), cancellationToken);
    }

    internal async Task<bool> BeUniqueMediaName(string name, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(name))
            return true;

        return !await _context.Medias
          .AsNoTracking()
          .AnyAsync(p => p.Name.ToLower().Equals(name.ToLower()), cancellationToken);
    }
}