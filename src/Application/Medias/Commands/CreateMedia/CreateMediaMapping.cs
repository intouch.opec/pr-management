using PRManagement.Application.Common.BusinessLogics;
using PRManagement.Application.Common.Models;
using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;
using PRManagement.Domain.Exceptions;

namespace PRManagement.Application.Medias.Commands.CreateMedia;
public class CreateMediaMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<Media, CreateMediaResponse>()
                .ForMember(s => s.SectionId, op => op.MapFrom(o => o.MediaSections!.FirstOrDefault()!.SectionId.ToString() ?? string.Empty))
                .ForMember(s => s.SubSectionId, op => op.MapFrom(o => o.MediaSections!.FirstOrDefault()!.SubSectionId.ToString() ?? string.Empty))
                .ForMember(s => s.Email, op => op.MapFrom(o => o.Address!.Email ?? Array.Empty<string>()))
                .ForMember(s => s.PhoneNumbers, op => op.MapFrom(o => o.Address!.PhoneNumbers ?? Array.Empty<string>()))
                .ForMember(s => s.Address, op => op.MapFrom(o => o.Address!.Text ?? string.Empty))
                .ForMember(s => s.ProvinceId, op => op.MapFrom(o => o.Address!.ProvinceId ?? string.Empty))
                .ForMember(s => s.DistrictId, op => op.MapFrom(o => o.Address!.DistrictId ?? string.Empty))
                .ForMember(s => s.SubDistrictId, op => op.MapFrom(o => o.Address!.SubDistrictId ?? string.Empty))
                .ForMember(s => s.PostCode, op => op.MapFrom(o => o.Address!.PostCode ?? string.Empty))
                .ForMember(s => s.TypeOfMedia, op => op.MapFrom(o => o.MediaSections!.FirstOrDefault()!.TypeOfMedia.GetEnumMemberValue() ?? string.Empty))
                .ForMember(s => s.TypeOfMedia, op => op.MapFrom(o => o.MediaSections!.FirstOrDefault()!.Id.ToString() ?? string.Empty))
                .ForMember(s => s.MediaSectionId, op => op.MapFrom(o => o.MediaSections!.FirstOrDefault()!.Id))
                .ForMember(s => s.MediaSectionEmail, op => op.MapFrom(o => o.MediaSections!.FirstOrDefault()!.Address!.Email ?? Array.Empty<string>()))
                .ForMember(s => s.MediaSectionPhoneNumbers, op => op.MapFrom(o => o.MediaSections!.FirstOrDefault()!.Address!.PhoneNumbers ?? Array.Empty<string>()));

            CreateMap<CreateMediaCommand, Media>()
                .ForMember(s => s.Address, op => op.MapFrom<CreateMediaCommandAddressResolver>())
                .ForMember(s => s.MediaSections, op => op.MapFrom<CreateMediaSectionCommandResolver>())
                .ForMember(s => s.PercentageOfFullField, op => op.MapFrom(o => CalculatePercentageOfFullFieldExtensions.CalculateCreateMediaCommand(o)));

            CreateMap<PlatformDto, Platform>();

            CreateMap<CreateMediaCommand, Address>()
                .ForMember(s => s.Text, op => op.MapFrom(o => o.Address));

            CreateMap<CreateMediaCommand, MediaSection>()
                .ForMember(s => s.TypeOfMedia, op => op.MapFrom(o => o.TypeOfMedia.ToEnum<TypeOfMedia>()))
                .ForMember(s => s.Platforms, op => op.MapFrom<CreatePlatformCommandResolver>())
                .ForMember(s => s.Address, o => o.MapFrom<CreateMediaCommandToMediaSectionAddressResolver>())
                .ForMember(s => s.TierPotential, o => o.MapFrom<CreateMediaCommandToMediaSectionResolver>());

        }
    }

    public class CreateMediaCommandAddressResolver : IValueResolver<CreateMediaCommand, Media, Address?>
    {
        private readonly IMapper _mapper;

        public CreateMediaCommandAddressResolver(IMapper mapper)
        {
            _mapper = mapper;
        }

        public Address? Resolve(CreateMediaCommand source, Media destination, Address? member, ResolutionContext context)
        {
            if (SomePropertiesAddressHaveValue(source))
                return null;

            return _mapper.Map<Address>(source);
        }

        public static bool SomePropertiesAddressHaveValue(CreateMediaCommand source)
        {
            if (source is null)
                return false;

            return !source.Email.Any() &&
                   !source.PhoneNumbers.Any() &&
                   string.IsNullOrEmpty(source.Address) &&
                   string.IsNullOrEmpty(source.PostCode) &&
                   string.IsNullOrEmpty(source.ProvinceId) &&
                   string.IsNullOrEmpty(source.DistrictId) &&
                   string.IsNullOrEmpty(source.SubDistrictId);
        }
    }

    public class CreateMediaSectionCommandResolver :
      IValueResolver<CreateMediaCommand, Media, ICollection<MediaSection>>
    {
        private readonly IMapper _mapper;

        public CreateMediaSectionCommandResolver(IMapper mapper)
        {
            _mapper = mapper;
        }

        public ICollection<MediaSection> Resolve(CreateMediaCommand source, Media destination, ICollection<MediaSection> member, ResolutionContext context)
        => new List<MediaSection>(){
          _mapper.Map<MediaSection>(source)
    };
    }

    public class CreatePlatformCommandResolver :
      IValueResolver<CreateMediaCommand, MediaSection, ICollection<Platform>>
    {
        private readonly IMapper _mapper;
        public CreatePlatformCommandResolver(IMapper mapper)
        {
            _mapper = mapper;
        }

        public ICollection<Platform> Resolve(CreateMediaCommand source, MediaSection destination, ICollection<Platform> member, ResolutionContext context)
        {
            if (source.Platforms is null) return Array.Empty<Platform>();

            return source.Platforms.Select(_mapper.Map<Platform>).ToList();
        }
    }

    public class CreateMediaCommandToMediaSectionAddressResolver :
        IValueResolver<CreateMediaCommand, MediaSection, Address?>
    {
        public Address? Resolve(CreateMediaCommand source, MediaSection destination, Address? member, ResolutionContext context)
        {
            if (!source.Email.Any() & !source.PhoneNumbers.Any())
                return null;

            return new Address()
            {
                Email = source.MediaSectionEmail,
                PhoneNumbers = source.MediaSectionPhoneNumbers
            };
        }
    }

    public class CreateMediaCommandToMediaSectionResolver : IValueResolver<CreateMediaCommand, MediaSection, uint>
    {
        public uint Resolve(CreateMediaCommand source, MediaSection destination, uint member, ResolutionContext context)
        {
            return FindTier(source.Platforms);
        }

        internal uint FindTier(IEnumerable<PlatformDto>? records)
        {
            return records?.Min(e => e.TierPotential) ?? 0;
        }
    }

}