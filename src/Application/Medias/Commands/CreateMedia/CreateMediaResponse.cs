namespace PRManagement.Application.Medias.Commands.CreateMedia;

public class CreateMediaResponse
{
    public Guid Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public string Address { get; set; } = string.Empty;
    public string[] Email { get; set; } = Array.Empty<string>();
    public string[] PhoneNumbers { get; set; } = Array.Empty<string>();
    public string? ProvinceId { get; set; }
    public string DistrictId { get; set; } = string.Empty;
    public string SubDistrictId { get; set; } = string.Empty;
    public string PostCode { get; set; } = string.Empty;
    public string SectionId { get; set; } = string.Empty;
    public string SubSectionId { get; set; } = string.Empty;
    public string TypeOfMedia { get; set; } = string.Empty;
    public Guid MediaSectionId { get; set; }
    public string[] MediaSectionEmail { get; set; } = Array.Empty<string>();
    public string[] MediaSectionPhoneNumbers { get; set; } = Array.Empty<string>();
}
