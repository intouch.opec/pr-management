﻿using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.Common.Models;

namespace PRManagement.Application.Users.Queries.GetUserWithPagination;

public record GetUsersQuery : IRequest<IEnumerable<OptionItemBase>>
{
    public string? Name { get; set; }
}

public class GetUsersQueryHandler : IRequestHandler<GetUsersQuery, IEnumerable<OptionItemBase>>
{
    private readonly IIdentityService _identityService;
    private readonly IMapper _mapper;

    public GetUsersQueryHandler(IIdentityService identityService, IMapper mapper)
    {
        _identityService = identityService;
        _mapper = mapper;
    }

    public async Task<IEnumerable<OptionItemBase>> Handle(GetUsersQuery request, CancellationToken cancellationToken)
    {
        return await _identityService.GetQueryableUser(_mapper.Map<FilterUser>(request))
            .ProjectTo<OptionItemBase>(_mapper.ConfigurationProvider)
            .ToListAsync(cancellationToken);
    }
}
