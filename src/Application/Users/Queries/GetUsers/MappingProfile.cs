namespace PRManagement.Application.Users.Queries.GetUserWithPagination;

public class MappingProfile
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<GetUsersQuery, FilterUser>();

        }
    }
}
