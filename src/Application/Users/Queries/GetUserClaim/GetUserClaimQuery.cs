namespace PRManagement.Application.Users.Queries.GetUserClaim;

public class GetUserClaimResponse
{
    public string Key { get; set; } = string.Empty;
    public string Value { get; set; } = string.Empty;
}
