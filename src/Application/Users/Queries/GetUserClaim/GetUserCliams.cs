using System.Security.Claims;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.Common.Security;

namespace PRManagement.Application.Users.Queries.GetUserClaim;

[Authorize]
public record GetUserClaimQuery : IRequest<IEnumerable<GetUserClaimResponse>>;

public class GetUserClaimQueryHandler : IRequestHandler<GetUserClaimQuery, IEnumerable<GetUserClaimResponse>>
{
    private readonly ClaimsPrincipal? _principal;

    public GetUserClaimQueryHandler(IUser user)
    {
        _principal = user.Principal;
    }

    public Task<IEnumerable<GetUserClaimResponse>> Handle(GetUserClaimQuery _, CancellationToken cancellationToken)
    {
        return Task.FromResult(
                _principal!.Claims.Select(e => new GetUserClaimResponse { Key = e.Type, Value = e.Value }) 
                ?? Enumerable.Empty<GetUserClaimResponse>());
    }

}
