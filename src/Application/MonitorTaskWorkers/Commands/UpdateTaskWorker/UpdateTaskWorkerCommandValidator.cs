using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Constants;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.TaskWorkers.Commands.UpdateTaskWorker;

public class UpdateTaskWorkerCommandValidator : AbstractValidator<UpdateTaskWorkerCommand>
{
    private readonly IApplicationDbContext _context;

    public UpdateTaskWorkerCommandValidator(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(e => e.Id)
            .NotEmpty()
            .MustAsync(HasTaskWorker)
            .WithMessage(ValidatorMessages.NotFound);

        RuleFor(e => e.TaskWorkerStatus)
            .NotNull()
            .NotEmpty()
            .MustEnum<UpdateTaskWorkerCommand, TaskWorkerStatus>();

        RuleFor(e => e.PRActivityId)
            .StringIsGuid()
            .MustAsync(HasPRActivity)
            .WithMessage(ValidatorMessages.NotFound);
    }

    internal async Task<bool> HasTaskWorker(string? monitorTaskWorkerId, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(monitorTaskWorkerId))
            return true;

        return await _context.MonitorTaskWorkers
          .AsNoTracking()
          .AnyAsync(p => p.Id.Equals(monitorTaskWorkerId.ToGuid()), cancellationToken);
    }

    internal async Task<bool> HasPRActivity(string prActivityId, CancellationToken cancellationToken)
        => !prActivityId.IsValidGuid() || await _context.PRActivities
          .AnyAsync(p => p.Id.Equals(new Guid(prActivityId)), cancellationToken);

}
