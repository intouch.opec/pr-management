using PRManagement.Application.Common.Extensions;
using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;
using PRManagement.Domain.Exceptions;

namespace PRManagement.Application.TaskWorkers.Commands.UpdateTaskWorker;

public class UpdateTaskWorkerMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<UpdateTaskWorkerCommand, MonitorTaskWorker>()
                .ForMember(dest => dest.TaskWorkerStatus, op => op.MapFrom(o => o.TaskWorkerStatus.ToEnum<TaskWorkerStatus>()))
                .ForMember(dest => dest.PRActivityId, op => op.MapFrom(src => src.PRActivityId.ToGuid()));

            CreateMap<MonitorTaskWorker, UpdateTaskWorkerResponse>();
        }
    }
}
