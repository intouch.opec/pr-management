﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.TaskWorkers.Commands.UpdateTaskWorker;

public record UpdateTaskWorkerCommand : IRequest<UpdateTaskWorkerResponse>
{
    public string Id { get; set; } = string.Empty;
    public string Topic { get; set; } = string.Empty;
    public string KeywordQuery { get; set; } = string.Empty;
    public bool IsUrgent { get; set; }
    public string TaskWorkerStatus { get; set; } = string.Empty;
    public string PRActivityId { get; set; } = string.Empty;
}

public class UpdateTaskWorkerCommandHandler : IRequestHandler<UpdateTaskWorkerCommand, UpdateTaskWorkerResponse>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public UpdateTaskWorkerCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<UpdateTaskWorkerResponse> Handle(UpdateTaskWorkerCommand request, CancellationToken cancellationToken)
    {
        var entity = await _context.MonitorTaskWorkers
            .SingleAsync(e => e.Id == request.Id.ToGuid(), cancellationToken);

        Guard.Against.NotFound(request.Id, entity);

        _mapper.Map<UpdateTaskWorkerCommand, MonitorTaskWorker>(request, entity);

        await _context.SaveChangesAsync(cancellationToken);

        return _mapper.Map<UpdateTaskWorkerResponse>(entity);
    }
}
