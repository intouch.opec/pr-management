using PRManagement.Domain.Entities;

namespace PRManagement.Application.TaskWorkers.Commands.RemoveTaskWorker;
 
public class UpdateTaskWorkerMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<MonitorTaskWorker, RemoveTaskWorkerResponse>();
        }
    }
}
