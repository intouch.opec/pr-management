﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;

namespace PRManagement.Application.TaskWorkers.Commands.RemoveTaskWorker;

public record RemoveTaskWorkerCommand : IRequest<RemoveTaskWorkerResponse>
{
    public string Id { get; set; } = string.Empty;
    public string Remark { get; set; } = string.Empty;
}

public class RemoveTaskWorkerCommandHandler : IRequestHandler<RemoveTaskWorkerCommand, RemoveTaskWorkerResponse>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public RemoveTaskWorkerCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<RemoveTaskWorkerResponse> Handle(RemoveTaskWorkerCommand request, CancellationToken cancellationToken)
    {
        var entity = await _context.MonitorTaskWorkers
            .SingleAsync(e => e.Id == request.Id.ToGuid(), cancellationToken);

        Guard.Against.NotFound(request.Id, entity);

        _context.MonitorTaskWorkers.RemoveAndAddRemark(entity, request.Remark);

        await _context.SaveChangesAsync(cancellationToken);

        return _mapper.Map<RemoveTaskWorkerResponse>(entity);
    }
}
