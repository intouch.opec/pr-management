using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Constants;

namespace PRManagement.Application.TaskWorkers.Commands.RemoveTaskWorker;

public class RemoveTaskWorkerCommandValidator : AbstractValidator<RemoveTaskWorkerCommand>
{
    private readonly IApplicationDbContext _context;
    
    public RemoveTaskWorkerCommandValidator(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(e => e.Id)
          .NotEmpty()
          .StringIsGuid()
          .MustAsync(HasTaskWorker)
          .WithMessage(ValidatorMessages.NotFound);
    }

    internal async Task<bool> HasTaskWorker(string sectionId, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(sectionId))
            return true;

        return await _context.MonitorTaskWorkers
          .AsNoTracking()
          .AnyAsync(p => p.Id.Equals(new Guid(sectionId)), cancellationToken);
    }

}
