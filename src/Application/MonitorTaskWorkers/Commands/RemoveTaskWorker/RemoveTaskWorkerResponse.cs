namespace PRManagement.Application.TaskWorkers.Commands.RemoveTaskWorker;

public class RemoveTaskWorkerResponse
{
    public Guid Id { get; set; }
}
