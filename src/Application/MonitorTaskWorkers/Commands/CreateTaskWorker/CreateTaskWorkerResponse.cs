namespace PRManagement.Application.TaskWorkers.Commands.CreateTaskWorker;

public class CreateTaskWorkerResponse
{
    public Guid Id { get; set; }
    public string Topic { get; set; } = string.Empty;
    public string KeywordQuery { get; set; } = string.Empty;
    public Guid PRActivityId { get; set; }
    public bool IsUrgent { get; set; }
}
