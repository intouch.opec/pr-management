using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Constants;

namespace PRManagement.Application.TaskWorkers.Commands.CreateTaskWorker;

public class CreateTaskWorkerCommandValidator : AbstractValidator<CreateTaskWorkerCommand>
{
    private readonly IApplicationDbContext _context;

    public CreateTaskWorkerCommandValidator(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(e => e.KeywordQuery)
          .NotEmpty();

        RuleFor(e => e.Topic)
            .NotEmpty();

        RuleFor(e => e.PRActivityId)
            .StringIsGuid()
            .MustAsync(HasPRActivity)
            .WithMessage(ValidatorMessages.NotFound);
    }

    internal async Task<bool> HasPRActivity(string prActivityId, CancellationToken cancellationToken)
    => !prActivityId.IsValidGuid() || await _context.PRActivities
        .AnyAsync(p => p.Id.Equals(new Guid(prActivityId)), cancellationToken);

}
