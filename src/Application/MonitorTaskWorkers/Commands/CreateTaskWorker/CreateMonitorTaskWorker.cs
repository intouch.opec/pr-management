﻿using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.TaskWorkers.Commands.CreateTaskWorker;

public record CreateTaskWorkerCommand : IRequest<CreateTaskWorkerResponse>
{
    public string Topic { get; set; } = string.Empty;
    public string KeywordQuery { get; set; } = string.Empty;
    public string PRActivityId { get; set; } = string.Empty;
    public bool IsUrgent { get; set; }
}

public class CreateTaskWorkerCommandHandler : IRequestHandler<CreateTaskWorkerCommand, CreateTaskWorkerResponse>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public CreateTaskWorkerCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<CreateTaskWorkerResponse> Handle(CreateTaskWorkerCommand request, CancellationToken cancellationToken)
    {
        var entity = _mapper.Map<MonitorTaskWorker>(request);
        await _context.MonitorTaskWorkers.AddAsync(entity);
        await _context.SaveChangesAsync(cancellationToken);
        return _mapper.Map<CreateTaskWorkerResponse>(entity);
    }
}
