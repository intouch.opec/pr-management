using PRManagement.Application.Common.Extensions;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.TaskWorkers.Commands.CreateTaskWorker;

public class CreateTaskWorkerMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<CreateTaskWorkerCommand, MonitorTaskWorker>()
                .ForMember(dest => dest.PRActivityId, op => op.MapFrom(src => src.PRActivityId.ToGuid()));
                
            CreateMap<MonitorTaskWorker, CreateTaskWorkerResponse>();
        }
    }
}
