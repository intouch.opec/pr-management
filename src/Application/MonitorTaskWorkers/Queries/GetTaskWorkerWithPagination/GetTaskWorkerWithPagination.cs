﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.Common.Mappings;
using PRManagement.Application.Common.Models;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.TaskWorkers.Queries.GetTaskWorkerWithPagination;

public record GetTaskWorkerWithPaginationQuery : IRequest<PaginatedList<GetTaskWorkerItem>>
{
    public string? Topic { get; set; }
    public string? KeywordQuery { get; set; }
    public TaskWorkerStatus? TaskWorkerStatus { get; set; }
    public string? PRActivityId { get; set; }
    public bool? IsUrgent { get; set; }

    public int? PageNumber { get; init; } = 1;
    public int? PageSize { get; init; } = 10;
    public SortType? Sort { get; set; }
    public string? SortBy { get; set; }
}

public class GetTaskWorkerWithPaginationQueryHandler : IRequestHandler<GetTaskWorkerWithPaginationQuery, PaginatedList<GetTaskWorkerItem>>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public GetTaskWorkerWithPaginationQueryHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<PaginatedList<GetTaskWorkerItem>> Handle(GetTaskWorkerWithPaginationQuery request, CancellationToken cancellationToken)
    {
        return await Search(request)
            .ProjectTo<GetTaskWorkerItem>(_mapper.ConfigurationProvider)
            .OrderBy(request.SortBy ?? string.Empty, ascending: request.Sort)
            .PaginatedListAsync(request.PageNumber ?? 1, request.PageSize ?? 10, cancellationToken);
    }

    internal IQueryable Search(GetTaskWorkerWithPaginationQuery request)
    {
        return _context.MonitorTaskWorkers
            .AsNoTracking()
            .WhereEqual(e => e.TaskWorkerStatus, request.TaskWorkerStatus)
            .WhereEqual(e => e.PRActivityId, request.PRActivityId.ToGuid())
            .WhereEqual(e => e.IsUrgent, request.IsUrgent)
            .WhereLike(e => e.KeywordQuery, request.KeywordQuery)
            .WhereLike(e => e.Topic, request.Topic);
    }

}
