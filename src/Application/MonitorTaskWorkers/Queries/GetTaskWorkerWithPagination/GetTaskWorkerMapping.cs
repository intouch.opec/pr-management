using PRManagement.Domain.Entities;

namespace PRManagement.Application.TaskWorkers.Queries.GetTaskWorkerWithPagination;

public class GetTaskWorkerMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<MonitorTaskWorker, GetTaskWorkerItem>();
        }
    }
}
