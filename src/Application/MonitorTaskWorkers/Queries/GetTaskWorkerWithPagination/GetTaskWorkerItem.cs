using PRManagement.Domain.Enums;

namespace PRManagement.Application.TaskWorkers.Queries.GetTaskWorkerWithPagination;

public class GetTaskWorkerItem
{
    public Guid Id { get; set; }
    public string Topic { get; set; } = string.Empty;
    public TaskWorkerStatus? TaskWorkerStatus { get; set; }
    public string KeywordQuery { get; set; } = string.Empty;
    public bool IsUrgent { get; set; }
    public DateTimeOffset Created { get; set; }
    public string? CreatedBy { get; set; }
    public DateTimeOffset LastModified { get; set; }
    public string? LastModifiedBy { get; set; }
}
