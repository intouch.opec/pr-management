using PRManagement.Domain.Enums;

namespace PRManagement.Application.MonitorTaskWorkers.Queries.GetMonitorTaskWorkerCoverageWithPagination;

public class GetMonitorTaskWorkerCoverageItem
{
    public string MediaName { get; set; } = string.Empty;
    public string Link { get; set; } = string.Empty;
    public int Piece { get; set; }
    public decimal PrValue { get; set; }
    public uint PotentialReach { get; set; }
    public uint TierPotential { get; set; }
    public string SectionName { get; set; } = string.Empty;
    public string SubSectionName { get; set; } = string.Empty;
    public uint TierOder { get; set; }
    public DateTimeOffset? PublishDate { get; set; }
    public TypeOfMedia TypeOfMedia { get; set; }
    public PlatformType PlatFormType { get; set; }
    public Guid PlatFormId { get; set; }
}
