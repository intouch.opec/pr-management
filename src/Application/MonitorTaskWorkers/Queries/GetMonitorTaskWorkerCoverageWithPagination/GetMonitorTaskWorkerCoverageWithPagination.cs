﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.Common.Mappings;
using PRManagement.Application.Common.Models;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.MonitorTaskWorkers.Queries.GetMonitorTaskWorkerCoverageWithPagination;

public record GetMonitorTaskWorkerCoverageWithPaginationQuery : IRequest<PaginatedList<GetMonitorTaskWorkerCoverageItem>>
{
    public string Id { get; set; } = string.Empty;

    public int? PageNumber { get; init; } = 1;
    public int? PageSize { get; init; } = 10;
    public SortType? Sort { get; set; }
    public string? SortBy { get; set; }
}

public class GetMonitorTaskWorkerCoverageWithPaginationQueryHandler : IRequestHandler<GetMonitorTaskWorkerCoverageWithPaginationQuery, PaginatedList<GetMonitorTaskWorkerCoverageItem>>
{
    private readonly IApplicationDbContext _context;

    public GetMonitorTaskWorkerCoverageWithPaginationQueryHandler(IApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<PaginatedList<GetMonitorTaskWorkerCoverageItem>> Handle(GetMonitorTaskWorkerCoverageWithPaginationQuery request, CancellationToken cancellationToken)
    {
        return await _context.News
            .Include(n => n.Platform)
            .ThenInclude(pl => pl!.MediaSection)
            .ThenInclude(pl => pl!.SubSection)
            .Include(n => n.Platform)
            .ThenInclude(pl => pl!.MediaSection)
            .ThenInclude(pl => pl!.Section)
            .Include(n => n.Platform)
            .ThenInclude(pl => pl!.MediaSection)
            .ThenInclude(pl => pl!.Media)
            .Where(n =>
                n.MonitorTaskWorkerId.Equals(request.Id.ToGuid()) &&
                n.Mapping.Equals(MappingType.DONE)
            )
            .GroupBy(e => e.Platform)
            .Select(n => new GetMonitorTaskWorkerCoverageItem()
            {
                MediaName = n.Key!.MediaSection!.Media.Name,
                Link = n.Key!.Link,
                TypeOfMedia = n.Key!.MediaSection!.TypeOfMedia,
                TierPotential = n.Key!.TierPotential,
                SectionName = n.Key.MediaSection.Section.Name,
                SubSectionName = n.Key.MediaSection.SubSection.Name,
                Piece = n.Count(),
                PrValue = n.Key!.PRValue!,
                PotentialReach = n.Key!.Potential!,
                TierOder = n.Key!.TierOder!,
                PlatFormType = n.Key!.PlatFormType,
                PublishDate = n.Min(ne => ne.PublicationDate),
                PlatFormId = n.Key!.Id
            })
            .OrderBy(request.SortBy ?? string.Empty, ascending: request.Sort)
            .PaginatedListAsync(request.PageNumber ?? 1, request.PageSize ?? 10, cancellationToken);
    }
}
