using PRManagement.Application.Common.Extensions;
using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;
using PRManagement.Domain.Exceptions;

namespace PRManagement.Application.MonitorTaskWorkers.Queries.GetMonitorTaskWorkerCoverages;

public class MappingProfile
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<IGrouping<TypeOfMedia, New>, MediaSummary>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Key)) 
                .ForMember(dest => dest.TypeOfMedia, opt => opt.MapFrom(src => src.Key)) 
                .ForMember(dest => dest.Piece, opt => opt.MapFrom(src => src.Count()))
                .ForMember(dest => dest.PrValue, opt => opt.MapFrom(src => src.Sum(n => n.Platform!.PRValue))) 
                .ForMember(dest => dest.PotentialReach, opt => opt.MapFrom(src => src.Sum(n => n.Platform!.Potential))); 
        }
    }
}
