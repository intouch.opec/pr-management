﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.MonitorTaskWorkers.Queries.GetMonitorTaskWorkerCoverages;

public record GetMonitorTaskWorkerCoveragesQuery(string Id) : IRequest<GetMonitorTaskWorkerCoveragesResponse>;

public class GetMonitorTaskWorkerCoveragesQueryHandler : IRequestHandler<GetMonitorTaskWorkerCoveragesQuery, GetMonitorTaskWorkerCoveragesResponse>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public GetMonitorTaskWorkerCoveragesQueryHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<GetMonitorTaskWorkerCoveragesResponse> Handle(GetMonitorTaskWorkerCoveragesQuery request, CancellationToken cancellationToken)
    {
        var entity = await _context.MonitorTaskWorkers
            .FirstOrDefaultCacheAsync(n =>
                    n.Id.Equals(request.Id.ToGuid()),
                cancellationToken);

        Guard.Against.NotFound(request.Id, entity);
        
        var numberOfNewSummaryCount = await _context.News
            .CountAsync(n =>
                    n.MonitorTaskWorkerId.Equals(request.Id.ToGuid()),
                cancellationToken);

        var numberOfActivity = await _context.News
            .GroupBy(e => e.PlatformId)
            .CountAsync(cancellationToken);

        var prValue = await _context.News
            .GroupBy(e => e.Platform)
            .SumAsync(n => n.Key!.PRValue, cancellationToken);

        var piecesOfCoverage = await _context.News
            .CountAsync(n =>
                n.Mapping.Equals(MappingType.DONE) &&
                n.MonitorTaskWorkerId.Equals(request.Id.ToGuid()),
            cancellationToken);

        var potentialReach = await _context.News
            .Include(n => n.Platform)
            .Where(n =>
                    n.MonitorTaskWorkerId.Equals(request.Id.ToGuid()) &&
                    n.Mapping.Equals(MappingType.DONE)
            ).SumAsync(e => e.Platform!.Potential, cancellationToken);

        var mediaSummaries = await _context.News
            .Include(n => n.Platform)
            .ThenInclude(p => p!.MediaSection)
            .Where(n =>
                    n.MonitorTaskWorkerId.Equals(request.Id.ToGuid()) &&
                    n.Mapping.Equals(MappingType.DONE)
            )
            .GroupBy(e => e.Platform!.MediaSection!.TypeOfMedia)
            .ProjectTo<MediaSummary>(_mapper.ConfigurationProvider)
            .ToListAsync(cancellationToken);

        return new GetMonitorTaskWorkerCoveragesResponse()
        {
            NumberOfNewSummaryCount = numberOfNewSummaryCount,
            NumberOfActivity = numberOfActivity,
            PrValue = prValue,
            PiecesOfCoverage = piecesOfCoverage,
            PotentialReach = potentialReach,
            MediaSummaries = mediaSummaries
        };
    }
}
