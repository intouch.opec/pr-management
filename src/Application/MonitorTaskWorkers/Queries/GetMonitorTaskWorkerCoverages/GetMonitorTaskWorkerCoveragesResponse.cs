using PRManagement.Domain.Enums;

namespace PRManagement.Application.MonitorTaskWorkers.Queries.GetMonitorTaskWorkerCoverages;

public class GetMonitorTaskWorkerCoveragesResponse
{
    public int NumberOfNewSummaryCount { get; set; }
    public int NumberOfActivity { get; set; }
    public decimal PrValue { get; set; }
    public int PiecesOfCoverage { get; set; }
    public long PotentialReach { get; set; }
    public IEnumerable<MediaSummary> MediaSummaries { get; set; } = Enumerable.Empty<MediaSummary>();
}

public class MediaSummary
{
    public TypeOfMedia Id { get; set; }
    public TypeOfMedia TypeOfMedia { get; set; }
    public int Piece { get; set; }
    public int PrValue { get; set; }
    public int PotentialReach { get; set; }
}