﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;

namespace PRManagement.Application.TaskWorkers.Queries.GetTaskWorker;

public record GetTaskWorkerQuery(string Id) : IRequest<GetTaskWorkerResponse>;

public class GetTaskWorkerQueryHandler : IRequestHandler<GetTaskWorkerQuery, GetTaskWorkerResponse>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public GetTaskWorkerQueryHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<GetTaskWorkerResponse> Handle(GetTaskWorkerQuery request, CancellationToken cancellationToken)
    {
        var entity = await _context.MonitorTaskWorkers
            .ProjectTo<GetTaskWorkerResponse>(_mapper.ConfigurationProvider)
            .SingleAsync(e => e.Id.Equals(request.Id.ToGuidOnly()), cancellationToken);

        Guard.Against.NotFound(request.Id, entity);

        return entity;
    }
}
