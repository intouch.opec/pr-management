using PRManagement.Domain.Enums;

namespace PRManagement.Application.TaskWorkers.Queries.GetTaskWorker;

public class GetTaskWorkerResponse
{
    public Guid Id { get; set; }
    public string KeywordQuery { get; set; } = string.Empty;
    public DateTimeOffset RunAt { get; set; }
    public TaskWorkerStatus TaskWorkerStatus { get; set; }
    public DateTimeOffset Created { get; set; }
    public string? CreatedBy { get; set; }
    public DateTimeOffset LastModified { get; set; }
    public string? LastModifiedBy { get; set; }
}
