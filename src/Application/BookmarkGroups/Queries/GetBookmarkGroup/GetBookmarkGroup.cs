﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.Common.Mappings;
using PRManagement.Application.Common.Models;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.BookmarkGroups.Queries.GetBookmarkGroup;

public record GetBookmarkGroupQuery : IRequest<PaginatedList<GetBookmarkGroupsResponse>>
{
    public string? Name { get; init; } = string.Empty;
    public string? Tag { get; set; } = string.Empty;

    public int? PageNumber { get; init; } = 1;
    public int? PageSize { get; init; } = 10;
    public SortType? Sort { get; set; } = SortType.DESC;
    public string? SortBy { get; set; } = nameof(GetBookmarkGroupsResponse.Name);
}

public class GetBookmarkGroupQueryHandler : IRequestHandler<GetBookmarkGroupQuery, PaginatedList<GetBookmarkGroupsResponse>>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public GetBookmarkGroupQueryHandler(IApplicationDbContext context, IMapper mapper)
    {
        _mapper = mapper;
        _context = context;
    }

    public async Task<PaginatedList<GetBookmarkGroupsResponse>> Handle(GetBookmarkGroupQuery request, CancellationToken cancellationToken)
    {
        return await Search(request)
            .ProjectTo<GetBookmarkGroupsResponse>(_mapper.ConfigurationProvider)
            .OrderBy(request.SortBy ?? string.Empty, ascending: request.Sort)
            .PaginatedListAsync(request.PageNumber ?? 1, request.PageSize ?? 10, cancellationToken);
    }

    internal IQueryable Search(GetBookmarkGroupQuery request)
    {
        var queryable = _context.BookmarkGroups
            .WhereLike(e => e.Name.ToLower(), request.Name);

        if (!string.IsNullOrWhiteSpace(request.Tag))
            queryable = queryable
                .Where(e =>
                    e.Tags.Any(
                        tag => request.Tag.Contains(tag.Name)
                        ));

        return queryable;
    }
}
