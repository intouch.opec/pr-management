using PRManagement.Domain.Entities;

namespace PRManagement.Application.BookmarkGroups.Queries.GetBookmarkGroup;

public class GetBookmarkGroupsResponse
{
    public Guid Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public Tag[] Tags { get; set; } = Array.Empty<Tag>();
}
