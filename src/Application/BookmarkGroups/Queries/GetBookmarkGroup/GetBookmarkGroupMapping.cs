using PRManagement.Domain.Entities;

namespace PRManagement.Application.BookmarkGroups.Queries.GetBookmarkGroup;

public class GetBookmarkGroupMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<BookmarkGroup, GetBookmarkGroupsResponse>();
        }
    }
}
