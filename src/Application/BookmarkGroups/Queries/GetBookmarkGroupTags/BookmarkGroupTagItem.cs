using PRManagement.Application.Common.Models;

namespace PRManagement.Application.BookmarkGroups.Queries.GetBookmarkGroupTags;

public class BookmarkGroupTagItem : OptionItemBase
{
    public string Color { get; set; } = string.Empty;
}