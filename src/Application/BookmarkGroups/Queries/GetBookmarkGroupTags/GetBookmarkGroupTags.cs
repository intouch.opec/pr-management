﻿using PRManagement.Application.Common.Interfaces;

namespace PRManagement.Application.BookmarkGroups.Queries.GetBookmarkGroupTags;

public record GetBookmarkGroupTagsQuery : IRequest<IEnumerable<BookmarkGroupTagItem>>
{
    public string Tag { get; set; } = string.Empty;
}

public class GetBookmarkGroupTagsQueryHandler : IRequestHandler<GetBookmarkGroupTagsQuery, IEnumerable<BookmarkGroupTagItem>>
{
    private readonly IApplicationDbContext _context;

    public GetBookmarkGroupTagsQueryHandler(IApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<IEnumerable<BookmarkGroupTagItem>> Handle(GetBookmarkGroupTagsQuery request, CancellationToken cancellationToken)
    {
        var tags = await _context.BookmarkGroups
            .Select(bg => bg.Tags)
            .ToListAsync(cancellationToken);

        return tags.SelectMany(tag => tag)
            .GroupBy(tag => tag.Name)
            .Select(group => new BookmarkGroupTagItem()
            {
                Id = group.Key,
                Name = group.Key,
                Color = group.First().Color
            });
    }

}
