namespace PRManagement.Application.BookmarkGroups.Commands.ExportBookmarkGroup;

public class ExportBookmarkGroupResponse
{
    public byte[] File { get; set; } = Array.Empty<byte>();
    public string FileName { get; set; } = string.Empty;
    public string ContentType { get; set; } = string.Empty;
}
