﻿using PRManagement.Application.Common.Interfaces;

namespace PRManagement.Application.BookmarkGroups.Commands.ExportBookmarkGroup;

public record ExportBookmarkGroupCommand(Guid Id) : IRequest<ExportBookmarkGroupResponse>;

public class ExportBookmarkGroupCommandHandler : IRequestHandler<ExportBookmarkGroupCommand, ExportBookmarkGroupResponse>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public ExportBookmarkGroupCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<ExportBookmarkGroupResponse> Handle(ExportBookmarkGroupCommand request, CancellationToken cancellationToken)
    {
        var bookmarkGroup = await _context.BookmarkGroups
            .Include(bg => bg.BookMarks!.Where(b => b != null))
            .ThenInclude(b => b.Address)
            .ThenInclude(a => a!.Province)
            .Include(bg => bg.BookMarks!.Where(b => b != null))
            .ThenInclude(b => b.Address)
            .ThenInclude(a => a!.District)
            .Include(bg => bg.BookMarks!.Where(b => b != null))
            .ThenInclude(b => b.Address)
            .ThenInclude(a => a!.SubDistrict)
            .Include(bg => bg.BookMarks!.Where(b => b != null))
            .ThenInclude(b => b.Address)
            .ThenInclude(a => a!.Contact)
            .ThenInclude(c => c!.ContactMediaSections!
                .Where(cms => cms != null))
            .ThenInclude(c => c!.MediaSection)
            .ThenInclude(c => c!.Media)
            .Include(bg => bg.BookMarks!.Where(b => b != null))
            .ThenInclude(b => b.Address)
            .ThenInclude(a => a!.Contact)
            .ThenInclude(c => c!.ContactMediaSections!
                .Where(cms => cms != null))
            .ThenInclude(c => c!.MediaSection)
            .ThenInclude(c => c!.Section)
            .Include(bg => bg.BookMarks!.Where(b => b != null))
            .ThenInclude(b => b.Address)
            .ThenInclude(a => a!.Contact)
            .ThenInclude(c => c!.ContactMediaSections!
                .Where(cms => cms != null))
            .ThenInclude(c => c!.MediaSection)
            .ThenInclude(c => c!.SubSection)
            .Include(bg => bg.BookMarks!.Where(b => b != null))
            .ThenInclude(e => e.Address)
            .ThenInclude(e => e!.Media)
            .ThenInclude(c => c!.MediaSections)
            .ThenInclude(c => c!.Section)
            .Include(bg => bg.BookMarks!.Where(b => b != null))
            .ThenInclude(e => e.Address)
            .ThenInclude(e => e!.Media)
            .ThenInclude(c => c!.MediaSections)
            .ThenInclude(c => c!.SubSection)
            .AsNoTracking()
            .SingleOrDefaultAsync(bg => bg.Id.Equals(request.Id),
                cancellationToken);

        Guard.Against.NotFound(request.Id, bookmarkGroup);

        return _mapper.Map<ExportBookmarkGroupResponse>(bookmarkGroup);
    }

}
