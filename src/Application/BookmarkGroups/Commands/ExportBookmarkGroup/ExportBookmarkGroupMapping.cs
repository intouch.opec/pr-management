using Microsoft.Extensions.Options;
using PRManagement.Application.Common.Configurations;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.BookmarkGroups.Commands.ExportBookmarkGroup;

public class ExportBookmarkGroupMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<BookMark, IEnumerable<string>>()
                .ConvertUsing<BookMarkToStringEnumerableConverter>();

            CreateMap<BookmarkGroup, ExportBookmarkGroupResponse>()
                .ForMember(s => s.FileName, op => op.MapFrom(o => $"{o.Name}_{DateTimeOffset.UtcNow:yyyy_MM_dd_hh_mm}.xlsx"))
                .ForMember(s => s.File, op => op.MapFrom<BookmarkGroupToFileResolver>())
                .ForMember(s => s.ContentType, op => op.MapFrom(o => "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
        }
    }

    public class BookmarkGroupToFileResolver : IValueResolver<BookmarkGroup, ExportBookmarkGroupResponse, byte[]>
    {
        private readonly IMapper _mapper;
        private readonly IGenerateExcel _generateExcel;
        private readonly ExportExcelConfiguration _exportExcelConfiguration;

        public BookmarkGroupToFileResolver(IMapper mapper,
                                           IGenerateExcel generateExcel,
                                           IOptions<ExportExcelConfiguration> exportExcelConfiguration)
        {
            _mapper = mapper;
            _generateExcel = generateExcel;
            _exportExcelConfiguration = exportExcelConfiguration.Value;
        }

        public byte[] Resolve(BookmarkGroup source, ExportBookmarkGroupResponse destination, byte[] member, ResolutionContext context)
        {
            var bookmarksAsString = _mapper.Map<IEnumerable<IEnumerable<string>>>(source.BookMarks);
            var task = _generateExcel.GenerateExcelAsync(
                _exportExcelConfiguration.BookmarkTemplate,
                _exportExcelConfiguration.HeaderText
                    .Replace("BOOKMARK_NAME", source.Name)
                    .Replace("DATE_TIME", $"{DateTimeOffset.UtcNow:yyyy/MM/dd hh:mm}"),
                bookmarksAsString
            );

            task.Wait();
            return task.Result;
        }
    }

    public class BookMarkToStringEnumerableConverter : ITypeConverter<BookMark, IEnumerable<string>>
    {
        public IEnumerable<string> Convert(BookMark source, IEnumerable<string> destination, ResolutionContext context)
        {
            var result = new List<string>();
            if (source.BookMarkType.Equals(BookMarkType.Contact))
            {
                result.Add(string.Join(",", source.Address?.Contact?.ContactMediaSections?.GroupBy(s => s.MediaSection!.Section!.Name).Select(s => s.Key) ?? Array.Empty<string>()));
                result.Add(string.Join(",", source.Address?.Contact?.ContactMediaSections?.GroupBy(s => s.MediaSection!.Section.Name).Select(s => s.Key) ?? Array.Empty<string>()));
                result.Add(string.Join(",", source.Address?.Contact?.ContactMediaSections?.GroupBy(s => s.MediaSection!.SubSection.Name).Select(s => s.Key) ?? Array.Empty<string>()));
                result.Add(source.Address?.Contact?.FirstName ?? string.Empty);
                result.Add(source.Address?.Contact?.LastName ?? string.Empty);
                result.Add(source.Address?.Contact?.NickName ?? string.Empty);
                result.Add(source.Address?.Contact?.Position ?? string.Empty);
                result.Add(source.Address?.Contact?.LineId ?? string.Empty);
                result.Add(string.Join(",", source.Address!.Email));
                result.Add(string.Join(",", source.Address!.PhoneNumbers));
                result.Add(source.Address?.Province?.NameTH ?? string.Empty);
                result.Add(source.Address?.District?.NameTH ?? string.Empty);
                result.Add(source.Address?.SubDistrict?.NameTH ?? string.Empty);
                result.Add(source.Address?.Text ?? string.Empty);
                result.Add(source.Address?.PostCode ?? string.Empty);
            }
            else
            {
                result.Add(source.Address?.Media?.Name ?? string.Empty);
                result.Add(string.Join(",", source.Address?.Media?.MediaSections?.GroupBy(s => s.Section.Name).Select(s => s.Key) ?? Array.Empty<string>()));
                result.Add(string.Join(",", source.Address?.Media?.MediaSections?.GroupBy(s => s.SubSection.Name).Select(s => s.Key) ?? Array.Empty<string>()));
                result.Add(string.Empty);
                result.Add(string.Empty);
                result.Add(string.Empty);
                result.Add(string.Empty);
                result.Add(string.Empty);
                result.Add(string.Join(",", source.Address?.Email ?? Array.Empty<string>()));
                result.Add(string.Join(",", source.Address?.PhoneNumbers?? Array.Empty<string>()));
                result.Add(source.Address?.Province?.NameTH ?? string.Empty);
                result.Add(source.Address?.District?.NameTH ?? string.Empty);
                result.Add(source.Address?.SubDistrict?.NameTH ?? string.Empty);
                result.Add(source.Address?.Text ?? string.Empty);
                result.Add(source.Address?.PostCode ?? string.Empty);
            }
            return result;
        }
    }
}
