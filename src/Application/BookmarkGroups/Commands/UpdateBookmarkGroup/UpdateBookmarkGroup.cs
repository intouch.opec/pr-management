﻿using PRManagement.Application.Bookmarks.Commands.CreateBookmarkContacts;
using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.BookmarkGroups.Commands.UpdateBookmarkGroup;

public record UpdateBookmarkGroupCommand() : IRequest<CreateBookmarkContactsResponse>
{
    public string Id { get; set; } = string.Empty;
    public string Name { get; set; } = string.Empty;
    public List<Tag> Tags { get; set; } = new List<Tag>();
}

public class UpdateBookmarkGroupCommandHandler : IRequestHandler<UpdateBookmarkGroupCommand, CreateBookmarkContactsResponse>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public UpdateBookmarkGroupCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<CreateBookmarkContactsResponse> Handle(UpdateBookmarkGroupCommand request, CancellationToken cancellationToken)
    {
        var bookmarkGroup = await _context.BookmarkGroups
            .Include(e => e.BookMarks)
            .AsTracking()
            .Where(e => e.Id.Equals(request.Id.ToGuidOnly()))
            .FirstOrDefaultAsync(cancellationToken);

        Guard.Against.NotFound(request.Id, bookmarkGroup);

        _mapper.Map<UpdateBookmarkGroupCommand, BookmarkGroup>(request, bookmarkGroup);

        await _context.SaveChangesAsync(cancellationToken);

        return _mapper.Map<CreateBookmarkContactsResponse>(bookmarkGroup);
    }
}
