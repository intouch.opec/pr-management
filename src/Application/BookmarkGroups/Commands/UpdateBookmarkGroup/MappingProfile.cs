using PRManagement.Domain.Entities;

namespace PRManagement.Application.BookmarkGroups.Commands.UpdateBookmarkGroup;

public class MappingProfile
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<UpdateBookmarkGroupCommand, BookmarkGroup>()
                .ForMember(dest => dest.Id, op => op.Ignore());
        }
    }
}
