using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Constants;

namespace PRManagement.Application.BookmarkGroups.Commands.UpdateBookmarkGroup;

public class UpdateBookmarkGroupCommandValidator : AbstractValidator<UpdateBookmarkGroupCommand>
{
    private readonly IApplicationDbContext _context;

    public UpdateBookmarkGroupCommandValidator(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(src => src.Id)
            .StringIsGuid()
            .NotEmpty()
            .MustAsync(HasBookmarkContactAsync)
            .WithMessage(ValidatorMessages.NotFound);

        RuleFor(src => src.Name)
            .NotEmpty();

    }

    internal async Task<bool> HasBookmarkContactAsync(string id, CancellationToken cancellationToken)
    {
        if (!id.IsValidGuid())
            return true;

        return await _context.BookmarkGroups
            .AnyCacheAsync(
                e => e.Id.Equals(id.ToGuidOnly()),
                cancellationToken
            );
    }
}
