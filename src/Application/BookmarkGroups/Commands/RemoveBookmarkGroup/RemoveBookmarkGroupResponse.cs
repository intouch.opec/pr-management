namespace PRManagement.Application.BookmarkGroups.Commands.RemoveBookmarkGroup;

public class RemoveBookmarkGroupResponse
{
    public Guid Id { get; set; }
    public string Name { get; set; } = string.Empty;
}
