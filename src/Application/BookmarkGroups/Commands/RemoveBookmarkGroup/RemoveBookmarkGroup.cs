﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;

namespace PRManagement.Application.BookmarkGroups.Commands.RemoveBookmarkGroup;

public class RemoveBookmarkGroupCommand : IRequest<RemoveBookmarkGroupResponse>
{
    public Guid Id { get; set; }
    public string Remark { get; set; } = string.Empty;
}

public class RemoveBookmarkGroupCommandHandler : IRequestHandler<RemoveBookmarkGroupCommand, RemoveBookmarkGroupResponse>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public RemoveBookmarkGroupCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<RemoveBookmarkGroupResponse> Handle(RemoveBookmarkGroupCommand request, CancellationToken cancellationToken)
    {
        var entity = await _context.BookmarkGroups
            .Where(e => e.Id == request.Id)
            .SingleOrDefaultAsync(cancellationToken);

        Guard.Against.NotFound(request.Id, entity);
        _context.BookmarkGroups.RemoveAndAddRemark(entity, request.Remark);

        await _context.SaveChangesAsync(cancellationToken);

        return _mapper.Map<RemoveBookmarkGroupResponse>(entity);
    }
}
