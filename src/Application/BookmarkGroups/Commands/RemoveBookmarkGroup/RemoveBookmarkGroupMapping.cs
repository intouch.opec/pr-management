using PRManagement.Domain.Entities;

namespace PRManagement.Application.BookmarkGroups.Commands.RemoveBookmarkGroup;
public class RemoveBookmarkGroupMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<BookmarkGroup, RemoveBookmarkGroupResponse>();
        }
    }
}
