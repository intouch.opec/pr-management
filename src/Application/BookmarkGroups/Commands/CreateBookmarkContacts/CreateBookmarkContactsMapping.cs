using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.Bookmarks.Commands.CreateBookmarkContacts;

public class CreateBookmarkContactsMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<CreateBookmarkContactsCommand, BookmarkGroup>()
                .ForMember(s => s.Name, op => op.MapFrom(o => o.Name))
                .ForMember(s => s.Tags, op => op.MapFrom(o => o.Tags));

            CreateMap<Contact, BookMark>()
                .ForMember(s => s.Id, op => op.Ignore())
                .ForMember(s => s.BookMarkType, op => op.MapFrom(o => BookMarkType.Contact))
                .ForMember(s => s.AddressId, op => op.MapFrom(o => o.AddressId))
                .ForMember(s => s.Id, op => op.Ignore());

            CreateMap<BookmarkGroup, CreateBookmarkContactsResponse>()
                .ForMember(s => s.Name, op => op.MapFrom(o => o.Name))
                .ForMember(s => s.Id, op => op.MapFrom(o => o.Id))
                .ForMember(s => s.Total, op => op.MapFrom(o => o.BookMarks!.Count));
        }
    }
}
