using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Constants;

namespace PRManagement.Application.Bookmarks.Commands.CreateBookmarkContacts;

public class CreateBookmarkContactsCommandValidator : AbstractValidator<CreateBookmarkContactsCommand>
{
    private readonly IApplicationDbContext _context;

    public CreateBookmarkContactsCommandValidator(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(command => command.Name)
            .NotEmpty();

        RuleFor(command => command.SectionId)
            .StringIsGuid()
            .NotEmpty()
            .MustAsync(HasSection)
            .WithMessage(ValidatorMessages.NotFound);

        RuleFor(command => command.SubSectionId)
            .StringIsGuid()
            .MustAsync(HasSubSection)
            .WithMessage(ValidatorMessages.NotFound);

        RuleForEach(command => command.IgnoreContactIds)
            .StringIsGuid();

        RuleFor(command => command.IgnoreContactIds)
            .MustAsync(HasContacts)
            .WithMessage(ValidatorMessages.NotFound)
            .DependentRules(() =>
            {
                RuleForEach(Command => Command.IgnoreContactIds).SetValidator((command, mediaSection) =>
                {
                    var contactIds = _context.Contacts
                        .Include(c => c.ContactMediaSections)
                        .AsNoTracking()
                        .WhereEqualArray(e => e.ContactMediaSections!,
                            item => item.MediaSection!.SectionId,
                            command.SectionId.ToGuid())
                        .WhereEqualArray(e => e.ContactMediaSections!,
                            item => item.MediaSection!.SubSectionId,
                            command.SubSectionId.ToGuid())
                        .Select(e => e.Id.ToString());
                    return new ContactIdsValidator(contactIds);
                });
            });

    }

    internal async Task<bool> HasSection(string sectionId, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(sectionId) || !sectionId.IsValidGuid())
            return true;

        return await _context.Sections
          .AsNoTracking()
          .AnyAsync(p => p.Id.Equals(sectionId.ToGuid()), cancellationToken);
    }

    internal async Task<bool> HasSubSection(string subSectionId, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(subSectionId) || !subSectionId.IsValidGuid())
            return true;

        return await _context.SubSections
          .AsNoTracking()
          .AnyAsync(p => p.Id.Equals(subSectionId.ToGuid()), cancellationToken);
    }

    internal async Task<bool> HasContacts(CreateBookmarkContactsCommand src, IEnumerable<string> contactIds, CancellationToken cancellationToken)
    {
        if (!contactIds.Any())
            return true;

        return (await _context.Contacts
            .AsNoTracking()
            .WhereEqualArray(e => e.ContactMediaSections!,
                            item => item.MediaSection!.SectionId,
                            src.SectionId.ToGuid())
            .WhereEqualArray(e => e.ContactMediaSections!,
                            item => item.MediaSection!.SubSectionId,
                            src.SubSectionId.ToGuid())
            .CountAsync(p => contactIds.Select(e => e.ToGuid()).Contains(p.Id), cancellationToken))
            .Equals(contactIds.Count());
    }

}

public class ContactIdsValidator : AbstractValidator<string>
{
    private readonly IEnumerable<string> _contactIds;

    public ContactIdsValidator(IEnumerable<string> contactIds)
    {
        _contactIds = contactIds;

        RuleFor(pl => pl)
            .Must(HasContact)
            .WithMessage("The IgnoreContactIds is not found.");
    }

    private bool HasContact(string contactId)
    {
        if (!Guid.TryParse(contactId, out _))
            return false;
        return _contactIds.Contains(contactId);
    }
}
