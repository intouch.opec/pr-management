﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.Bookmarks.Commands.CreateBookmarkContacts;

public record CreateBookmarkContactsCommand : IRequest<CreateBookmarkContactsResponse>
{
    public string Name { get; set; } = string.Empty;
    public List<Tag> Tags { get; set; } = new List<Tag>();
    public string SectionId { get; set; } = string.Empty;
    public string SubSectionId { get; set; } = string.Empty;
    public IEnumerable<string> IgnoreContactIds { get; set; } = Enumerable.Empty<string>();
}

public class CreateBookmarkContactsCommandHandler : IRequestHandler<CreateBookmarkContactsCommand, CreateBookmarkContactsResponse>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public CreateBookmarkContactsCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<CreateBookmarkContactsResponse> Handle(CreateBookmarkContactsCommand request, CancellationToken cancellationToken)
    {
        var bookmarkGroup = await _context.BookmarkGroups
            .Include(e => e.BookMarks)
            .AsNoTracking()
            .Where(e => e.Name.Equals(request.Name))
            .FirstOrDefaultAsync(cancellationToken);

        if (bookmarkGroup == null)
        {
            bookmarkGroup = _mapper.Map<BookmarkGroup>(request);
            bookmarkGroup.BookMarks = await GetBookMarks(request)
                .ToListAsync(cancellationToken);
            await _context.BookmarkGroups.AddAsync(bookmarkGroup);
        }
        else
        {
            var bookmarks = await GetBookMarks(
                    request,
                    bookmarkGroup!.BookMarks!.Select(e => (Guid?)e.AddressId))
                .ToListAsync(cancellationToken);

            for (int i = 0; i < bookmarks.Count; i++)
                bookmarks[i].BookmarkGroupId = bookmarkGroup.Id;

            await _context.BookMarks.AddRangeAsync(bookmarks);
        }

        await _context.SaveChangesAsync(cancellationToken);

        return _mapper.Map<CreateBookmarkContactsResponse>(bookmarkGroup);
    }

    internal IQueryable<BookMark> GetBookMarks(CreateBookmarkContactsCommand request, IEnumerable<Guid?>? addressIds = null)
    {
        return _context.Contacts
            .Include(con => con.Address)
            .WhereEqualArray(e => e.ContactMediaSections!,
                item => item.MediaSection!.SectionId,
                request.SectionId.ToGuidOnly())
            .WhereEqualArray(e => e.ContactMediaSections!,
                item => item.MediaSection!.SubSectionId,
                request.SubSectionId.ToGuid())
            .WhereNotContains(e => e.AddressId, addressIds)
            .WhereNotContains(e => e.Id, request.IgnoreContactIds.Select(e => e.ToGuidOnly()))
            .ProjectTo<BookMark>(_mapper.ConfigurationProvider);
    }
}
