namespace PRManagement.Application.Bookmarks.Commands.CreateBookmarkContacts;

public class CreateBookmarkContactsResponse
{
    public Guid Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public int Total { get; set; }
}
