using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Constants;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.BookmarkGroups.Commands.CreateBookmarkGroup;
public class CreateBookmarkGroupCommandValidator : AbstractValidator<CreateBookmarkGroupCommand>
{
    private readonly IApplicationDbContext _context;

    public CreateBookmarkGroupCommandValidator(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(v => v.Name)
          .NotEmpty();

        RuleFor(v => v.MediaIds)
            .MustAsync(HasMedia)
            .WithMessage(ValidatorMessages.NotFound)
            .DependentRules(() =>
            {
                RuleForEach(command => command.MediaIds)
                    .SetValidator((command)
                        => CreateMediaItemValidator(command));
            });

        RuleFor(v => v.ContactIds)
            .MustAsync(HasContact)
            .WithMessage(ValidatorMessages.NotFound)
            .DependentRules(() =>
            {
                RuleForEach(command => command.ContactIds)
                    .SetValidator((command)
                        => CreateContactItemValidator(command));
            });
    }

    internal async Task<bool> HasContact(string[] contactIds, CancellationToken cancellationToken)
    {
        if (contactIds.Length == 0)
            return true;

        return (await _context.Contacts
            .CountAsync(p => contactIds.Select(e => e.ToGuid()).Contains(p.Id), cancellationToken))
            .Equals(contactIds.Length);
    }

    internal async Task<bool> HasMedia(string[] mediaIds, CancellationToken cancellationToken)
    {
        if (mediaIds.Length == 0)
            return true;

        return (await _context.Medias
            .CountAsync(p => mediaIds.Select(e => e.ToGuid()).Contains(p.Id), cancellationToken))
            .Equals(mediaIds.Length);
    }

    internal IValidator<string> CreateContactItemValidator(CreateBookmarkGroupCommand command)
    {
        var contactIds = _context.BookMarks
            .Include(e => e.BookmarkGroup)
            .Include(e => e.Address!.Contact)
            .Where(e => e.BookmarkGroup!.Name.ToLower().Equals(command.Name.ToLower()) &&
                command.ContactIds.Select(id => id.ToGuid()).Contains(e.Address!.Contact!.Id) &&
                e.BookMarkType.Equals(BookMarkType.Contact))
            .Select(e => e.Address!.Contact!.Id.ToString()).ToList();

        return new ContactItemValidator(contactIds);
    }

    internal IValidator<string> CreateMediaItemValidator(CreateBookmarkGroupCommand command)
    {
        var mediaIds = _context.BookMarks
            .Include(e => e.BookmarkGroup)
            .Include(e => e.Address!.Media)
            .Where(bm => bm.BookmarkGroup!.Name.ToLower().Equals(command.Name.ToLower()) &&
                        command.MediaIds.Select(mId => mId.ToGuidOnly())
                            .Contains(bm.Address!.Media!.Id) &&
                        bm.BookMarkType.Equals(BookMarkType.Media)
        )
        .Select(e => e.Address!.Media!.Id.ToString());
        return new MediaItemValidator(mediaIds);
    }

}

public class MediaItemValidator : AbstractValidator<string>
{
    private readonly IEnumerable<string> _mediaIds;

    public MediaItemValidator(IEnumerable<string> mediaIds)
    {
        _mediaIds = mediaIds;

        RuleFor(m => m)
            .Must(BeUniqueMedia)
            .WithMessage("The 'MediaId' is aleady exits.");
    }

    private bool BeUniqueMedia(string mediaId)
    {
        if (!Guid.TryParse(mediaId, out _))
            return false;
        return !_mediaIds.Contains(mediaId);
    }
}

public class ContactItemValidator : AbstractValidator<string>
{
    private readonly IEnumerable<string> _contactIds;

    public ContactItemValidator(IEnumerable<string> mediaIds)
    {
        _contactIds = mediaIds;

        RuleFor(m => m)
            .Must(BeUniqueMedia)
            .WithMessage("The 'ContactId' is already exits.");
    }

    private bool BeUniqueMedia(string contactId)
    {
        if (!Guid.TryParse(contactId, out _))
            return false;
        return !_contactIds.Contains(contactId);
    }
}
