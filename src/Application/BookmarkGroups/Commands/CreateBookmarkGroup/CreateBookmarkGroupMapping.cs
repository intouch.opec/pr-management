using PRManagement.Domain.Entities;

namespace PRManagement.Application.BookmarkGroups.Commands.CreateBookmarkGroup;
public class CreateBookmarkGroupMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<CreateBookmarkGroupCommand, BookmarkGroup>();

            CreateMap<BookmarkGroup, CreateBookmarkGroupResponse>();
        }
    }
}
