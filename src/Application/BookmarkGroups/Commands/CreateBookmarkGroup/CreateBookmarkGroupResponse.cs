namespace PRManagement.Application.BookmarkGroups.Commands.CreateBookmarkGroup;

public class CreateBookmarkGroupResponse
{
    public Guid Id { get; set; }
    public string Name { get; set; } = string.Empty;
}
