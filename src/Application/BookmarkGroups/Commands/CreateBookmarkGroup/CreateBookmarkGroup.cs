﻿using System.Linq.Expressions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.BookmarkGroups.Commands.CreateBookmarkGroup;

public record CreateBookmarkGroupCommand : IRequest<CreateBookmarkGroupResponse>
{
    public string Name { get; set; } = string.Empty;
    public List<Tag> Tags { get; set; } = new List<Tag>();
    public string[] ContactIds { get; set; } = Array.Empty<string>();
    public string[] MediaIds { get; set; } = Array.Empty<string>();
    public string[] MediaSectionIds { get; set; } = Array.Empty<string>();
}

public class CreateBookmarkGroupCommandHandler : IRequestHandler<CreateBookmarkGroupCommand, CreateBookmarkGroupResponse>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public CreateBookmarkGroupCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<CreateBookmarkGroupResponse> Handle(CreateBookmarkGroupCommand request, CancellationToken cancellationToken)
    {
        var entity = await GetBookmarkByNameAsync(request.Name, cancellationToken);

        var bookmarks = (await GetBookMarksAsync(
                        _context.Contacts,
                        request.ContactIds,
                        cancellationToken,
                        contact => contact.AddressId,
                        (contact, id) => contact.AddressId = id
                    ))
            .Concat(await GetBookMarksAsync(
                        _context.Medias,
                        request.MediaIds,
                        cancellationToken,
                        media => media.AddressId,
                        (media, id) => media.AddressId = id
                    ))
            .Concat(await GetBookMarksAsync(
                        _context.MediaSections,
                        request.MediaSectionIds,
                        cancellationToken,
                        mediaSection => mediaSection.AddressId,
                        (mediaSection, id) => mediaSection.AddressId = id
                    ));

        var isNotFound = entity is null;
        if (isNotFound)
        {
            entity = _mapper.Map<BookmarkGroup>(request);
            entity.BookMarks = bookmarks.ToList();
            await _context.BookmarkGroups.AddAsync(entity);
        }
        else
        {
            if (!entity!.BookMarks!.Any())
                entity.BookMarks = new List<BookMark>();

            foreach (var item in bookmarks)
            {
                entity.BookMarks?.Add(item);
            }
        }

        await _context.SaveChangesAsync(cancellationToken);

        return _mapper.Map<CreateBookmarkGroupResponse>(entity);
    }

    internal async Task<BookmarkGroup?> GetBookmarkByNameAsync(string name, CancellationToken cancellationToken)
    {
        return await _context.BookmarkGroups
            .Include(e => e.BookMarks)
            .FirstOrDefaultAsync(e => e.Name.ToLower().Equals(name.ToLower()), cancellationToken);
    }

    internal async Task<IEnumerable<BookMark>> GetBookMarksAsync<T>(
        DbSet<T> dbSet,
        string[] ids,
        CancellationToken cancellationToken,
        Func<T, Guid?> getAddressId,
        Action<T, Guid> setAddressId
    ) where T : class
    {
        if (!ids.Any())
            return new List<BookMark>();

        var guidIds = ids.Select(id => Guid.Parse(id)).ToList();
        var parameter = Expression.Parameter(typeof(T), "e");
        var property = Expression.Property(parameter, typeof(T).GetProperty("Id")!);
        var containsMethod = typeof(List<Guid>).GetMethod("Contains", new[] { typeof(Guid) });
        var guidIdsExpression = Expression.Constant(guidIds);
        var containsExpression = Expression.Call(guidIdsExpression, containsMethod!, property);
        var lambdaExpression = Expression.Lambda<Func<T, bool>>(containsExpression, parameter);

        var entities = await dbSet.AsNoTracking()
            .Where(lambdaExpression)
            .ToListAsync(cancellationToken);

        var bookmarks = new List<BookMark>();

        foreach (var entity in entities)
        {
            var bookMark = new BookMark
            {
                BookMarkType = typeof(T) switch
                {
                    Type t when t == typeof(Contact) => BookMarkType.Contact,
                    Type t when t == typeof(Media) => BookMarkType.Media,
                    Type t when t == typeof(MediaSection) => BookMarkType.MediaSection,
                    _ => throw new InvalidOperationException("Unsupported type")
                }
            };

            var addressId = getAddressId(entity);
            if (addressId is null)
            {
                var address = new Address();
                await _context.Address.AddAsync(address, cancellationToken);
                setAddressId(entity, address.Id);
                bookMark.AddressId = address.Id;
                dbSet.Update(entity);
            }
            else
            {
                bookMark.AddressId = addressId ?? Guid.Empty;
            }

            bookmarks.Add(bookMark);
        }

        await _context.SaveChangesAsync(cancellationToken);

        return bookmarks;
    }

}
