﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.PRActivityTasks.Commands.CreatePRActivityTask;

namespace PRManagement.Application.PRActivityTasks.Commands.RemovePRActivityTaskOwner;

public record RemovePRActivityTaskOwnerCommand(string Id, string PRActivityTaskId) : IRequest<OwnerDto>;

public class RemovePRActivityTaskOwnerCommandHandler : IRequestHandler<RemovePRActivityTaskOwnerCommand, OwnerDto>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public RemovePRActivityTaskOwnerCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<OwnerDto> Handle(RemovePRActivityTaskOwnerCommand request, CancellationToken cancellationToken)
    {
        var entity = await _context.PRActivityTaskResponsiblePersons
            .FirstOrDefaultAsync(e => 
                e.Id == request.Id.ToGuid() &&
                e.PRActivityTaskId == request.PRActivityTaskId.ToGuid(), 
                cancellationToken);

        Guard.Against.NotFound(request.Id, entity);

        _context.PRActivityTaskResponsiblePersons.Remove(entity);
        await _context.SaveChangesAsync(cancellationToken);

        return _mapper.Map<OwnerDto>(entity);
    }
}
