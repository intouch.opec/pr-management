using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Constants;

namespace PRManagement.Application.PRActivityTasks.Commands.RemovePRActivityTaskOwner;

public class RemovePRActivityTaskOwnerCommandValidator : AbstractValidator<RemovePRActivityTaskOwnerCommand>
{
    private readonly IApplicationDbContext _context;

    public RemovePRActivityTaskOwnerCommandValidator(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(src => src.Id)
            .StringIsGuid()
            .MustAsync(HasPrActivityTaskOwner)
            .WithMessage(ValidatorMessages.NotFound);
    }

    public async Task<bool> HasPrActivityTaskOwner(string id, CancellationToken cancellationToken)
    {
        if (!id.IsValidGuid())
            return false;

        return await _context.PRActivityTaskResponsiblePersons
            .AnyAsync(e => e.Id == id.ToGuid(), cancellationToken);
    }
}