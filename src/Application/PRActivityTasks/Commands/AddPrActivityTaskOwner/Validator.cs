using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Constants;

namespace PRManagement.Application.PRActivityTasks.Commands.AddPrActivityTaskOwner;

public class AddPrActivityTaskOwnerCommandValidator : AbstractValidator<AddPrActivityTaskOwnerCommand>
{
    private readonly IApplicationDbContext _context;
    private readonly IIdentityService _identityService;

    public AddPrActivityTaskOwnerCommandValidator(
        IApplicationDbContext context,
        IIdentityService identityService)
    {
        _context = context;
        _identityService = identityService;

        RuleFor(e => e.PrActivityTaskId)
            .NotEmpty()
            .StringIsGuid()
            .MustAsync(HasPrActivityTask)
            .WithMessage(ValidatorMessages.NotFound);

        RuleFor(e => e.OwnerId)
            .NotEmpty()
            .StringIsGuid()
            .MustAsync(HasUserAsync)
            .WithMessage(ValidatorMessages.NotFound)
            .MustAsync(PrActivityTaskHasOwnerAlreadyAsync)
            .WithMessage(ValidatorMessages.ShouldNotBeDuplicate);
    }

    internal async Task<bool> HasPrActivityTask(string prActivityTaskId, CancellationToken cancellationToken)
        => !prActivityTaskId.IsValidGuid() || await _context.PRActivityTasks
            .AnyAsync(p => p.Id.Equals(prActivityTaskId.ToGuid()), cancellationToken);

    internal async Task<bool> HasUserAsync(string id, CancellationToken cancellationToken)
    {
        if (!id.IsValidGuid()) return true;

        return await _identityService.HasUserAsync(id);
    }

    internal async Task<bool> PrActivityTaskHasOwnerAlreadyAsync(AddPrActivityTaskOwnerCommand command, string ownerId, CancellationToken cancellationToken)
    {
        if (!ownerId.IsValidGuid() ||
            !command.PrActivityTaskId.IsValidGuid())
            return true;
            
        return !await _context.PRActivityTaskResponsiblePersons
            .AnyAsync(p =>
                p.OwnerId.Equals(ownerId) &&
                p.PRActivityTaskId.Equals(command.PrActivityTaskId.ToGuid()),
            cancellationToken);
    }

}
