﻿using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.PRActivityTasks.Commands.CreatePRActivityTask;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.PRActivityTasks.Commands.AddPrActivityTaskOwner;

public record AddPrActivityTaskOwnerCommand : IRequest<OwnerDto>
{
    public string PrActivityTaskId { get; set; } = string.Empty;
    public string OwnerId { get; set; } = string.Empty;
}

public class AddPrActivityTaskOwnerCommandHandler : IRequestHandler<AddPrActivityTaskOwnerCommand, OwnerDto>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public AddPrActivityTaskOwnerCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<OwnerDto> Handle(AddPrActivityTaskOwnerCommand request, CancellationToken cancellationToken)
    {
        var entity = _mapper.Map<PRActivityTaskResponsiblePerson>(request);

        await _context.PRActivityTaskResponsiblePersons
            .AddAsync(entity, cancellationToken);

        await _context.SaveChangesAsync(cancellationToken);

        return await _context.PRActivityTaskResponsiblePersons
            .Include(e => e.Owner)
            .ProjectTo<OwnerDto>(_mapper.ConfigurationProvider)
            .FirstAsync(e => e.Id.Equals(entity.Id), cancellationToken);
    }
}
