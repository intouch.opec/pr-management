using PRManagement.Application.Common.Extensions;
using PRManagement.Application.PRActivityTasks.Commands.CreatePRActivityTask;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.PRActivityTasks.Commands.AddPrActivityTaskOwner;

public class MappingProfile
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<PRActivityTaskResponsiblePerson, OwnerDto>()
                .ForMember(src => src.Name, op => op.MapFrom(src => $"{src.Owner!.NickName ?? "-"}"))
                .ForMember(src => src.Email, op => op.MapFrom(src => src.Owner!.Email ?? string.Empty))
                .ForMember(src => src.UserId, op => op.MapFrom(src => src.OwnerId));

            CreateMap<AddPrActivityTaskOwnerCommand, PRActivityTaskResponsiblePerson>()
                .ForMember(dest => dest.OwnerId, op => op.MapFrom(src => src.OwnerId))
                .ForMember(dest => dest.PRActivityTaskId, op => op.MapFrom(src => src.PrActivityTaskId.ToGuidOnly()));

        }
    }

}
