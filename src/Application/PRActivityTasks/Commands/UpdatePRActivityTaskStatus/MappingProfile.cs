using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;
using PRManagement.Domain.Exceptions;

namespace PRManagement.Application.PRActivityTasks.Commands.UpdatePRActivityTaskCommentStatus;

public class MappingProfile
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<UpdatePRActivityTaskStatusCommand, PRActivityTask>()
                .ForMember(dest => dest.TaskWorkerStatus, op => op.MapFrom(src => src.TaskWorkerStatus.ToEnum<TaskWorkerStatus>()));

        }
    }
}
