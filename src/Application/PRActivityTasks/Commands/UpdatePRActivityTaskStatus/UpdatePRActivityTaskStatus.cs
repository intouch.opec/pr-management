﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.PRActivityTasks.Commands.CreatePRActivityTask;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.PRActivityTasks.Commands.UpdatePRActivityTaskCommentStatus;

public record UpdatePRActivityTaskStatusCommand : IRequest<CreatePRActivityTaskDto>
{
    public string Id { get; set; } = string.Empty;
    public string TaskWorkerStatus { get; set; } = string.Empty;
}

public class UpdatePRActivityTaskStatusCommandHandler : IRequestHandler<UpdatePRActivityTaskStatusCommand, CreatePRActivityTaskDto>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public UpdatePRActivityTaskStatusCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<CreatePRActivityTaskDto> Handle(UpdatePRActivityTaskStatusCommand request, CancellationToken cancellationToken)
    {
        var entity = await _context.PRActivityTasks
            .AsTracking()
            .FirstOrDefaultAsync(
                e => e.Id.Equals(request.Id.ToGuidOnly()),
                cancellationToken);

        Guard.Against.NotFound(request.Id, entity);
        _mapper.Map<UpdatePRActivityTaskStatusCommand, PRActivityTask>(request, entity);

        await _context.SaveChangesAsync(cancellationToken);

        return await _context.PRActivityTasks
            .Include(e => e.PRActivityTaskResponsiblePersons!)
            .ThenInclude(e => e.Owner)
            .AsNoTracking()
            .Where(e => e.Id.Equals(entity.Id))
            .ProjectTo<CreatePRActivityTaskDto>(_mapper.ConfigurationProvider)
            .FirstAsync(cancellationToken);
    }
}
