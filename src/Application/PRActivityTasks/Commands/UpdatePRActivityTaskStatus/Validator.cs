using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Constants;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.PRActivityTasks.Commands.UpdatePRActivityTaskCommentStatus;

public class UpdatePRActivityTaskCommentStatusCommandValidator : AbstractValidator<UpdatePRActivityTaskStatusCommand>
{
    private readonly IApplicationDbContext _context;

    public UpdatePRActivityTaskCommentStatusCommandValidator(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(e => e.Id)
            .StringIsGuid()
            .MustAsync(HasPRActivityTask)
            .WithMessage(ValidatorMessages.NotFound)
            .NotEmpty();

        RuleFor(e => e.TaskWorkerStatus)
            .NotEmpty()
            .MustEnum<UpdatePRActivityTaskStatusCommand, TaskWorkerStatus>();

    }

    internal async Task<bool> HasPRActivityTask(string id, CancellationToken cancellationToken)
    {
        if (id.IsValidGuid())
            return true;

        return await _context.PRActivityTasks
            .AnyAsync(
                src => src.Id.Equals(id.ToGuid()),
                cancellationToken);
    }

}