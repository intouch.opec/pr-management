using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Constants;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.PRActivityTasks.Commands.CreatePRActivityTask;

public class CreatePRActivityTaskCommandValidator : AbstractValidator<CreatePRActivityTaskCommand>
{
    private readonly IApplicationDbContext _context;
    private readonly IIdentityService _identityService;
    public CreatePRActivityTaskCommandValidator(IApplicationDbContext context, IIdentityService identityService)
    {
        _context = context;
        _identityService = identityService;

        RuleForEach(e => e.OwnerIds)
            .NotEmpty()
            .StringIsGuid()
            .MustAsync(HasUserAsync)
            .WithMessage(ValidatorMessages.NotFound);

        RuleFor(e => e.WorkTask)
            .MustEnum<CreatePRActivityTaskCommand, WorkTask>()
            .NotEmpty();

        RuleFor(e => e.TaskWorkerStatus)
            .MustEnum<CreatePRActivityTaskCommand, TaskWorkerStatus>()
            .NotEmpty();

        RuleFor(e => e.PRActivityId)
            .MustAsync(HasPRActivity)
            .WithMessage(ValidatorMessages.NotFound)
            .NotEmpty();
    }

    internal async Task<bool> HasUserAsync(string id, CancellationToken cancellationToken)
    {
        if (!id.IsValidGuid()) return true;

        return await _identityService.HasUserAsync(id);
    }

    internal async Task<bool> HasPRActivity(string id, CancellationToken cancellationToken)
    {
        if (id.IsValidGuid())
            return true;

        return await _context.PRActivities
            .AsNoTracking()
            .AnyCacheAsync(
                src => src.Id.Equals(id.ToGuid()),
                cancellationToken);
    }

}