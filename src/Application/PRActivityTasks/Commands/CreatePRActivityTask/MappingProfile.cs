using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;
using PRManagement.Domain.Exceptions;

namespace PRManagement.Application.PRActivityTasks.Commands.CreatePRActivityTask;

public class MappingProfile
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<CreatePRActivityTaskCommand, PRActivityTask>()
                .ForMember(dest => dest.TaskWorkerStatus, op => op.MapFrom(src => src.TaskWorkerStatus.ToEnumOrNull<TaskWorkerStatus>()))
                .ForMember(dest => dest.WorkTask, op => op.MapFrom(src => src.WorkTask.ToEnumOrNull<WorkTask>()))
                .ForMember(dest => dest.PRActivityTaskResponsiblePersons, op => op.MapFrom<BookmarkGroupToFileResolver>());

            CreateMap<PRActivityTask, CreatePRActivityTaskDto>()
                .ForMember(dest => dest.Owners, op => op.MapFrom(src =>
                    src.PRActivityTaskResponsiblePersons!
                    .Select(p => new OwnerDto()
                    {
                        Id = p.Id,
                        Name = $"{p.Owner!.NickName}",
                        Email = $"{p.Owner!.Email}",
                        UserId = p.OwnerId,
                    })));
        }
    }

    public class BookmarkGroupToFileResolver : IValueResolver<CreatePRActivityTaskCommand, PRActivityTask, ICollection<PRActivityTaskResponsiblePerson>?>
    {
        public ICollection<PRActivityTaskResponsiblePerson>? Resolve(
            CreatePRActivityTaskCommand source,
            PRActivityTask destination,
            ICollection<PRActivityTaskResponsiblePerson>? member, ResolutionContext context)
        {
            var prActivityTaskResponsiblePersons = source.OwnerIds
                .Select(id =>
                    new PRActivityTaskResponsiblePerson()
                    {
                        OwnerId = id,
                        PRActivityTaskId = Guid.NewGuid()
                    }
                ).ToList();
            return prActivityTaskResponsiblePersons;
        }
    }

}
