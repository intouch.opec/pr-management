using PRManagement.Domain.Enums;

namespace PRManagement.Application.PRActivityTasks.Commands.CreatePRActivityTask;

public class CreatePRActivityTaskDto
{
    public Guid Id { get; set; }
    public WorkTask WorkTask { get; set; }
    public TaskWorkerStatus TaskWorkerStatus { get; set; }
    public DateTimeOffset DueDate { get; set; }
    public string Description { get; set; } = string.Empty;
    public DateTimeOffset Created { get; set; }
    public string? CreatedBy { get; set; }
    public DateTimeOffset LastModified { get; set; }
    public string? LastModifiedBy { get; set; }
    public IEnumerable<OwnerDto> Owners { get; set; } = Enumerable.Empty<OwnerDto>();
}

public class OwnerDto
{
    public Guid Id { get; set; } 
    public string Name { get; set; } = string.Empty;
    public string Email { get; set; } = string.Empty;
    public string UserId { get; set; } = string.Empty;
}
