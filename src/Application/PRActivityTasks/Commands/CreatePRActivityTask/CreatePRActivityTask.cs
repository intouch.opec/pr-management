﻿using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.PRActivityTasks.Commands.CreatePRActivityTask;

public record CreatePRActivityTaskCommand : IRequest<CreatePRActivityTaskDto>
{
    public DateTimeOffset DueDate { get; set; }
    public string TaskWorkerStatus { get; set; } = string.Empty;
    public string WorkTask { get; set; } = string.Empty;
    public string Description { get; set; } = string.Empty;
    public string PRActivityId { get; set; } = string.Empty;
    public IEnumerable<string> OwnerIds { get; set; } = Enumerable.Empty<string>();
}

public class CreatePRActivityTaskCommandHandler : IRequestHandler<CreatePRActivityTaskCommand, CreatePRActivityTaskDto>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public CreatePRActivityTaskCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<CreatePRActivityTaskDto> Handle(CreatePRActivityTaskCommand request, CancellationToken cancellationToken)
    {
        var entity = _mapper.Map<PRActivityTask>(request);

        await _context.PRActivityTasks.AddAsync(entity, cancellationToken);
        await _context.SaveChangesAsync(cancellationToken);

        return await _context.PRActivityTasks
            .Include(e => e.PRActivityTaskResponsiblePersons!)
            .ThenInclude(e => e.Owner)
            .AsNoTracking()
            .Where(e => e.Id.Equals(entity.Id))
            .ProjectTo<CreatePRActivityTaskDto>(_mapper.ConfigurationProvider)
            .FirstAsync(cancellationToken);
    }
}
