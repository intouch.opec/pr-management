using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Constants;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.PRActivityTasks.Commands.UpdatePRActivityTask;

public class UpdatePRActivityTaskCommandValidator : AbstractValidator<UpdatePRActivityTaskCommand>
{
    private readonly IApplicationDbContext _context;

    public UpdatePRActivityTaskCommandValidator(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(e => e.Id)
            .StringIsGuid()
            .MustAsync(HasPRActivityTask)
            .WithMessage(ValidatorMessages.NotFound)
            .NotEmpty();

        RuleFor(e => e.TaskWorkerStatus)
            .NotEmpty()
            .MustEnum<UpdatePRActivityTaskCommand, TaskWorkerStatus>();

        RuleFor(e => e.WorkTask)
            .MustEnum<UpdatePRActivityTaskCommand, WorkTask>()
            .NotEmpty();
    }

    internal async Task<bool> HasPRActivityTask(string id, CancellationToken cancellationToken)
    {
        if (id.IsValidGuid())
            return true;

        return await _context.PRActivityTasks
            .AnyAsync(
                src => src.Id.Equals(id.ToGuid()),
                cancellationToken);
    }

}