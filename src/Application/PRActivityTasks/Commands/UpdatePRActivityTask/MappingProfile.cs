using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;
using PRManagement.Domain.Exceptions;

namespace PRManagement.Application.PRActivityTasks.Commands.UpdatePRActivityTask;

public class MappingProfile
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<UpdatePRActivityTaskCommand, PRActivityTask>()
                .ForMember(dest => dest.WorkTask, op => op.MapFrom(src => src.WorkTask.ToEnumOrNull<WorkTask>()))
                .ForMember(dest => dest.TaskWorkerStatus, op => op.MapFrom(src => src.TaskWorkerStatus.ToEnum<TaskWorkerStatus>()));

        }
    }
}
