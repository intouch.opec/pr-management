﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.PRActivityTasks.Commands.CreatePRActivityTask;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.PRActivityTasks.Commands.UpdatePRActivityTask;

public record UpdatePRActivityTaskCommand : IRequest<CreatePRActivityTaskDto>
{
    public string Id { get; set; } = string.Empty;
    public string WorkTask { get; set; } = string.Empty;
    public string TaskWorkerStatus { get; set; } = string.Empty;
    public DateTimeOffset DueDate { get; set; }
    public string Description { get; set; } = string.Empty;
}

public class UpdatePRActivityTaskCommandHandler : IRequestHandler<UpdatePRActivityTaskCommand, CreatePRActivityTaskDto>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public UpdatePRActivityTaskCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<CreatePRActivityTaskDto> Handle(UpdatePRActivityTaskCommand request, CancellationToken cancellationToken)
    {
        var entity = await _context.PRActivityTasks
           .AsTracking()
           .FirstOrDefaultAsync(
               e => e.Id.Equals(request.Id.ToGuidOnly()),
               cancellationToken);

        Guard.Against.NotFound(request.Id, entity);
        _mapper.Map<UpdatePRActivityTaskCommand, PRActivityTask>(request, entity);

        await _context.SaveChangesAsync(cancellationToken);

        return await _context.PRActivityTasks
           .Include(e => e.PRActivityTaskResponsiblePersons!)
           .ThenInclude(e => e.Owner)
           .AsNoTracking()
           .Where(e => e.Id.Equals(entity.Id))
           .ProjectTo<CreatePRActivityTaskDto>(_mapper.ConfigurationProvider)
           .FirstAsync(cancellationToken);
    }
}
