﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.PRActivityTasks.Commands.CreatePRActivityTask;

namespace PRManagement.Application.PRActivityTasks.Commands.RemovePRActivityTask;

public record RemovePRActivityTaskCommand(string Id) : IRequest<CreatePRActivityTaskDto>;

public class RemovePRActivityTaskCommandHandler : IRequestHandler<RemovePRActivityTaskCommand, CreatePRActivityTaskDto>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public RemovePRActivityTaskCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<CreatePRActivityTaskDto> Handle(RemovePRActivityTaskCommand request, CancellationToken cancellationToken)
    {
        var entity = await _context.PRActivityTasks
            .AsTracking()
            .SingleOrDefaultAsync(e => e.Id.Equals(request.Id.ToGuid()), cancellationToken);

        Guard.Against.NotFound(request.Id, entity);
      
        _context.PRActivityTasks.Remove(entity);
        await _context.SaveChangesAsync(cancellationToken);

        return _mapper.Map<CreatePRActivityTaskDto>(entity);

    }
}
