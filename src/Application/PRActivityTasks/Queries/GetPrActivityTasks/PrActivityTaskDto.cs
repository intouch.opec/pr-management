using PRManagement.Application.PRActivityTaskComments.Commands.CreatePRActivityTask;
using PRManagement.Application.PRActivityTasks.Commands.CreatePRActivityTask;

namespace PRManagement.Application.PRActivityTasks.Queries.GetPrActivityTasks;

public class PrActivityTaskDto : CreatePRActivityTaskDto
{
    public IEnumerable<PRActivityTaskCommentDto> Comments { get; set; } = Enumerable.Empty<PRActivityTaskCommentDto>();
}
