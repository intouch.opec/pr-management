using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Constants;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.PRActivityTasks.Queries.GetPrActivityTasks;

public class GetPrActivityTasksQueryValidator : AbstractValidator<GetPrActivityTasksQuery>
{
    private readonly IApplicationDbContext _context;
    
    public GetPrActivityTasksQueryValidator(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(src => src.Id)
            .StringIsGuid()
            .NotEmpty()
            .MustAsync(HasPRActivity)
            .WithMessage(ValidatorMessages.NotFound);

        RuleFor(src => src.TaskWorkerStatus)
            .MustEnum<GetPrActivityTasksQuery, TaskWorkerStatus>();
            
    }

    internal async Task<bool> HasPRActivity(string prActivityId, CancellationToken cancellationToken)
       => !prActivityId.IsValidGuid() || await _context.PRActivities
           .AnyAsync(p => p.Id.Equals(new Guid(prActivityId)), cancellationToken);

}
