using PRManagement.Application.PRActivityTaskComments.Commands.CreatePRActivityTask;
using PRManagement.Application.PRActivityTasks.Commands.CreatePRActivityTask;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.PRActivityTasks.Queries.GetPrActivityTasks;

public class MappingProfile
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<PRActivityTask, PrActivityTaskDto>()
               .ForMember(dest => dest.Owners, op => op.MapFrom(src =>
                    src.PRActivityTaskResponsiblePersons != null
                        ? src.PRActivityTaskResponsiblePersons.Select(p => new OwnerDto
                        {
                            Id = p.Id,
                            Name = p.Owner!.NickName ?? string.Empty,
                            Email = p.Owner!.Email ?? string.Empty, 
                            UserId = p.OwnerId,
                        }).ToList() : new List<OwnerDto>()
                ))

            .ForMember(dest => dest.Comments, op => op.MapFrom(src =>
                src.PRActivityTaskComments != null
                    ? src.PRActivityTaskComments.Select(p => new PRActivityTaskCommentDto
                    {
                        Id = p.Id,
                        Content = p.Content ?? string.Empty,
                        OwnerName = p.Owner!.Email ?? string.Empty,
                        Created = p.Created,
                        LastModified = p.LastModified,
                        LastModifiedBy = p.LastModifiedBy ?? string.Empty,
                    }).ToList()
                    : new List<PRActivityTaskCommentDto>()
                ));


        }
    }

}
