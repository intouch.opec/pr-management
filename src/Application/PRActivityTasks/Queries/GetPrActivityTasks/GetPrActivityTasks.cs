﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Enums;
using PRManagement.Domain.Exceptions;

namespace PRManagement.Application.PRActivityTasks.Queries.GetPrActivityTasks;

public record GetPrActivityTasksQuery(string Id) : IRequest<IEnumerable<PrActivityTaskDto>>
{
    public string TaskWorkerStatus { get; set; } = string.Empty;
}

public class GetPrActivityTasksQueryHandler : IRequestHandler<GetPrActivityTasksQuery, IEnumerable<PrActivityTaskDto>>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public GetPrActivityTasksQueryHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<IEnumerable<PrActivityTaskDto>> Handle(GetPrActivityTasksQuery request, CancellationToken cancellationToken)
    {
        return await _context.PRActivityTasks
            .Include(e => e.PRActivityTaskResponsiblePersons!)
            .ThenInclude(e => e.Owner)
            .Include(e => e.PRActivityTaskComments!)
            .ThenInclude(e => e.Owner)
            .WhereEqual(e => e.TaskWorkerStatus, request.TaskWorkerStatus.ToEnumOrNull<TaskWorkerStatus>())
            .Where(e => e.PRActivityId.Equals(request.Id.ToGuid()))
            .ProjectTo<PrActivityTaskDto>(_mapper.ConfigurationProvider)
            .ToListAsync(cancellationToken);
    }
}
