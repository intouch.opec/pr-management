﻿using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.PRActivityEvents.Commands.CreatePRActivityEvent;

public record CreatePRActivityEventCommand : IRequest<PRActivityEventResponse>
{
    public DateTimeOffset EventDate { get; set; }
    public string PRActivityId { get; set; } = string.Empty;
}

public class CreatePRActivityEventCommandHandler : IRequestHandler<CreatePRActivityEventCommand, PRActivityEventResponse>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public CreatePRActivityEventCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<PRActivityEventResponse> Handle(CreatePRActivityEventCommand request, CancellationToken cancellationToken)
    {
        var entity = _mapper.Map<PRActivityEvent>(request);
        await _context.PRActivityEvents.AddAsync(entity, cancellationToken);
        await _context.SaveChangesAsync(cancellationToken);
        return _mapper.Map<PRActivityEventResponse>(entity);
    }
}
