using PRManagement.Application.Common.Extensions;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.PRActivityEvents.Commands.CreatePRActivityEvent;

public class MappingProfile
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<PRActivityEventResponse, PRActivityEvent>()
                .ForMember(dest => dest.EventCode, op => op.MapFrom(src => StringExtension.GenerateRandomString(10)));

        }
    }

}
