using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Constants;

namespace PRManagement.Application.PRActivityEvents.Commands.CreatePRActivityEvent;

public class CreatePRActivityEventCommandValidator : AbstractValidator<CreatePRActivityEventCommand>
{
    private readonly IApplicationDbContext _context;

    public CreatePRActivityEventCommandValidator(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(e => e.PRActivityId)
            .StringIsGuid()
            .NotEmpty()
            .MustAsync(_context.PRActivities.HasPRActivity)
            .WithMessage(ValidatorMessages.NotFound);
            
        RuleFor(e => e.EventDate)
            .NotEmpty();

    }
}
