﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.PRActivityEvents.Commands.CreatePRActivityEvent;

namespace PRManagement.Application.PRActivityEvents.Queries.GetPRActivityEvent;

public record GetPRActivityEventQuery(string PrActivityId) : IRequest<PRActivityEventResponse>;

public class GetPRActivityEventQueryHandler : IRequestHandler<GetPRActivityEventQuery, PRActivityEventResponse>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public GetPRActivityEventQueryHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<PRActivityEventResponse> Handle(GetPRActivityEventQuery request, CancellationToken cancellationToken)
    {
        var entity = await _context.PRActivityEvents
            .AsNoTracking()
            .FirstOrDefaultAsync(
                e => e.PRActivityId == request.PrActivityId.ToGuidOnly(),
                cancellationToken);

        Guard.Against.NotFound(request.PrActivityId, entity);

        return _mapper.Map<PRActivityEventResponse>(entity);
    }
}
