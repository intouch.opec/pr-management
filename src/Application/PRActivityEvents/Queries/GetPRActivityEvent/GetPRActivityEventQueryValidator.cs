
using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Constants;

namespace PRManagement.Application.PRActivityEvents.Queries.GetPRActivityEvent;

public class GetPRActivityEventQueryValidator : AbstractValidator<GetPRActivityEventQuery>
{
    private readonly IApplicationDbContext _context;
    public GetPRActivityEventQueryValidator(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(e => e.PrActivityId)
            .StringIsGuid()
            .NotEmpty()
            .MustAsync(_context.PRActivities.HasPRActivity)
            .WithMessage(ValidatorMessages.NotFound);

    }
}