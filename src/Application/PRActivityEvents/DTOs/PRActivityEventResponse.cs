namespace PRManagement.Application.PRActivityEvents.Commands.CreatePRActivityEvent;

public class PRActivityEventResponse
{
    public Guid Id { get; set; }
    public string EventCode { get; set; } = string.Empty;
    public int NumberOfJoiner { get; set; }
    public DateTimeOffset EventDate { get; set; }
    public Guid PRActivityId { get; set; }
    public DateTimeOffset Created { get; set; }
    public string? CreatedBy { get; set; }
    public DateTimeOffset LastModified { get; set; }
    public string? LastModifiedBy { get; set; }
}
