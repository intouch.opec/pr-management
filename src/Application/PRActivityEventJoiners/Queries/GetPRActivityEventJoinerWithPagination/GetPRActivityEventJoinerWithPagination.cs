﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.Common.Mappings;
using PRManagement.Application.Common.Models;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.PRActivityEventJoiners.Queries.GetPRActivityEventJoinerWithPagination;

public record GetPRActivityEventJoinerWithPaginationQuery : IRequest<PaginatedList<GetContactItem>>
{
    public string? FirstName { get; set; }
    public string? Search { get; set; }
    public string? LastName { get; set; }
    public string? NickName { get; set; }
    public string? Email { get; set; }
    public string PrActivityId { get; set; } = string.Empty;

    public int? PageNumber { get; init; } = 1;
    public int? PageSize { get; init; } = 10;
    public SortType? Sort { get; set; }
    public string? SortBy { get; set; }
}

public class GetPRActivityEventJoinerWithPaginationQueryHandler : IRequestHandler<GetPRActivityEventJoinerWithPaginationQuery, PaginatedList<GetContactItem>>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public GetPRActivityEventJoinerWithPaginationQueryHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<PaginatedList<GetContactItem>> Handle(GetPRActivityEventJoinerWithPaginationQuery request, CancellationToken cancellationToken)
    {
        return await Search(request)
            .ProjectTo<GetContactItem>(_mapper.ConfigurationProvider)
            .OrderBy(request.SortBy ?? nameof(GetContactItem.LastModified), ascending: request.Sort ?? SortType.DESC)
            .PaginatedListAsync(request.PageNumber ?? 1, request.PageSize ?? 10, cancellationToken);
    }

    internal IQueryable Search(GetPRActivityEventJoinerWithPaginationQuery request)
    {
        return _context.PRActivityEventJoiners
            .Include(e => e.Contact)
            .ThenInclude(e => e!.Address)
            .Include(e => e.Contact)
            .ThenInclude(a => a!.ContactMediaSections!.Where(cms => cms != null))
            .Include(e => e.Contact)
            .ThenInclude(a => a!.PRActivityEventJoiners!.Where(pr => pr != null))
            .AsNoTracking()
            .Where(pr => pr.PRActivityId.Equals(request.PrActivityId.ToGuidOnly()))
            .WhereLike(e => e.Contact!.FirstName.ToLower(), request.FirstName)
            .WhereLike(e => e.Contact!.LastName.ToLower(), request.LastName)
            .WhereLike(e => e.Contact!.NickName.ToLower(), request.NickName)
            .WhereLikeArray(e => e.Contact!.Address!.Email.Select(e => e.ToLower()).ToArray(), request.Email)
            .SearchOrMultiple(request.Search,
                e => e.Contact!.FirstName,
                e => e.Contact!.LastName,
                e => e.Contact!.NickName,
                e => e.Contact!.Position,
                e => e.Contact!.Address!.Text,
                e => e.Contact!.Address!.PhoneNumbers,
                e => e.Contact!.Address!.Email,
                e => e.Contact!.ContactMediaSections!.Select(e => e.MediaSection!.Section.Name),
                e => e.Contact!.ContactMediaSections!.Select(e => e.MediaSection!.SubSection.Name));
    }
}
