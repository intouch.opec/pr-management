using PRManagement.Domain.Entities;

namespace PRManagement.Application.PRActivityEventJoiners.Queries.GetPRActivityEventJoinerWithPagination;

public class MappingProfile
{
     public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<PRActivityEventJoiner, GetContactItem>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src!.Contact!.Address!.Email))
                .ForMember(dest => dest.PhoneNumbers, opt => opt.MapFrom(src => src.Contact!.Address!.PhoneNumbers))
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.Contact!.FirstName))
                .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.Contact!.LastName))
                .ForMember(dest => dest.NickName, opt => opt.MapFrom(src => src.Contact!.NickName))
                .ForMember(dest => dest.Created, opt => opt.MapFrom(src => src.Created))
                .ForMember(dest => dest.CreatedBy, opt => opt.MapFrom(src => src.CreatedBy))
                .ForMember(dest => dest.LastModified, opt => opt.MapFrom(src => src.LastModified))
                .ForMember(dest => dest.LastModifiedBy, opt => opt.MapFrom(src => src.LastModifiedBy));
            
            
        }
    }
}
