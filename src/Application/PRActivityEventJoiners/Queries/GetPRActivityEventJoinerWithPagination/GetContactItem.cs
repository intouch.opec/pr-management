using PRManagement.Application.Contacts.Queries.GetContactAndMediaWithPagination;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.PRActivityEventJoiners.Queries.GetPRActivityEventJoinerWithPagination;

public class GetContactItem 
{
    public Guid Id { get; set; }
    public Guid ContactId { get; set; }
    public Guid PRActivityId { get; set; }
    public string FirstName { get; set; } = string.Empty;
    public string LastName { get; set; } = string.Empty;
    public string NickName { get; set; } = string.Empty;
    public string[] Email { get; set; } = Array.Empty<string>();
    public string[] PhoneNumbers { get; set; } = Array.Empty<string>();
    public string Position { get; set; } = string.Empty;
    public string LineId { get; set; } = string.Empty;
    public IEnumerable<string> MediaNames { get; set; } = Enumerable.Empty<string>();
    public JoinType JoinType { get; set; }
    public DateTimeOffset Created { get; set; }
    public string? CreatedBy { get; set; }
    public DateTimeOffset LastModified { get; set; }
    public string? LastModifiedBy { get; set; }

}
