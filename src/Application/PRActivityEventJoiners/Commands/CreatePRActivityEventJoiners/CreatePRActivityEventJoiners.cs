﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.PRActivityEventJoiners.Commands.CreatePRActivityEventJoiners;

public record CreatePRActivityEventJoinersCommand : IRequest<PRActivityEventJoinerResponse>
{
    public string ContactId { get; set; } = string.Empty;
    public string Code { get; set; } = string.Empty;
    public string PRActivityId { get; set; } = string.Empty;
}

public class CreatePRActivityEventJoinersCommandHandler : IRequestHandler<CreatePRActivityEventJoinersCommand, PRActivityEventJoinerResponse>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public CreatePRActivityEventJoinersCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<PRActivityEventJoinerResponse> Handle(CreatePRActivityEventJoinersCommand request, CancellationToken cancellationToken)
    {
        var entity = _mapper.Map<PRActivityEventJoiner>(request);

        var prActivityEvent = await _context.PRActivityEvents
            .FirstOrDefaultAsync(x => x.PRActivityId == request.PRActivityId.ToGuidOnly(), cancellationToken);
        
        await _context.PRActivityEventJoiners.AddAsync(entity, cancellationToken);
        prActivityEvent!.NumberOfJoiner += 1;
        await _context.SaveChangesAsync(cancellationToken);

        return _mapper.Map<PRActivityEventJoinerResponse>(entity);
    }
}
