using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.PRActivityEventJoiners.Commands.CreatePRActivityEventJoiners;

public class MappingProfile
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<CreatePRActivityEventJoinersCommand, PRActivityEventJoiner>()
                .ForMember(dest => dest.ContactId, op => op.MapFrom<CreatePRActivityEventJoinerContactIdCommandResolver>())
                .ForMember(dest => dest.PRActivityId, op => op.MapFrom(src => src.PRActivityId.ToGuidOnly()))
                .ForMember(dest => dest.JoinType, op => op.MapFrom<CreatePRActivityEventJoinersCommandResolver>());

            CreateMap<PRActivityEventJoiner, PRActivityEventJoinerResponse>();

        }
    }

    public class CreatePRActivityEventJoinersCommandResolver : IValueResolver<CreatePRActivityEventJoinersCommand, PRActivityEventJoiner, JoinType>
    {
        private readonly IApplicationDbContext _context;

        public CreatePRActivityEventJoinersCommandResolver(IApplicationDbContext context)
        {
            _context = context;
        }

        public JoinType Resolve(CreatePRActivityEventJoinersCommand source, PRActivityEventJoiner destination, JoinType member, ResolutionContext context)
        {
            var isBook = _context.PRActivityReceivers
                .Any(e => e.ContactId.Equals(source.ContactId.ToGuidOnly()) ||
                    e.Code.Equals(source.Code));

            return isBook ? JoinType.Book : JoinType.WalkIn;
        }
    }

    public class CreatePRActivityEventJoinerContactIdCommandResolver : IValueResolver<CreatePRActivityEventJoinersCommand, PRActivityEventJoiner, Guid>
    {
        private readonly IApplicationDbContext _context;

        public CreatePRActivityEventJoinerContactIdCommandResolver(IApplicationDbContext context)
        {
            _context = context;
        }

        public Guid Resolve(CreatePRActivityEventJoinersCommand source, PRActivityEventJoiner destination, Guid member, ResolutionContext context)
        {
            if (!string.IsNullOrWhiteSpace(source.ContactId))
                return source.ContactId.ToGuidOnly();

            var prActivityReceiver = _context.PRActivityReceivers
                .FirstOrDefault(e => e.ContactId.Equals(source.ContactId.ToGuidOnly()) ||
                    e.Code.Equals(source.Code));

            return prActivityReceiver!.ContactId;
        }
    }
}
