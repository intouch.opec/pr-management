namespace PRManagement.Application.PRActivityEventJoiners.Commands.CreatePRActivityEventJoiners;

public class PRActivityEventJoinerResponse
{
    public Guid Id { get; set; }
    public Guid ContactId { get; set; }
    public Guid PRActivityId { get; set; }
    public DateTimeOffset Created { get; set; }
    public string? CreatedBy { get; set; }
    public DateTimeOffset LastModified { get; set; }
    public string? LastModifiedBy { get; set; }
}
