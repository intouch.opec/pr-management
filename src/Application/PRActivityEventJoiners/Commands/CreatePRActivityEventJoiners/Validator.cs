using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Constants;

namespace PRManagement.Application.PRActivityEventJoiners.Commands.CreatePRActivityEventJoiners;

public class CreatePRActivityEventJoinersCommandValidator : AbstractValidator<CreatePRActivityEventJoinersCommand>
{
    private readonly IApplicationDbContext _context;

    public CreatePRActivityEventJoinersCommandValidator(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(e => e.PRActivityId)
            .StringIsGuid()
            .NotEmpty()
            .MustAsync(_context.PRActivities.HasPRActivity)
            .WithMessage(ValidatorMessages.NotFound);

        When(e => !string.IsNullOrWhiteSpace(e.ContactId), () =>
        {
            RuleFor(e => e.ContactId)
                .StringIsGuid()
                .NotEmpty()
                .WithMessage(ValidatorMessages.NotFound)
                .MustAsync(HasContact)
                .WithMessage(ValidatorMessages.NotFound);
        });

        When(e => !string.IsNullOrWhiteSpace(e.Code), () =>
        {
            RuleFor(e => e.Code)
                .NotEmpty()
                .WithMessage(ValidatorMessages.NotFound);
        });

        RuleFor(e => new { e.Code, e.ContactId })
            .Must(x => (!string.IsNullOrWhiteSpace(x.Code) && string.IsNullOrWhiteSpace(x.ContactId)) ||
                        (string.IsNullOrWhiteSpace(x.Code) && !string.IsNullOrWhiteSpace(x.ContactId)))
            .WithMessage("Either Code or ContactId must be provided, but not both");

    }

    internal async Task<bool> HasContact(string? contactId, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(contactId) || !contactId.IsValidGuid())
            return true;

        return await _context.Contacts
          .AnyAsync(p => p.Id.Equals(new Guid(contactId)), cancellationToken);
    }
}
