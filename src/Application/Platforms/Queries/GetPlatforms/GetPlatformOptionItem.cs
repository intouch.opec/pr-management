using PRManagement.Application.Common.Models;

namespace PRManagement.Application.Platforms.Queries.GetPlatforms;

public class GetPlatformOptionItem: OptionItemBase
{
    public string TypeOfMedia { get; set; }  = string.Empty;
}