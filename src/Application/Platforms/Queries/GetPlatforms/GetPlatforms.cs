﻿using PRManagement.Domain.Enums;
using PRManagement.Domain.Exceptions;

namespace PRManagement.Application.Platforms.Queries.GetPlatforms;

public record GetPlatformsQuery : IRequest<IEnumerable<GetPlatformOptionItem>>
{
    public string? TypeOfMedia { get; set; } = string.Empty;
}

public class GetPlatformsQueryHandler : IRequestHandler<GetPlatformsQuery, IEnumerable<GetPlatformOptionItem>>
{

    public async Task<IEnumerable<GetPlatformOptionItem>> Handle(GetPlatformsQuery request, CancellationToken cancellationToken)
    {
        var typeOfMedia = request.TypeOfMedia?.ToEnum<TypeOfMedia>();
        if (typeOfMedia.Equals(TypeOfMedia.DIGITAL))
            return DigitalPlatformType.PlatformTypeStrings.Select(e => new GetPlatformOptionItem()
            {
                Name = e,
                Id = e,
                TypeOfMedia = TypeOfMedia.DIGITAL.ToString()
            });

        if (typeOfMedia.Equals(TypeOfMedia.MAINSTREAM))
            return MainStreamPlatformType.PlatformTypeStrings.Select(e => new GetPlatformOptionItem()
            {
                Name = e,
                Id = e,
                TypeOfMedia = TypeOfMedia.MAINSTREAM.ToString()
            });

        return await Task.FromResult(PlatformTypes.Select(e => new GetPlatformOptionItem(){
            Id = e.GetEnumMemberValue(),
            Name = e.GetEnumMemberValue(),
            TypeOfMedia = MainStreamPlatformType.PlatformTypes.Any(pl => pl.Equals(e)) ? TypeOfMedia.MAINSTREAM.ToString() : TypeOfMedia.DIGITAL.ToString()
        }));
    }

    public IEnumerable<PlatformType> PlatformTypes { get; set; } = new List<PlatformType>
    {
        PlatformType.NEWSPAPER,
        PlatformType.MAGAZINE,
        PlatformType.TV,
        PlatformType.RADIO,
        PlatformType.FACEBOOK,
        PlatformType.INSTAGRAM,
        PlatformType.X,
        PlatformType.WEBSITE,
        PlatformType.TIKTOK,
        PlatformType.BLOG,
        PlatformType.LINE_Today,
        PlatformType.YOUTUBE,
        PlatformType.BLOCKDIT
    };
}
