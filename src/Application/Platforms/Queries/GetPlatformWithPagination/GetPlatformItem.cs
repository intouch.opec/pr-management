using PRManagement.Application.Common.Models;

namespace PRManagement.Application.Platforms.Queries.GetPlatformWithPagination;

public class GetPlatformItem : PlatformDto
{
    public string MediaName { get; set; } = string.Empty;
    public Guid Id { get; set; }
    public DateTimeOffset Created { get; set; }
    public string? CreatedBy { get; set; }
    public DateTimeOffset LastModified { get; set; }
    public string? LastModifiedBy { get; set; }
}
