using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.Common.Mappings;
using PRManagement.Application.Common.Models;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.Platforms.Queries.GetPlatformWithPagination;

public class GetPlatformWithPaginationQuery : IRequest<PaginatedList<GetPlatformItem>>
{
    public string? Name { get; set; } = string.Empty;
    public string? MediaName { get; set; } = string.Empty;
    public decimal? PRValueStart { get; set; }
    public decimal? PRValueEnd { get; set; }
    public decimal? ADValueStart { get; set; }
    public decimal? ADValueEnd { get; set; }
    public uint? PotentialStart { get; set; }
    public uint? PotentialEnd { get; set; }
    public uint? TierOderStart { get; set; }
    public uint? TierOderEnd { get; set; }
    public uint? TierPotentialStart { get; set; }
    public uint? TierPotentialEnd { get; set; }
    public PlatformType? PlatformType { get; set; }

    public int? PageNumber { get; init; } = 1;
    public int? PageSize { get; init; } = 10;
    public SortType? Sort { get; set; }
    public string? SortBy { get; set; }
}

public class GetPlatformWithPaginationHandler : IRequestHandler<GetPlatformWithPaginationQuery, PaginatedList<GetPlatformItem>>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public GetPlatformWithPaginationHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<PaginatedList<GetPlatformItem>> Handle(GetPlatformWithPaginationQuery request, CancellationToken cancellationToken)
    {
        return await Search(request)
            .ProjectTo<GetPlatformItem>(_mapper.ConfigurationProvider)
            .OrderBy(request.SortBy ?? string.Empty, ascending: request.Sort)
            .PaginatedListAsync(request.PageNumber ?? 1, request.PageSize ?? 10, cancellationToken);
    }

    internal IQueryable Search(GetPlatformWithPaginationQuery request)
    {
        return _context.Platforms
            .Include(p => p.MediaSection!.Media)
            .AsNoTracking()
            .WhereEqual(e => e.PlatFormType, request.PlatformType)
            .WhereLike(e => e.Name.ToLower(), request.Name)
            .WhereLike(e => e.MediaSection!.Media.Name.ToLower(), request.MediaName)
            .WhereRange(e => e.TierOder, request.TierOderStart, request.TierOderEnd)
            .WhereRange(e => e.TierPotential, request.TierPotentialStart, request.TierPotentialEnd)
            .WhereRange(e => e.Potential, request.PotentialStart, request.PotentialEnd)
            .WhereRange(e => e.PRValue, request.PRValueStart, request.ADValueEnd)
            .WhereRange(e => e.ADValue, request.ADValueStart, request.ADValueEnd);
    }
}
