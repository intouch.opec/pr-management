using PRManagement.Domain.Entities;

namespace PRManagement.Application.Platforms.Queries.GetPlatformWithPagination;

public class GetPlatformItemMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<Platform, GetPlatformItem>()
                .ForMember(dest => dest.MediaName, op => op.MapFrom(src => src.MediaSection!.Media.Name));
        }
    }
}
