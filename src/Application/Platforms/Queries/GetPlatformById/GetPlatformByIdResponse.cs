using PRManagement.Application.Common.Models;

namespace PRManagement.Application.Platforms.Queries.GetPlatformById;

public class GetPlatformByIdResponse : PlatformDto
{
    public Guid Id { get; set; }
    public DateTimeOffset Created { get; set; }
    public string? CreatedBy { get; set; }
    public DateTimeOffset LastModified { get; set; }
    public string? LastModifiedBy { get; set; }
}
