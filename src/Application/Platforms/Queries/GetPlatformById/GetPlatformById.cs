using PRManagement.Application.Common.Interfaces;

namespace PRManagement.Application.Platforms.Queries.GetPlatformById;

public record GetPlatformByIdQuery(Guid Id) : IRequest<GetPlatformByIdResponse>;

public class GetPlatformByIdRequestHandler : IRequestHandler<GetPlatformByIdQuery, GetPlatformByIdResponse>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public GetPlatformByIdRequestHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<GetPlatformByIdResponse> Handle(GetPlatformByIdQuery request, CancellationToken cancellationToken)
    {
        var entity = await _context.Platforms
          .AsNoTracking()
          .FirstOrDefaultAsync(e => e.Id.Equals(request.Id), cancellationToken);

        Guard.Against.NotFound(request.Id, entity);

        return _mapper.Map<GetPlatformByIdResponse>(entity);
    }
}
