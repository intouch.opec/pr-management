using PRManagement.Domain.Entities;

namespace PRManagement.Application.Platforms.Queries.GetPlatformById;

public class GetPlatformByIdMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<Platform, GetPlatformByIdResponse>();
        }
    }
}
