using PRManagement.Domain.Entities;

namespace PRManagement.Application.Platforms.Commands.CreatePlatform;

public class CreatePlatformMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<Platform, CreatePlatformResponse>();
            CreateMap<CreatePlatformCommand, Platform>();
        }
    }
}
