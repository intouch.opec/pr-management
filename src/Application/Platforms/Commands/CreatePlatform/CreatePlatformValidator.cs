using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.Platforms.Commands.CreatePlatform;

public class CreatePlatformValidator : AbstractValidator<CreatePlatformCommand>
{
    private readonly IApplicationDbContext _context;

    public CreatePlatformValidator(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(e => e.Link)
          .StringIsUrl()
          .NotEmpty()
          .When(e => e.PlatformType is not null);

        RuleFor(e => e.Name)
          .NotEmpty()
          .MustAsync(BeUniquePlatform)
          .WithMessage($"The {nameof(CreatePlatformCommand.Name)} , {nameof(CreatePlatformCommand.PlatformType)} combination is duplicated.");

        RuleFor(e => e.PlatformType)
          .MustAsync(BeMediaSectionDigitalType)
          .WithMessage($"The {nameof(CreatePlatformCommand.PlatformType)} is required");
    }

    public async Task<bool> BeMediaSectionDigitalType(CreatePlatformCommand request, string platformType, CancellationToken cancellationToken)
    {
        var mediaIsDigitalType = await _context.MediaSections.AsNoTracking()
          .AnyAsync(e =>
            e.Id.Equals(request.MediaSectionId) &&
            e.TypeOfMedia.Equals(TypeOfMedia.DIGITAL),
            cancellationToken);

        return mediaIsDigitalType && platformType != null;
    }

    public async Task<bool> BeUniquePlatform(CreatePlatformCommand request, string name, CancellationToken cancellationToken)
     => !await _context.Platforms
          .AsNoTracking()
          .AnyAsync(ms =>
            ms.MediaSectionId.Equals(request.MediaSectionId) &&
            ms.Name.Equals(name) &&
            ms.PlatFormType.Equals(request.PlatformType), cancellationToken);
}
