using PRManagement.Application.Common.Models;

namespace PRManagement.Application.Platforms.Commands.CreatePlatform;

public class CreatePlatformResponse : PlatformDto
{
    public Guid Id { get; set; }
}
