using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.Common.Models;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.Platforms.Commands.CreatePlatform;

public class CreatePlatformCommand : PlatformDto, IRequest<CreatePlatformResponse>
{
    public Guid MediaSectionId { get; set; }
}

public class CreatePlatformCommandHandler : IRequestHandler<CreatePlatformCommand, CreatePlatformResponse>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public CreatePlatformCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<CreatePlatformResponse> Handle(CreatePlatformCommand request, CancellationToken cancellationToken)
    {
        var entity = _mapper.Map<Platform>(request);

        var mediaSection = await _context.MediaSections
            .AsTracking()
            .SingleAsync(
                ms => ms.Id.Equals(entity.MediaSectionId), 
                cancellationToken);

        if (mediaSection.TierPotential > entity.TierPotential && entity.TierPotential != 0)
            mediaSection.TierPotential = entity.TierPotential;
            
        await _context.Platforms.AddAsync(entity);
        await _context.SaveChangesAsync(cancellationToken);

        return _mapper.Map<CreatePlatformResponse>(entity);
    }
}
