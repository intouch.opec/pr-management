using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;

namespace PRManagement.Application.Platforms.Commands.DeletePlatform;

public record DeletePlatformCommand : IRequest<DeletePlatformResponse>
{
    public Guid Id { get; set; }
    public string Remark { get; set; } = string.Empty;
}

public class DeletePlatformCommandHandler : IRequestHandler<DeletePlatformCommand, DeletePlatformResponse>
{
    private readonly IMapper _mapper;
    private readonly IApplicationDbContext _context;

    public DeletePlatformCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<DeletePlatformResponse> Handle(DeletePlatformCommand request, CancellationToken cancellationToken)
    {
        var entity = await _context.Platforms
          .SingleOrDefaultAsync(e => e.Id.Equals(request.Id), cancellationToken);

        Guard.Against.NotFound(request.Id, entity);

        _context.Platforms.RemoveAndAddRemark(entity, request.Remark);

        await _context.SaveChangesAsync(cancellationToken);

        return _mapper.Map<DeletePlatformResponse>(entity);
    }
}
