namespace PRManagement.Application.Platforms.Commands.DeletePlatform;

public class DeletePlatformResponse
{
    public Guid Id { get; set; }
}
