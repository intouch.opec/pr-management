using PRManagement.Domain.Entities;

namespace PRManagement.Application.Platforms.Commands.DeletePlatform;

public class UpdatePlatformMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<Platform, DeletePlatformResponse>();
        }
    }

}
