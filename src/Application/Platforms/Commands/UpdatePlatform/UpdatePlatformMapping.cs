using PRManagement.Domain.Entities;

namespace PRManagement.Application.Platforms.Commands.UpdatePlatform;

public class UpdatePlatformMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<Platform, UpdatePlatformResponse>();
            CreateMap<UpdatePlatformCommand, Platform>();
        }
    }
}
