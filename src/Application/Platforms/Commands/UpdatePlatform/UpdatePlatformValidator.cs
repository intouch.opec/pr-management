using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.Platforms.Commands.UpdatePlatform;

public class UpdatePlatformValidator : AbstractValidator<UpdatePlatformCommand>
{
    private readonly IApplicationDbContext _context;

    public UpdatePlatformValidator(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(e => e.Link)
          .StringIsUrl()
          .NotEmpty()
          .When(e => e.PlatformType is not null);

        RuleFor(e => e.Name)
          .NotEmpty()
          .MustAsync(BeUniquePlatform)
          .WithMessage($"The {nameof(UpdatePlatformCommand.Name)} , {nameof(UpdatePlatformCommand.PlatformType)} combination is duplicated.");

        RuleFor(e => e.PlatformType)
          .MustAsync(BeMediaSectionDigitalType)
          .WithMessage($"The {nameof(UpdatePlatformCommand.PlatformType)} is required");

    }

    public async Task<bool> BeMediaSectionDigitalType(UpdatePlatformCommand request, string platformType, CancellationToken cancellationToken)
    {
        var mediaIsDigitalType = await _context.MediaSections.AsNoTracking()
          .AnyAsync(e =>
            e.Id != request.Id &&
            e.Id.Equals(request.MediaSectionId) &&
            e.TypeOfMedia.Equals(TypeOfMedia.DIGITAL),
            cancellationToken);

        return mediaIsDigitalType && platformType != null;
    }

    public async Task<bool> BeUniquePlatform(UpdatePlatformCommand request, string name, CancellationToken cancellationToken)
     => !await _context.Platforms
          .AsNoTracking()
          .AnyAsync(ms =>
            ms.Id != request.Id &&
            ms.MediaSectionId.Equals(request.MediaSectionId) &&
            ms.Name.Equals(name) &&
            ms.PlatFormType.Equals(request.PlatformType), cancellationToken);
}
