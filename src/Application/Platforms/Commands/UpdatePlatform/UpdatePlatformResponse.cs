using PRManagement.Application.Common.Models;

namespace PRManagement.Application.Platforms.Commands.UpdatePlatform;

public class UpdatePlatformResponse : PlatformDto
{
    public Guid Id { get; set; }
}
