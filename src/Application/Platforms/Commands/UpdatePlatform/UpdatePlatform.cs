using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.Common.Models;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.Platforms.Commands.UpdatePlatform;

public class UpdatePlatformCommand : PlatformDto, IRequest<UpdatePlatformResponse>
{
    public Guid Id { get; set; }
    public Guid MediaSectionId { get; set; }
}

public class UpdatePlatformCommandHandler : IRequestHandler<UpdatePlatformCommand, UpdatePlatformResponse>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public UpdatePlatformCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<UpdatePlatformResponse> Handle(UpdatePlatformCommand request, CancellationToken cancellationToken)
    {
        var entity = await _context.Platforms
            .AsTracking()
            .FirstOrDefaultAsync(e => e.Id.Equals(request.Id), cancellationToken);

        Guard.Against.NotFound(request.Id, entity);

        var mediaSection = await _context.MediaSections
           .AsTracking()
           .SingleAsync(
               ms => ms.Id.Equals(entity.MediaSectionId),
               cancellationToken);

        if (mediaSection.TierPotential > entity.TierPotential && entity.TierPotential != 0)
            mediaSection.TierPotential = entity.TierPotential;

        _mapper.Map<UpdatePlatformCommand, Platform>(request, entity);
        await _context.SaveChangesAsync(cancellationToken);

        return _mapper.Map<UpdatePlatformResponse>(entity);
    }
}
