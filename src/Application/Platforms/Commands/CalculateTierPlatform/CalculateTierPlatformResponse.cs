namespace PRManagement.Application.Platforms.Commands.CalculateTierPlatform;

public class CalculateTierPlatformResponse
{
    public decimal PRValue { get; set; }
    public uint TierPotential { get; set; }
}
