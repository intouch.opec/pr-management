using PRManagement.Application.Common.Configurations;
using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Entities;
using Microsoft.Extensions.Options;

namespace PRManagement.Application.Platforms.Commands.CalculateTierPlatform;

public class CalculateTierPlatformCommand : IRequest<CalculateTierPlatformResponse>
{
    public string SectionId { get; set; } = string.Empty;
    public uint NumberOfFollower { get; set; }
}

public class CalculateTierPlatformCommandHandler : IRequestHandler<CalculateTierPlatformCommand, CalculateTierPlatformResponse>
{
    private readonly IApplicationDbContext _context;
    private readonly DefaultPlatFromCalculationConfiguration _defaultPlatFromCalculationConfiguration;

    public CalculateTierPlatformCommandHandler(
      IApplicationDbContext context, IOptions<DefaultPlatFromCalculationConfiguration> defaultPlatFromCalculationConfiguration)
    {
        _context = context;
        _defaultPlatFromCalculationConfiguration = defaultPlatFromCalculationConfiguration.Value;
    }

    public async Task<CalculateTierPlatformResponse> Handle(CalculateTierPlatformCommand request, CancellationToken cancellationToken)
    {
        var section = await _context.Sections
          .FirstOrDefaultAsync(s =>
              s.Id.Equals(request.SectionId.ToGuid()),
            cancellationToken);

        var formulaForCalculation = FindRecord(
          GetFormulaForCalculation(section).ToList(),
          request.NumberOfFollower);

        return new()
        {
            TierPotential = formulaForCalculation!.Tier,
            PRValue = formulaForCalculation.PriceOfPurchaseRequisition
        };
    }

    internal static FormulaForCalculation FindRecord(List<FormulaForCalculation> records, uint numberOfFollower)
    {
        for (int i = 0; i < records.Count; i++)
        {
            if (numberOfFollower <= records[i].MinimumFollowers)
                return records[i];

            if (records.Count == i + 1)
                return records[i];

        }
        return records.LastOrDefault()!;
    }

    internal IEnumerable<FormulaForCalculation> GetFormulaForCalculation(Section? section)
    {
        if (!section!.FormulaForCalculations.Any())
            return _defaultPlatFromCalculationConfiguration.OrderBy(r => r.MinimumFollowers);
        return section.FormulaForCalculations.OrderBy(r => r.MinimumFollowers);
    }
}
