using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.Platforms.Commands.CalculateTierPlatform;
using PRManagement.Domain.Constants;

namespace PRManagement.Application.Platforms.Commands.CreatePlatform;

public class CalculateTierPlatformValidator : AbstractValidator<CalculateTierPlatformCommand>
{
    private readonly IApplicationDbContext _context;

    public CalculateTierPlatformValidator(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(e => e.SectionId)
          .StringIsGuid()
          .NotEmpty()
          .MustAsync(HasSection)
          .WithMessage(ValidatorMessages.NotFound);

    }

    internal async Task<bool> HasSection(string sectionId, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(sectionId))
            return true;

        return await _context.Sections
          .AsNoTracking()
          .AnyAsync(p => p.Id.Equals(new Guid(sectionId)), cancellationToken);
    }
}
