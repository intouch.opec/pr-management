using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.Common.Models;
using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;
using PRManagement.Domain.Exceptions;
using FluentValidation.Results;
using ValidationException = PRManagement.Application.Common.Exceptions.ValidationException;
using PRManagement.Application.Common.Extensions;

namespace PRManagement.Application.Platforms.Commands.CreatePlatformByMediaSectionId;

public class CreatePlatformByMediaSectionIdCommand : IRequest<CreatePlatformByMediaSectionIdResponse>
{
    public string Id { get; set; } = string.Empty;
    public IEnumerable<PlatformByMediaSectionIdItemDto> Platforms { get; set; } = Enumerable.Empty<PlatformByMediaSectionIdItemDto>();
}

public class PlatformByMediaSectionIdItemDto : PlatformDto
{
    public string Id { get; set; } = string.Empty;
}

public class CreatePlatformByMediaSectionIdCommandHandle : IRequestHandler<CreatePlatformByMediaSectionIdCommand, CreatePlatformByMediaSectionIdResponse>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public CreatePlatformByMediaSectionIdCommandHandle(IMapper mapper, IApplicationDbContext context)
    {
        _mapper = mapper;
        _context = context;
    }

    public async Task<CreatePlatformByMediaSectionIdResponse> Handle(CreatePlatformByMediaSectionIdCommand request, CancellationToken cancellationToken)
    {
        var mediaSectionId = request.Id.ToGuidOnly();
        var mediaSection = await _context.MediaSections
            .Include(ms => ms.Platforms)
            .FirstOrDefaultAsync(e => e.Id.Equals(mediaSectionId), cancellationToken);

        Guard.Against.NotFound(mediaSectionId, mediaSection!.Id);
        var platformCreated = request.Platforms
            .Where(e => string.IsNullOrWhiteSpace(e.Id));

        VerifyPlatforms(platformCreated, mediaSection);
        VerifyPlatformPlatformTypes(request.Platforms, mediaSection);

        await _context.Platforms.AddRangeAsync(MapperPlatform(platformCreated, mediaSectionId).ToList());

        var platformUpdated = request.Platforms.Where(e => !string.IsNullOrWhiteSpace(e.Id));
        var platformUpdatedIds = platformUpdated.Select(e => new Guid(e.Id));
        var platforms = mediaSection.Platforms.Where(e => platformUpdatedIds.Contains(e.Id));
        foreach (var platform in platforms)
        {
            var platformMustUpdate = platformUpdated.First(pl => new Guid(pl.Id!).Equals(platform.Id));
            _mapper.Map<PlatformByMediaSectionIdItemDto, Platform>(platformMustUpdate, platform);
        }

        var platformsHasTierPotential = mediaSection.Platforms.Where(pl => pl.TierPotential != 0);
        if (platformsHasTierPotential.Any())
            mediaSection.TierPotential = platformsHasTierPotential.Min(p => p.TierPotential);

        await _context.SaveChangesAsync(cancellationToken);

        return _mapper.Map<CreatePlatformByMediaSectionIdResponse>(mediaSection);
    }

    internal void VerifyPlatforms(IEnumerable<PlatformByMediaSectionIdItemDto> platforms, MediaSection mediaSection)
    {
        var validator = new CreatePlatformsValidator(mediaSection.Platforms);
        var validationResults = validator.Validate(platforms);
        if (!validationResults.IsValid)
            throw new ValidationException(validationResults.Errors);
    }

    internal void VerifyPlatformPlatformTypes(IEnumerable<PlatformByMediaSectionIdItemDto> platforms, MediaSection mediaSection)
    {
        var errors = new List<ValidationFailure>();
        var platformList = platforms.ToList();
        foreach (var platform in platformList)
        {
            var plType = platform.PlatformType.ToEnum<PlatformType>();
            var isDigitalType = mediaSection.TypeOfMedia is TypeOfMedia.DIGITAL && !DigitalPlatformType.PlatformTypes.Contains(plType);
            if (isDigitalType)
                errors.Add(new ValidationFailure(
                  $"{nameof(CreatePlatformByMediaSectionIdCommand.Platforms)}[{platformList.IndexOf(platform)}].{nameof(PlatformByMediaSectionIdItemDto.PlatformType)}",
                  "The PlatformType is invalid. The PlatformTpe should be (" + string.Join(",", DigitalPlatformType.PlatformTypeStrings) + ")"));

            var isMainStream = mediaSection.TypeOfMedia is TypeOfMedia.MAINSTREAM && !MainStreamPlatformType.PlatformTypes.Contains(plType);
            if (isMainStream)
                errors.Add(new ValidationFailure(
                  $"{nameof(CreatePlatformByMediaSectionIdCommand.Platforms)}[{platformList.IndexOf(platform)}].{nameof(PlatformByMediaSectionIdItemDto.PlatformType)}",
                  "The Platform is invalid.The PlatformTpe should be (" + string.Join(",", MainStreamPlatformType.PlatformTypes) + ")"));
        }
        if (errors.Any())
            throw new ValidationException(errors);
    }

    internal IEnumerable<Platform> MapperPlatform(IEnumerable<PlatformByMediaSectionIdItemDto> platforms, Guid id)
      => platforms.Select(pl =>
        {
            var pf = _mapper.Map<Platform>(pl);
            pf.MediaSectionId = id;
            return pf;
        });

}
