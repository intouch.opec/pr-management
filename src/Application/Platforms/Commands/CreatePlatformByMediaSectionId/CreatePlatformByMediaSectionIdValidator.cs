using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Constants;
using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;
using PRManagement.Domain.Exceptions;

namespace PRManagement.Application.Platforms.Commands.CreatePlatformByMediaSectionId;

public class CreatePlatformByMediaSectionIdValidator : AbstractValidator<CreatePlatformByMediaSectionIdCommand>
{
    private readonly IApplicationDbContext _context;
    public CreatePlatformByMediaSectionIdValidator(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(x => x.Platforms)
            .Must(UniquePlatform)
            .WithMessage("Enum values in the array must be unique.");

        RuleForEach(e => e.Platforms)
            .ChildRules(platform =>
                platform.RuleFor(pl => pl.Link)
                .StringIsUrl())
            .ChildRules(platform =>
                platform.RuleFor(pl => pl.PlatformType)
                .MustEnum<PlatformByMediaSectionIdItemDto, PlatformType>())
            .ChildRules(platform =>
                platform.RuleFor(pl => pl.Id!.ToString())
                .StringIsGuid());

        RuleFor(e => e.Id)
            .NotEmpty()
            .StringIsGuid()
            .MustAsync(HasMediaSection)
            .WithMessage(ValidatorMessages.NotFound);

    }

    internal bool UniquePlatform(IEnumerable<Common.Models.PlatformDto>? platforms)
    {
        if (platforms is null || !platforms.Any())
            return true;

        return platforms.Select(item => item.PlatformType).Distinct().Count() == platforms.Count();
    }

    internal async Task<bool> HasMediaSection(string mediaSectionId, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(mediaSectionId))
            return true;

        return await _context.MediaSections
            .AsNoTracking()
            .AnyAsync(p => p.Id.Equals(new Guid(mediaSectionId)), cancellationToken);
    }

}

public class CreatePlatformsValidator : AbstractValidator<IEnumerable<PlatformByMediaSectionIdItemDto>>
{
    private readonly IEnumerable<Platform> _platforms;
    public CreatePlatformsValidator(IEnumerable<Platform> platforms)
    {
        _platforms = platforms;

        RuleForEach(Platforms => Platforms)
            .ChildRules(platform =>
                    platform.RuleFor(pl => pl.PlatformType)
                .MustEnum<PlatformByMediaSectionIdItemDto, PlatformType>()
                .Must(BeUniquePlatformType)
                .WithMessage(ValidatorMessages.ShouldBeUnique)
            );

    }

    internal bool BeUniquePlatformType(string platformType)
    {
        if (string.IsNullOrWhiteSpace(platformType))
            return true;
        return !_platforms.Any(e => e.PlatFormType.Equals(platformType.ToEnum<PlatformType>()));
    }

}
