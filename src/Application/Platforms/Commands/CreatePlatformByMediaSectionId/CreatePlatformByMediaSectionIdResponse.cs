using PRManagement.Domain.Enums;

namespace PRManagement.Application.Platforms.Commands.CreatePlatformByMediaSectionId;

public class CreatePlatformByMediaSectionIdResponse
{
    public Guid MediaSectionId { get; set; }
    public TypeOfMedia? TypeOfMedia { get; set; }
    public Guid SectionId { get; set; }
    public Guid SubSectionId { get; set; }
    public Guid MediaId { get; set; }
}
