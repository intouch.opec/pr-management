using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;
using PRManagement.Domain.Exceptions;

namespace PRManagement.Application.Platforms.Commands.CreatePlatformByMediaSectionId;

public class CreatePlatformByMediaSectionIdMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<PlatformByMediaSectionIdItemDto, Platform>()
                .ForMember(src => src.Id, op => op.MapFrom(o => string.IsNullOrEmpty(o.Id) ? Guid.NewGuid() : new Guid(o.Id)))
                .ForMember(src => src.PlatFormType, op => op.MapFrom(o => o.PlatformType.ToEnum<PlatformType>()));

            CreateMap<MediaSection, CreatePlatformByMediaSectionIdResponse>()
                .ForMember(src => src.MediaSectionId, op => op.MapFrom(o => o.Id));
        }
    }
}
