using PRManagement.Domain.Entities;

namespace PRManagement.Application.Bookmarks.Commands.RemoveBookmark;
public class RemoveBookmarkMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<BookMark, RemoveBookmarkResponse>();
        }
    }
}