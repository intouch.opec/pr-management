﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;

namespace PRManagement.Application.Bookmarks.Commands.RemoveBookmark;

public class RemoveBookmarkCommand : IRequest<RemoveBookmarkResponse>
{
    public Guid Id { get; set; }
    public string Remark { get; set; } = string.Empty;
}

public class RemoveBookmarkCommandHandler : IRequestHandler<RemoveBookmarkCommand, RemoveBookmarkResponse>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public RemoveBookmarkCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<RemoveBookmarkResponse> Handle(RemoveBookmarkCommand request, CancellationToken cancellationToken)
    {
        var entity = await _context.BookMarks
            .Where(e => e.Id == request.Id)
            .SingleOrDefaultAsync(cancellationToken);

        Guard.Against.NotFound(request.Id, entity);

        _context.BookMarks.RemoveAndAddRemark(entity, request.Remark);

        await _context.SaveChangesAsync(cancellationToken);

        return _mapper.Map<RemoveBookmarkResponse>(entity);
    }
}
