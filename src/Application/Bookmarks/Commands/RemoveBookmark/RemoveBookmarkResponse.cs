namespace PRManagement.Application.Bookmarks.Commands.RemoveBookmark;

public class RemoveBookmarkResponse
{
    public Guid Id { get; set; }
    public Guid BookmarkGroupId { get; set; }
    public Guid AddressId { get; set; }
    public int[] ActiveEmail { get; set; } = Array.Empty<int>();
}
