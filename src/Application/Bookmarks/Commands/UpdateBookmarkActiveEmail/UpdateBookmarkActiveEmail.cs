﻿using PRManagement.Application.Bookmarks.Commands.RemoveBookmark;
using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.Bookmarks.Commands.UpdateBookmarkActiveEmail;

public record UpdateBookmarkActiveEmailCommand() : IRequest<RemoveBookmarkResponse>
{
    public string Id { get; set; } = string.Empty;
    public List<int> ActiveEmail { get; set; } = null!;
}

public class UpdateBookmarkActiveEmailCommandHandler : IRequestHandler<UpdateBookmarkActiveEmailCommand, RemoveBookmarkResponse>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;
    public UpdateBookmarkActiveEmailCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<RemoveBookmarkResponse> Handle(UpdateBookmarkActiveEmailCommand request, CancellationToken cancellationToken)
    {
        var entity = await _context.BookMarks
            .AsTracking()
            .Where(e => e.Id == request.Id.ToGuidOnly())
            .SingleOrDefaultAsync(cancellationToken);

        Guard.Against.NotFound(request.Id, entity);

        _mapper.Map<UpdateBookmarkActiveEmailCommand, BookMark>(request, entity);
        await _context.SaveChangesAsync(cancellationToken);

        return _mapper.Map<RemoveBookmarkResponse>(entity);
    }
}
