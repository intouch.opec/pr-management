using PRManagement.Application.Common.Extensions;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.Bookmarks.Commands.UpdateBookmarkActiveEmail;

public class MappingProfile
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<UpdateBookmarkActiveEmailCommand, BookMark>()
                .ForMember(dest => dest.Id, op => op.Ignore())
                .ForMember(dest => dest.ActiveEmail, op => op.MapFrom(src => src.ActiveEmail.ToArray()));
            
            CreateMap<BookMark, UpdateBookmarkActiveEmailCommand>()
                .ForMember(dest => dest.Id, op => op.Ignore())
                .ForMember(dest => dest.ActiveEmail, op => op.MapFrom(src => src.ActiveEmail));

        }
    }
}
