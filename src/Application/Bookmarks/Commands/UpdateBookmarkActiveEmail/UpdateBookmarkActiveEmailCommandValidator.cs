using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Constants;

namespace PRManagement.Application.Bookmarks.Commands.UpdateBookmarkActiveEmail;

public class UpdateBookmarkActiveEmailCommandValidator : AbstractValidator<UpdateBookmarkActiveEmailCommand>
{
    private readonly IApplicationDbContext _context;

    public UpdateBookmarkActiveEmailCommandValidator(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(v => v.Id)
            .NotEmpty()
            .StringIsGuid()
            .MustAsync(HasBookMark)
            .WithMessage(ValidatorMessages.NotFound);

        RuleFor(v => v.ActiveEmail)
           .MustAsync(HasIndex)
           .WithMessage(ValidatorMessages.InvalidFormat);
    }

    private async Task<bool> HasBookMark(string id, CancellationToken cancellationToken)
    {
        if (!id.IsValidGuid())
            return true;

        var bookMark = await _context.BookMarks.AsNoTracking()
            .Include(book => book.Address)
            .FirstOrDefaultCacheAsync(
                book => book.Id.Equals(id.ToGuidOnly()),
                cancellationToken);

        return bookMark != null;
    }

    private async Task<bool> HasIndex(
        UpdateBookmarkActiveEmailCommand command,
        List<int> activeEmail,
        CancellationToken cancellationToken)
    {
        if (!command.Id.IsValidGuid())
            return true;

        var bookMark = await _context.BookMarks.AsNoTracking()
            .Include(book => book.Address)
            .FirstOrDefaultAsync(
                book => book.Id.Equals(command.Id.ToGuidOnly()),
                cancellationToken);

        if (bookMark == null)
            return true;

        foreach (var item in activeEmail!)
            if (bookMark.Address!.Email.ElementAtOrDefault(item) == null)
                return false;

        return true;
    }

}
