using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.Bookmarks.Queries.GetBookmarkWithPagination;

public class GetBookmarkMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<BookMark, GetBookmarkItem>()
                .ForMember(src => src.Email, op => op.MapFrom(o => o.Address!.Email))
                .ForMember(src => src.PhoneNumbers, op => op.MapFrom(o => o.Address!.PhoneNumbers))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src =>
                    src.BookMarkType == BookMarkType.Media ? $"{src.Address!.Media!.Name}" :
                    src.BookMarkType == BookMarkType.MediaSection ? $"{src.Address!.MediaSection!.Media!.Name} [{src.Address!.MediaSection!.Section.Name}] ({src.Address!.MediaSection!.SubSection.Name})" :
                    src.BookMarkType == BookMarkType.Contact ? $"{src.Address!.Contact!.FirstName} {src.Address.Contact.LastName}[{src.Address.Contact.NickName}] ({string.Join(",", src.Address.Contact.ContactMediaSections!.Select(cms => cms.MediaSection!.Media.Name).GroupBy(m => m).Select(group => group.Key))})" : "-"))
                .ForMember(dest => dest.ContactId, opt => opt.MapFrom(src =>
                    src.BookMarkType == BookMarkType.Contact ? src.Address!.Contact!.Id.ToString() : "-"))
                .ForMember(dest => dest.MediaId, opt => opt.MapFrom(src =>
                    src.BookMarkType == BookMarkType.Media ? src.Address!.Media!.Id.ToString() :
                    src.BookMarkType == BookMarkType.MediaSection ? src.Address!.MediaSection!.MediaId.ToString() : "-"))
                .ForMember(dest => dest.MediaSectionId, opt => opt.MapFrom(src =>
                    src.BookMarkType == BookMarkType.MediaSection ? src.Address!.MediaSection!.Id.ToString() : "-"));
        }

    }
}
