using PRManagement.Domain.Enums;

namespace PRManagement.Application.Bookmarks.Queries.GetBookmarkWithPagination;

public class GetBookmarkItem
{
    public Guid Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public string[] Email { get; set; } = Array.Empty<string>();
    public string[] PhoneNumbers { get; set; } = Array.Empty<string>();
    public BookMarkType BookMarkType { get; set; }
    public int[] ActiveEmail { get; set; } = Array.Empty<int>();
    public string ContactId { get; set; } = string.Empty;
    public string MediaId { get; set; } = string.Empty;
    public string MediaSectionId { get; set; } = string.Empty;
    public DateTimeOffset Created { get; set; }
    public string? CreatedBy { get; set; }
    public DateTimeOffset LastModified { get; set; }
    public string? LastModifiedBy { get; set; }
}
