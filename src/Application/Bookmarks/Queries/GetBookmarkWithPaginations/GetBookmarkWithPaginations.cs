﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.Common.Mappings;
using PRManagement.Application.Common.Models;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.Bookmarks.Queries.GetBookmarkWithPagination;

public record GetBookmarkWithPaginationQuery : IRequest<PaginatedList<GetBookmarkItem>>
{
    public string BookMarkGroupId { get; set; } = string.Empty;
    public string? Email { get; set; } = string.Empty;
    public string? PhoneNumbers { get; set; } = string.Empty;
    public string? Search { get; set; } = string.Empty;
    public int? PageNumber { get; init; } = 1;
    public int? PageSize { get; init; } = 10;
    public SortType? Sort { get; set; }
    public string? SortBy { get; set; }
}

public class GetBookmarkWithPaginationQueryHandler : IRequestHandler<GetBookmarkWithPaginationQuery, PaginatedList<GetBookmarkItem>>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public GetBookmarkWithPaginationQueryHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<PaginatedList<GetBookmarkItem>> Handle(GetBookmarkWithPaginationQuery request, CancellationToken cancellationToken)
    {
        return await Search(request)
            .ProjectTo<GetBookmarkItem>(_mapper.ConfigurationProvider)
            .OrderBy(request.SortBy ?? string.Empty, ascending: request.Sort)
            .PaginatedListAsync(request.PageNumber ?? 1, request.PageSize ?? 10, cancellationToken);
    }

    internal IQueryable Search(GetBookmarkWithPaginationQuery request)
        => _context.BookMarks
            .Include(c => c.Address)
            .ThenInclude(e => e!.Contact)
            .Include(e => e.Address)
            .ThenInclude(e => e!.Media)
            .Include(e => e.Address)
            .ThenInclude(e => e!.MediaSection)
            .ThenInclude(e => e!.Media)
            .AsNoTracking()
            .Where(e => e.BookmarkGroupId.Equals(request.BookMarkGroupId.ToGuid()))
            .WhereLikeArray(e => e.Address!.PhoneNumbers, request.PhoneNumbers)
            .WhereLikeArray(e => e.Address!.Email, request.Email)
            .SearchOrMultiple(request.Search,
                e => e.Address!.Contact!.FirstName,
                e => e.Address!.Contact!.LastName,
                e => e.Address!.Contact!.NickName,
                e => e.Address!.Contact!.Position,
                e => e.Address!.Contact!.LineId,
                e => e.Address!.Media!.Name,
                e => e.Address!.Text,
                e => e.Address!.PhoneNumbers,
                e => e.Address!.Email
            );

}
