using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Constants;

namespace PRManagement.Application.Bookmarks.Queries.GetBookmarkWithPagination;

public class GetBookmarkValidator : AbstractValidator<GetBookmarkWithPaginationQuery>
{
    private readonly IApplicationDbContext _context;

    public GetBookmarkValidator(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(v => v.BookMarkGroupId)
          .NotEmpty()
          .MustAsync(HasBookMarkGroup)
          .WithMessage(ValidatorMessages.NotFound);
    }

    internal async Task<bool> HasBookMarkGroup(string bookMarkGroupId, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(bookMarkGroupId))
            return true;

        return await _context.BookmarkGroups
          .AsNoTracking()
          .AnyAsync(p => p.Id.Equals(bookMarkGroupId.ToGuid()), cancellationToken);
    }
}
