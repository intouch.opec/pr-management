﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.Common.Mappings;
using PRManagement.Application.Common.Models;
using PRManagement.Domain.Enums;
using PRManagement.Domain.Exceptions;

namespace PRManagement.Application.News.Queries.GetNewWithPagination;

public record GetNewWithPaginationQuery : IRequest<PaginatedList<GetNewItem>>
{
    public string? Title { get; set; } = string.Empty;
    public string? Owner { get; set; } = string.Empty;
    public string? Link { get; set; } = string.Empty;
    public string? Description { get; set; } = string.Empty;
    public string? MonitorTaskWorkerId { get; set; }
    public string? PRActivityId { get; set; }
    public string? Mapping { get; set; }
    public int? PageNumber { get; init; } = 1;
    public int? PageSize { get; init; } = 10;
    public SortType? Sort { get; set; } = SortType.DESC;
    public string? SortBy { get; set; } = nameof(GetNewItem.Title);
}

public class GetNewWithPaginationQueryHandler : IRequestHandler<GetNewWithPaginationQuery, PaginatedList<GetNewItem>>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public GetNewWithPaginationQueryHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<PaginatedList<GetNewItem>> Handle(GetNewWithPaginationQuery request, CancellationToken cancellationToken)
    {
        return await Search(request)
            .ProjectTo<GetNewItem>(_mapper.ConfigurationProvider)
            .OrderBy(request.SortBy ?? string.Empty, ascending: request.Sort)
            .PaginatedListAsync(request.PageNumber ?? 1, request.PageSize ?? 10, cancellationToken);
    }

    internal IQueryable Search(GetNewWithPaginationQuery request)
    {
        var x =  _context.News
            .Include(e => e.MonitorTaskWorker)
            .ThenInclude(e => e!.PRActivity)
            // .WhereEqual(n => n.MonitorTaskWorker!.PRActivityId, request.PRActivityId.ToGuid())
            .ToList();
        return _context.News
            .Include(e => e.MonitorTaskWorker)
            .Include(e => e.Platform)
            .ThenInclude(e => e!.MediaSection)
            .ThenInclude(e => e!.Media)
            .Include(e => e.Platform)
            .ThenInclude(e => e!.MediaSection)
            .ThenInclude(e => e!.Section)
            .Include(e => e.Platform)
            .ThenInclude(e => e!.MediaSection)
            .ThenInclude(e => e!.SubSection)
            .AsNoTracking()
            .WhereEqual(n => n.Mapping, request.Mapping.ToEnumOrNull<MappingType>())
            .WhereEqual(n => n.MonitorTaskWorker!.PRActivityId, request.PRActivityId.ToGuid())
            .WhereEqual(n => n.MonitorTaskWorkerId, request.MonitorTaskWorkerId.ToGuid())
            .WhereLike(n => n.Title.ToLower(), request.Title)
            .WhereLike(n => n.Owner.ToLower(), request.Owner)
            .WhereLike(n => n.Link.ToLower(), request.Link)
            .WhereLike(n => n.Description.ToLower(), request.Description);
    }
}
