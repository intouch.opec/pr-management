using PRManagement.Domain.Entities;

namespace PRManagement.Application.News.Queries.GetNewWithPagination;

public class GetNewWithPaginationMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<New, GetNewItem>()
                .ForMember(dest => dest.SectionName, op => op.MapFrom(src => src.Platform!.MediaSection!.Section.Name ?? string.Empty))
                .ForMember(dest => dest.mediaName, op => op.MapFrom(src => src.Platform!.MediaSection!.SubSection!.Name ?? string.Empty))
                .ForMember(dest => dest.SubSectionName, op => op.MapFrom(src => src.Platform!.MediaSection!.Media!.Name ?? string.Empty))
                .ForMember(dest => dest.PlatformName, op => op.MapFrom(src => src.Platform!.Name ?? string.Empty))
                .ForMember(dest => dest.PlatformType, op => op.MapFrom(src => src.Platform!.PlatFormType));
        }
    }
}
