using PRManagement.Domain.Enums;

namespace PRManagement.Application.News.Queries.GetNewWithPagination;

public class GetNewItem
{
    public Guid Id { get; set; }
    public string Title { get; set; } = string.Empty;
    public string Owner { get; set; } = string.Empty;
    public string Link { get; set; } = string.Empty;
    public string Description { get; set; } = string.Empty;
    public MappingType Mapping { get; set; }
    public PlatformType? PlatformType { get; set; }
    public string mediaName { get; set; } = string.Empty;
    public string SectionName { get; set; } = string.Empty;
    public string SubSectionName { get; set; } = string.Empty;
    public string PlatformName { get; set; } = string.Empty;
    public TypeOfMedia TypeOfMedia { get; set; }
    public Guid MediaSectionId { get; set; }
    public Guid MonitorTaskWorkerId { get; set; }
    public DateTimeOffset Created { get; set; }
    public string? CreatedBy { get; set; }
    public DateTimeOffset LastModified { get; set; }
    public string? LastModifiedBy { get; set; }
}
