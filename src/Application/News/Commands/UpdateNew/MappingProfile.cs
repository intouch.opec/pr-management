using PRManagement.Application.Common.Extensions;
using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;
using PRManagement.Domain.Exceptions;

namespace PRManagement.Application.News.Commands.UpdateNew;

public class MappingProfile
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<UpdateNewCommand, New>()
                .ForMember(dest => dest.Id, op => op.Ignore())
                .ForMember(dest => dest.Mapping, op => op.MapFrom(src => src.Mapping.ToEnumOrNull<MappingType>()))
                .ForMember(dest => dest.PlatformType, op => op.MapFrom(src => src.Mapping.ToEnumOrNull<PlatformType>()))
                .ForMember(dest => dest.PlatformId, op => op.MapFrom(src => src.PlatformId.ToGuid()));
        }
    }
}
