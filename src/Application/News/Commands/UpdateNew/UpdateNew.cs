﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.News.Queries.GetNewWithPagination;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.News.Commands.UpdateNew;

public record UpdateNewCommand : IRequest<GetNewItem>
{
    public string Id { get; set; } = string.Empty;
    public string Title { get; set; } = string.Empty;
    public string Owner { get; set; } = string.Empty;
    public string Link { get; set; } = string.Empty;
    public string Description { get; set; } = string.Empty;
    public string Mapping { get; set; } = string.Empty;
    public string? PlatformType { get; set; }
    public string? PlatformId { get; set; }
    public DateTimeOffset? PublicationDate { get; set; }
}

public class UpdateNewCommandHandler : IRequestHandler<UpdateNewCommand, GetNewItem>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public UpdateNewCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<GetNewItem> Handle(UpdateNewCommand request, CancellationToken cancellationToken)
    {
        var entity = await _context.News
            .AsTracking()
            .FirstOrDefaultAndRemoveCacheAsync(n => n.Id.Equals(request.Id.ToGuid()), cancellationToken);

        Guard.Against.NotFound(request.Id, entity);

        _mapper.Map<UpdateNewCommand, New>(request, entity);

        await _context.SaveChangesAsync(cancellationToken);

        return _mapper.Map<GetNewItem>(entity);
    }
}
