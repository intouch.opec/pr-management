using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Constants;
using PRManagement.Domain.Enums;
using PRManagement.Domain.Exceptions;

namespace PRManagement.Application.News.Commands.MappingNew;

public class MappingNewCommandValidator : AbstractValidator<MappingNewCommand>
{
    private readonly IApplicationDbContext _context;

    public MappingNewCommandValidator(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(src => src.Id)
            .StringIsGuid()
            .MustAsync(HasNewAsync)
            .WithMessage(ValidatorMessages.NotFound)
            .When(e => e.Mapping.ToEnumOrNull<MappingType>().Equals(MappingType.DONE))
            .NotEmpty();

        RuleFor(src => src.Mapping)
            .NotEmpty()
            .MustEnum<MappingNewCommand, MappingType>();
    }

    internal async Task<bool> HasNewAsync(string id, CancellationToken cancellationToken)
    {
        if (!id.IsValidGuid())
            return true;

        var entity = await _context.News
            .AsNoTracking()
            .FirstOrDefaultCacheAsync(
                e => e.Id.Equals(id.ToGuid()),
                cancellationToken);
        return entity != null;
    }
}
