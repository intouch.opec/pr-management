﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.News.Queries.GetNewWithPagination;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.News.Commands.MappingNew;

public record MappingNewCommand : IRequest<GetNewItem>
{
    public string Id { get; set; } = string.Empty;
    public string? PlatformId { get; set; }
    public string Mapping { get; set; } = string.Empty;
}

public class MappingNewCommandHandler : IRequestHandler<MappingNewCommand, GetNewItem>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public MappingNewCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<GetNewItem> Handle(MappingNewCommand request, CancellationToken cancellationToken)
    {
        var entity = await _context.News
            .AsTracking()
            .FirstOrDefaultAsync(
                n => n.Id.Equals(request.Id.ToGuid()), 
                cancellationToken
            );

        Guard.Against.NotFound(request.Id, entity);

        _mapper.Map<MappingNewCommand, New>(request, entity);

        await _context.SaveChangesAsync(cancellationToken);

        return _mapper.Map<GetNewItem>(entity);
    }
}
