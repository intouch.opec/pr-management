using PRManagement.Domain.Entities;

namespace PRManagement.Application.Clients.Queries.GetClientById;

public class GetClientMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<Client, GetClientByIdResponse>()
              .ForMember(s => s.Email, op => op.MapFrom(o => o!.Address!.Email ?? Array.Empty<string>()))
              .ForMember(s => s.PhoneNumbers, op => op.MapFrom(o => o!.Address!.PhoneNumbers ?? Array.Empty<string>()))
              .ForMember(s => s.Contacts, op => op.MapFrom(o => o!.ContactClients))
              .ForMember(s => s.Address, op => op.MapFrom(o => o.Address!.Text ?? string.Empty))
              .ForMember(s => s.Email, op => op.MapFrom(o => o.Address!.Email ?? Array.Empty<string>()))
              .ForMember(s => s.PhoneNumbers, op => op.MapFrom(o => o.Address!.PhoneNumbers ?? Array.Empty<string>()))
              .ForMember(s => s.ProvinceId, op => op.MapFrom(o => o.Address!.ProvinceId ?? string.Empty))
              .ForMember(s => s.DistrictId, op => op.MapFrom(o => o.Address!.DistrictId ?? string.Empty))
              .ForMember(s => s.SubDistrictId, op => op.MapFrom(o => o.Address!.SubDistrictId ?? string.Empty))
              .ForMember(s => s.PostCode, op => op.MapFrom(o => o.Address!.PostCode ?? string.Empty))
              .ForMember(s => s.ProvinceEN, op => op.MapFrom(o => o.Address!.Province!.NameEN ?? string.Empty))
              .ForMember(s => s.ProvinceTH, op => op.MapFrom(o => o.Address!.Province!.NameTH ?? string.Empty))
              .ForMember(s => s.DistrictEN, op => op.MapFrom(o => o.Address!.District!.NameEN ?? string.Empty))
              .ForMember(s => s.DistrictTH, op => op.MapFrom(o => o.Address!.District!.NameTH ?? string.Empty))
              .ForMember(s => s.SubDistrictEN, op => op.MapFrom(o => o.Address!.SubDistrict!.NameEN ?? string.Empty))
              .ForMember(s => s.SubDistrictTH, op => op.MapFrom(o => o.Address!.SubDistrict!.NameTH ?? string.Empty));
        }
    }
}
