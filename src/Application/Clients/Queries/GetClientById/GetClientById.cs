﻿using PRManagement.Application.Common.Interfaces;

namespace PRManagement.Application.Clients.Queries.GetClientById;

public record GetClientByIdQuery(Guid Id) : IRequest<GetClientByIdResponse>;

public class GetClientByIdQueryHandler : IRequestHandler<GetClientByIdQuery, GetClientByIdResponse>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public GetClientByIdQueryHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<GetClientByIdResponse> Handle(GetClientByIdQuery request, CancellationToken cancellationToken)
    {
        var entity = await _context.Clients
            .Include(c => c.Address)
            .Include(a => a.Address!.Province)
            .Include(a => a.Address!.District)
            .Include(a => a.Address!.SubDistrict)
            .Include(c => c.ContactClients!.Where(cm => cm != null))
            .ThenInclude(e => e.Contact)
            .ThenInclude(a => a!.Address!.Province)
            .Include(c => c.ContactClients!.Where(cm => cm != null))
            .ThenInclude(e => e.Contact)
            .ThenInclude(a => a!.Address!.District)
            .Include(c => c.ContactClients!.Where(cm => cm != null))
            .ThenInclude(e => e.Contact)
            .ThenInclude(a => a!.Address!.SubDistrict)
            .AsNoTracking()
            .ProjectTo<GetClientByIdResponse>(_mapper.ConfigurationProvider)
            .SingleOrDefaultAsync(e => e.Id.Equals(request.Id));

        Guard.Against.NotFound(request.Id, entity);

        return entity;
    }
}
