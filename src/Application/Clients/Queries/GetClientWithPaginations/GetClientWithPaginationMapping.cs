using PRManagement.Domain.Entities;

namespace PRManagement.Application.Clients.Queries.GetClientWithPagination;

public class GetContactWithPaginationBySectionIdMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<Client, GetClientItem>()
              .ForMember(s => s.Address, op => op.MapFrom(o => o!.Address!.Text ?? string.Empty))
              .ForMember(s => s.Email, op => op.MapFrom(o => o!.Address!.Email ?? Array.Empty<string>()))
              .ForMember(s => s.PhoneNumbers, op => op.MapFrom(o => o!.Address!.PhoneNumbers ?? Array.Empty<string>()))
              .ForMember(s => s.Contacts, op => op.MapFrom(o => o!.ContactClients));

            CreateMap<ContactClient, ContactItem>()
              .ForMember(s => s.Id, op => op.MapFrom(o => o.Contact!.Id))
              .ForMember(s => s.ContactClientId, op => op.MapFrom(o => o.Id))
              .ForMember(s => s.Email, op => op.MapFrom(o => o.Contact!.Address!.Email ?? Array.Empty<string>()))
              .ForMember(s => s.PhoneNumbers, op => op.MapFrom(o => o.Contact!.Address!.PhoneNumbers ?? Array.Empty<string>()))
              .ForMember(s => s.FirstName, op => op.MapFrom(o => o.Contact!.FirstName ?? string.Empty))
              .ForMember(s => s.LastName, op => op.MapFrom(o => o.Contact!.LastName ?? string.Empty))
              .ForMember(s => s.Position, op => op.MapFrom(o => o.Contact!.Position ?? string.Empty))
              .ForMember(s => s.LineId, op => op.MapFrom(o => o.Contact!.LineId ?? string.Empty))
              .ForMember(s => s.ProvinceId, op => op.MapFrom(o => o.Contact!.Address!.ProvinceId ?? string.Empty))
              .ForMember(s => s.DistrictId, op => op.MapFrom(o => o.Contact!.Address!.DistrictId ?? string.Empty))
              .ForMember(s => s.SubDistrictId, op => op.MapFrom(o => o.Contact!.Address!.SubDistrictId ?? string.Empty))
              .ForMember(s => s.PostCode, op => op.MapFrom(o => o.Contact!.Address!.PostCode ?? string.Empty))
              .ForMember(s => s.ProvinceEN, op => op.MapFrom(o => o.Contact!.Address!.Province!.NameEN ?? string.Empty))
              .ForMember(s => s.ProvinceTH, op => op.MapFrom(o => o.Contact!.Address!.Province!.NameTH ?? string.Empty))
              .ForMember(s => s.DistrictEN, op => op.MapFrom(o => o.Contact!.Address!.District!.NameEN ?? string.Empty))
              .ForMember(s => s.DistrictTH, op => op.MapFrom(o => o.Contact!.Address!.District!.NameTH ?? string.Empty))
              .ForMember(s => s.SubDistrictEN, op => op.MapFrom(o => o.Contact!.Address!.SubDistrict!.NameEN ?? string.Empty))
              .ForMember(s => s.SubDistrictTH, op => op.MapFrom(o => o.Contact!.Address!.SubDistrict!.NameTH ?? string.Empty));
        }
    }
}
