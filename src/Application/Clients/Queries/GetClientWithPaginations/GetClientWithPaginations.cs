﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.Common.Mappings;
using PRManagement.Application.Common.Models;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.Clients.Queries.GetClientWithPagination;

public record GetClientWithPaginationQuery : IRequest<PaginatedList<GetClientItem>>
{
    public string? Name { get; set; } = string.Empty;
    public string? Email { get; set; } = string.Empty;
    public string? PhoneNumbers { get; set; } = string.Empty;
    public string? Search { get; set; } = string.Empty;

    public int? PageNumber { get; init; } = 1;
    public int? PageSize { get; init; } = 10;
    public SortType? Sort { get; set; } = SortType.DESC;
    public string? SortBy { get; set; } = nameof(GetClientItem.LastModified);
}

public class GetClientWithPaginationQueryHandler : IRequestHandler<GetClientWithPaginationQuery, PaginatedList<GetClientItem>>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public GetClientWithPaginationQueryHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<PaginatedList<GetClientItem>> Handle(GetClientWithPaginationQuery request, CancellationToken cancellationToken)
    {
        return await Search(request)
            .ProjectTo<GetClientItem>(_mapper.ConfigurationProvider)
            .OrderBy(request.SortBy ?? string.Empty, ascending: request.Sort)
            .PaginatedListAsync(request.PageNumber ?? 1, request.PageSize ?? 10, cancellationToken);
    }

    internal IQueryable Search(GetClientWithPaginationQuery request)
        => _context.Clients
            .AsNoTracking()
            .Include(c => c.Address)
            .Include(a => a.Address!.Province)
            .Include(a => a.Address!.District)
            .Include(a => a.Address!.SubDistrict)
            .Include(a => a.ContactClients!)
            .ThenInclude(a => a.Contact)
            .ThenInclude(a => a!.Address)
            .WhereLike(e => e.Name.ToLower(), request.Name?.ToLower())
            .WhereLikeArray(e => e.Address!.PhoneNumbers, request.PhoneNumbers)
            .WhereLikeArray(e => e.Address!.Email, request.Email)
            .SearchOrMultiple(request.Search,
                e => e.Name,
                e => e.Address!.Text,
                e => e.Address!.PhoneNumbers,
                e => e.Address!.Email
            );
}
