namespace PRManagement.Application.Clients.Queries.GetClientWithPagination;

public class GetClientItem
{
    public Guid Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public string Address { get; set; } = string.Empty;
    public string[] Email { get; set; } = Array.Empty<string>();
    public string[] PhoneNumbers { get; set; } = Array.Empty<string>();
    public string[] ClientTypes { get; set; } = Array.Empty<string>();
    public IEnumerable<ContactItem> Contacts { get; set; } = Enumerable.Empty<ContactItem>();
    public DateTimeOffset Created { get; set; }
    public string? CreatedBy { get; set; }
    public DateTimeOffset LastModified { get; set; }
    public string? LastModifiedBy { get; set; }
}

public class ContactItem
{
    public Guid Id { get; set; }
    public Guid ContactClientId { get; set; }
    public string FirstName { get; set; } = string.Empty;
    public string LastName { get; set; } = string.Empty;
    public string Position { get; set; } = string.Empty;
    public string[] Email { get; set; } = Array.Empty<string>();
    public string[] PhoneNumbers { get; set; } = Array.Empty<string>();
    public string LineId { get; set; } = string.Empty;
    public string? ProvinceId { get; set; }
    public string? ProvinceEN { get; set; }
    public string? ProvinceTH { get; set; }
    public string DistrictId { get; set; } = string.Empty;
    public string DistrictEN { get; set; } = string.Empty;
    public string DistrictTH { get; set; } = string.Empty;
    public string SubDistrictId { get; set; } = string.Empty;
    public string SubDistrictEN { get; set; } = string.Empty;
    public string SubDistrictTH { get; set; } = string.Empty;
    public string PostCode { get; set; } = string.Empty;
}
