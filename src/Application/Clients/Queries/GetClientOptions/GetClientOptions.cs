using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Application.Common.Models;

namespace PRManagement.Application.Clients.Queries.GetClientOptions;
public record GetClientOptionsQuery : IRequest<IEnumerable<OptionItemBase>>
{
    public string Name { get; set; } = string.Empty;
}

public class GetClientOptionsQueryHandler : IRequestHandler<GetClientOptionsQuery, IEnumerable<OptionItemBase>>
{
    private readonly IApplicationDbContext _context;
    public readonly IMapper _mapper;

    public GetClientOptionsQueryHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<IEnumerable<OptionItemBase>> Handle(GetClientOptionsQuery request, CancellationToken cancellationToken)
    {
        return await _context.Clients.WhereLike(e => e.Name, request.Name)
            .ProjectTo<OptionItemBase>(_mapper.ConfigurationProvider)
            .ToListAsync(cancellationToken);
    }
}