using PRManagement.Application.Common.Models;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.Clients.Queries.GetClientWithPagination;
public class GetClientOptionMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<Client, OptionItemBase>();
        }
    }
}