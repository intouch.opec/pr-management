using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Constants;

namespace PRManagement.Application.Clients.Commands.UpdateClient;

public class UpdateClientCommandValidator : AbstractValidator<UpdateClientCommand>
{
    private readonly IApplicationDbContext _context;

    public UpdateClientCommandValidator(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(v => v.Name)
            .MustAsync(ClientNameBeUnique)
            .WithMessage(ValidatorMessages.ShouldBeUnique)
            .MaximumLength(35)
            .NotEmpty();

        RuleForEach(v => v.Email)
            .EmailAddress()
            .WithMessage(ValidatorMessages.InvalidArrayFormat);

        RuleForEach(e => e.PhoneNumbers)
            .Matches(Patterns.PhoneNumber)
            .WithMessage(ValidatorMessages.InvalidArrayFormat);

        RuleFor(e => e.ProvinceId)
            .MustAsync(HasProvince)
            .WithMessage(ValidatorMessages.NotFound);

        RuleFor(e => e.DistrictId)
            .MustAsync(HasDistrict)
            .WithMessage(ValidatorMessages.NotFound);

        RuleFor(e => e.SubDistrictId)
            .MustAsync(HasSubDistrict)
            .WithMessage(ValidatorMessages.NotFound);

        RuleFor(e => e.PostCode)
            .MaximumLength(5)
            .MinimumLength(0)
            .When(postCode => postCode is null);
    }

    public async Task<bool> HasProvince(string? ProvinceId, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(ProvinceId))
            return true;

        return await _context.Provinces
            .AnyAsync(p => p.Id.Equals(ProvinceId), cancellationToken);
    }

    public async Task<bool> HasDistrict(string? districtId, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(districtId))
            return true;

        return await _context.Districts
            .AnyAsync(p => p.Id.Equals(districtId), cancellationToken);
    }

    public async Task<bool> HasSubDistrict(string? subDistrictId, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(subDistrictId))
            return true;

        return await _context.SubDistricts
            .AnyAsync(p => p.Id.Equals(subDistrictId), cancellationToken);
    }

    public async Task<bool> ClientNameBeUnique(UpdateClientCommand command,string name, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(name))
            return true;

        return !await _context.Clients
            .AnyAsync(e => 
                e.Id != command.Id &&
                e.Name.ToLower()
                .Equals(name.ToLower()), cancellationToken);
    }
}