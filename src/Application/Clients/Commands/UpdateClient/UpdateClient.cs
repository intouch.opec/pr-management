﻿using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.Clients.Commands.UpdateClient;

public record UpdateClientCommand : IRequest<UpdateClientResponse>
{
    public Guid Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public string Address { get; set; } = string.Empty;
    public string[] Email { get; set; } = Array.Empty<string>();
    public string[] PhoneNumbers { get; set; } = Array.Empty<string>();
    public string? ProvinceId { get; set; }
    public string? DistrictId { get; set; }
    public string? SubDistrictId { get; set; }
    public string? PostCode { get; set; }
}

public class UpdateClientCommandHandler : IRequestHandler<UpdateClientCommand, UpdateClientResponse>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public UpdateClientCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<UpdateClientResponse> Handle(UpdateClientCommand request, CancellationToken cancellationToken)
    {
        var entity = await _context.Clients
            .Include(e => e.Address)
            .AsTracking()
            .FirstOrDefaultAsync(e => e.Id.Equals(request.Id), cancellationToken);

        Guard.Against.NotFound(request.Id, entity);

        _mapper.Map<UpdateClientCommand, Client>(request, entity);

        await _context.SaveChangesAsync(cancellationToken);

        return _mapper.Map<UpdateClientResponse>(entity);
    }
}
