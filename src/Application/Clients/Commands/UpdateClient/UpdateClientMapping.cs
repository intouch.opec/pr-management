using PRManagement.Domain.Entities;

namespace PRManagement.Application.Clients.Commands.UpdateClient;
public class UpdateClientMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<UpdateClientCommand, Client>()
                .ForMember(s => s.Id, s => s.Ignore())
                .ForMember(s => s.Address, op => op.MapFrom<UpdateClientCommandAddressResolver>());

            CreateMap<Client, UpdateClientResponse>()
                .ForMember(s => s.Email, op => op.MapFrom(o => o.Address!.Email ?? Array.Empty<string>()))
                .ForMember(s => s.PhoneNumbers, op => op.MapFrom(o => o.Address!.PhoneNumbers ?? Array.Empty<string>()))
                .ForMember(s => s.Address, op => op.MapFrom(o => o.Address!.Text ?? string.Empty))
                .ForMember(s => s.ProvinceId, op => op.MapFrom(o => o.Address!.ProvinceId ?? string.Empty))
                .ForMember(s => s.DistrictId, op => op.MapFrom(o => o.Address!.DistrictId ?? string.Empty))
                .ForMember(s => s.SubDistrictId, op => op.MapFrom(o => o.Address!.SubDistrictId ?? string.Empty))
                .ForMember(s => s.PostCode, op => op.MapFrom(o => o.Address!.PostCode ?? string.Empty));

            CreateMap<UpdateClientCommand, Address>()
                .ForMember(s => s.Text, op => op.MapFrom(o => o.Address))
                .ForMember(s => s.Id, op => op.Ignore());

            CreateMap<UpdateClientResponse, Address>()
                .ForMember(s => s.Id, op => op.Ignore())
                .ForMember(s => s.Text, op => op.MapFrom(o => o.Address));

        }

        public class UpdateClientCommandAddressResolver : IValueResolver<UpdateClientCommand, Client, Address?>
        {
            private readonly IMapper _mapper;

            public UpdateClientCommandAddressResolver(IMapper mapper)
            {
                _mapper = mapper;
            }

            public Address? Resolve(UpdateClientCommand source, Client destination, Address? member, ResolutionContext context)
            {
                if (!SomePropertiesAddressHaveValue(source))
                {
                    _mapper.Map<UpdateClientCommand, Address?>(source, destination.Address);
                    if (destination.AddressId is not null)
                        member!.Id = (Guid)destination.AddressId;
                    else 
                        return _mapper.Map<Address>(source);
                }
                return member;
            }

            public static bool SomePropertiesAddressHaveValue(UpdateClientCommand source)
            {
                if (source is null)
                    return false;

                return !source.Email.Any() ||
                       !source.PhoneNumbers.Any() ||
                       string.IsNullOrEmpty(source.Address) ||
                       string.IsNullOrEmpty(source.PostCode) ||
                       string.IsNullOrEmpty(source.ProvinceId) ||
                       string.IsNullOrEmpty(source.DistrictId) ||
                       string.IsNullOrEmpty(source.SubDistrictId);
            }
        }
    }
}