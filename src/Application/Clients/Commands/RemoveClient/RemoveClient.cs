﻿using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;

namespace PRManagement.Application.Clients.Commands.RemoveClient;

public record RemoveClientCommand : IRequest<RemoveClientResponse>
{
    public Guid Id { get; set; }
    public string Remark { get; set; } = string.Empty;
}

public class RemoveClientCommandHandler : IRequestHandler<RemoveClientCommand, RemoveClientResponse>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public RemoveClientCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<RemoveClientResponse> Handle(RemoveClientCommand request, CancellationToken cancellationToken)
    {
        var entity = await _context.Clients
            .Where(e => e.Id == request.Id)
            .SingleOrDefaultAsync(cancellationToken);

        Guard.Against.NotFound(request.Id, entity);

        _context.Clients.RemoveAndAddRemark(entity, request.Remark);

        await _context.SaveChangesAsync(cancellationToken);

        return _mapper.Map<RemoveClientResponse>(entity);
    }
}
