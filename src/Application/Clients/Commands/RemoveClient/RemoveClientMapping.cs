using PRManagement.Domain.Entities;

namespace PRManagement.Application.Clients.Commands.RemoveClient;

public class RemoveClientMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<Client, RemoveClientResponse>();
        }
    }
}
