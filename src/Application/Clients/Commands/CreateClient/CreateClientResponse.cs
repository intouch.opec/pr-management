namespace PRManagement.Application.Clients.Commands.CreateClient;

public class CreateClientResponse
{
    public Guid Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public string Address { get; set; } = string.Empty;
    public string[] Email { get; set; } = Array.Empty<string>();
    public string[] PhoneNumbers { get; set; } = Array.Empty<string>();
    public string? ProvinceId { get; set; } = string.Empty;
    public string? DistrictId { get; set; } = string.Empty;
    public string? SubDistrictId { get; set; } = string.Empty;
    public string PostCode { get; set; } = string.Empty;
}
