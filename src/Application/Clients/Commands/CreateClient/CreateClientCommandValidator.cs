using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Constants;

namespace PRManagement.Application.Clients.Commands.CreateClient;

public class CreateClientCommandValidator : AbstractValidator<CreateClientCommand>
{
    private readonly IApplicationDbContext _context;

    public CreateClientCommandValidator(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(v => v.Name)
            .MaximumLength(35)
            .MustAsync(ClientNameShouldBeUnique)
            .WithMessage(ValidatorMessages.ShouldBeUnique)
            .NotEmpty();

        RuleForEach(v => v.Email)
            .NotNull()
            .Must((e, x) =>
                e.Email.Where(p => !string.IsNullOrEmpty(p)).Count(p => p.Equals(x)) < 2)
            .WithMessage(ValidatorMessages.ShouldNotBeDuplicate)
            .EmailAddress()
            .WithMessage(ValidatorMessages.InvalidArrayFormat);

        RuleForEach(e => e.PhoneNumbers)
            .NotNull()
            .Must((e, x) =>
                e.PhoneNumbers.Where(p => !string.IsNullOrEmpty(p)).Count(p => p.Equals(x)) < 2)
            .WithMessage(ValidatorMessages.ShouldNotBeDuplicate)
            .Matches(Patterns.PhoneNumber)
            .WithMessage(ValidatorMessages.InvalidArrayFormat);

        RuleFor(e => e.ProvinceId)
            .MustAsync(HasProvince)
            .WithMessage(ValidatorMessages.NotFound);

        RuleFor(e => e.DistrictId)
            .MustAsync(HasDistrict)
            .WithMessage(ValidatorMessages.NotFound);

        RuleFor(e => e.SubDistrictId)
            .MustAsync(HasSubDistrict)
            .WithMessage(ValidatorMessages.NotFound);

        RuleFor(e => e.PostCode)
            .MaximumLength(5)
            .MinimumLength(0)
            .When(postCode => postCode is not null);
    }

    internal async Task<bool> HasProvince(string? ProvinceId, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(ProvinceId))
            return true;

        return await _context.Provinces
            .AsNoTracking()
            .AnyAsync(p => p.Id.Equals(ProvinceId), cancellationToken);
    }

    internal async Task<bool> ClientNameShouldBeUnique(string? name, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(name))
            return true;

        return !await _context.Clients
            .AsNoTracking()
            .AnyAsync(p => p.Name.Equals(name),
              cancellationToken);
    }


    internal async Task<bool> HasDistrict(string? districtId, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(districtId))
            return true;

        return await _context.Districts
          .AsNoTracking()
          .AnyAsync(p => p.Id.Equals(districtId), cancellationToken);
    }

    internal async Task<bool> HasSubDistrict(string? subDistrictId, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(subDistrictId))
            return true;

        return await _context.SubDistricts
          .AsNoTracking()
          .AnyAsync(p => p.Id.Equals(subDistrictId), cancellationToken);
    }
}
