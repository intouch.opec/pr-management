﻿using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.Clients.Commands.CreateClient;

public record CreateClientCommand : IRequest<CreateClientResponse>
{
    public string Name { get; set; } = string.Empty;
    public string Address { get; set; } = string.Empty;
    public string[] Email { get; set; } = Array.Empty<string>();
    public string[] PhoneNumbers { get; set; } = Array.Empty<string>();
    public string? ProvinceId { get; set; } 
    public string? DistrictId { get; set; } 
    public string? SubDistrictId { get; set; } 
    public string PostCode { get; set; } = string.Empty;
}

public class CreateClientCommandHandler : IRequestHandler<CreateClientCommand, CreateClientResponse>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public CreateClientCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<CreateClientResponse> Handle(CreateClientCommand request, CancellationToken cancellationToken)
    {
        var entity = _mapper.Map<Client>(request);
        await _context.Clients.AddAsync(entity);
        await _context.SaveChangesAsync(cancellationToken);
        return _mapper.Map<CreateClientResponse>(entity);
    }
}
