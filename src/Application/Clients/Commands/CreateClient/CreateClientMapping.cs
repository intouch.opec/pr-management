using PRManagement.Domain.Entities;

namespace PRManagement.Application.Clients.Commands.CreateClient;

public class CreateClientMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {

            CreateMap<CreateClientCommand, Client>()
              .ForMember(s => s.Address, op => op.MapFrom<CreateClientCommandAddressResolver>());

            CreateMap<CreateClientCommand, Address>()
              .ForMember(s => s.Text, op => op.MapFrom(o => o.Address));

            CreateMap<Client, CreateClientResponse>()
              .ForMember(s => s.Address, op => op.MapFrom(o => o.Address!.Text ?? string.Empty))
              .ForMember(s => s.Email, op => op.MapFrom(o => o.Address!.Email ?? Array.Empty<string>()))
              .ForMember(s => s.PhoneNumbers, op => op.MapFrom(o => o.Address!.PhoneNumbers ?? Array.Empty<string>()))
              .ForMember(s => s.Address, op => op.MapFrom(o => o.Address!.Text ?? string.Empty))
              .ForMember(s => s.ProvinceId, op => op.MapFrom(o => o.Address!.ProvinceId ?? string.Empty))
              .ForMember(s => s.DistrictId, op => op.MapFrom(o => o.Address!.DistrictId ?? string.Empty))
              .ForMember(s => s.SubDistrictId, op => op.MapFrom(o => o.Address!.SubDistrictId ?? string.Empty))
              .ForMember(s => s.PostCode, op => op.MapFrom(o => o.Address!.PostCode ?? string.Empty));

        }
    }
    public class CreateClientCommandAddressResolver : IValueResolver<CreateClientCommand, Client, Address?>
    {
        private readonly IMapper _mapper;

        public CreateClientCommandAddressResolver(IMapper mapper)
        {
            _mapper = mapper;
        }

        public Address? Resolve(CreateClientCommand source, Client destination, Address? member, ResolutionContext context)
        {
            if (SomePropertiesAddressHaveValue(source))
                return null;

            return _mapper.Map<Address>(source);
        }

        public static bool SomePropertiesAddressHaveValue(CreateClientCommand source)
        {
            if (source is null)
                return false;

            return !source.Email.Any() &&
                 !source.PhoneNumbers.Any() &&
                 string.IsNullOrEmpty(source.Address) &&
                 string.IsNullOrEmpty(source.PostCode) &&
                 string.IsNullOrEmpty(source.ProvinceId) &&
                 string.IsNullOrEmpty(source.DistrictId) &&
                 string.IsNullOrEmpty(source.SubDistrictId);
        }
    }
}
