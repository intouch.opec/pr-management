using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Constants;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.CreateContactMediaSections.Commands.CreateContactMediaSections;
public class CreateContactMediaSectionsValidation : AbstractValidator<CreateContactMediaSectionCommand>
{
    private readonly IApplicationDbContext _context;

    public CreateContactMediaSectionsValidation(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(e => e.ContactId)
            .StringIsGuid()
            .NotNull()
            .NotEmpty()
            .MustAsync(HasContact)
            .WithMessage(ValidatorMessages.NotFound)
            .MustAsync(BeUniqueContactMediaSection)
            .WithMessage(ValidatorMessages.ShouldNotBeDuplicate);

        RuleFor(e => e.MediaSectionId)
            .StringIsGuid()
            .NotNull()
            .NotEmpty()
            .MustAsync(HasMediaSection)
            .WithMessage(ValidatorMessages.NotFound);

        RuleFor(e => e.ContactStatus)
            .NotNull()
            .NotEmpty()
            .MustEnum<CreateContactMediaSectionCommand, ContactStatus>();

    }

    internal async Task<bool> HasMediaSection(string? mediaSectionId, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(mediaSectionId) || !mediaSectionId.IsValidGuid())
            return true;

        return await _context.MediaSections
          .AnyAsync(ms => ms.Id.Equals(new Guid(mediaSectionId)), cancellationToken);
    }

    internal async Task<bool> HasContact(string? contactId, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(contactId) || !contactId.IsValidGuid())
            return true;

        return await _context.Contacts
          .AnyAsync(p => p.Id.Equals(new Guid(contactId)), cancellationToken);
    }

    internal async Task<bool> BeUniqueContactMediaSection(
        CreateContactMediaSectionCommand request, string? contactId, CancellationToken cancellationToken)
    {
        var contactIsInValid = string.IsNullOrWhiteSpace(request.ContactId) || !request.ContactId.IsValidGuid() ||
                                string.IsNullOrWhiteSpace(request.MediaSectionId) || !request.MediaSectionId.IsValidGuid();
        if (contactIsInValid)
            return true;

        return await _context.ContactMediaSections
          .AnyAsync(e =>
            e.ContactId.Equals(new Guid(request.ContactId)) &&
            e.MediaSectionId.Equals(new Guid(request.MediaSectionId)),
            cancellationToken);
    }

}
