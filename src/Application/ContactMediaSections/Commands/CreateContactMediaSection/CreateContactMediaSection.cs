using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.CreateContactMediaSections.Commands.CreateContactMediaSections;
public class CreateContactMediaSectionCommand : IRequest<CreateContactMediaSectionsResponse>
{
    public string MediaSectionId { get; set; } = string.Empty;
    public string ContactId { get; set; } = string.Empty;
    public string ContactStatus { get; set; } = string.Empty;
}

public class CreateContactMediaSectionsCommandHandler : IRequestHandler<CreateContactMediaSectionCommand, CreateContactMediaSectionsResponse>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public CreateContactMediaSectionsCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<CreateContactMediaSectionsResponse> Handle(CreateContactMediaSectionCommand request, CancellationToken cancellationToken)
    {
        var entity = _mapper.Map<ContactMediaSection>(request);

        await _context.ContactMediaSections.AddAsync(entity);
        await _context.SaveChangesAsync(cancellationToken);

        return _mapper.Map<CreateContactMediaSectionsResponse>(entity);
    }
}