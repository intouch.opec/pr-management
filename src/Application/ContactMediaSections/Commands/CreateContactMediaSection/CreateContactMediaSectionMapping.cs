using PRManagement.Domain.Entities;

namespace PRManagement.Application.CreateContactMediaSections.Commands.CreateContactMediaSections;
public class CreateContactMediaSectionMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<CreateContactMediaSectionCommand, ContactMediaSection>();
            CreateMap<ContactMediaSection, CreateContactMediaSectionsResponse>();
        }
    }
}