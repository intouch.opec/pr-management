namespace PRManagement.Application.CreateContactMediaSections.Commands.CreateContactMediaSections;

public class CreateContactMediaSectionsResponse
{
    public Guid Id { get; set; }
    public Guid ContactId { get; set; }
    public Guid MediaSectionId { get; set; }
}
