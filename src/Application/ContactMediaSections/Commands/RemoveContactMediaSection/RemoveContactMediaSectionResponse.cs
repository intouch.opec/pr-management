namespace PRManagement.Application.RemoveContactMediaSection.Commands.RemoveContactMediaSection;

public class RemoveContactMediaSectionResponse
{
    public Guid ContactId { get; set; }
    public Guid MediaSectionId { get; set; }
}
