using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Constants;

namespace PRManagement.Application.RemoveContactMediaSection.Commands.RemoveContactMediaSection;

public class RemoveContactMediaSectionValidator : AbstractValidator<RemoveContactMediaSectionCommand>
{
    private readonly IApplicationDbContext _context;
    public RemoveContactMediaSectionValidator(IApplicationDbContext context)
    {
        _context = context;
        RuleFor(e => e.ContactId)
            .NotEmpty()
            .StringIsGuid()
            .MustAsync(HasContact)
            .WithMessage(ValidatorMessages.NotFound);

        RuleFor(e => e.MediaSectionId)
            .NotEmpty()
            .StringIsGuid()
            .MustAsync(HasMediaSection)
            .WithMessage(ValidatorMessages.NotFound);
    }


    internal async Task<bool> HasContact(string? contactId, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(contactId) || !contactId.IsValidGuid())
            return true;

        return await _context.Contacts
          .AnyAsync(p => p.Id.Equals(new Guid(contactId)), cancellationToken);
    }

    internal async Task<bool> HasMediaSection(string? mediaSectionId, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(mediaSectionId) || !mediaSectionId.IsValidGuid())
            return true;

        return await _context.MediaSections
          .AnyAsync(ms => ms.Id.Equals(new Guid(mediaSectionId)), cancellationToken);
    }

}