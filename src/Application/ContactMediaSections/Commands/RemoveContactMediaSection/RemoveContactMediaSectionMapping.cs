using PRManagement.Domain.Entities;

namespace PRManagement.Application.RemoveContactMediaSection.Commands.RemoveContactMediaSection;

public class RemoveContactMediaSectionMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<ContactMediaSection, RemoveContactMediaSectionResponse>();
        }
    }
}
