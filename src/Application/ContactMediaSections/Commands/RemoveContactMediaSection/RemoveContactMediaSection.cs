using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;

namespace PRManagement.Application.RemoveContactMediaSection.Commands.RemoveContactMediaSection;

public record RemoveContactMediaSectionCommand : IRequest<RemoveContactMediaSectionResponse>
{
    public string ContactId { get; set; } = string.Empty;
    public string MediaSectionId { get; set; } = string.Empty;
    public string Remark { get; set; } = string.Empty;
}

public class RemoveContactMediaSectionCommandHandler : IRequestHandler<RemoveContactMediaSectionCommand, RemoveContactMediaSectionResponse>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public RemoveContactMediaSectionCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<RemoveContactMediaSectionResponse> Handle(RemoveContactMediaSectionCommand request, CancellationToken cancellationToken)
    {
        var entity = await _context.ContactMediaSections
          .Where(e => e.ContactId == request.ContactId.ToGuid() && e.MediaSectionId == request.MediaSectionId.ToGuid())
          .SingleOrDefaultAsync(cancellationToken);

        Guard.Against.NotFound(request.ContactId, entity);

        _context.ContactMediaSections.RemoveAndAddRemark(entity, request.Remark);

        await _context.SaveChangesAsync(cancellationToken);

        return _mapper.Map<RemoveContactMediaSectionResponse>(entity);
    }
}
