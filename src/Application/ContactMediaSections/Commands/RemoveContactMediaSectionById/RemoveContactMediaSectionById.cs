using PRManagement.Application.Common.Interfaces;

namespace PRManagement.Application.RemoveContactMediaSectionById.Commands.RemoveContactMediaSectionById;
public record RemoveContactMediaSectionByIdCommand(Guid Id) : IRequest<RemoveContactMediaSectionByIdResponse>;

public class RemoveContactMediaSectionByIdCommandHandler : IRequestHandler<RemoveContactMediaSectionByIdCommand, RemoveContactMediaSectionByIdResponse>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public RemoveContactMediaSectionByIdCommandHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<RemoveContactMediaSectionByIdResponse> Handle(RemoveContactMediaSectionByIdCommand request, CancellationToken cancellationToken)
    {
        var entity = await _context.ContactMediaSections
          .Where(e => e.Id == request.Id)
          .SingleOrDefaultAsync(cancellationToken);

        Guard.Against.NotFound(request.Id, entity);

        _context.ContactMediaSections.Remove(entity);

        await _context.SaveChangesAsync(cancellationToken);

        return _mapper.Map<RemoveContactMediaSectionByIdResponse>(entity);
    }
}
