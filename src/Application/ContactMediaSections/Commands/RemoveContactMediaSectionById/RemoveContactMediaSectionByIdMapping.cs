using PRManagement.Domain.Entities;

namespace PRManagement.Application.RemoveContactMediaSectionById.Commands.RemoveContactMediaSectionById;
public class RemoveContactMediaSectionByIdMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<ContactMediaSection, RemoveContactMediaSectionByIdResponse>();
        }
    }
}
