namespace PRManagement.Application.RemoveContactMediaSectionById.Commands.RemoveContactMediaSectionById;

public class RemoveContactMediaSectionByIdResponse
{
    public Guid Id { get; set; }
}
