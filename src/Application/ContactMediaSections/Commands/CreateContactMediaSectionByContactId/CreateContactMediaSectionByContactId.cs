
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Entities;

namespace PRManagement.Application.ContactMediaSections.Commands.CreateContactMediaSectionByContactId;

public class CreateContactMediaSectionByContactIdCommand : IRequest<IEnumerable<CreateContactMediaSectionByContactIdResponse>>
{
	public string ContactId { get; set; } = string.Empty;
	public IEnumerable<MediaSectionItem> MediaSections { get; set; } = Enumerable.Empty<MediaSectionItem>();
}

public class MediaSectionItem
{
	public string MediaSectionId { get; set; } = string.Empty;
	public string ContactStatus { get; set; } = string.Empty;
}

public class CreateContactMediaSectionsCommandHandler : IRequestHandler<CreateContactMediaSectionByContactIdCommand, IEnumerable<CreateContactMediaSectionByContactIdResponse>>
{
	private readonly IApplicationDbContext _context;
	private readonly IMapper _mapper;

	public CreateContactMediaSectionsCommandHandler(IApplicationDbContext context, IMapper mapper)
	{
		_context = context;
		_mapper = mapper;
	}

	public async Task<IEnumerable<CreateContactMediaSectionByContactIdResponse>> Handle(CreateContactMediaSectionByContactIdCommand request, CancellationToken cancellationToken)
	{
		var entityies = _mapper.Map<IEnumerable<ContactMediaSection>>(request);

		await _context.ContactMediaSections.AddRangeAsync(entityies);
		await _context.SaveChangesAsync(cancellationToken);
		return entityies.Select(_mapper.Map<CreateContactMediaSectionByContactIdResponse>);
	}
}
