using PRManagement.Domain.Enums;

namespace PRManagement.Application.ContactMediaSections.Commands.CreateContactMediaSectionByContactId;

public class CreateContactMediaSectionByContactIdResponse
{
    public Guid ContactId { get; set; }
    public Guid MediaSectionId { get; set; }
    public ContactStatus ContactStatus { get; set; }
}
