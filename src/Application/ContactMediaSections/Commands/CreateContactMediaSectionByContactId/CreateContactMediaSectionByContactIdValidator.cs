using PRManagement.Application.Common.Extensions;
using PRManagement.Application.Common.Interfaces;
using PRManagement.Domain.Constants;
using PRManagement.Domain.Enums;

namespace PRManagement.Application.ContactMediaSections.Commands.CreateContactMediaSectionByContactId;

public class CreateContactMediaSectionByContactIdValidator : AbstractValidator<CreateContactMediaSectionByContactIdCommand>
{
    private readonly IApplicationDbContext _context;

    public CreateContactMediaSectionByContactIdValidator(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(e => e.MediaSections)
            .Must(mediaSections => mediaSections.Select(ms => ms.MediaSectionId).Distinct().Count() == mediaSections.Count())
            .WithMessage("Each MediaSectionId must be unique.")
            .DependentRules(() =>
            {
                RuleForEach(e => e.MediaSections).SetValidator((command, mediaSection) =>
                {
                    var mediaSectionIds = _context.ContactMediaSections
                        .Where(cms => cms.ContactId.Equals(command.ContactId.ToGuidOnly()) &&
                            command.MediaSections.Select(e => e.MediaSectionId.ToGuidOnly())
                            .Contains(cms.MediaSectionId))
                    .Select(e => e.MediaSectionId.ToString());
                    return new MediaSectionItemValidator(mediaSectionIds);
                });
            })
          .Must(mediaSections => mediaSections != null && mediaSections.Any())
          .WithMessage("{PropertyName} must contain at least one item.");

        RuleFor(e => e.ContactId)
            .StringIsGuid()
            .NotNull()
            .NotEmpty()
            .MustAsync(HasContact)
            .WithMessage(ValidatorMessages.NotFound);

        RuleForEach(e => e.MediaSections)
            .ChildRules((mediaSections) =>
                mediaSections.RuleFor(pl => pl.MediaSectionId)
                .StringIsGuid()
                .MustAsync(HasMediaSection)
                .WithMessage(ValidatorMessages.NotFound)
                .NotEmpty())
            .ChildRules((mediaSections) =>
                mediaSections.RuleFor(pl => pl.ContactStatus)
                .NotNull()
                .NotEmpty()
                .MustEnum<MediaSectionItem, ContactStatus>()
            );
    }

    internal async Task<bool> HasContact(string? contactId, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(contactId) || !contactId.IsValidGuid())
            return true;

        return await _context.Contacts
          .AnyAsync(p => p.Id.Equals(new Guid(contactId)), cancellationToken);
    }

    internal async Task<bool> HasMediaSection(string? mediaSectionId, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(mediaSectionId) || !mediaSectionId.IsValidGuid())
            return true;

        return await _context.MediaSections
          .AnyAsync(ms => ms.Id.Equals(new Guid(mediaSectionId)), cancellationToken);
    }

}

public class MediaSectionItemValidator : AbstractValidator<MediaSectionItem>
{
    private readonly IEnumerable<string> _mediaSectionIds;

    public MediaSectionItemValidator(IEnumerable<string> mediaSectionIds)
    {
        _mediaSectionIds = mediaSectionIds;

        RuleFor(pl => pl.MediaSectionId)
            .Must(BeUniqueMediaSection)
            .WithMessage(ValidatorMessages.ShouldNotBeDuplicate);
    }

    private bool BeUniqueMediaSection(string mediaSectionId)
    {
        if (!Guid.TryParse(mediaSectionId, out _))
            return false;
        return !_mediaSectionIds.Contains(mediaSectionId);
    }
}
