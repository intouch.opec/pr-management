using PRManagement.Application.Common.Extensions;
using PRManagement.Domain.Entities;
using PRManagement.Domain.Enums;
using PRManagement.Domain.Exceptions;

namespace PRManagement.Application.ContactMediaSections.Commands.CreateContactMediaSectionByContactId;
public class CreateContactMediaSectionMapping
{
    public sealed class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<CreateContactMediaSectionByContactIdCommand, IEnumerable<ContactMediaSection>>()
             .ConvertUsing(command => command.MediaSections
                .Select(ms => new ContactMediaSection
                {
                    ContactId = command.ContactId.ToGuidOnly(),
                    MediaSectionId = ms.MediaSectionId.ToGuidOnly(),
                    ContactStatus = ms.ContactStatus.ToEnum<ContactStatus>()
                }));

            CreateMap<ContactMediaSection, CreateContactMediaSectionByContactIdResponse>();

        }
    }

}
