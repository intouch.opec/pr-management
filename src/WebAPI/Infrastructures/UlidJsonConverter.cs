using System.Text.Json;
using System.Text.Json.Serialization;
using NUlid;

namespace PRManagement.Web.Infrastructure;
public class UlidJsonConverter : JsonConverter<Ulid>
{
    public override Ulid Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
    {
        var ulidString = reader.GetString();
        return Ulid.Parse(ulidString);
    }

    public override void Write(Utf8JsonWriter writer, Ulid value, JsonSerializerOptions options)
    {
        writer.WriteStringValue(value.ToString());
    }
}