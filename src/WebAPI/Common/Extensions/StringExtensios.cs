using System.Text;
using Newtonsoft.Json.Serialization;

namespace PRManagement.WebAPI.Common.Extensions;
public static class StringExtensions
{
    public static string? ToCustomCase(this string? text,
        Type? type = null, string separateWords = "-")
    {
        if (text is null)
            throw new ArgumentNullException(nameof(text));

        if (text.Length < 2)
            return text;
            
        var isSnakeCase = string.IsNullOrWhiteSpace(separateWords) && type == typeof(SnakeCaseNamingStrategy);
        if (isSnakeCase)
            separateWords = "_";

        var sb = new StringBuilder();
        sb.Append(char.ToLowerInvariant(text[0]));
        for (int i = 1; i < text.Length; ++i)
        {
            char c = text[i];
            if (char.IsUpper(c))
            {
                sb.Append(separateWords);
                sb.Append(char.ToLowerInvariant(c));
            }
            else
            {
                sb.Append(c);
            }
        }
        return sb.ToString();
    }

    public static string RemoveHttpPrefix(this string url)
    {
        if (url.StartsWith("http://"))
        {
            return url.Substring(7);
        }
        else if (url.StartsWith("https://"))
        {
            return url.Substring(8);
        }
        else
        {
            return url;
        }
    }
}
