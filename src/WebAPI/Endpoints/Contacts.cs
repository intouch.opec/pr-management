using Microsoft.AspNetCore.Mvc;
using PRManagement.Application.Common.Models;
using PRManagement.Application.ContactMediaSections.Commands.CreateContactMediaSectionByContactId;
using PRManagement.Application.Contacts.Commands.CreateContact;
using PRManagement.Application.Contacts.Commands.DeleteContact;
using PRManagement.Application.Contacts.Commands.UpdateContact;
using PRManagement.Application.Contacts.Queries.GetContactAndMediaWithPagination;
using PRManagement.Application.Contacts.Queries.GetContactById;
using PRManagement.Application.Contacts.Queries.GetContactWithPagination;
using PRManagement.Application.RemoveContactMediaSection.Commands.RemoveContactMediaSection;
using PRManagement.WebAPI.Common.Extensions;

namespace PRManagement.Web.Endpoints;

public class Contacts : EndpointGroupBase
{
    public override void Map(WebApplication app)
    {
        app.MapGroup(this)
            .RequireAuthorization()
            .MapGet(GetContactWithPagination)
            .MapGet(GetContactAndMediaWithPagination, "/medias")
            .MapGet(GetContactById, "{id}")
            .MapPost<CreateContactResponse>(CreateContact)
            .MapPut(UpdateContact, "{id}")
            .MapDelete(DeleteContact, "{id}")
            .MapPost(CreateContactMediaSectionById, "{id}/contact-media-sections")
            .MapDelete(RemoveContactMediaSectionByContactId, "{id}/media-sections/{mediaSectionId}");
    }

    internal async Task<PaginatedList<GetContactWithPaginationResponse>> GetContactWithPagination(ISender sender, [AsParameters] GetContactWithPaginationQuery request)
        => await sender.Send(request);

    internal async Task<PaginatedList<GetContactAndMediaWithPaginationResponse>> GetContactAndMediaWithPagination(ISender sender, [AsParameters] GetContactAndMediaWithPaginationQuery request)
        => await sender.Send(request);
    
    internal async Task<GetContactByIdResponse> GetContactById(ISender sender, Guid id)
        => await sender.Send(new GetContactByIdQuery(id));

    internal async Task<IResult> CreateContact(ISender sender, CreateContactCommand request)
    {
        var contact = await sender.Send(request);
        return TypedResults.Created(
            $"/{GetType().Name.ToCustomCase()}/{contact.Id}",
            contact
        );
    }

    internal async Task<UpdateContactResponse> UpdateContact(ISender sender, Guid id, UpdateContactCommand request)
    {
        request.Id = id;
        return await sender.Send(request);
    }

    internal async Task<DeleteContactResponse> DeleteContact(
      ISender sender, Guid id, [FromBody] DeleteContactCommand request)
    {
        request.Id = id;
        return await sender.Send(request);
    }

    internal async Task<IResult> CreateContactMediaSectionById(ISender sender, string id, CreateContactMediaSectionByContactIdCommand request)
    {
        request.ContactId = id;
        var contacts = await sender.Send(request);
        return TypedResults.Created(
            $"/{GetType().Name.ToCustomCase()}/{request.ContactId}/contact-media-sections",
            contacts
        );
    }

    internal async Task<RemoveContactMediaSectionResponse> RemoveContactMediaSectionByContactId(
      ISender sender, string id, string mediaSectionId, [FromBody] RemoveContactMediaSectionCommand command)
    {
        command.ContactId = id;
        command.MediaSectionId = mediaSectionId;
        return await sender.Send(command);
    }
}
