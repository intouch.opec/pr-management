using Microsoft.AspNetCore.Mvc;
using PRManagement.Application.Common.Models;
using PRManagement.Application.PRActivities.Commands.CreatePRActivity;
using PRManagement.Application.PRActivities.Commands.RemovePRActivity;
using PRManagement.Application.PRActivities.Commands.UpdatePRActivity;
using PRManagement.Application.PRActivities.Queries.GetPRActivity;
using PRManagement.Application.PRActivities.Queries.GetPRActivityWithPagination;
using PRManagement.Application.PRActivityBriefings.Commands.CreatePRActivityBriefing;
using PRManagement.Application.PRActivityBriefings.Commands.UpdatePRActivityBriefing;
using PRManagement.Application.PRActivityBriefings.Queries.GetPRActivityBriefing;
using PRManagement.Application.PRActivityEmails.Commands.CreatePRActivityEmail;
using PRManagement.Application.PRActivityEmails.Commands.UpdatePRActivityEmail;
using PRManagement.Application.PRActivityEmails.Queries.GetPRActivityEmail;
using PRManagement.Application.PRActivityLocalFiles.Commands.UploadFiles;
using PRManagement.Application.PRActivityLocations.Commands.CreatePRActivityLocation;
using PRManagement.Application.PRActivityLocations.Commands.UpdatePRActivityLocation;
using PRManagement.Application.PRActivityLocations.Queries.GetPRActivityLocation;
using PRManagement.Application.PRActivityReceivers.Commands.CreatePRActivityReceivers;
using PRManagement.Application.PRActivityTasks.Commands.CreatePRActivityTask;
using PRManagement.Application.PRActivityTasks.Queries.GetPrActivityTasks;
using PRManagement.Application.WorkProgressList.Queries.GetWorkProgressByPrActivityId;

namespace PRManagement.Web.Endpoints;
public class PrActivities : EndpointGroupBase
{
    public override void Map(WebApplication app)
    {
        app.MapGroup(this)
            .RequireAuthorization()
            .MapPost(CreatePRActivity)
            .MapGet(GetPRActivityWithPagination)
            .MapGet(GetPRActivity, "{id}")
            .MapPut(UpdatePRActivity, "{id}")
            .MapDelete(DeletePRActivity, "{id}")
            .MapGet(GetPRActivityBriefing, "{id}/briefings")
            .MapPost(CreatePRActivityBriefing, "{id}/briefings")
            .MapPut(UpdatePRActivityBriefing, "{id}/briefings")
            .MapGet(GetPRActivityEmail, "{id}/emails")
            .MapPost(CreatePRActivityEmail, "{id}/emails")
            .MapPut(UpdatePRActivityEmail, "{id}/emails")
            .MapGet(GetPRActivityLocation, "{id}/locations")
            .MapPost(CreatePRActivityLocation, "{id}/locations")
            .MapPut(UpdatePRActivityLocation, "{id}/locations")
            .MapGet(GetPRActivityTasks, "{id}/tasks")
            .MapPost(CreatePRActivityTask, "{id}/tasks")
            .MapGet(GetWorkProgress, "{id}/work-progress")
            .MapPost(UploadFiles, "{id}/pr-activity-local-files")
            .MapPost(CreatePRActivityReceivers, "{id}/pr-activity-receivers");

    }

    public async Task<PRActivityDto> CreatePRActivity(ISender sender, CreatePRActivityCommand command)
    {
        return await sender.Send(command);
    }

    public async Task<PRActivityBriefingDto> GetPRActivityBriefing(ISender sender, string id)
    {
        return await sender.Send(new GetPRActivityBriefingQuery(id));
    }

    public async Task<PRActivityBriefingDto> CreatePRActivityBriefing(ISender sender, CreatePRActivityBriefingCommand command)
    {
        return await sender.Send(command);
    }

    public async Task<PRActivityBriefingDto> UpdatePRActivityBriefing(
        ISender sender,
        string id,
        UpdatePRActivityBriefingCommand command)
    {
        command.PRActivityId = id;
        return await sender.Send(command);
    }

    public async Task<PRActivityEmailDto> GetPRActivityEmail(ISender sender, string id)
    {
        return await sender.Send(new GetPRActivityEmailQuery(id));
    }

    public async Task<PRActivityEmailDto> CreatePRActivityEmail(
        ISender sender,
        string id,
        CreatePRActivityEmailCommand command)
    {
        command.PRActivityId = id;
        return await sender.Send(command);
    }

    public async Task<PRActivityEmailDto> UpdatePRActivityEmail(
        ISender sender,
        string id,
        UpdatePRActivityEmailCommand command)
    {
        command.PRActivityId = id;
        return await sender.Send(command);
    }

    public async Task<PRActivityLocationDto> GetPRActivityLocation(
        ISender sender,
        string id
    )
    {
        return await sender.Send(new GetPRActivityLocationQuery(id));
    }

    public async Task<PRActivityLocationDto> CreatePRActivityLocation(
        ISender sender,
        string id,
        CreatePRActivityLocationCommand command
    )
    {
        command.PRActivityId = id;
        return await sender.Send(command);
    }

    public async Task<PRActivityLocationDto> UpdatePRActivityLocation(
        ISender sender,
        string id,
        UpdatePRActivityLocationCommand command
    )
    {
        command.PRActivityId = id;
        return await sender.Send(command);
    }

    internal async Task<PaginatedList<PRActivityItem>> GetPRActivityWithPagination(ISender sender, [AsParameters] GetPRActivityWithPaginationQuery query)
    {
        return await sender.Send(query);
    }

    internal async Task<PRActivityItem> GetPRActivity(ISender sender, string id)
    {
        return await sender.Send(new GetPRActivityQuery(id));
    }

    internal async Task<PRActivityDto> UpdatePRActivity(
        ISender sender,
        string id,
        UpdatePRActivityCommand command
    )
    {
        return await sender.Send(command);
    }

    internal async Task<PRActivityDto> DeletePRActivity
    (ISender sender, string id, [FromBody] RemovePRActivityCommand request)
    {
        request.Id = id;
        return await sender.Send(request);
    }

    internal async Task<IEnumerable<PrActivityTaskDto>> GetPRActivityTasks
    (ISender sender, string id, [AsParameters] GetPrActivityTasksQuery request)
    {
        return await sender.Send(request);
    }

    internal async Task<CreatePRActivityTaskDto> CreatePRActivityTask
    (ISender sender, string id, CreatePRActivityTaskCommand request)
    {
        return await sender.Send(request);
    }

    internal async Task<GetWorkProgressByPrActivityIdResponse> GetWorkProgress(ISender sender, string id)
    {
        return await sender.Send(new GetWorkProgressByPrActivityIdQuery(id));
    }

    internal async Task<UploadFileResponse> UploadFiles(ISender sender, UploadFilesCommand command, string id)
    {
        return await sender.Send(command);
    }

    internal async Task<IEnumerable<PRActivityReceiverDto>> CreatePRActivityReceivers(
        ISender sender, 
        [FromForm] CreatePRActivityReceiversCommand command, 
        string id)
    {
        command.PRActivityId = id;
        return await sender.Send(command);
    }
}
