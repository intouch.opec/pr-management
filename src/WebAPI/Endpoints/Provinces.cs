using PRManagement.Application.Provinces.Queries.GetProvinceOptions;

namespace PRManagement.Web.Endpoints;
public class Provinces : EndpointGroupBase
{
    public override void Map(WebApplication app)
    {
        app.MapGroup(this)
            .RequireAuthorization()
            .MapGet(GetProvinceOptions);
    }

    internal async Task<IEnumerable<GetProvinceOptionsResponse>> GetProvinceOptions(
      ISender sender, [AsParameters] GetProvinceOptionsQuery request)
      => await sender.Send(request);

}
