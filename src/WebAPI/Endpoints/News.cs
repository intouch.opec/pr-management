using Microsoft.AspNetCore.Mvc;
using PRManagement.Application.Common.Models;
using PRManagement.Application.News.Commands.MappingNew;
using PRManagement.Application.News.Queries.GetNewWithPagination;

namespace PRManagement.Web.Endpoints;

public class News : EndpointGroupBase
{
    public override void Map(WebApplication app)
    {
        app.MapGroup(this)
            .RequireAuthorization()
            .MapGet(GetNewWithPagination)
            .MapPatch(MappingNew, "{id}/mapping");
    }

    internal async Task<PaginatedList<GetNewItem>> GetNewWithPagination
    (ISender sender, [AsParameters] GetNewWithPaginationQuery request)
      => await sender.Send(request);

    internal async Task<GetNewItem> MappingNew
    (ISender sender, string id, [FromBody] MappingNewCommand request)
    {
        request.Id = id;
        return await sender.Send(request);
    }

}
