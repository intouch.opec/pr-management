using PRManagement.Application.Districts.Queries.GetDistrictOptions;

namespace PRManagement.Web.Endpoints;

public class Districts : EndpointGroupBase
{
    public override void Map(WebApplication app)
    {
        app.MapGroup(this)
            .RequireAuthorization()
            .MapGet(GetDistrictsOptions);
    }

    internal async Task<IEnumerable<GetDistrictOptionsResponse>> GetDistrictsOptions(
        ISender sender, [AsParameters] GetDistrictOptionsQuery request)
        => await sender.Send(request);

}
