﻿using PRManagement.Application.Common.Models;
using PRManagement.Application.Users.Queries.GetUserWithPagination;
using PRManagement.Domain.Entities;
using PRManagement.Infrastructure.Identity;
namespace PRManagement.Web.Endpoints;

public class Users : EndpointGroupBase
{
    public override void Map(WebApplication app)
    {
        app.MapGroup(this)
            .MapIdentityApi<ApplicationUser>();

        app.MapGroup(this)
            .RequireAuthorization()
            .MapGet(GetUserOptions, "options");
    }

    internal Task<IEnumerable<OptionItemBase>> GetUserOptions(ISender sender, [AsParameters] GetUsersQuery query)
    {
        return sender.Send(query);
    }
}
