using Microsoft.AspNetCore.Mvc;
using PRManagement.Application.Bookmarks.Commands.RemoveBookmark;
using PRManagement.Application.Bookmarks.Commands.UpdateBookmarkActiveEmail;
using PRManagement.Application.Bookmarks.Queries.GetBookmarkWithPagination;
using PRManagement.Application.Common.Models;

namespace PRManagement.Web.Endpoints;
public class Favourites : EndpointGroupBase
{
    public override void Map(WebApplication app)
    {
        app.MapGroup(this)
            .RequireAuthorization()
            .MapGet(GetBookmarkWithPagination)
            .MapPatch(UpdateBookmarkActiveEmail, "{id}")
            .MapDelete(DeleteBookmark, "{id}");
    }

    internal async Task<PaginatedList<GetBookmarkItem>> GetBookmarkWithPagination
        (ISender sender, [AsParameters] GetBookmarkWithPaginationQuery request)
    {
        return await sender.Send(request);
    }

    internal async Task<RemoveBookmarkResponse> DeleteBookmark(
        ISender sender, Guid id, [FromBody] RemoveBookmarkCommand request)
    {
        request.Id = id;
        return await sender.Send(request);
    }

    internal async Task<RemoveBookmarkResponse> UpdateBookmarkActiveEmail(
        ISender sender, string id, [FromBody] UpdateBookmarkActiveEmailCommand request)
    {
        request.Id = id;
        return await sender.Send(request);
    }

}