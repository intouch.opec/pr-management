
using Microsoft.AspNetCore.Mvc;
using PRManagement.Application.Common.Models;
using PRManagement.Application.PRActivityReceivers.Commands.CreatePRActivityReceivers;
using PRManagement.Application.PRActivityReceivers.Commands.RemovePRActivityReceivers;
using PRManagement.Application.PRActivityReceivers.Queries.GetPRActivityReceiverWithPagination;

namespace PRManagement.Web.Endpoints;
public class PRActivityReceivers : EndpointGroupBase
{
    public override void Map(WebApplication app)
    {
        app.MapGroup(this)
            .RequireAuthorization()
            .MapGet(GetPRActivityReceiverWithPagination)
            .MapDelete(RemovePRActivityReceivers);
    }

    internal async Task<PaginatedList<PRActivityReceiverItem>> GetPRActivityReceiverWithPagination
     (ISender sender, [AsParameters] GetPRActivityReceiverWithPaginationQuery query)
      => await sender.Send(query);

    internal async Task<IEnumerable<PRActivityReceiverDto>> RemovePRActivityReceivers(ISender sender, [FromBody] RemovePRActivityReceiversCommand command)
        => await sender.Send(command);
}
