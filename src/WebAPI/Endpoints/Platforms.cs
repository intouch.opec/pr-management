using Microsoft.AspNetCore.Mvc;
using PRManagement.Application.Common.Models;
using PRManagement.Application.Platforms.Commands.CalculateTierPlatform;
using PRManagement.Application.Platforms.Commands.CreatePlatform;
using PRManagement.Application.Platforms.Commands.DeletePlatform;
using PRManagement.Application.Platforms.Commands.UpdatePlatform;
using PRManagement.Application.Platforms.Queries.GetPlatformById;
using PRManagement.Application.Platforms.Queries.GetPlatforms;
using PRManagement.Application.Platforms.Queries.GetPlatformWithPagination;

namespace PRManagement.Web.Endpoints;
public class Platforms : EndpointGroupBase
{
    public override void Map(WebApplication app)
    {
        app.MapGroup(this)
            .RequireAuthorization()
            .MapGet(GetPlatformWithPagination)
            .MapGet(GetPlatformById, "{id}")
            .MapGet(GetPlatformOptionTypes, "type/options")
            .MapPost(CreatePlatform)
            .MapPut(UpdatePlatform, "{id}")
            .MapPost(CalculatePlatformTier, "tier/calculate")
            .MapDelete(DeletePlatform, "{id}");
    }

    internal async Task<PaginatedList<GetPlatformItem>> GetPlatformWithPagination(ISender sender, [AsParameters] GetPlatformWithPaginationQuery request)
        => await sender.Send(request);

    internal async Task<GetPlatformByIdResponse> GetPlatformById(ISender sender, Guid id)
        => await sender.Send(new GetPlatformByIdQuery(id));

    internal async Task<CreatePlatformResponse> CreatePlatform(ISender sender, CreatePlatformCommand request)
        => await sender.Send(request);

    internal async Task<UpdatePlatformResponse> UpdatePlatform(ISender sender, Guid id, UpdatePlatformCommand request)
    {
        Guard.Against.NotFound(request.Id, id);
        return await sender.Send(request);
    }

    internal async Task<CalculateTierPlatformResponse> CalculatePlatformTier(ISender sender, CalculateTierPlatformCommand request)
        => await sender.Send(request);

    internal async Task<DeletePlatformResponse> DeletePlatform
    (ISender sender, Guid id, [FromBody] DeletePlatformCommand request)
    {
        request.Id = id;
        return await sender.Send(request);
    }
    
    internal async Task<IEnumerable<GetPlatformOptionItem>> GetPlatformOptionTypes
        (ISender sender, [AsParameters] GetPlatformsQuery request)
    {
        return await sender.Send(request);
    }
}
