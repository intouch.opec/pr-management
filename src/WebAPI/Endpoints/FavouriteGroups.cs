using PRManagement.Application.BookmarkGroups.Commands.CreateBookmarkGroup;
using PRManagement.Application.BookmarkGroups.Commands.RemoveBookmarkGroup;
using PRManagement.WebAPI.Common.Extensions;
using PRManagement.Application.BookmarkGroups.Queries.GetBookmarkGroup;
using Microsoft.AspNetCore.Mvc;
using PRManagement.Application.Bookmarks.Commands.CreateBookmarkContacts;
using PRManagement.Application.Common.Models;
using PRManagement.Application.BookmarkGroups.Commands.ExportBookmarkGroup;
using PRManagement.Application.BookmarkGroups.Queries.GetBookmarkGroupTags;
using PRManagement.Application.BookmarkGroups.Commands.UpdateBookmarkGroup;

namespace PRManagement.Web.Endpoints;

public class FavouriteGroups : EndpointGroupBase
{
    public override void Map(WebApplication app)
    {
        app.MapGroup(this)
            .RequireAuthorization()
            .MapGet(GetBookmarkGroupWithPagination)
            .MapPost<CreateBookmarkGroupResponse>(CreateBookmarkGroup)
            .MapPost<CreateBookmarkGroupResponse>(CreateBookmarkGroupContact, "contacts")
            .MapPost(ExportFavouring, "{id}/exports")
            .MapDelete(DeleteBookmarkGroup, "{id}")
            .MapGet(GetBookmarkGroupTags, "tags")
            .MapPut(UpdateBookmarkGroup, "{id}");

    }

    internal async Task<PaginatedList<GetBookmarkGroupsResponse>> GetBookmarkGroupWithPagination(ISender sender, [AsParameters] GetBookmarkGroupQuery request)
    {
        return await sender.Send(request);
    }

    internal async Task<IResult> CreateBookmarkGroup(ISender sender, CreateBookmarkGroupCommand request)
    {
        var bookmarkGroup = await sender.Send(request);
        return TypedResults.Created(
            $"/{GetType().Name.ToCustomCase()}/{bookmarkGroup.Id}",
            bookmarkGroup
        );
    }

    internal async Task<RemoveBookmarkGroupResponse> DeleteBookmarkGroup(
      ISender sender, Guid id, [FromBody] RemoveBookmarkGroupCommand request)
    {
        request.Id = id;
        return await sender.Send(request);
    }

    internal async Task<IResult> CreateBookmarkGroupContact(ISender sender, CreateBookmarkContactsCommand command)
    {
        var bookmarkGroup = await sender.Send(command);
        return TypedResults.Created(
            $"/{GetType().Name.ToCustomCase()}/{bookmarkGroup.Id}",
            bookmarkGroup
        );
    }

    internal async Task<IResult> ExportFavouring(ISender sender, Guid id)
    {
        var file = await sender.Send(new ExportBookmarkGroupCommand(id));
        return Results.File(file.File, file.ContentType, file.FileName);
    }

    internal async Task<IEnumerable<OptionItemBase>> GetBookmarkGroupTags(ISender sender, [AsParameters] GetBookmarkGroupTagsQuery command)
    {
        return await sender.Send(command);
    }

    internal async Task<CreateBookmarkContactsResponse> UpdateBookmarkGroup(ISender sender, UpdateBookmarkGroupCommand command, string id)
    {
        return await sender.Send(command);
    }
}
