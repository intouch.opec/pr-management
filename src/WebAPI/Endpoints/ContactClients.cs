
using PRManagement.Application.Common.Models;
using PRManagement.Application.ContactClients.Commands.RemoveContactClient;
using PRManagement.Application.ContactClients.Queries.GetContactWithPagination;

namespace PRManagement.Web.Endpoints;

public class ContactClients : EndpointGroupBase
{
    public override void Map(WebApplication app)
    {
        app.MapGroup(this)
            .RequireAuthorization()
            .MapGet(GetContactClientWithPagination)
            .MapDelete(DeleteClientContact, "{id}");

    }

    internal async Task<PaginatedList<GetContactItem>> GetContactClientWithPagination(ISender sender, 
        [AsParameters] GetContactWithPaginationQuery query)
    {
        return await sender.Send(query);
    }

    internal async Task<RemoveContactClientResponse> DeleteClientContact(ISender sender, Guid id)
    {
        return await sender.Send(new RemoveContactClientCommand(id));
    }
}