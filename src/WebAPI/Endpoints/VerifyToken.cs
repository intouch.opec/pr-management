using PRManagement.Application.Users.Queries.GetUserClaim;

namespace PRManagement.Web.Endpoints;

public class VerifyToken : EndpointGroupBase
{
    public override void Map(WebApplication app)
    {
        app.MapGroup(this)
            .RequireAuthorization()
            .MapGet(GetClaims);
    }

    private async Task<IEnumerable<GetUserClaimResponse>> GetClaims(ISender sender)
        => await sender.Send(new GetUserClaimQuery());

}
