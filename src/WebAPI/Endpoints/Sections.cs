using PRManagement.Application.Common.Models;
using PRManagement.Application.Sections.Commands.CreateSection;
using PRManagement.Application.Sections.Commands.UpdateSection;
using PRManagement.Application.Sections.Queries.GetSectionOptions;
using PRManagement.Application.Sections.Queries.GetSectionWithPagination;

namespace PRManagement.Web.Endpoints;
public class Sections : EndpointGroupBase
{
    public override void Map(WebApplication app)
    {
        app.MapGroup(this)
            .RequireAuthorization()
            .MapGet(GetSectionOptions)
            .MapPost(CreateSection)
            .MapPut(UpdateSection, "{id}")
            .MapGet("/with-pagination", GetSection);
    }

    internal async Task<IEnumerable<GetSectionOptionsResponse>> GetSectionOptions(
        ISender sender, [AsParameters] GetSectionOptionsQuery request)
        => await sender.Send(request);

    internal async Task<SectionDto> CreateSection(ISender sender, CreateSectionCommand command)
    {
        return await sender.Send(command);
    }

    internal async Task<SectionDto> UpdateSection(ISender sender, string id, UpdateSectionCommand command)
    {
        command.Id = id;
        return await sender.Send(command);
    }

    internal async Task<PaginatedList<GetSectionItem>> GetSection(ISender sender, [AsParameters] GetSectionWithPaginationQuery query)
    {
        return await sender.Send(query);
    }
}