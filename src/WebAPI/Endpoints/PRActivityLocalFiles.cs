using PRManagement.Application.Common.Models;
using PRManagement.Application.PRActivityLocalFiles.Commands.RemovePRActivityLocalFiles;
using PRManagement.Application.PRActivityLocalFiles.Commands.UploadFiles;
using PRManagement.Application.PRActivityLocalFiles.Queries.GetPRActivityLocalFiles;
using PRManagement.Application.PRActivityLocalFiles.Queries.PRActivityLocalFileWithPagination;

namespace PRManagement.Web.Endpoints;
public class PRActivityLocalFiles : EndpointGroupBase
{
    public override void Map(WebApplication app)
    {
        app.MapGroup(this)
            .RequireAuthorization()
            .MapGet(GetPRActivityLocalFileWithPagination)
            .MapGet(GetPRActivityLocalFile, "{id}")
            .MapDelete(RemovePRActivityLocalFile, "{id}");
    }

    internal async Task<PaginatedList<PRActivityLocalFileItem>> GetPRActivityLocalFileWithPagination
     (ISender sender, [AsParameters] PRActivityLocalFileWithPaginationQuery request)
      => await sender.Send(request);

    internal async Task<UploadFileResponse> RemovePRActivityLocalFile
     (ISender sender, string id)
      => await sender.Send(new RemovePRActivityLocalFilesCommand(id));

    internal async Task<IResult> GetPRActivityLocalFile(ISender sender, string id)
    {
        var file = await sender.Send(new GetPRActivityLocalFilesQuery(id));
        return Results.File(file.File, file.ContentType, file.FileName);
    }
}
