using Microsoft.AspNetCore.Mvc;
using PRManagement.Application.Common.Models;
using PRManagement.Application.Contacts.Commands.CreateContactByMediaSectionId;
using PRManagement.Application.Contacts.Queries.GetContactWithPaginationByMediaSectionId;
using PRManagement.Application.MediaSections.Commands.CreateMediaSection;
using PRManagement.Application.MediaSections.Commands.DeleteMediaSections;
using PRManagement.Application.MediaSections.Commands.UpdateMediaSection;
using PRManagement.Application.MediaSections.Queries.GetMediaById;
using PRManagement.Application.MediaSections.Queries.GetMediaSectionPlatformByUniqueField;
using PRManagement.Application.MediaSections.Queries.GetMediaSectionWithPagination;
using PRManagement.Application.Platforms.Commands.CreatePlatformByMediaSectionId;
using PRManagement.Application.RemoveContactMediaSection.Commands.RemoveContactMediaSection;
using PRManagement.WebAPI.Common.Extensions;

namespace PRManagement.Web.Endpoints;

public class MediaSections : EndpointGroupBase
{
    public override void Map(WebApplication app)
    {
        app.MapGroup(this)
          .RequireAuthorization()
          .MapGet(GetMediaSectionWithPagination)
          .MapGet(GetMediaSectionById, "{id}")
          .MapPost<CreateMediaSectionResponse>(CreateMediaSection)
          .MapPut(UpdateMediaSection, "{id}")
          .MapPost<CreateContactByMediaSectionIdResponse>(CreateContactBySectionId, "{id}/contacts")
          .MapGet(GetMediaSectionPlatformByUniqueField, "/plateforms")
          .MapPost<CreatePlatformByMediaSectionIdResponse>(CreatePlatformByMediaSectionId, "{id}/platforms")
          .MapDelete(DeleteMediaSection, "{id}")
          .MapGet(GetContactWithPaginationByMediaSectionId, "{id}/contacts")
          .MapDelete(RemoveContactMediaSectionByMediaSectionId, "{id}/contacts/{contactId}");
    }

    internal async Task<PaginatedList<GetMediaSectionItem>> GetMediaSectionWithPagination(ISender sender, [AsParameters] GetMediaSectionWithPaginationQuery request)
        => await sender.Send(request);

    internal async Task<GetMediaSectionByIdResponse> GetMediaSectionById(ISender sender, Guid id)
        => await sender.Send(new GetMediaSectionByIdQuery(id));

    internal async Task<IResult> CreateMediaSection(ISender sender, CreateMediaSectionCommand request)
    {
        var mediaSection = await sender.Send(request);
        return Results.Created(
          $"/{GetType().Name.ToCustomCase()}/{mediaSection.Id}",
          mediaSection
        );
    }

    internal async Task<UpdateMediaSectionResponse> UpdateMediaSection(ISender sender, Guid id, UpdateMediaSectionCommand request)
    {
        Guard.Against.NotFound(request.Id, id);
        return await sender.Send(request);
    }

    internal async Task<GetMediaSectionByUniqueFieldResponse> GetMediaSectionPlatformByUniqueField(ISender sender, [AsParameters] GetMediaSectionByUniqueFieldQuery request)
      => await sender.Send(request);

    internal async Task<IResult> CreatePlatformByMediaSectionId(ISender sender, Guid id, CreatePlatformByMediaSectionIdCommand request)
    {
        Guard.Against.NotFound(request.Id, id);
        var result = await sender.Send(request);
        return Results.Created(
          $"/{GetType().Name.ToCustomCase()}/{result.MediaSectionId}",
          result
        );
    }

    internal async Task<DeleteMediaSectionResponse> DeleteMediaSection(
      ISender sender, Guid id, [FromBody] DeleteMediaSectionCommand request)
    {
        request.Id = id;
        return await sender.Send(request);
    }

    internal async Task<IResult> CreateContactBySectionId(ISender sender, string id, CreateContactByMediaSectionIdCommand request)
    {
        request.MediaSectionId = id;
        var result = await sender.Send(request);
        return Results.Created(
          $"/{GetType().Name.ToCustomCase()}/{result.MediaSectionId}",
          result
        );
    }

    internal async Task<PaginatedList<GetContactWithPaginationByMediaSectionIdResponse>> GetContactWithPaginationByMediaSectionId
    (ISender sender, Guid id, [AsParameters] GetContactWithPaginationByMediaSectionIdQuery request)
    {
        request.MediaSectionId = id;
        return await sender.Send(request);
    }

    internal async Task<RemoveContactMediaSectionResponse> RemoveContactMediaSectionByMediaSectionId
    (ISender sender, string id, string contactId, [FromBody] RemoveContactMediaSectionCommand command)
    {
        command.ContactId = contactId;
        command.MediaSectionId = id;
        return await sender.Send(command);
    }
}
