using Microsoft.AspNetCore.Mvc;
using PRManagement.Application.Common.Models;
using PRManagement.Application.MonitorTaskWorkers.Queries.GetMonitorTaskWorkerCoverages;
using PRManagement.Application.MonitorTaskWorkers.Queries.GetMonitorTaskWorkerCoverageWithPagination;
using PRManagement.Application.TaskWorkers.Commands.CreateTaskWorker;
using PRManagement.Application.TaskWorkers.Commands.RemoveTaskWorker;
using PRManagement.Application.TaskWorkers.Commands.UpdateTaskWorker;
using PRManagement.Application.TaskWorkers.Queries.GetTaskWorkerWithPagination;

namespace PRManagement.Web.Endpoints;
public class MonitorTaskWorkers : EndpointGroupBase
{
    public override void Map(WebApplication app)
    {
        app.MapGroup(this)
            .RequireAuthorization()
            .MapGet(GetTaskWorkerWithPagination)
            .MapPost(CreateTaskWorker)
            .MapPut(UpdateTaskWorker, "{id}")
            .MapDelete(RemoveTaskWorker, "{id}")
            .MapGet(GetMonitorTaskWorkerCoverages, "{id}/summaries")
            .MapGet(GetMonitorTaskWorkerCoverageWithPagination, "{id}/coverages");

    }

    internal async Task<PaginatedList<GetTaskWorkerItem>> GetTaskWorkerWithPagination
    (ISender sender, [AsParameters] GetTaskWorkerWithPaginationQuery request)
      => await sender.Send(request);

    internal async Task<CreateTaskWorkerResponse> CreateTaskWorker
    (ISender sender, CreateTaskWorkerCommand request)
      => await sender.Send(request);

    internal async Task<UpdateTaskWorkerResponse> UpdateTaskWorker
    (ISender sender, string id, UpdateTaskWorkerCommand request)
    {
        request.Id = id;
        return await sender.Send(request);
    }

    internal async Task<RemoveTaskWorkerResponse> RemoveTaskWorker
    (ISender sender, string id, [FromBody] RemoveTaskWorkerCommand request)
    {
        request.Id = id;
        return await sender.Send(request);
    }

    internal async Task<GetMonitorTaskWorkerCoveragesResponse> GetMonitorTaskWorkerCoverages(ISender sender, string id)
    {
        return await sender.Send(new GetMonitorTaskWorkerCoveragesQuery(id));
    }

    internal async Task<PaginatedList<GetMonitorTaskWorkerCoverageItem>> GetMonitorTaskWorkerCoverageWithPagination(
        ISender sender, 
        [AsParameters] GetMonitorTaskWorkerCoverageWithPaginationQuery request)
    {
        return await sender.Send(request);
    }
}
