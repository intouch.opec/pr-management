using Microsoft.AspNetCore.Mvc;
using PRManagement.Application.GetPRActivityTaskComments.Queries.GetPRActivityTaskCommands;
using PRManagement.Application.PRActivityTaskComments.Commands.CreatePRActivityTask;
using PRManagement.Application.PRActivityTaskComments.Commands.RemovePRActivityTaskComments;
using PRManagement.Application.PRActivityTaskComments.Commands.UpdatePRActivityTaskCommands;
using PRManagement.Application.PRActivityTasks.Commands.AddPrActivityTaskOwner;
using PRManagement.Application.PRActivityTasks.Commands.CreatePRActivityTask;
using PRManagement.Application.PRActivityTasks.Commands.RemovePRActivityTask;
using PRManagement.Application.PRActivityTasks.Commands.RemovePRActivityTaskOwner;
using PRManagement.Application.PRActivityTasks.Commands.UpdatePRActivityTask;
using PRManagement.Application.PRActivityTasks.Commands.UpdatePRActivityTaskCommentStatus;

namespace PRManagement.Web.Endpoints;
public class PrActivityTasks : EndpointGroupBase
{
    public override void Map(WebApplication app)
    {
        app.MapGroup(this)
            .RequireAuthorization()
            .MapGet(GetPRActivityTaskComments, "{id}/comments")
            .MapPut(UpdatePRActivityTask, "{id}")
            .MapPost(AddOwnerPRActivityTask)
            .MapPatch(UpdatePRActivityTaskCommentStatus, "{id}")
            .MapPost(CreatePRActivityTaskComment, "{id}/comments")
            .MapPut(UpdatePRActivityTaskComment, "{id}/comments/{prActivityTaskCommentId}")
            .MapDelete(RemovePRActivityTaskComment, "{id}/comments/{commentId}")
            .MapDelete(RemovePRActivityTask, "{id}")
            .MapDelete(RemovePRActivityTaskOwner, "{id}/owners/{ownerId}");
    }

    public async Task<CreatePRActivityTaskDto> UpdatePRActivityTask(ISender sender, string id, UpdatePRActivityTaskCommand command)
    {
        command.Id = id;
        return await sender.Send(command);
    }

    public async Task<OwnerDto> AddOwnerPRActivityTask(ISender sender, AddPrActivityTaskOwnerCommand command)
    {
        return await sender.Send(command);
    }

    public async Task<CreatePRActivityTaskDto> RemovePRActivityTask(ISender sender, string id)
    {
        return await sender.Send(new RemovePRActivityTaskCommand(id));
    }

    public async Task<OwnerDto> RemovePRActivityTaskOwner(ISender sender, string id, string ownerId)
    {
        return await sender.Send(new RemovePRActivityTaskOwnerCommand(ownerId, id));
    }

    public async Task<PRActivityTaskCommentDto> CreatePRActivityTaskComment(ISender sender, string id,CreatePRActivityTaskCommentCommand command)
    {
        
        return await sender.Send(command);
    }

    public async Task<PRActivityTaskCommentDto> UpdatePRActivityTaskComment(
        ISender sender,
        string id,
        string prActivityTaskCommentId,
        UpdatePRActivityTaskCommentCommand command)
    {
        command.Id = prActivityTaskCommentId;
        command.PrActivityTaskId = id;
        return await sender.Send(command);
    }

    public async Task<PRActivityTaskCommentDto> RemovePRActivityTaskComment(ISender sender, string id, string commentId)
    {
        return await sender.Send(new RemovePRActivityTaskCommentCommand(commentId, id));
    }

    public async Task<CreatePRActivityTaskDto> UpdatePRActivityTaskCommentStatus(ISender sender, string id, [FromBody] UpdatePRActivityTaskStatusCommand request)
    {
        request.Id = id;
        return await sender.Send(request);
    }

    public async Task<IEnumerable<PRActivityTaskCommentDto>> GetPRActivityTaskComments(ISender sender, string id)
    {
        return await sender.Send(new GetPRActivityTaskCommentsQuery(id));
    }
}
