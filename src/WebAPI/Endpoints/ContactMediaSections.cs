using PRManagement.Application.CreateContactMediaSections.Commands.CreateContactMediaSections;
using PRManagement.Application.RemoveContactMediaSectionById.Commands.RemoveContactMediaSectionById;
using PRManagement.WebAPI.Common.Extensions;

namespace PRManagement.Web.Endpoints;

public class ContactMediaSections : EndpointGroupBase
{
    public override void Map(WebApplication app)
    {
        app.MapGroup(this)
          .RequireAuthorization()
          .MapPost<CreateContactMediaSectionsResponse>(CreateContactMediaSection)
          .MapDelete(RemoveContactMediaSectionById, "{id}");

    }

    internal async Task<IResult> CreateContactMediaSection(ISender sender, CreateContactMediaSectionCommand request)
    {
        var contact = await sender.Send(request);
        return TypedResults.Created(
            $"/{GetType().Name.ToCustomCase()}/{contact.Id}",
            contact
        );
    }

    internal async Task<RemoveContactMediaSectionByIdResponse> RemoveContactMediaSectionById(ISender sender, Guid id)
    {
        return await sender.Send(new RemoveContactMediaSectionByIdCommand(id));
    }
}
