using PRManagement.Application.SubDistricts.Queries.GetSubDistrictOptions;

namespace PRManagement.Web.Endpoints;
public class SubDistricts : EndpointGroupBase
{
    public override void Map(WebApplication app)
    {
        app.MapGroup(this)
            .RequireAuthorization()
            .MapGet(GetSubDistrictOptions);
    }

    internal async Task<IEnumerable<GetSubDistrictOptionsResponse>> GetSubDistrictOptions(
      ISender sender, [AsParameters] GetSubDistrictOptionsQuery request)
      => await sender.Send(request);

}