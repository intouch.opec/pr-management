using Microsoft.AspNetCore.Mvc;
using PRManagement.Application.Common.Models;
using PRManagement.Application.RegisterForms.Commands.CreateRegisterForm;
using PRManagement.Application.RegisterForms.Commands.UpdateRegisterForm;
using PRManagement.Application.RegisterForms.Queries.GetRegisterForm;
using PRManagement.Application.RegisterForms.Queries.GetRegisterFormOption;
using PRManagement.Application.RegisterForms.Queries.GetRegisterFormWithPagination;

namespace PRManagement.Web.Endpoints;
public class RegisterForms : EndpointGroupBase
{
    public override void Map(WebApplication app)
    {
        app.MapGroup(this)
            .RequireAuthorization()
            .MapGet(GetRegisterFormWithPagination)
            .MapGet(GetRegisterFormById, "{id}")
            .MapGet(GetRegisterFormOptionTypes, "type/options")
            .MapPost(CreateRegisterForm)
            .MapPut(UpdateRegisterForm, "{id}");
    }

    internal async Task<PaginatedList<RegisterFormDto>> GetRegisterFormWithPagination(ISender sender, [AsParameters] GetRegisterFormWithPaginationQuery request)
        => await sender.Send(request);

    internal async Task<RegisterFormDto> GetRegisterFormById(ISender sender, string id)
        => await sender.Send(new GetRegisterFormQuery(id));

    internal async Task<RegisterFormDto> CreateRegisterForm(ISender sender, CreateRegisterFormCommand request)
        => await sender.Send(request);

    internal async Task<RegisterFormDto> UpdateRegisterForm(ISender sender, Guid id, UpdateRegisterFormCommand request)
    {
        Guard.Against.NotFound(request.Id, id);
        return await sender.Send(request);
    }
    
    internal async Task<IEnumerable<RegisterFormOption>> GetRegisterFormOptionTypes
        (ISender sender, [AsParameters] GetRegisterFormOptionQuery request)
    {
        return await sender.Send(request);
    }
}
