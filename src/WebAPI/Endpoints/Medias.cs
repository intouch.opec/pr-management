using Microsoft.AspNetCore.Mvc;
using PRManagement.Application.Common.Models;
using PRManagement.Application.Medias.Commands.CreateMedia;
using PRManagement.Application.Medias.Commands.DeleteMedia;
using PRManagement.Application.Medias.Commands.UpdateMedia;
using PRManagement.Application.Medias.Queries.GetMediaById;
using PRManagement.Application.Medias.Queries.GetMediaOptions;
using PRManagement.Application.Medias.Queries.GetMediaWithPagination;
using PRManagement.WebAPI.Common.Extensions;

namespace PRManagement.Web.Endpoints;
public class Medias : EndpointGroupBase
{
    public override void Map(WebApplication app)
    {
        app.MapGroup(this)
            .RequireAuthorization()
            .MapGet(GetMediaWithPagination)
            .MapGet(GetMediaById, "{id}")
            .MapPost<CreateMediaResponse>(CreateMedia)
            .MapPut(UpdateMedia, "{id}")
            .MapDelete(DeleteMedia, "{id}")
            .MapGet(GetMediaOptions, "options");
    }

    internal async Task<PaginatedList<GetMediaWithPaginationResponse>> GetMediaWithPagination(ISender sender, [AsParameters] GetMediaWithPaginationQuery request)
        => await sender.Send(request);

    internal async Task<GetMediaByIdResponse> GetMediaById(ISender sender, Guid id)
        => await sender.Send(new GetMediaByIdQuery(id));

    internal async Task<IResult> CreateMedia(ISender sender, CreateMediaCommand request)
    {
        var media = await sender.Send(request);
        return TypedResults.Created(
            $"/{GetType().Name.ToCustomCase()}/{media.Id}",
            media
        );
    }

    internal async Task<UpdateMediaResponse> UpdateMedia(ISender sender, Guid id, UpdateMediaCommand request)
    {
        Guard.Against.NotFound(request.Id, id);
        return await sender.Send(request);
    }

    internal async Task<DeleteMediaResponse> DeleteMedia(
        ISender sender, Guid id, [FromBody] DeleteMediaCommand request)
    {
        request.Id = id;
        return await sender.Send(request);
    }

    internal async Task<IEnumerable<GetMediaOptionsResponse>> GetMediaOptions(ISender sender, [AsParameters] GetMediaOptionsQuery request)
        => await sender.Send(request);
}
