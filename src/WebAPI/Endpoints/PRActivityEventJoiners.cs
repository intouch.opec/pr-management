using PRManagement.Application.Common.Models;
using PRManagement.Application.PRActivityEventJoiners.Commands.CreatePRActivityEventJoiners;
using PRManagement.Application.PRActivityEventJoiners.Queries.GetPRActivityEventJoinerWithPagination;

namespace PRManagement.Web.Endpoints;

public class PRActivityEventJoiners : EndpointGroupBase
{
    public override void Map(WebApplication app)
    {
        app.MapGroup(this)
            .RequireAuthorization()
            .MapGet(GetPRActivityEventJoiners)
            .MapPost(CreatePRActivityEventJoiner);
    }

    internal async Task<PaginatedList<GetContactItem>> GetPRActivityEventJoiners
    (ISender sender, [AsParameters] GetPRActivityEventJoinerWithPaginationQuery request)
    {
        return await sender.Send(request);
    }

    internal async Task<PRActivityEventJoinerResponse> CreatePRActivityEventJoiner
    (ISender sender, CreatePRActivityEventJoinersCommand request)
    {
        return await sender.Send(request);
    }
}
