using PRManagement.Application.Common.Models;
using PRManagement.Application.SubSections.Commands.CreateSubSection;
using PRManagement.Application.SubSections.Commands.UpdateSubSection;
using PRManagement.Application.SubSections.Queries.GetSubSectionOptions;
using PRManagement.Application.SubSections.Queries.GetSubSectionWithPagination;

namespace PRManagement.Web.Endpoints;
public class SubSections : EndpointGroupBase
{
    public override void Map(WebApplication app)
    {
        app.MapGroup(this)
            .RequireAuthorization()
            .MapGet(GetSubSectionOptions)
            .MapPost(CreateSubSection)
            .MapPut(UpdateSebSection, "{id}")
            .MapGet("/with-pagination", GetSection);
    }

    internal async Task<IEnumerable<GetSubSectionOptionsResponse>> GetSubSectionOptions(
      ISender sender, [AsParameters] GetSubSectionOptionsQuery request)
        => await sender.Send(request);

    internal async Task<SubSectionDto> CreateSubSection(
     ISender sender, CreateSubSectionCommand command)
       => await sender.Send(command);

    internal async Task<SubSectionDto> UpdateSebSection(
        ISender sender, string id, UpdateSubSectionCommand command)
    {
        command.Id = id;
        return await sender.Send(command);
    }
    internal async Task<PaginatedList<GetSubSectionItem>> GetSection(ISender sender, [AsParameters] GetSubSectionWithPaginationQuery query)
    {
        return await sender.Send(query);
    }
}