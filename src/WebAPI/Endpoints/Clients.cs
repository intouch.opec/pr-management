
using PRManagement.Application.Common.Models;
using PRManagement.Application.Clients.Commands.CreateClient;
using PRManagement.Application.Clients.Commands.UpdateClient;
using PRManagement.Application.Clients.Queries.GetClientById;
using PRManagement.Application.Clients.Queries.GetClientWithPagination;
using PRManagement.Application.Clients.Commands.RemoveClient;
using PRManagement.WebAPI.Common.Extensions;
using Microsoft.AspNetCore.Mvc;
using PRManagement.Application.Clients.Queries.GetClientOptions;
using PRManagement.Application.ContactClients.Commands.CreateContactByClientId;

namespace PRManagement.Web.Endpoints;

public class Clients : EndpointGroupBase
{
    public override void Map(WebApplication app)
    {
        app.MapGroup(this)
            .RequireAuthorization()
            .MapGet(GetClientWithPagination)
            .MapGet(GetClientById, "{id}")
            .MapPost<CreateClientResponse>(CreateClient)
            .MapPut(UpdateClient, "{id}")
            .MapDelete(DeleteClient, "{id}")
            .MapGet(GetClientOptions, "options")
            .MapPost(CreateContactByClientId, "{id}/contacts");
    }

    internal async Task<PaginatedList<GetClientItem>> GetClientWithPagination(ISender sender, [AsParameters] GetClientWithPaginationQuery request)
    {
        return await sender.Send(request);
    }

    internal async Task<GetClientByIdResponse> GetClientById(ISender sender, Guid id)
        => await sender.Send(new GetClientByIdQuery(id));

    internal async Task<IResult> CreateClient(ISender sender, CreateClientCommand request)
    {
        var Client = await sender.Send(request);
        return TypedResults.Created(
            $"/{GetType().Name.ToCustomCase()}/{Client.Id}",
            Client
        );
    }

    internal async Task<UpdateClientResponse> UpdateClient(ISender sender, Guid id, UpdateClientCommand request)
    {
        request.Id = id;
        return await sender.Send(request);
    }

    internal async Task<RemoveClientResponse> DeleteClient(ISender sender, Guid id, [FromBody] RemoveClientCommand request)
    {
        request.Id = id;
        return await sender.Send(request);
    }

    internal async Task<IEnumerable<OptionItemBase>> GetClientOptions(ISender sender, [AsParameters] GetClientOptionsQuery request)
      => await sender.Send(request);


    internal async Task<CreateContactByClientIdResponse> CreateContactByClientId(
        ISender sender, string id, CreateContactByClientIdCommand command)
    {
        command.ClientId = id;
        return await sender.Send(command);
    }
}
